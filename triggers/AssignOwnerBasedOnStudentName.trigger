/*****************************************************************************************************************************************************************************
Author: Anup Prakash (agupta@rainmaker-llc.com); 

Description: When Qualification.Student <> NULL, update Qualification.Owner such that Owner Email = Student Email. If User does not exist, display error: Student is not a Salesforce user.

Created Date : 

TestClass : Test_AssignOwnerBasedOnStudentName

Updates: 7/1/2014 - Alex Schach
            -Updated to run on user/contact email instead of name
*****************************************************************************************************************************************************************************/

trigger AssignOwnerBasedOnStudentName on Certification__c (before insert, before update) {
    
if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore)
{
    map<Id,String> contactIdToEmailMap = new map<Id,String>();
    map<String,Id> userEmailToIdMap = new map<String,Id>();
    
    for(Certification__c certObj : trigger.new)
    {
        contactIdToEmailMap .put(certObj.Student_Name_2__c,'');
    }
    System.debug('contactIdToEmailMap :: '+contactIdToEmailMap );
    
    if(contactIdToEmailMap != null)
    {
        for(Contact contactObj : [Select FirstName, LastName, email, Id,Name from Contact where Id IN: contactIdToEmailMap .keyset()])
        {
            contactIdToEmailMap .put(contactObj.Id,contactObj.email);
        }
            System.debug('contactIdToEmailMap :: '+contactIdToEmailMap );

        for(User userObj: [Select Id,Name,email from User where email in : contactIdToEmailMap.values()])
        {
            userEmailToIdMap .put(userObj.Email,userObj.Id);
        }
        System.debug('userEmailToIdMap :: '+userEmailToIdMap );
    }

    for(Certification__c certObj : trigger.new)
    {
        String newOwner;
        if(contactIdToEmailMap.get(certObj.Student_Name_2__c) != null || contactIdToEmailMap.get(certObj.Student_Name_2__c) != '')
        {
            newOwner = userEmailToIdMap.get(contactIdToEmailMap.get(certObj.Student_Name_2__c));
            System.debug('newOwner :: '+newOwner);
        }

        if(String.isNotBlank(newOwner))
        {
            certObj.OwnerId = newOwner;
        }
        else
        {
            certObj.addError('Can\'t find any owner with the Student Name :: '+contactIdToEmailMap.get(certObj.Student_Name_2__c));
        }
    }
}
}