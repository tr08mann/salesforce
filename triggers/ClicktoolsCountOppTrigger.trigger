trigger ClicktoolsCountOppTrigger on Clicktools_Survey__c (after insert, after delete) {
    /****************************************************************************************************
    *Description:   Counts the number of Clicktools Surveys on an opp and updates the field
    *               Number of Survey Responses Received
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     5/21/2014    Alex Schach         Initial Version
    *****************************************************************************************************/  
integer surveycount = 0;
id oppid = null;
id doppid = null;
string oppoid = '';
string doppoid = '';
opportunity opp = new opportunity();
    
If(trigger.isinsert)
{
    for(clicktools_survey__c c :trigger.new)
    {
        oppid = c.Opportunity__c;
        system.debug('oppid: '+oppid);
        
    }
    if(oppid!= null)
    {
        surveycount = [select count() from clicktools_survey__c where opportunity__r.id =:oppid];
        system.debug('surveycount: ' +surveycount);
        oppoid = string.valueof(oppid);
        if(oppoid.startsWith('006'))
        {
            opp.id = oppoid;
            opp.Survey_Response_Received__c = surveycount;
            update opp; 
        }
        
    }
}
if(trigger.isdelete)
{
    for(clicktools_survey__c c :trigger.old)
    {
        doppid = c.Opportunity__c;
        system.debug('doppid: '+doppid);
    }
    if(doppid!=null)
    {
        surveycount = [select count() from clicktools_survey__c where opportunity__r.id =:doppid];
        system.debug('deleted surveycount: ' +surveycount);
        doppoid = string.valueof(doppid);
        if(doppoid.startsWith('006'))
        {
            opp.id = doppid;
            opp.Survey_Response_Received__c = surveycount;
            update opp; 
        }
    }
}
}