trigger QualificationAttachmentTrigger on Attachment (after insert, after delete) {

    Set<Id> qualificationId = new Set<Id>();
    //list<Certification__c> qualification = new list
    list<Certification__c> qualification = new list<Certification__c>();
    if(trigger.isInsert){
        for(Attachment att:trigger.new){
            String parentObjId = att.ParentId;
            if(String.valueOf(parentObjId).StartsWith(Certification__c.sObjectType.getDescribe().getKeyPrefix())){
                qualificationId.add(parentObjId);
             }
            
        }
    
        list<Certification__c> qual = [SELECT Id,Proof_of_Certification_Attached__c,(SELECT id FROM Attachments) FROM Certification__c WHERE id IN: qualificationId];
        for(Certification__c att:qual){
            if(att.Attachments.size()>0){
                if(att.Proof_of_Certification_Attached__c == false){
                    att.Proof_of_Certification_Attached__c = true;
                }
                qualification.add(att);
            }
            
        }
        if(!qualification.isEmpty()){
            update qualification;
        }
    }
    if(trigger.isDelete){
        for(Attachment att:trigger.old){
            String parentObjId = att.ParentId;
            if(String.valueOf(parentObjId).StartsWith(Certification__c.sObjectType.getDescribe().getKeyPrefix())){
                qualificationId.add(parentObjId);
             }
            
        }
    
        list<Certification__c> qual = [SELECT Id,Proof_of_Certification_Attached__c,(SELECT id FROM Attachments) FROM Certification__c WHERE id IN: qualificationId];
        for(Certification__c att:qual){
            if(att.Attachments.size() == 0){
                if(att.Proof_of_Certification_Attached__c == true){
                    att.Proof_of_Certification_Attached__c = false;
                }
                qualification.add(att);
            }
            
        }
        if(!qualification.isEmpty()){
            update qualification;
        }
    }
    
}