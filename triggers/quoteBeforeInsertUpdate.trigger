trigger quoteBeforeInsertUpdate on SFDC_520_Quote__c (after insert, after update) {
	
	//JGP 5/22/2012 Changed to Standard Quote Object /*SFDC_520_Quote__c*/
    List<id> oppids = new List<id>();
    String[] dealRegistrationCodes = new String[]{};
    
    for(SFDC_520_Quote__c q : trigger.new){
        if(q.Opportunity__c != null) oppids.add(q.Opportunity__c);
        if(q.Deal_Registration_Code__c != null){
        	dealRegistrationCodes.add(q.Deal_Registration_Code__c);
        }
    }
    System.debug('Deal Reg Check');
    System.debug(dealRegistrationCodes);
    
    Task[] tlist = new Task[]{};
    /*
    tlist = [Select Id, Deal_Registration_Code__c from Task where Deal_Registration_Code__c IN :dealRegistrationCodes];*/
    Map<String, Task> mapDRCtoTask = new Map<String, Task>();

    //String debug ='';
	for(Task t : tlist){
		//debug+= t.Deal_Registration_Code__c+' ';
		if(!mapDRCtoTask.containsKey(t.Deal_Registration_Code__c)){
			mapDRCtoTask.put(t.Deal_Registration_Code__c, t);}
	}
    //throw new MyException(debug);
    Map<id, Opportunity> oppmap = new Map<id, Opportunity>([Select Id, OwnerId, Deal_Reg_Desk_Rep__c from Opportunity where Id IN :oppids]);

    List<Task> tasks = new List<Task>();
    
    
    for(SFDC_520_Quote__c q : trigger.new){
    	//throw new MyException('Quote Reg Status: '+q.Deal_Registration_Status__c+' length: '+q.Deal_Registration_Status__c.length());
        SFDC_520_Quote__c oldquote = null;
        
        if (Trigger.IsUpdate)
        {
            oldquote = Trigger.OldMap.get(q.Id);
        }
        
        Opportunity opp = oppmap.get(q.Opportunity__c);
        //throw new MyException('\nOpp:'+opp.Id+'\nExpiration: '+q.Deal_Expiration__c+'\nContains Key:'+mapDRCtoTask.containsKey(q.Deal_Registration_Code__c)+'\nRegistration Status: '+q.Deal_Registration_Status__c+'\n Old Status Logic: '+(Trigger.IsUpdate && !(oldquote.Deal_Registration_Status__c == 'Deal Registration Approved' && oldquote.Deal_Registration_Code__c != null && oldquote.Deal_Registration_Code__c != '')));
        //        	 
        if (opp != null && q.Deal_Expiration__c != null
			 && (!mapDRCtoTask.containsKey(q.Deal_Registration_Code__c))
             && (q.Deal_Registration_Status__c == 'Deal Registration Approved' && q.Deal_Registration_Code__c != null && q.Deal_Registration_Code__c != '')
             && (Trigger.IsInsert || (Trigger.IsUpdate && !(oldquote.Deal_Registration_Status__c == 'Deal Registration Approved' && oldquote.Deal_Registration_Code__c != null && oldquote.Deal_Registration_Code__c != ''))))
        {
        	//throw new MyException('Entered');
            Task t1 = new Task();
            t1.WhatId = q.Opportunity__c;
            t1.Subject = 'Deal Registration Expiring-Follow up on Quote ' + q.Name;
            //t1.OwnerId = q.OwnerId;
            //JGP Change to Deal_Reg_Desk_Rep__c
            if (opp.Deal_Reg_Desk_Rep__c != null)
            	t1.OwnerId = opp.Deal_Reg_Desk_Rep__c;
            else
            	throw new MyException('There is no Deal Reg Desk Rep associated to this opp email sf.com support for assistance');
            
            Date dte = q.Deal_Expiration__c;
            if (dte != null)
                t1.ActivityDate = dte.addDays(-15);
            tasks.add(t1);
            t1.Deal_Registration_Code__c = q.Deal_Registration_Code__c;
			mapDRCtoTask.put(q.Deal_Registration_Code__c, t1);

        }
        if (opp != null
             && (q.Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval')
             && (Trigger.IsInsert || (Trigger.IsUpdate && oldquote.Deal_Registration_Status__c != 'Deal Registration Submitted, Pending Approval')))
        {
            Task t2 = new Task();
            t2.WhatId = q.opportunity__c;
            t2.Subject = 'Follow up on Deal Registration Status ' +q.Name;
            //t2.OwnerId = q.OwnerId;
            //JGP Change to Deal_Reg_Desk_Rep__c
            if (opp.Deal_Reg_Desk_Rep__c != null)
            	t2.OwnerId = opp.Deal_Reg_Desk_Rep__c;
            else
            	throw new MyException('There is no Deal Reg Desk Rep associated to this opp email sf.com support for assistance');
            
            Date dte = q.Deal_Expiration__c;
            if (dte != null)
                t2.ActivityDate = System.Today().addDays(2);
            tasks.add(t2);
        }        
    }

    if (tasks.size() > 0) insert tasks;
    
    public class MyException extends Exception{}
}