trigger RollupOpportunityInvoices on Invoice_2__c(after insert, after update, after delete, after undelete) {
    
    Set<ID> oppIds = new Set<ID>(); 
    
    if(Trigger.isInsert || Trigger.isUndelete) {
        for(Invoice_2__c inv :Trigger.new){
            if(inv.Opportunity__c != null){
                oppIds.add(inv.Opportunity__c);    
            }
        }
    } else if(Trigger.isUpdate) {
        for(Invoice_2__c newInv :Trigger.new){
            Invoice_2__c oldInv = Trigger.oldMap.get(newInv.Id);
            if(oldInv.Opportunity__c != newInv.Opportunity__c){
                if(oldInv.Opportunity__c != null)
                    oppIds.add(oldInv.Opportunity__c); 
                if(newInv.Opportunity__c != null)
                    oppIds.add(newInv.Opportunity__c);    
            }
        }    
    } else if(Trigger.isDelete){
        for(Invoice_2__c inv :Trigger.old){
            if(inv.Opportunity__c != null){
                oppIds.add(inv.Opportunity__c);    
            }
        }    
    }
    
    if(!oppIds.isEmpty()){
        List<Opportunity> oppList = [SELECT Id, Number_of_Invoice__c FROM Opportunity WHERE Id IN :oppIds];
        Map<Id,AggregateResult> invoiceCountMap = new Map<id,AggregateResult>([SELECT Opportunity__c Id, Count(Id) totalInvoice FROM Invoice_2__c WHERE Opportunity__c IN: oppIds GROUP BY Opportunity__c]);
        
        for(Opportunity opp :oppList){
            if(invoiceCountMap.containsKey(opp.Id)){
                opp.Number_of_Invoice__c = (integer)invoiceCountMap.get(opp.Id).get('totalInvoice');   
            } else {
                opp.Number_of_Invoice__c = 0;    
            }
        }
        try {
            TriggerStopper.stopOpp  = true; // don't run opportunity triggers
            update oppList;
        } catch(Exception e){
            Trigger.new[0].addError(e.getMessage());    
        }
    }
         

}