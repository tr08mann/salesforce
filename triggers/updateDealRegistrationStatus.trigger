trigger updateDealRegistrationStatus on SFDC_520_Quote__c (after insert, after update) {
/*
Author: LGMK
Date: 11/7/2011
Purpose: Keep the Opportunity Deal Registration Status in sync with that of the
primary quote.
*/
  List<id> oppids = new List<id>();
	for(SFDC_520_Quote__c q : trigger.new)
	{
   		if(q.Opportunity__c != null) oppids.add(q.Opportunity__c);
	}
	
    Map<Id, Opportunity> oMap = new Map<Id, Opportunity>([Select Id, Quote_Deal_Registration_Status__c from Opportunity where Id IN :oppids]);
  List<Opportunity> opportunities = new List<Opportunity>();
     
  for(SFDC_520_Quote__c q : trigger.new){
        SFDC_520_Quote__c oldquote = null;
        
        boolean matchPrimary = false;
        boolean matchStatus = false;
 
        if (Trigger.IsUpdate)
        {
            oldquote = Trigger.OldMap.get(q.Id);

            if (oldquote.Primary__c == q.Primary__c)
            	matchPrimary = true;

            if (oldquote.Deal_Registration_Status__c == q.Deal_Registration_Status__c)
            	matchStatus = true;
        }
        
        if(q.Primary__c
          && q.Opportunity__c != null
          && (!matchPrimary || !matchStatus)
		){

          if(oMap.containsKey(q.Opportunity__c)){
          	Opportunity opp = oMap.get(q.Opportunity__c);
            opp.Quote_Deal_Registration_Status__c = q.Deal_Registration_Status__c;
             opportunities.add(opp);
          }
          
          
        }
  }
  if (opportunities.size() > 0) update opportunities;
}