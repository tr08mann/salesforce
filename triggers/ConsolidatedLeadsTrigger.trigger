trigger ConsolidatedLeadsTrigger on Lead (before insert, before update, after insert, after update, after delete) {

    if (trigger.isBefore)
    {
        if (Trigger.isInsert || Trigger.isUpdate)
        {
            List<Lead> nonConvertedLeads = new List<Lead>();
            Map<Id,Lead> nonConvertedLeadsMap = new Map<Id,Lead>();
            //Create the setting of the Primary Lead for the Account
            for (Lead l:trigger.new)
            {
            system.debug('l: '+l);
                if (l.IsConverted == false && l.company != null)
                {
                    nonConvertedLeads.add(l);
                    nonConvertedLeadsMap.put(l.Id,l);
                }
            }
            system.debug('nonConvertedLeads beforetriggers: ' +nonConvertedLeads);
            system.debug('nonConvertedLeadsMap beforetriggers: ' +nonConvertedLeadsMap);
            if(!nonconvertedleads.isempty() && !nonconvertedleadsmap.isempty())
            {
            	ConsolidatedLeads.SetPrimaryLead(trigger.IsInsert, trigger.isUpdate, nonConvertedLeads, nonConvertedLeadsMap);
            }
        }
    
        if (trigger.isUpdate)
        {
            //Lead Converted, Create Task and Email
            Map<Id,Lead> tempmap = trigger.newMap;
            Map<Id,Lead> oldmap = trigger.oldMap;
            List<lead> leads = new List<lead>();
            for (Lead l:tempmap.values()) //Only run on the first time this lead is converted
            {
                Lead oldChanged = oldmap.get(l.Id);
                if (oldChanged != null && oldChanged.IsConverted == false && l.IsConverted == true)
                {
                    //only create task and email when lead is from the web
                    //if(l.Nature_of_Inquiry__c == 'Request for Sales contact or information' && l.Number_of_Employees_LEAD_TEAM__c =='More than 250 Employees')
                    if(l.Nature_of_Inquiry__c != null)
                    {
                        leads.add(l);
                    }
                }
            }  
            system.debug('converted leads to get task: ' +leads); 
            if(!leads.isempty())
            {
            ConsolidatedLeads.LeadConvertedTask(leads);
            }
        }
    }
    
    if (trigger.isAfter)
    {
        if (!trigger.isDelete)
        {
            List<Lead> nonConvertedLeads = new List<Lead>();
            Map<Id,Lead> nonConvertedLeadsMap = new Map<Id,Lead>();
            //If another Lead is marked as Primary and there's already a Primary Lead for that company, set the previous primary lead to false
            for (Lead l:trigger.new)
            {
                if (l.Company != null && l.Company != '' && l.IsConverted == false)
                {
                    nonConvertedLeads.add(l);
                    nonConvertedLeadsMap.put(l.Id,l);
                }
            }
            system.debug('nonConvertedLeads aftertriggers: ' +nonConvertedLeads);
            system.debug('nonConvertedLeadsMap aftertriggers: ' +nonConvertedLeadsMap);
            if(!nonconvertedleads.isempty() && !nonconvertedleadsmap.isempty())
            {
            ConsolidatedLeads.SetPrimaryLeadAfter(nonConvertedLeads, nonConvertedLeadsMap);
            }
        }
    }
}