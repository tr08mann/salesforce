trigger trackLegalDocStatus on Legal_Document__c (after insert, after update) {
    /*This trigger tracks the status of a Legal_Document__c by updating the associated
    Legal_Document_Status_Tracking__c object (in the case of an update) and creating a new
    Legal_Document_Status_Tracking__c object (on both an update and an insert)*/
    
    List<Legal_Document__c> oldLDList = new List<Legal_Document__C>();
    
    if(!trigger.isInsert){
        for(Legal_Document__c ld : trigger.old){
            oldLDList.add(ld);
        }
    }
    
    List<Legal_Document__c> newLDList = new List<Legal_Document__c>();
    for(Legal_Document__c ld : trigger.new){
        newLDList.add(ld);
    }
    
    for(Integer x = 0; x < newLDList.size(); x++){
        
        Legal_Document__c newLD = newLDList[x];
        Legal_Document__c oldLD = new Legal_Document__c();
        
        if(!trigger.isInsert)
            oldLD = oldLDList[x];
        
        if(trigger.isInsert || newLD.Status__c != oldLD.Status__c){
            
            if(trigger.isUpdate){
                List<Legal_Document_Status_Tracking__c> LDST_list = [select Id, Legal_Document__c, Status_End_Date__c, Status_Start_Date__c from Legal_Document_Status_Tracking__c where Status_End_Date__c = null order by Status_Start_Date__c asc nulls last];
                for(Integer i = LDST_list.size()-1; i > -1; i--){
                    if(LDST_list[i].Legal_Document__c == newLD.Id){
                        LDST_list[i].Status_End_Date__c = system.now();
                        update(LDST_list[i]);
                        break;
                    }
                }
            }
            
            Legal_Document_Status_Tracking__c newLDST = new Legal_Document_Status_Tracking__c();
            newLDST.Legal_Document__c = newLD.Id;
            newLDST.Status_Start_Date__c = system.now();
            newLDST.Status_Name__c = newLD.Status__c;
            newLDST.Status_Changed_By__c = newLD.LastModifiedById;
            insert(newLDST);
            
        }
        
        
    }
}