trigger OpportunityLineItemSolutionPracticeUnlockedTrigger on OpportunityLineItem (after insert, before delete, after delete, before insert) {
    /****************************************************************************************************
    *Description:   Moved from ConsolidatedOpportunityLineItemTrigger
    *               Trigger to aggregate Solution Set values from related Opportunity Products 
    *            
    *               Aggregates the changes to revenue and profit and the products changed when an
    *               opportunity has been unlocked and products are changed - creates a new
    *               Unlocked Opp GP record.
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     2/13/2013   Justin Padilla      WO#1428 Initial Version 
    *   1.1     4/10/2014   Justin Padilla      WO#1628 Practice Area additions
    *   2.0     5/1/2014    Alex Schach         Added unlocked opportunity tracking and 
    *                                           fixed issue where solution set and practice area fields
    *                                           were not filling properly or were clearing improperly
    *   2.1     6/12/2014   Alex Schach         Updated unlocked opp tracking to run off of "Opportunity Unlocked" stage
    * 	2.2	 	12/1/2014	Alex Schach			Updated to not exit trigger when test is running to provide better coverage
    *****************************************************************************************************/
    if (Trigger.isbefore && !ExecuteNeeded.OnTrigger('All before OpportunityLineItemTriggers') && !test.isrunningTest()) return;
    else if((trigger.isafter && !trigger.isdelete) && !ExecuteNeeded.OnTrigger('All after OpportunityLineItemTriggers') && !test.isrunningTest()) return;
    
    Set<Id> OpportunityIds = new Set<Id>();        //Id of current opportunity
    Opportunity workingOpportunity = new opportunity();        //current opportunity
    list<opportunitylineitem> oldoli = new list<opportunitylineitem>();        //list of new opportunity line items
    list<opportunitylineitem> newoli = new list<opportunitylineitem>();        //list of old opportuntiy line items
    set<id> oldoliid = new set<id>();        //set of old opportunity line item ids
    decimal beforeprofit = 0;        //before the change value of profit
    decimal afterprofit = 0;        //after the change value of profit
    decimal beforerev = 0;
    decimal afterrev = 0;
    decimal beforecost = 0;
    decimal aftercost = 0;
    boolean toend = false;        //used to verify that the after trigger has been run before inserting or updating records
    string beforeproducts = '';        //string of before the change products
    string afterproducts = '';        //string of after the change products
    list<string> beforeproductlist = new list<string>();        //list to store and sort before the change products
    list<string> afterproductlist = new list<string>();        //list to store and sort after the change products
    string urlval = URL.getCurrentRequestURL().toExternalForm();
    system.debug('url: '+urlval);
        
    if (trigger.isInsert)
    {
        for (OpportunityLineItem oli: trigger.new)
        {
            if (!OpportunityIds.contains(oli.OpportunityId))
                OpportunityIds.add(oli.OpportunityId);
        }
    }
    else
    {
        //IsDelete
        for (OpportunityLineItem oli:trigger.old)
        {
            if (!OpportunityIds.contains(oli.OpportunityId))
                OpportunityIds.add(oli.OpportunityId);
            system.debug('deleted');
        }
    }
    if(!OpportunityIds.isEmpty())
    {
        Map<Id,Opportunity> OpportunityMap = new Map<Id,Opportunity>([SELECT Id, stagename, Solution_Set_s__c, practice_area__c, closedate FROM Opportunity WHERE Opportunity.Id IN: OpportunityIds]);
        for (Id oppId : OpportunityIds)
        {
            workingOpportunity = OpportunityMap.get(oppId);
        }
    }
     system.debug('workingopportunity: '+workingopportunity);

if(workingopportunity !=null)
{
    //add up before values
    if(trigger.isbefore)
    {
        if(trigger.isdelete && !urlval.contains('CONFIRMATIONTOKEN'))     
        {
            system.debug('got here');
            for(opportunitylineitem oli:trigger.old)        //pull the ids of the olis that were deleted or changed
            {
                if(!oldoliid.contains(oli.id))
                {
                    oldoliid.add(oli.id);
                }
            }
        }
        else     //is insert
        {
           //before insert we can just pull all the current existing olis from the opportunity
           oldoli = [select id, totalprice, extended_profit__c, extended_cost__c, PricebookEntry.Product2.productcode from opportunitylineitem where opportunityid  =:workingopportunity.id];
           for(opportunitylineitem tempoli:oldoli)
           {
               //add up values and products
               decimal rev = tempoli.totalprice;
               decimal profit = tempoli.extended_profit__c;
               decimal cost = tempoli.extended_cost__c;
               beforecost += cost;
               beforerev += rev;
               beforeprofit += profit;
               beforeproductlist.add(tempoli.pricebookentry.product2.productcode);
           }
               //send the values and products to an apex class for storage - now they can be retrieved at the end after the after triggers fire
               beforeproductlist.sort();        //sort the products alphabetically
               for(string s:beforeproductlist)
               {
                   beforeproducts += s + '\r\n';
               }
               UnlockedOpp.setbeforerevenue(beforerev);
               UnlockedOpp.setbeforeprofit(beforeprofit);
               UnlockedOpp.setbeforeproducts(beforeproducts.trim());
               system.debug('beforerev: '+ beforerev);
               system.debug('beforeprofit: '+ beforeprofit);
               system.debug('beforeproducts : ' +beforeproducts );
        }

        if(!oldoliid.isempty())        //this was a delete
        {
           //pull all olis from the old map
           oldoli = [select id, totalprice, extended_profit__c, extended_cost__c, PricebookEntry.Product2.productcode from opportunitylineitem where id =:trigger.oldmap.keyset()];
           system.debug('oldoli: ' +oldoli);
           for(opportunitylineitem tempoli:oldoli)
           {
               //add up values and products
               decimal rev = tempoli.totalprice;
               decimal profit = tempoli.extended_profit__c;
               decimal cost = tempoli.extended_cost__c;
               beforeproductlist.add(tempoli.pricebookentry.product2.productcode);
               beforecost += cost;
               beforerev += rev;
               beforeprofit += profit;
           }
               //send the values and products to an apex class for storage - now they can be retrieved at the end after the after triggers fire
               beforeproductlist.sort();        //sort the products alphabetically
               for(string s:beforeproductlist)
               {
                   beforeproducts += s + '\r\n';
               }
               UnlockedOpp.setbeforerevenue(beforerev);
               UnlockedOpp.setbeforeprofit(beforeprofit);
               UnlockedOpp.setbeforeproducts(beforeproducts.trim());
               system.debug('beforerev: '+ beforerev);
               system.debug('beforeprofit: '+ beforeprofit);
               system.debug('beforeproducts : ' +beforeproducts );
               system.debug('beforeproductlist : ' +beforeproductlist );
        }
    }        
//add up after values 
    if((trigger.isafter && trigger.isinsert) || (trigger.isdelete && trigger.isafter &&  urlval.contains('CONFIRMATIONTOKEN') ))
    
    {
        //after insert we can just pull all the new olis from the opportunity
        newoli= [select id, totalprice, extended_profit__c, extended_cost__c, PricebookEntry.Product2.productcode, PricebookEntry.Product2.Solution_Set__c, PricebookEntry.Product2.Practice__c, PricebookEntry.Product2.name from opportunitylineitem where opportunityid =:workingopportunity.id];
        system.debug('newoli: ' +newoli);
        for(opportunitylineitem tempoli: newoli)
        {
            //add up values
            decimal rev = tempoli.totalprice;
            decimal profit = tempoli.extended_profit__c;
            decimal cost = tempoli.extended_cost__c;
            aftercost += cost;
            afterrev += rev;
            afterprofit += profit;
            afterproductlist.add(tempoli.pricebookentry.product2.productcode);
        }
        //we don't need to store them, as this is the last trigger to fire - data will still be available
        system.debug('afterrev: '+ afterrev);
        system.debug('afterprofit: '+ afterprofit);
        system.debug('aftercost : ' + aftercost );
        
        afterproductlist.sort();        //sort the products alphabetically
        for(string s:afterproductlist)
        {
            afterproducts += s + '\r\n';
        }
        toend = true;        //after triggers have fired, now we can update/insert records
    }
    system.debug('toend: '+toend);
    
    if(toend && (UnlockedOpp.getbeforerevenue() != afterrev || UnlockedOpp.getbeforeprofit() != afterprofit || UnlockedOpp.getbeforeproducts() != afterproducts.trim()))
    {
        //after triggers have fired and some sort of change was made to the olis
        Set<String> SolutionValues = new Set<String>();
        Set<String> PracticeValues = new Set<String>();
        afterproductlist.clear();
        afterproducts = '';
        for(opportunitylineitem oli : newoli)
        {
            //add up after products
            afterproductlist.add(oli.pricebookentry.product2.productcode);
            if (oli.PricebookEntry.Product2.Solution_Set__c != null && oli.pricebookentry.product2.name == 'Fishnet Security')
            {
                //aggregates the solution set for Fishnet products
                string[] temp = oli.PricebookEntry.Product2.Solution_Set__c.split(';');
                for(string s: temp)
                {
                    if (!SolutionValues.contains(s))        //no duplicates
                    SolutionValues.add(s);
                }
            }
            if (oli.PricebookEntry.Product2.Practice__c != null && oli.pricebookentry.product2.name == 'Fishnet Security')
            {
                //aggregates the practice area for Fishnet products
                string[] temp = oli.PricebookEntry.Product2.Practice__c.split(';');
                for(string s: temp)
                {
                    if (!PracticeValues.contains(s))        //no duplicates
                    PracticeValues.add(s);
                }
            }
        }
        afterproductlist.sort();        //sort the products alphabetically
        for(string s:afterproductlist)
            {
                afterproducts += s + '\r\n';
            }
                system.debug('afterproducts: '+afterproducts);
        //create final strings for insertion to multi pick lists on opp
        string finalSolutionValue = '';
        if(solutionvalues != null)
        {
            for (string s: SolutionValues)
            {
                finalSolutionValue += s+';';
            }
        }
        string finalPracticeValue = '';
        if(practicevalues != null)
        {
            for (string s: PracticeValues)
            {
                finalPracticeValue += s+';';
            }
        }
        system.debug('Final Solution Set Values: '+finalSolutionValue);
        system.debug('Final Practice Set Values: '+finalPracticeValue);
        //update workingopportunity
        List<Opportunity> toUpdate = new List<Opportunity>();
        if (finalSolutionValue != '')
        {
            workingOpportunity.Solution_Set_s__c = finalSolutionValue;
        }
        else
        {
            workingOpportunity.Solution_Set_s__c = null;
        }
        if (finalPracticeValue != '')
        {
            workingOpportunity.Practice_Area__c = finalPracticeValue;
        }
        else
        {
            workingOpportunity.Practice_Area__c = null;
        }
        toUpdate.add(workingOpportunity);
        if(!toUpdate.isEmpty())
        {
            try
            {
                update(toUpdate);
            }
            catch (exception e)
            {
                system.debug('Failed to update opportunity: '+e);
            }
        }
        //commented out until future release - Case 62685
        /*if(workingOpportunity.stagename == 'Opportunity Unlocked')
        {        
            //if the opportunity was unlocked, we create an Unlocked Opp GP record to track the changes for reporting
            //removed product codes from final output - Bryan said we don't need them right now
            Unlocked_Opp_GP_Changes__c uogc = new Unlocked_Opp_GP_Changes__c();
            uogc.Revenue_Before__c = UnlockedOpp.getbeforerevenue();        //call before values from storage
            uogc.Revenue_After__c = afterrev;
            uogc.GP_Before__c = UnlockedOpp.getbeforeprofit();        //call before values from storage
            uogc.GP_After__c = afterprofit;
            //uogc.Products_Before__c = UnlockedOpp.getbeforeproducts();        //call before products from storage
            //uogc.Products_After__c = afterproducts.trim();
            uogc.ownerid = userinfo.getuserid();
            uogc.user__c = userinfo.getuserid();
            uogc.opportunity__c = workingopportunity.id;
            uogc.Date_changed__c = system.now();
            uogc.Close_Date_Before__c = workingopportunity.closedate;
            system.debug('uogc: ' +uogc);
            try
            {
                insert uogc;
            }
            catch (exception e)
            {
                system.debug('Failed to insert Unlocked Opp GP Change: '+e);
            }
        }*/
    }
}
}