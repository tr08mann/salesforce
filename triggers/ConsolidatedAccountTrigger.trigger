trigger ConsolidatedAccountTrigger on Account (before insert, before update) {
    if (!ExecuteNeeded.OnTrigger('All AccountTriggers')) return;
    /****************************************************************************************************
    *Description:   Sends an Email notification to the OLD Account Owner when the Account Owner has changed 

    /****************************************************************************************************
    *Description:    Populates old values with new values for historical backup for Owner & PreviousOwner
	*				 Sends an Email notification to the OLD Account Owner when the Account Owner has changed
    *                   
    *
    *Revision   |Date                           |AdditionalNotes
    *====================================================================================================
    *   1.0     4/16/2013                       WO#1252 Initial Version
    * 	2.0		1/21/2015		Alex Schach		Updated to send notification email only when old owner is active
    *****************************************************************************************************/
    if (trigger.isBefore && trigger.isUpdate)
    {
        Set<Id> userIds = new Set<Id>(); 
        List<Messaging.Singleemailmessage> emailToSend = new List<Messaging.Singleemailmessage>(); //Listing of Emails to be sent
        //Retrieve Email Template
        List<EmailTemplate> et = [SELECT id FROM EmailTemplate WHERE developerName = 'Account_Owner_Change_Notification'];       
        for (Account acctNew: trigger.new)
        {  
            Account oldAcct = null;    
            oldAcct = (Account) Trigger.OldMap.get(acctNew.id);            
            if(acctNew.OwnerID != oldAcct.OwnerId)
            {
                userIds.add(oldAcct.OwnerId);
            }            
        }
        
        Map<Id, User> usersName = new Map<Id, User>([SELECT id, name, isActive FROM user WHERE id IN : userIds ]);        
        if(!usersName.IsEmpty())
        {
            for (Account acct: trigger.new)
            {
                Account oldAcct1 = null;
                if (Trigger.OldMap != null)
                { 
                    oldAcct1 = (Account) Trigger.OldMap.get(acct.id);
                    if(acct.OwnerID != oldAcct1.OwnerID)
                    {
                        if(usersName.containsKey(oldAcct1.OwnerId))
                        {
                            User userName = usersName.get(oldAcct1.OwnerId);
                            acct.Previous_Account_Owner__c  = userName.name;
                        }   
                        if(usersName.get(oldAcct1.OwnerId).isActive) //Account Owner has changed. Create an email to be sent to the old owner
                        {
                            if (et != null && !et.isEmpty())
                            {
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                mail.setTemplateId(et[0].Id);
                                mail.setTargetObjectId(oldAcct1.OwnerId);
                                mail.setSaveAsActivity(false);
                                mail.setWhatId(acct.Id);
                                emailToSend.add(mail);
                            }
                        }
                    }
                }
            }
        } 
        if (!emailToSend.isEmpty())
        {
            Messaging.sendEmail(emailToSend);
        }
    }
    /****************************************************************************************************
    *Description:    Addressable Pool of Accounts
    *                   
    *
    *Revision   |Date                           |AdditionalNotes
    *====================================================================================================
    *   1.0     7/09/2014                       WO#1610 Initial Version
    *   1.5     8/18/2014    Alex Schach        Updated to only fill the participant box/date when campaign field is filled and box isn't already checked
    *****************************************************************************************************/
    if (trigger.isBefore)
    {
        for (Account a:trigger.new)
        {
            if(trigger.isupdate && trigger.oldmap.get(a.id).LEAD_Call_Campaign__c == null && trigger.newmap.get(a.id).LEAD_Call_Campaign__c != null && a.Lead_Campaign_participant__c == false)
            {
                a.Lead_Campaign_Participant__c = true;
                a.Lead_Campaign_Participant_Added__c = system.now();
            }
            else if(trigger.isinsert && a.LEAD_Call_Campaign__c != null && a.Lead_Campaign_Participant__c == false)
            {
                a.Lead_Campaign_Participant__c = true;
                a.Lead_Campaign_Participant_Added__c = system.now();
            }
        }
    }
}