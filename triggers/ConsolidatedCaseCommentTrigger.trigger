trigger ConsolidatedCaseCommentTrigger on CaseComment (before insert) {
	/****************************************************************************************************
	*Description:	Place the last inserted Case Comment and Date Time on the Case record for reporting purposes.
	*				WO#1335	
	*
	*Revision	|Date		|Author				|AdditionalNotes
	*====================================================================================================
	*	1.0		4/16/2013	Justin Padilla		WO#1335 Initial Implementation
	*****************************************************************************************************/
	//Get the Case information
	Set<Id> CaseIds = new Set<Id>();
	//Create a listing of Cases that need to be updated *affected by this trigger and need comment and date fields updated
	Map<Id,Case> toUpdate = new Map<Id,Case>();
	for (CaseComment cc: trigger.new)
	{
		if (cc.ParentId != null && !CaseIds.contains(cc.ParentId))
		{
			//Add the Case Id for Query *bulkied
			CaseIds.add(cc.ParentId);
		}
	}
	//Gather the Case information for Comment and Date population. Map for recall
	Map<Id,Case> cases = new Map<Id,Case>([SELECT
	Case.Id,
	Case.Most_Recent_Case_Comment__c,
	Case.Most_Recent_Case_Comment_Date__c
	FROM Case WHERE Case.Id IN: CaseIds]);
	//Now Process the Cases that need updating
	for (CaseComment cc: trigger.new)
	{
		
		Case workingCase = cases.get(cc.ParentId);
		if (workingCase != null && !toUpdate.containsKey(cc.ParentId))
		{
			workingCase.Most_Recent_Case_Comment__c = cc.CommentBody;
			workingCase.Most_Recent_Case_Comment_Date__c = Date.today();
			toUpdate.put(workingCase.Id,workingCase);
		}
	}
	if (!toUpdate.isEmpty())
		update(toUpdate.values());
}