trigger ConsolidatedQuoteTrigger on Quote (before insert, before update, after insert, after update) {

//commented out everything dealing with deal registration as those actions have moved to the deal registration object and triggers
//06/05/2014 Alex Schach

if (trigger.isBefore && !ExecuteNeeded.OnTrigger('All QuoteTriggers Before')) return;
else if (trigger.isAfter && !ExecuteNeeded.OnTrigger('All QuoteTriggers After')) return;
    //Attempt to Assign Fishnet Quote Number to the Quote, taken from the opportunity
        if (trigger.isBefore)
        {
            Map<Id,Opportunity> opp = new Map<Id,Opportunity>();
            Map<Id,OpportunityContactRole> quoteContact = new Map<Id,OpportunityContactRole>();
            //if (trigger.isInsert || trigger.isUpdate)
            if (trigger.isInsert || trigger.isUpdate)
            {
                for (Quote q:trigger.new)
                {
                    //Get the Opportunity Number from the Opportunity AND a count of how many other Quotes are associated
                    if (q.OpportunityId != null)
                    {
                        if (!opp.containsKey(q.OpportunityId))
                        {
                            //Get the opportunity number off of the opportunity
                            //Opportunity o = [select id, opportunity_number__c from opportunity where id = :q.opportunityId];
                            Opportunity o = [select id, opportunity_number__c from opportunity where id = :q.opportunityId];
                            opp.put(o.Id,o);
                        }
                        Opportunity workingOpportunity = opp.get(q.OpportunityId);
                        system.debug('Quote OpportunityId: '+workingOpportunity.Id);
                        //Now get the count of Quotes on the opportunity
                        
                        if (trigger.isInsert || q.Fishnet_Quote_Number__c == null)
                        {
                            Integer quoteCount = [SELECT Count() FROM Quote WHERE Quote.OpportunityId =: q.OpportunityId];
                            q.Fishnet_Quote_Number__c = workingOpportunity.opportunity_number__c+'-'+string.ValueOf(quoteCount+1);
                            system.debug('Quote Number assigned: '+q.Fishnet_Quote_Number__c);
                        }
                        
                        //JGP 9/25/2012 Modified to pull over Contact Information from the Opportunity
                        
                        if (!quoteContact.containsKey(q.OpportunityId))
                        {
                            List<OpportunityContactRole> workingContactRole = new List<OpportunityContactRole>();
                            workingContactRole = [SELECT
                            OpportunityContactRole.Id,
                            OpportunityContactRole.ContactId,
                            OpportunityContactRole.OpportunityId
                            FROM OpportunityContactRole WHERE OpportunityContactRole.Role = 'Quote Contact'
                            AND OpportunityContactRole.IsPrimary = true
                            AND OpportunityContactRole.OpportunityId =:q.OpportunityId LIMIT 1];
                            if (workingContactRole.size() > 0)
                                quoteContact.put(q.OpportunityId,workingContactRole[0]);
                        }
                        OpportunityContactRole workingContactRole = quoteContact.get(q.OpportunityId);
                        if (workingContactRole != null)
                            q.ContactId = workingContactRole.ContactId;
                        
                    }
                }
            }
        }

    public class MyException extends Exception{}
}

    /****************************************************************************************************
    *Description:   Deal Registration Status Notifications
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     2/13/2012   Justin Padilla      WO#1277 Initial Version
    *   1.1     4/4/2013    Justin Padilla      WO#1277 Change to use the Inside_Sales_Rep__c picklist with user lookup on that field
    *   1.2     5/1/2013    Justin Padilla      WO#1355 Change to now include Pending Submission kicking off an email.
    *   1.3     5/6/2013    Justin Padilla      WO#1369 Email needs to be sent to Deal Reg Rep ONLY if the status is Pending Submission
    *   1.4     5/6/2013    Justin Padilla      WO#1356 Bypass sending emails and tasks on quote clone
    *****************************************************************************************************/
    
    //JGP 3/27/2014 Removed Inside_Sales_NEW__c and replaced with Inside_Sales__c. To restore remove commented out lines and replace line below it with commented version if starting with JGP 3/27/2014
   /* if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
    {
        system.debug('Deal Registration Status Notification');
        system.debug('Trigger isAfter: '+trigger.isAfter);
        system.debug('Trigger isBefore: '+trigger.isBefore);
        system.debug('Trigger isInsert: '+trigger.isInsert);
        system.debug('Trigger isUpdate: '+trigger.isUpdate);
        Integer DealRegistrationStatusChanges = 0;
        for (Quote q:trigger.new)
        {
            if (trigger.isUpdate)
            {
                if (q.Deal_Registration_Status__c != trigger.oldMap.get(q.Id).Deal_Registration_Status__c) //Status has changed
                    DealRegistrationStatusChanges++;
            }
            else if (trigger.isInsert && (q.Cloned__c != null && !q.Cloned__c))
            {
                if(q.Deal_Registration_Status__c != null) DealRegistrationStatusChanges++;
            }
        }
        system.debug('Deal Registration Status Changes: '+DealRegistrationStatusChanges);
        //Don't Query unless we have changes that require processing
        if (DealRegistrationStatusChanges > 0)
        {
            system.debug('Deal Registration Status Notification Entered...');
            //Populate a listing of Deal Registration Reps and add them to a Map keyed by OpportunityId
            Map<Id,Opportunity> opportunities = new Map<Id,Opportunity>();
            //Create a Map to hold the Users from the Inside Sales Rep field. Id by Name (picklist value)
            Map<String,User> insideSales = new Map<String,User>();
            
            Set<Id> oppIds = new Set<Id>();
            for (Quote q:trigger.new)
            {
                if (!oppIds.contains(q.OpportunityId))
                    oppIds.add(q.OpportunityId);
            }
            system.debug('Opportunity Ids count: '+oppIds.size());
            if (!oppIds.isEmpty())
            {
                //Set to hold Inside Sales Rep Names
                Set<String> insideSR = new Set<String>();
                opportunities = new Map<Id,Opportunity>(
                [SELECT 
                Opportunity.Id,
                Opportunity.Deal_Reg_Desk_Rep__c,
                Opportunity.AccountId,
                Opportunity.OwnerId,                                // WO 1596 :: agupta@rainmaker-llc.com replaced the Old Picklist with the Look up corresponding to it.
                //JGP 3/27/2014 Opportunity.Inside_Sales_NEW__c
                Opportunity.Inside_Sales__c                         //Removed the picklist field with its corresponding lookup field.
                FROM Opportunity WHERE Opportunity.Id IN: oppIds]
                );
                if (!opportunities.isEmpty())
                {
                    for (Opportunity opp: opportunities.values())
                    {
                        // JGP 3/27/2014 if (opp.Inside_Sales_NEW__c != null && !insideSR.contains(opp.Inside_Sales_NEW__c))
                        if (opp.Inside_Sales__c != null && !insideSR.contains(opp.Inside_Sales__c))
                        {
                            // JGP 3/27/2014 insideSR.add(opp.Inside_Sales_NEW__c);
                            insideSR.add(opp.Inside_Sales__c);
                        }
                    }
                }
                if (!insideSR.isEmpty())
                {
                    // JGP 3/27/2014 for (User u: [SELECT User.Id, User.Name FROM User WHERE User.Id IN: insideSR])
                    for (User u: [SELECT User.Id, User.Name FROM User WHERE User.Name IN: insideSR])
                    {
                        //if (!insideSales.containsKey(u.Id)) ARS 3/27/2014
                        if (!insideSales.containsKey(u.Name)) 
                        {
                            //insideSales.put(u.Id,u); ARS 3/27/2014
                            insideSales.put(u.Name,u);
                        }
                    }
                }
            }
            System.debug('Inside Sales Reps User Map: '+insideSales);
            EmailTemplate followUp = null;
            EmailTemplate submitted = null;
            
            //Populate Email Templates
            List<EmailTemplate> etemplates = [SELECT id,developerName FROM EmailTemplate WHERE (developerName = 'Follow_up_on_Deal_Registration' OR developerName = 'Deal_Registration_Status')];
            if (etemplates != null && !etemplates.isEmpty())
            {
                for (EmailTemplate et:etemplates)
                {
                    if (et.developerName == 'Follow_up_on_Deal_Registration')
                    {
                        followUp = et;
                        system.debug('Followup Template Identified...');
                    }
                    else if (et.developerName == 'Deal_Registration_Status')
                    {
                        submitted = et;
                        system.debug('Deal Registration Status Template Identified...');
                    }
                }
            }           
            List<Messaging.SingleEmailMessage> emailToSend = new List<Messaging.SingleEmailMessage>(); 
                
            for (Quote q:trigger.new)
            {
                Boolean toProcess = false;
                if (trigger.isUpdate)
                {
                    if (q.Deal_Registration_Status__c != trigger.oldMap.get(q.Id).Deal_Registration_Status__c && q.Deal_Registration_Status__c != null)
                        toProcess = true;
                }
                else if (trigger.isInsert && (q.Cloned__c != null && !q.Cloned__c))
                {
                    if (q.Deal_Registration_Status__c != null)
                        toProcess = true;
                }
                
                if (toProcess) //Status has changed
                {
                    //Follow-up on Deal Registration - Pending Submission only - Not on pending approval
                    if (followUp != null && opportunities.get(q.OpportunityId).Deal_Reg_Desk_Rep__c !=null  && (q.Deal_Registration_Status__c.toLowerCase().contains('pending submission')))
                    {
                        system.debug('Follow-up being created.');
                        //Send an email to the Deal Registration Rep if populated
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTemplateId(followUp.Id);
                        mail.setTargetObjectId(opportunities.get(q.OpportunityId).Deal_Reg_Desk_Rep__c);
                        mail.setSaveAsActivity(false);
                        mail.setWhatId(q.Id);
                        emailToSend.add(mail);
                        system.debug('Deal Reg Desk Rep Email has been created for: '+opportunities.get(q.OpportunityId).Deal_Reg_Desk_Rep__c);
                    }
                    //Deal Registration Status - Send Email to the Opportunity Owner and the Inside Sales Rep (Inside_Sales_NEW__c)
                    else if (submitted != null && (q.Deal_Registration_Status__c.toLowerCase().contains('approved') || q.Deal_Registration_Status__c.toLowerCase().contains('denied')))
                    {
                        system.debug('Deal Registration Status (Opportunity Owner) being created.');
                        //Send to the Opportunity Owner (always populated)
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTemplateId(submitted.Id);
                        mail.setTargetObjectId(opportunities.get(q.OpportunityId).OwnerId);
                        mail.setSaveAsActivity(false);
                        //mail.setWhatId(opportunities.get(q.OpportunityId).AccountId);
                        mail.setWhatId(q.Id);
                        emailToSend.add(mail);
                        system.debug('Opportunity Owner Email created for: '+opportunities.get(q.OpportunityId).OwnerId);
                        
                        //Send to the Inside Sales Rep if populated and not the same as the opporutunity owner (don't duplicate)
                        //if (opportunities.get(q.OpportunityId).Inside_Sales_NEW__c != null && opportunities.get(q.OpportunityId).Inside_Sales_NEW__c != opportunities.get(q.OpportunityId).OwnerId) //
                        //JGP 3/27/2014 User insideSalesRep = insideSales.get(opportunities.get(q.OpportunityId).Inside_Sales_NEW__c);
                        User insideSalesRep = insideSales.get(opportunities.get(q.OpportunityId).Inside_Sales__c);
                        if (opportunities.get(q.OpportunityId).Inside_Sales__c != null && insideSalesRep !=null && insideSalesRep.Id != opportunities.get(q.OpportunityId).OwnerId) //
                        {
                            system.debug('Deal Registration Status (Inside Sales Rep) being created.');
                            Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
                            mail2.setTemplateId(submitted.Id);
                            mail2.setTargetObjectId(insideSalesRep.Id);
                            mail2.setSaveAsActivity(false);
                            mail2.setWhatId(q.Id);
                            emailToSend.add(mail2);
                            system.debug('Inside Sales Rep Email created for: '+insideSalesRep.Name);
                        }
                    }               
                }
            }
            if (!emailToSend.isEmpty())
                Messaging.sendEmail(emailToSend);
        }
    }
*/    

    
    //SM change 1/24/2013 to solve error message during FNS testing
 //   List<Task> taskListToUpdateonDate = new List<Task>() ;
    //Deal Registration Checker and adds tasks to that Deal Registration User if needed
     /* create Follow up on Deal Registration Task on Opportunity when Deal_Registration_Status__c equals to Deal Registration Pending Submission 
                   WO: 1183
                   Modification Date : 02/11/2013 */ 
    /****************************************************************************************************
    *Description:   Deal Registration - Task Handling
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/8/2013    Justin Padilla      Modifications to the Task Subject Lines
    *   1.1     4/9/2013    Justin Padilla      Modification to Pending to include the Account Name
    *   1.2     5/6/2013    Justin Padilla      Modified to only perform Task operations when Quote is not cloned
    *****************************************************************************************************/   
  /*  if (trigger.isAfter)
    {
        // SM change ConsolidatedQuoteTrigger (removed reference to  Deal_Registration_Code__c )
        //Set to Hold Quote Opportunity Ids
        Set<id> oppids = new Set<id>();
        //List to hold Quote Information
        List<Quote> quoteList = new List<Quote>();
        //List of Tasks
        List<Task> chkTasks = new List<Task>();
        //Map of String to Set of String
        Map<String,Set<String>> oppQuoteMap = new Map<String,Set<String>>();
        //Map of Opportunities
        Map<id, Opportunity> oppmap = new Map<id, Opportunity>();
        
        Map<String,Task> taskMap = new Map<String,Task>();
        Map<String,Quote> quoteMap = new Map<String,Quote>();
        List<Task> taskListToUpdate = new List<Task>();
        List<Task> taskListToInsert = new List<Task>(); 
        for(Quote q:trigger.new)
        {
            if (q.OpportunityId != null)
            {
                oppids.add(q.OpportunityId);
            }
        }
        Quote quoteMyCurrent = [SELECT Id, OpportunityId, Deal_Registration_Status__c,Name , Vendor__c,Cloned__c,  Opportunity.Deal_Reg_Desk_Rep__c,Deal_Expiration__c, Opportunity.Type,
                Opportunity.Account.Id, Opportunity.Account.Name, Opportunity.Deal_Reg_Desk_Rep__r.Email, Opportunity.Deal_Reg_Desk_Rep__r.Id FROM Quote WHERE Id =: trigger.new[0].Id LIMIT 1];       
        if (!oppids.isEmpty())
        {
            //Get the list of Quotes under same Opportunity  != current Quote
            
            quoteList = [SELECT Id, OpportunityId, Deal_Registration_Status__c,Name , Vendor__c, Opportunity.Deal_Reg_Desk_Rep__c, Deal_Expiration__c,
                    Opportunity.Deal_Reg_Desk_Rep__r.Email FROM Quote WHERE OpportunityId IN :oppids and Id NOT IN :Trigger.newMap.keySet()];
            chkTasks = [SELECT Id, WhatId,status,Subject,Vendor_Name__c FROM Task WHERE WhatId IN :oppids AND Status !=: 'Completed'];
        }
        //Segregate Tasks into a Map keyed off of the Deal Registration Status, OpportunityId, and vendor name since each vendor gets it's own version of Deal Registration Tracking Status
        for(Task tasktemp : chkTasks){
            //JGP 4/8/2013 /uodated task subject to handle previous task values as well as the new ones
            if(null == taskMap.get(tasktemp.Subject+'Key'+tasktemp.WhatId)){
                if(tasktemp.Subject.contains('Deal Registration Expiring ') || tasktemp.Subject.contains('Expiring'))               
                    taskMap.put('Deal Registration Expiring '+'Key'+tasktemp.WhatId+' Vendor'+tasktemp.Vendor_Name__c,tasktemp);
                if(tasktemp.Subject.contains('Follow up on Deal Registration Status ') || tasktemp.Subject.contains('Follow up'))               
                    taskMap.put('Follow up on Deal Registration Status '+'Key'+tasktemp.WhatId+' Vendor'+tasktemp.Vendor_Name__c,tasktemp);
                if(tasktemp.Subject.contains('Deal Registration Pending ') || tasktemp.Subject.contains('Pending'))             
                    taskMap.put('Deal Registration Pending '+'Key'+tasktemp.WhatId+' Vendor'+tasktemp.Vendor_Name__c,tasktemp);
            }
        }
        for(Quote quotetemp : quoteList){
            if(null == quoteMap.get(quotetemp.Deal_Registration_Status__c+'Key'+quotetemp.Vendor__c))
                quoteMap.put(quotetemp.Deal_Registration_Status__c+'Key'+quotetemp.Vendor__c,quotetemp);
        }
        ////////////////////////////////////////////////
        
        Currently in production when the deal registration is denied all of the deal registration tasks are completed and we want this to continue. In the sandbox that is not happening.
        if(Trigger.isUpdate && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Denied' && !ProcessControl.processed ){
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c != 'Deal Registration Denied') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Denied'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task ueTemp1 = taskMap.get('Deal Registration Expiring '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    Task ueTemp2 = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    Task ueTemp3 = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null != ueTemp1){
                        ueTemp1.Status='Completed';
                        taskListToUpdate.add(ueTemp1);
                    }
                    if(null != ueTemp2){
                        ueTemp2.Status='Completed';
                        taskListToUpdate.add(ueTemp2);
                    }
                    if(null != ueTemp3){
                        ueTemp3.Status='Completed';
                        taskListToUpdate.add(ueTemp3);
                    }
                }               
                //Create a new task with the Deal Registration Denied subject and complete it as well
                /*
                Task dealDenied = (QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  Date.today(),  'Deal Registration Denied',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    //Auto complete the task
                
                    //JGP 4/8/2013 Updated Task wtih new Subject
                    Task dealDenied = (QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  Date.today(),  'Denied',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    dealDenied.Status = 'Completed';
                    system.debug('Deal Registration Denied - Line #163');
                    taskListToInsert.add(dealDenied);
            }
        }
        
        /* We can update the trigger to create a new task and complete the old task when the expiration date is changed by the user
        if(Trigger.isUpdate && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Expiration__c != Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Expiration__c 
                            && !ProcessControl.processed 
                            && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c ){
            //Complete the Old Task
            Task ueTemp1 = taskMap.get('Deal Registration Expiring '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            Task ueTemp2 = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            Task ueTemp3 = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            String newTaskSubject ='';
            taskListToUpdateonDate = new List<Task>();
            set<Id> preventDuplicates = new Set<Id>();
            if(null != ueTemp1 && !preventDuplicates.contains(ueTemp1.Id)){
                ueTemp1.Status='Completed';
                taskListToUpdateonDate.add(ueTemp1);
                preventDuplicates.add(ueTemp1.Id);
                //JGP 4/8/2013 /change Task Subject
                //newTaskSubject ='Deal Registration Expiring ';
                newTaskSubject ='Expiring';
            }
            if(null != ueTemp2 && !preventDuplicates.contains(ueTemp2.Id)){
                ueTemp2.Status='Completed';
                taskListToUpdateonDate.add(ueTemp2);
                preventDuplicates.add(ueTemp2.Id);
                //JGP 4/8/2013 /change Task Subject
                //newTaskSubject ='Follow up on Deal Registration Status ';
                newTaskSubject ='Follow up';                
            }
            if(null != ueTemp3 && !preventDuplicates.contains(ueTemp3.Id)){
                ueTemp3.Status='Completed';
                taskListToUpdateonDate.add(ueTemp3);
                preventDuplicates.add(ueTemp3.Id);
                //JGP 4/8/2013 /change Task Subject
                //newTaskSubject ='Deal Registration Pending ';
                //JGP 4/9/2013 Added Account Name to the new Task Subject
                //newTaskSubject ='Pending';
                newTaskSubject ='Pending_'+quoteMyCurrent.Opportunity.Account.Name;
            }
            if(newTaskSubject!=''){
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                quoteMyCurrent.Vendor__c,  quoteMyCurrent.Deal_Expiration__c.addDays(-15),  newTaskSubject,  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                system.debug('Deal Registration Expiration Changed - Line #208');
            }
         }
        
        /* Deal Registration Pending 
        if(Trigger.isUpdate && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission' && !ProcessControl.processed ){
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Submitted, Pending Approval'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task uMeTemp = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null!=uMeTemp){
                        uMeTemp.Status='Completed';
                        taskListToUpdate.add(uMeTemp);
                        system.debug('Create Pending Submission, Follow-up - Line #222');
                    }
                    
                }
            }
            
            //Update OR Create a new Task
            Task updateTemp = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            if(null==updateTemp){// No task already exists
                //JGP 4/8/2013 Updated Task Name
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                    quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Deal Registration Pending ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                    quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Pending_'+quoteMyCurrent.Opportunity.Account.Name,  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    system.debug('Pending Submission, Create New - Line #238');
                /*taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                    quoteMyCurrent.Vendor__c,  System.Today(),  'Deal Registration Pending ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                
            }else{// Task already exists
                Quote updateTemQ = quoteMap.get(quoteMyCurrent.Deal_Registration_Status__c+'Key'+quoteMyCurrent.Vendor__c);
                if(null==updateTemQ){
                    //updateTemp.ActivityDate = System.Today().addDays(2);
                    updateTemp.ActivityDate = System.Today();
                    taskListToUpdate.add(updateTemp);
                    system.debug('Pending Submission, Update Existing - Line #248');
                }
            }
        }
        /* Deal Registration Submitted, Pending Approval 
        if(Trigger.isUpdate && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval' && !ProcessControl.processed ){
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Pending Submission'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task uMeTemp = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null!=uMeTemp){
                        uMeTemp.Status='Completed';
                        taskListToUpdate.add(uMeTemp);
                        system.debug('Registration Submitted, Pending Approval Update - Line #262');
                    }
                    
                }
            }
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Approved') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Approved'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task uMeTemp = taskMap.get('Deal Registration Expiring '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null!=uMeTemp){
                        uMeTemp.Status='Completed';
                        taskListToUpdate.add(uMeTemp);
                        system.debug('Registration Submitted, Pending Approval Update - Line #275');
                    }
                    
                }
            }
            //Update OR Create a new Task
            Task updateTemp = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            if(null==updateTemp){// No task already exists
                //JGP 4/8/2013 Updated Task Subject
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                    quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Follow up on Deal Registration Status ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                    quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Follow up',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    system.debug('Registration Submitted, Pending Approval New - Line #290');
            }else{// Task already exists
                Quote updateTemQ = quoteMap.get(quoteMyCurrent.Deal_Registration_Status__c+'Key'+quoteMyCurrent.Vendor__c);
                if(null==updateTemQ){
                    updateTemp.ActivityDate = System.Today().addDays(2);
                    taskListToUpdate.add(updateTemp);
                    system.debug('Registration Submitted, Pending Approval Update - Line #296');
                }
            }
        }
        /* Deal Registration Approved 
        if(Trigger.isUpdate && Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Approved' && !ProcessControl.processed ){
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Pending Submission'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task uMeTemp = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null!=uMeTemp){
                        uMeTemp.Status='Completed';
                        taskListToUpdate.add(uMeTemp);
                        system.debug('Registration Approved, Update - Line #310');
                    }
                    
                }
            }
            //throw new MyException('There is no Deal '+taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c)+' <<>> '+quoteMap.get('Deal Registration Pending Submission'+'Key'+quoteMyCurrent.Vendor__c));
            //Complete the Old Task
            if(Trigger.oldMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval') {
                Quote oldCategoryQuote = quoteMap.get('Deal Registration Submitted, Pending Approval'+'Key'+quoteMyCurrent.Vendor__c);
                if(null==oldCategoryQuote){
                    Task uMeTemp = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
                    if(null!=uMeTemp){
                        uMeTemp.Status='Completed';
                        taskListToUpdate.add(uMeTemp);
                        system.debug('Registration Approved, from Pending Approval Update - Line #324');
                    }
                    
                }
            }
            
            //Update OR Create a new Task
            Task updateTemp = taskMap.get('Deal Registration Expiring '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            //throw new MyException('There is no Deal '+updateTemp+'<<>>>'+quoteMyCurrent.Deal_Expiration__c);
            if(null==updateTemp){// No task already exists
                if(null!=quoteMyCurrent.Deal_Expiration__c){
                    //JGP 4/8/2013 Updated Task Subject
                    /*
                    taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  quoteMyCurrent.Deal_Expiration__c.addDays(-15),  'Deal Registration Expiring ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    
                    taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  quoteMyCurrent.Deal_Expiration__c.addDays(-15),  'Expiring',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    system.debug('Registration Approved, New - Line #342');
                }
            }
        }
        //throw new MyException('There is no Deal '+taskListToInsert+'<<>>>'+quoteMyCurrent.Deal_Expiration__c);
        /*     _________________________________________________   __________________________________       */
        /*
            JGP WO#1300 New task for Deal Registration Pending to be on current Date, not Current Date +2days
        */
        /* New, not Cloned, Pending Submission 
        if(Trigger.isInsert && (Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c != null && !Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c) &&
        Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission'){
            ProcessControl.processed = true;
            Task updateTemp = taskMap.get('Deal Registration Pending '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            if(null==updateTemp){// No task already exists
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Deal Registration Pending ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                */
                //JGP 4/8/2013 Updated Task Subject
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today(),  'Deal Registration Pending ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                */
                //JGP 4/9/2013 Added Account Name to the new Task Subject
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today(),  'Pending',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today(),  'Pending_'+quoteMyCurrent.Opportunity.Account.Name,  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
            }
        }
        /* New, not cloned, Deal Registration Submitted, Pending Approval 
        if(Trigger.isInsert && (Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c != null && !Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c) &&
        Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval'){
            ProcessControl.processed = true;
            Task updateTemp = taskMap.get('Follow up on Deal Registration Status '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            if(null==updateTemp){// No task already exists
                //JGP 4/8/2013 Updated Task Subject
                /*
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Follow up on Deal Registration Status ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                
                taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  System.Today().addDays(2),  'Follow up',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
            }
        }
        /* New, not cloned, Deal Registration Approved 
        if(Trigger.isInsert && (Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c != null && !Trigger.newMap.get(quoteMyCurrent.Id).Cloned__c) &&
        Trigger.newMap.get(quoteMyCurrent.Id).Deal_Registration_Status__c == 'Deal Registration Approved'){
            ProcessControl.processed = true;
            Task updateTemp = taskMap.get('Deal Registration Expiring '+'Key'+quoteMyCurrent.OpportunityId+' Vendor'+quoteMyCurrent.Vendor__c);
            //throw new MyException('There is no Deal '+updateTemp+' <<>> '+quoteMyCurrent.ExpirationDate);
            if(null==updateTemp){// No task already exists
                if(null!=quoteMyCurrent.Deal_Expiration__c){
                    //jgp 4/8/2013 Updated Task Subject
                    /*
                    taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  quoteMyCurrent.Deal_Expiration__c.addDays(-15),  'Deal Registration Expiring ',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                    
                    taskListToInsert.add(QuoteTask.getTask( quoteMyCurrent.Name, quoteMyCurrent.OpportunityId ,
                        quoteMyCurrent.Vendor__c,  quoteMyCurrent.Deal_Expiration__c.addDays(-15),  'Expiring',  quoteMyCurrent.Opportunity.Deal_Reg_Desk_Rep__r.Id ));
                }
            }
        }
        system.debug('taskListToUpdateonDate: '+taskListToUpdateonDate);
        system.debug('taskListToUpdate: '+taskListToUpdate);
        system.debug('taskListToInsert: '+taskListToInsert);
        if(!taskListToUpdateonDate.isEmpty()){
            update taskListToUpdateonDate;
        }else if(!taskListToUpdate.isEmpty()){
            update taskListToUpdate;
        }
        if(!taskListToInsert.isEmpty()&& quoteMyCurrent.Opportunity.Type !='Renewal Business'){
            insert taskListToInsert;
        }
        
    }

/* ======================================== END Deal Registration - Tasks =================================================    
    
  
    //Complete Task from Quote
    if (trigger.isAfter && trigger.isUpdate)
    {
        system.debug('Complete Task from Quote - Line# 426');
        Set<Id> wids = new Set<Id>();
        Map<Id, String> nMap = new Map<Id, String>();
        List<Task> toUpdate = new List<Task>();
        
//JGP 2/11/2013 Added from Production Code
     //If the Status has changed, add it to the nMap
        for(Quote q : trigger.new){
        if(trigger.oldMap.get(q.id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval'
        && q.Deal_Registration_Status__c != 'Deal Registration Submitted, Pending Approval'){
          nMap.put(q.OpportunityId, q.Name);
          }    
        }
        for(Task t : [Select WhatId, Subject, Status, Id from Task where WhatId IN :nMap.keySet()]){
        if(t.Subject == 'Follow up on Deal Registration Status ' + nMap.get(t.WhatId)){ t.Status = 'Completed';
        toUpdate.add(t);
          }
        }
        //Update Tasks if needed
        if(!toUpdate.isEmpty()) update toUpdate;
//JGP End addition        
        
        //JGP Addition to send Email Template when the deal registration status has changed to Deal Registration Pending Submission
        Map<Id,Opportunity> opportunities = new Map<Id,Opportunity>();
        //List to hold the Email Template
        List<EmailTemplate> em = new List<EmailTemplate>();
        for (Quote q : trigger.new)
        {
            //Insert Task for pending approval - auto complete when it changes          
            if(trigger.oldMap.get(q.id).Deal_Registration_Status__c != 'Deal Registration Submitted, Pending Approval'
            && q.Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval')
            {
                //If the previous status was not equal to Deal Registration Pending Submission, and it now does, send email
                if (q.OpportunityId != null && !opportunities.containsKey(q.OpportunityId))
                {
                    Opportunity opp = [SELECT
                    Opportunity.Id,
                    Opportunity.Deal_Reg_Desk_Rep__c,
                    Opportunity.Deal_Reg_Desk_Rep__r.Email
                    FROM Opportunity
                    WHERE Opportunity.Id =: q.OpportunityId];
                    opportunities.put(opp.Id,opp);
                }
                Opportunity workingOpportunity = opportunities.get(q.OpportunityId);
                //Retrieve Email Template
                if (em.isEmpty())
                {
                    em = [SELECT
                    EmailTemplate.Id
                    FROM EmailTemplate
                    WHERE EmailTemplate.DeveloperName = 'Deal_Registration_Pending_Submission'];
                }
                if (em.size() > 0)
                { 
                    Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
                    //mail.setTargetObjectId(opp.Deal_Reg_Desk_Rep__c);
                    //Create a temporary Contact to send email too. *Can't specify a user as recipent and a targetObject Id
                    Contact c = new Contact();
                    c.Email = workingOpportunity.Deal_Reg_Desk_Rep__r.Email;
                    c.LastName = 'Temp';
                    insert(c);
                    mail.setTargetObjectId(c.Id);               
                    mail.setSenderDisplayName('Salesforce Administrator');
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(q.Id);
                    mail.setTemplateId(em[0].Id);
                    try
                    {
                        system.debug('Sending Email to :'+c.Email);
                        Messaging.sendEmail(new Messaging.Singleemailmessage[] {mail});
                    }
                    catch (Exception e)
                    {
                        System.debug('Unable to send Email Message to recipient: '+mail.getTargetObjectId()+' Error:' +e.getMessage());
                    }
                    delete(c);
                }
            }
        }
    }
    /****************************************************************************************************
    *Description:   QuoteSalesRepLocation Update - Consolidated from outside trigger
    *               
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     5/6/2013    Justin Padilla      Initial Call - Within Consolidated Trigger And ensure it only runs once
    *****************************************************************************************************/
    /*
    if (trigger.isBefore && Utilities.SalesRepLocationFirstRun())
    {
        Utilities.SalesRepLocationFirstRunCompleted();
        QuoteSalesRepLocationUpdate.Check(trigger.new);
    }
    */