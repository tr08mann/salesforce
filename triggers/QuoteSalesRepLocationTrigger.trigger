trigger QuoteSalesRepLocationTrigger on Quote (before insert) {
 /****************************************************************************************************
    *Description:  Fills Sales Rep Location with matching location in opp owner user record when quote created
    *      
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     5/05/2014    Alex Schach         Initial Version 
    *   1.5     5/13/2014    Alex Schach         Made trigger bulk friendly, added override ability   
    *   2.0     6/06/2014    Alex Schach         Removed override ability, run only on insert
    *****************************************************************************************************/  
    opportunity opp = new opportunity();
    set<id> oppids = new set<id>();
     
    for(quote q:trigger.new)
    {
        if(q.opportunityid != null) 
        {
            oppids.add(q.opportunityid);
        }
    }
    if(!oppids.isempty())
    {
        map<id,opportunity> oppmap = new map<id,opportunity>([select owner.sales_rep_location_ID__c from opportunity where id in:oppids]);
        system.debug('oppmap: '+oppmap);
    
    for(quote q:trigger.new)
    { 
        opp = oppmap.get(q.opportunityid);
        if(q.opportunityid != null)
        {
            if(trigger.isinsert)
            {
                if(opp.owner.Sales_Rep_Location_ID__c != null)
                {
                    q.Sales_Rep_Location__c = opp.owner.Sales_Rep_Location_ID__c;
                    system.debug('quote location: '+q.sales_rep_location__c);
                    system.debug('owner location: '+opp.owner.Sales_Rep_Location_ID__c);
                }
                else
                {
                    q.Sales_Rep_Location__c = null;
                }
            }
            /*else
            {
                if(opp.owner.Sales_Rep_Location_ID__c != null)
                {
                    if(opp.owner.Sales_Rep_Location_ID__c != q.Sales_Rep_Location__c)
                    {
                        q.Sales_Rep_Location__c = opp.owner.Sales_Rep_Location_ID__c;
                        system.debug('quote location: '+q.sales_rep_location__c);
                        system.debug('owner location: '+opp.owner.Sales_Rep_Location_ID__c);
                    }
                }
                else
                {
                    q.Sales_Rep_Location__c = null;
                }  
            }*/
        }
    }
    }
}