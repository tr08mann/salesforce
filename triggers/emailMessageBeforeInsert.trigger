trigger emailMessageBeforeInsert on EmailMessage (before insert) 
{
	List<EmailMessage> MessagesToProcess = new List<EmailMessage>();
	List<Id> TaskIdsToProcess = new List<Id>(); 
	
	for (EmailMessage NewMessage : trigger.new) 
		if (NewMessage.Incoming)
		{
			MessagesToProcess.add(NewMessage);
			TaskIdsToProcess.add(NewMessage.ActivityId);
		}

	Map<Id,Task> TasksToUpdate = 
		new Map<Id,Task>
		([
			select
				Id, Description
			from
				Task
			where
				Id in :TaskIdsToProcess
		]);
		
	for (EmailMessage NewMessage : MessagesToProcess)
	{
		for (Task TaskToUpdate : TasksToUpdate.values())
		{
			if (TaskToUpdate.Id == NewMessage.ActivityId)
			{
				TaskToUpdate.Description = NewMessage.TextBody;
				break;
			}
		}
	}
	
	update TasksToUpdate.values();
}