trigger quoteAfterInsertUpdate on SFDC_520_Quote__c (after insert, after update) {

	//This maps the primary quotes to their opp
	List<id> opps = new List<id>();
	List<id> primary = new List<id>();
	
	for (SFDC_520_QUOTE__C q : Trigger.New) {
		if (q.Primary__c == true) {
			primary.add(q.id);
			opps.add(q.opportunity__c);
		}
	}
	
	List<SFDC_520_QUOTE__C> updates = new List<SFDC_520_QUOTE__C>();
	
	for (SFDC_520_QUOTE__c q : [select id, primary__c from SFDC_520_QUOTE__c where primary__C = true and opportunity__c in :opps and id not in :primary]) {
			q.primary__c = false;
			updates.add(q);
	}
	
	if (updates.size() > 0) {
		update updates;	
	}
}