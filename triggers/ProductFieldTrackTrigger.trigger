trigger ProductFieldTrackTrigger on Product2 (after update) {
    
    list<Product_Field_Tracking__c> proFieldTrackList = new list<Product_Field_Tracking__c>();

    for(Product2 proObj : trigger.new ) {       
        if(proObj.Frontline_Approved__c != trigger.oldMap.get(proObj.Id).Frontline_Approved__c) {
            Product_Field_Tracking__c trackObj = new Product_Field_Tracking__c();
            trackObj.Product__c = proObj.Id;
            trackObj.Date_changed__c = proObj.LastModifiedDate;
            trackObj.After_Value__c = String.valueOf(proObj.Frontline_Approved__c);
            trackObj.Before_Value__c = String.valueOf(trigger.oldMap.get(proObj.Id).Frontline_Approved__c);
            trackObj.User__c = proObj.LastModifiedById;
            trackObj.Field_Change__c = 'Frontline Approved';
            
            proFieldTrackList.add(trackObj);
        }
    if(proObj.Frontline__c != trigger.oldMap.get(proObj.Id).Frontline__c) {
            Product_Field_Tracking__c trackObj = new Product_Field_Tracking__c();
            trackObj.Product__c = proObj.Id;
            trackObj.Date_changed__c = proObj.LastModifiedDate;
            trackObj.After_Value__c = String.valueOf(proObj.Frontline__c);
            trackObj.Before_Value__c = String.valueOf(trigger.oldMap.get(proObj.Id).Frontline__c);
            trackObj.User__c = proObj.LastModifiedById;
            trackObj.Field_Change__c = 'Frontline';
            
            proFieldTrackList.add(trackObj);
        }
    
    }
    
    try {
        insert proFieldTrackList;
    } catch(Exception e){
        trigger.new[0].addError(e.getMessage());    
    }
}