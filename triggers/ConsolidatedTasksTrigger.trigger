trigger ConsolidatedTasksTrigger on Task (before insert, before update, after insert, after update, before delete) {
//if (trigger.isBefore && !ExecuteNeeded.OnTrigger('All QuoteTriggers Before')) return;
//else if (trigger.isAfter && !ExecuteNeeded.OnTrigger('All QuoteTriggers After')) return;
/********************** Old Quote Object Functions *******************************************************************/
    //Re-assign the task to the Opportunity from the Quote, now assigned to the deal registration rep if one is present
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
    {
        List<Task> tlist = new List<Task>();
        Set<Id> qids = new Set<Id>();
        //For Each Task, if the What Id is populated and the What Id is from the old Quote Object
        for(Task t : trigger.new){
            if(t.WhatId != null && String.valueOf(t.WhatId).startswith('a05')){ //a05 is the Id of the Old Custom Quote Object
                system.debug(t);
                tlist.add(t);
                qids.add(t.WhatId);
            }
        }
        if (!qids.isEmpty()) //Only Query the old Quote Objects if needed
        {
            Map<Id, SFDC_520_Quote__c> qMap = new Map<Id, SFDC_520_Quote__c>([Select Id, Name, Opportunity__c from SFDC_520_Quote__c where Id IN :qids]);
            for(Task t : tlist){
                SFDC_520_Quote__c q = qMap.get(t.WhatId);
                t.WhatId = q.Opportunity__c;
                t.Subject += ' ' + q.Name;
            }
        }
    }   
/*********************** Update Task.Opportunity_Owner__c with the Opportunity.Opportunity_Number if belonging to Opp */ 
    //Task Related Opportunity - Places the Opportunity Name on the task for viewing from the home page
    // Goal: Find the opp ID of the opportunity that a task is related to
    // and update the Opportunity_Number field on the task object with the value
    
    // If related to an Opportunity, query to find out the Opportunity number   
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
    {
        Set<Id> opIds = new Set<Id>();
        List<Task> toUpdateTask = new List<Task>();
        for(Task t : trigger.new)
        {
            String wId = t.WhatId;
            if(wId!=null && wId.startsWith('006') && !opIds.contains(t.WhatId)) //Task is related to an Opportunity
            {
                opIds.add(t.WhatId);
                toUpdateTask.add(t);
            }
        }
        if (!opIds.isEmpty())
        {
            // Pull in opp ids and related field to populate task record
            List<Opportunity> taskOps = [Select Id, Opportunity_Number__c from Opportunity where Id in :opIds];
            Map<Id, Opportunity> opMap = new Map<Id, Opportunity>();
            for(Opportunity o : taskOps)
            {
                opMap.put(o.Id,o);
            }           
            for (Task t:toUpdateTask)
            {
                Opportunity thisOp = opMap.get(t.WhatId);
                if(thisOp!=null)
                {
                    t.Opportunity_Owner__c = thisOp.Opportunity_Number__c ;
                } 
            }    
        }
    }
/********************** Create CCA TouchTasks *************************************************************************/    
    // moved code from CreateCCATouchTasks
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
    {
        List<Task> taskList = new List<Task>();
        List<Task> tasksToInsert = new List<Task>();
        List<Task> relatedTasks = new List<Task>();
        List<String> oppIds = new List<String>();
        Map<id, Opportunity> oppMap = new   Map<id, Opportunity>();
        String opp_prefix = Schema.SObjectType.Opportunity.getKeyPrefix();
        
        for(Task t1 : Trigger.new)
        {
            if (trigger.oldMap != null && trigger.oldMap.get(t1.Id) != null && t1.Activity_Type__c != trigger.oldMap.get(t1.Id).Activity_Type__c && t1.Activity_Type__c != null)
            {
                if(((t1.Subject.contains('CCA Initial Touch') || (t1.Subject.contains('CCA 2nd Touch')) ||(t1.Subject.contains('CCA 3rd Touch')) || (t1.Subject.contains('CCA 4th Touch')) 
                        || t1.Subject.contains('CCA 5th Touch') || t1.Subject.contains('AE/ISR Follow-up'))))
                {
                    if(t1.whatid != null)
                    {
                        if(string.valueOf(t1.WhatId).startsWith(opp_prefix))
                        {
                            taskList.add(t1);
                            oppIds.add(t1.whatId);
                            system.debug('Opportunity Added:' +t1.whatId);
                        }
                    }
                }
            }
        }
        system.debug('TaskList: '+taskList);
        system.debug('OppIds: '+oppIds);
        if(!taskList.IsEmpty())
        {
            System.debug('CreateCCATouchTasks - taskList -->' + taskList.Size());
            oppMap = new Map<id, Opportunity>([SELECT id, name, StageName, deadline__c, Customer_Care_Agent__c, Renewal_Do_Not_Call__c, Quote_Sent_Date__c, Region_Opp__c  FROM Opportunity WHERE id IN :oppIds]);
            System.debug('CreateCCATouchTasks - oppMap -->' + oppMap.Size());
            Date CCA_TRIGGER_BYPASSDATE = Date.newinstance(2013, 10, 09); 
            for(Task t: taskList)
            {
                Opportunity o = new Opportunity();
                if(oppMap.containskey(t.whatid))
                {
                    o = oppMap.get(t.whatid);
                    if (o.Renewal_Do_Not_Call__c)
                        continue;
                }
                else
                {
                    continue;
                }
                
                if(o.Deadline__c == null || o.StageName == 'Closed Won (100%)/SVC-Signed SOW'
                    // If Quote_Sent_Date__c < 9 October 2013, bypass automatic tasks.
                    || (o.Quote_Sent_Date__c != null && o.Quote_Sent_Date__c < CCA_TRIGGER_BYPASSDATE)
                    // WO#1525 change - if opportunity region is federal, do not create CCA touch tasks 
                    ||  (String.isNotEmpty(o.Region_Opp__c) && o.Region_Opp__c == 'Federal'))
                {
                    System.debug('CreateCCATouchTasks - opp from Map Due Date Null Cannot process -->');
                    continue;           
                }
                System.debug('CreateCCATouchTasks - opp from Map -->' + o.Name + ' renewal Date --' + o.Deadline__c);
                String subject = null;
                Date dueDate = Date.today();
                String lastResult = null;
                string initialResult = null;
                set<String> validActivityTypes = new Set<String>();
                validActivityTypes.add('Voicemail');
                validActivityTypes.add('Talk To');
                validActivityTypes.add('Initial Touch Reply');
                validActivityTypes.add('2nd/3rd Touch Reply');
                Boolean toProcess = false;
                for(String s:validActivityTypes)
                {
                    if(t.Activity_Type__c.contains(s))
                    {
                        toProcess = true;
                        break;
                    }               
                }
                if(toProcess)
                {
                    lastResult = t.Activity_Type__c;
                    system.debug('Task Closure Result: '+ lastResult);
                    if(t.Subject.contains('CCA Initial Touch'))
                    {    
                        system.debug('Processing Initial Touch');
                        subject = 'CCA 2nd Touch';  
                        if(t.Activity_Type__c == 'Talk To') 
                        {
                            dueDate = o.Deadline__c.addMonths(-1);
                        }
                        if(t.Activity_Type__c == 'Voicemail')
                        {
                            dueDate = Date.Today().addDays(7);
                        }
                        initialResult = t.Activity_Type__c;
                    }
                    else if(t.Subject.contains('CCA 2nd Touch'))
                    {   
                        system.debug('Processing 2nd Touch');
                        subject = 'CCA 3rd Touch';
                        initialResult = t.CCA_Initial_Touch__c; 
                        if(t.Activity_Type__c == 'Talk To')
                        {
                            if (t.CCA_Last_Result__c != null && t.CCA_Last_Result__c == 'Talk To')
                                dueDate = o.Deadline__c.addDays(-7);
                            else
                                dueDate = o.Deadline__c.addMonths(-1);
                        }
                        if(t.Activity_Type__c == 'Voicemail')
                        {
                            if (t.CCA_Last_Result__c != null && t.CCA_Last_Result__c == 'Talk To')
                                dueDate = o.Deadline__c.addDays(-14);
                            else
                                dueDate = o.Deadline__c.addMonths(-1);
                        }   
                        if(t.Activity_Type__c == 'Initial Touch Reply') 
                        {
                            dueDate = o.Deadline__c.addMonths(-1);
                        }
                        if(t.Activity_Type__c == '2nd/3rd Touch Reply')
                        {
                            dueDate = o.Deadline__c.addDays(-7);   
                        }
                    } 
                    else if(t.Subject.contains('CCA 3rd Touch'))
                    {
                        system.debug('Processing 3rd Touch');
                        subject = 'CCA 4th Touch';
                        initialResult = t.CCA_Initial_Touch__c; 
                        if(t.Activity_Type__c == 'Talk To') 
                        {
                            if (t.CCA_Last_Result__c != null && t.CCA_Last_Result__c == 'Talk To')
                            {
                                if (t.CCA_Initial_Touch__c != null && t.CCA_Initial_Touch__c == 'Talk To')
                                    dueDate = o.Deadline__c;
                                else
                                    dueDate = o.Deadline__c.addDays(-7);
                            }
                            else
                            {
                                if (t.CCA_Initial_Touch__c != null && t.CCA_Initial_Touch__c == 'Talk To')
                                    dueDate = o.Deadline__c;
                                else
                                    dueDate = o.Deadline__c.addDays(-7);
                            }
                        }
                        if(t.Activity_Type__c == 'Voicemail')
                        {
                            if (t.CCA_Last_Result__c != null && t.CCA_Last_Result__c == 'Talk To')
                            {
                                if (t.CCA_Initial_Touch__c != null && t.CCA_Initial_Touch__c == 'Talk To')
                                    dueDate = o.Deadline__c;
                                else
                                    dueDate = o.Deadline__c.addDays(-14);
                            }
                            else
                            {
                                if (t.CCA_Initial_Touch__c != null && t.CCA_Initial_Touch__c == 'Talk To')
                                    dueDate = o.Deadline__c;
                                else
                                    dueDate = o.Deadline__c.addDays(-14);
                            }
                        }               
                        if(t.Activity_Type__c == '2nd/3rd Touch Reply')
                        {
                            dueDate = o.Deadline__c.addDays(-7);
                        }
                                        
                    }
                    else if(t.Subject.contains('CCA 4th Touch'))
                    {
                        system.debug('Processing 4th Touch');
                        subject = 'CCA 5th Touch';
                        initialResult = t.CCA_Initial_Touch__c;
                        if(t.Activity_Type__c == 'Talk To')
                        {
                            dueDate = o.Deadline__c;
                        }
                        if(t.Activity_Type__c == 'Voicemail')
                        {
                            dueDate = o.Deadline__c;    
                      }
                        if(t.Activity_Type__c == '2nd/3rd Touch Reply')
                        {
                            dueDate = o.Deadline__c.addDays(-7);  
                        }
                    }
                    else if(t.Subject.contains('CCA 5th Touch'))
                    { 
                        system.debug('Processing 5th Touch');
                        initialResult = t.CCA_Initial_Touch__c;
                        subject = 'AE/ISR Follow-up'; 
                        if(t.Activity_Type__c == 'Voicemail' || t.Activity_Type__c == 'Talk To')
                        {
                            dueDate = o.Deadline__c; 
                        }
                    }             
                }
                if(subject <> null)
                {
                    Task t2 = new Task();
                    t2.Subject = subject;
                    if (dueDate == o.Deadline__c)
                    {
                        t2.Subject = 'AE/ISR Follow-up';
                    }
                    else
                    {
                        //Avoid weekends if not the Renewal Deadline Date
                        if(dueDate.daysBetween(dueDate.toStartOfWeek()) == -6) //Saturday
                        {
                            dueDate = dueDate.addDays(-1);
                        }
                        else if(dueDate.daysBetween(dueDate.toStartOfWeek()) == 0) //Sunday
                        {
                            dueDate = dueDate.addDays(1);
                        }
                    }
                    t2.Subject += ' - '+o.Name;
                    t2.OwnerId = o.Customer_Care_Agent__c;
                    t2.WhatId = o.id;
                    t2.ActivityDate = dueDate;
                    t2.CCA_Last_Result__c = lastResult;
                    t2.CCA_Initial_Touch__c = initialResult;
                    tasksToInsert.add(t2);
                    relatedTasks.add(t);
                }
                t.Status = 'Completed';
                t.CCA_Activity_Type__c = t.Activity_Type__c;             
          }
        }
    if(!tasksToInsert.IsEmpty())
        {
      System.debug(' CreateCCATouchTasks *** Task To Insert  *** ---> ' + tasksToInsert.Size()); 
            try
            {
                Database.SaveResult[] lsr = Database.insert(tasksToInsert, false);
                integer i=0;
        for(Database.SaveResult sr :lsr)
                {
          if(!sr.isSuccess())
          {
            Database.Error err = sr.getErrors()[0];
                        if(err.getMessage() == 'Assigned To ID: owner cannot be blank') 
                        {
                            relatedTasks[i].addError('CreateCCATouchTasks insert  Error: Customer Care Agent on Opportunity should not be blank');    
                        } 
            else 
                        {
              relatedTasks[i].addError('CreateCCATouchTasks insert  Error:' + err.getMessage());
                        }
                        System.debug('-->***** CreateCCATouchTasks insert  Error: ' + err.getMessage());
                    }
                    else
                    {
                      System.debug('--> ***** CreateCCATouchTasks Entity  Id: ' + sr.getId());
                    }
                    i++;
                 }
            }
            catch(DmlException ex)
            {
              System.debug('-->***** CreateCCATouchTasks insert  error: ' + ex.getMessage());
            }        
        }
    }
/***************************************** Send Email to Account Owner and Task Owner ********************************/
    //Process sending Emails to the Lead Team Manager (Account Owner and Task Owner)
    
    if(trigger.isAfter && trigger.isInsert)
    {
        ConsolidatedTasks.SendEmailToLeadTeamManager(trigger.new);
    }
    
/***************************************** Task Updating of Account Information **************************************/ 
/*Updated 9/1/14 by Alex Schach to avoid SOQL errors received when multiple tasks were updated at once. Also updated to fix issue where checkboxes were improperly checking/to avoid future potential issues*/   
    if(!trigger.isdelete && trigger.isafter)
    {
        set<id> LEADusers = new set<id>(); //list of IDs for users with LEAD team roles
        map<id, account> acctupdatemap = new map<id, account>(); //map of accounts to update for revenue tracking checkboxes
        map<id, account> acctcurrentmap = new map<id, account>(); //map of accounts as they currently exist
        map<id, user> ownermap = new map<id, user>(); //map of task owners
        map<id, Opportunity> oppmap = new map<id, opportunity>(); //map of opportunities to accounts
        list<account> taskaccounts = new list<account>(); //list of accounts the tasks are on
        list<id> taskaccountids = new list<id>(); //list of the account IDs the tasks are on
        list<Opportunity> oppList = new list<opportunity>(); //list of opportunities on the accounts
        list<account> acctupdate = new list<account>(); //list of accounts to update for LEAD member/expiry
        list<Messaging.SingleEmailMessage> emailToSend = new list<Messaging.SingleEmailMessage>(); //list of emails to send for already tracking notification
        list<string> owneremails = new list<string>(); //list of task owner emails
        list<user> LEADuserlist = new list<user>(); //list of users with LEAD team roles
        list<task> LEADtasklist = new list<task>(); //list of tasks owned by a LEAD team member
    
        for (User u: [SELECT Id, email, profile.name, name FROM User WHERE (UserRole.Name = 'LEAD Team Agent' OR userrole.Name = 'LEAD Team Manager') and isactive = true])
        {
            LEADusers.add(u.id);
            LEADuserlist.add(u);
        }
        system.debug('leaduserlist: '+ leaduserlist);
        for(task t:trigger.new)
        {
            if(t.accountid != null && LEADusers.contains(t.ownerid))
            {
                LEADtasklist.add(t);
                taskaccountids.add(t.accountid);
                for(user u : LEADuserlist)
                {
                    if(u.id == t.ownerid || u.profile.name == 'LEAD Team Management User')
                    {
                        owneremails.add(u.email);
                    }
                    if(u.id == t.ownerid)
                    {
                        ownermap.put(t.id,u);
                    }
                }
            }
        }
        if(!taskaccountids.isempty())
        {
            taskaccounts = [SELECT Id, Name, Lead_Expiry__c, LeadTeamMember__c, Existing_Account__c, Lead_Campaign_Participant__c, LEAD_Call_Campaign__c, Lead_Campaign_Participant_Added__c, Lead_Team_Touched__c, Lead_Team_Contacted__c, Lead_Team_Penetrated__c, Acct_Task_LEAD_Campaign__c 
            				from Account where id in: taskaccountids];
            for(task ta:LEADtasklist)
            {
                for(account an:taskaccounts)
                {
                    if(ta.accountid == an.id)
                    {
                        acctcurrentmap.put(an.id, an);
                    }
                }
            }
            Date d1 = Date.Today().addMonths(-12);
            oppList = [SELECT Id, Name, accountid from Opportunity where AccountId IN: taskaccounts AND IsClosed = true AND IsWon = true AND CloseDate >=: d1];
            for(account at:taskaccounts)
            {
                for(opportunity o:opplist)
                {
                    if(o.accountid == at.id)
                    {
                        oppmap.put(at.id, o);
                    }
                }
            }
        }
        system.debug('oppmap: '+oppmap); system.debug('LEADtasklist: '+LEADtasklist); system.debug('acctcurrentmap: '+acctcurrentmap); system.debug('acctupdatemap: '+acctupdatemap); system.debug('taskaccounts: '+taskaccounts); system.debug('oppList' + oppList);
        if(!LEADtasklist.isEmpty())
        {
            //LEAD revenue tracking checkboxes
            for(task t:LEADtasklist)
            {
                account a = new account();
                if(null == acctupdatemap.get(t.accountid))
                {
                    a = acctcurrentmap.get(t.accountid);
                }
                else
                {
                    a = acctupdatemap.get(t.accountid);
                }
                system.debug('initial account a: ' +a);
                //LEAD Campaign Participant checkbox
                if(a.lead_campaign_participant__c == false)
                {
                    a.LEAD_Campaign_Participant__c = true;
                    a.LEAD_Campaign_Participant_Added__c = datetime.now();
                    system.debug('participant account a: ' +a);
                }
                //LEAD Team Touched checkbox
                if(((t.Subject.startswith('Call') && t.Activity_Type__c != 'Spoke') || t.Subject.startswith('Email')) && t.status == 'Completed' && a.LEAD_Team_Touched__c == false)
                {
                    a.LEAD_Team_Touched__c = true;
                    system.debug('touched account a: ' +a);
                }
                //LEAD Team Contacted checkbox
                if(((t.Subject.startswith('Call') && t.Activity_Type__c == 'Spoke') || t.Subject == 'LEAD Team Event Registration' || t.Subject == 'LEAD Team Campaign Activity') && t.status == 'Completed' && (a.LEAD_Team_Touched__c == false || a.LEAD_Team_Contacted__c == false))
                {
                    a.LEAD_Team_Touched__c = true;
                    a.LEAD_Team_Contacted__c = true;
                    system.debug('contacted account a: ' +a);
                }
                //LEAD Team Penetrated checkbox
                if((t.subject ==  'LEAD Team On-site Appointment' || t.Subject == 'LEAD Team On-site Evaluation' || t.Subject == 'LEAD Team Remote Appointment' || t.Subject == 'LEAD Team Appointment - PAT') && t.status == 'Completed' && (a.LEAD_Team_Touched__c == false || a.LEAD_Team_Contacted__c == false || a.LEAD_Team_Penetrated__c == false))
                {
                    a.LEAD_Team_Touched__c = true;
                    a.LEAD_Team_Contacted__c = true;
                    a.LEAD_Team_Penetrated__c = true;
                    system.debug('penetrated account a: ' +a);
                }
                system.debug('before update account a: ' +a);
                system.debug('lead expiry: '+a.lead_expiry__c);
                acctupdatemap.put(t.accountid, a);
            }
            //LEAD Team Expiry and LEAD Team Member fields
            for(task t:LEADtasklist)
            {
                if((t.Subject =='LEAD Team On-site Appointment' || t.Subject =='LEAD Team Appointment - PAT' || t.Subject =='LEAD Team Remote Appointment' || t.Subject =='LEAD Team On-site Evaluation') && t.status == 'Completed' && null == oppmap.get(t.accountid) && t.activitydate != null)
                {
                    for(account aw:taskaccounts)
                    {
                        if(aw.id == t.accountid && (aw.Lead_Expiry__c != null && aw.Lead_Expiry__c >= system.today()))
                        {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            String[] toAddresses = owneremails;
                            string accounturl = url.getsalesforcebaseurl().toExternalForm() + '/' + aw.id;
                            mail.setToAddresses(toAddresses);
                            mail.setSubject('Creation of Unexpected LEAD Team Task');
                            mail.setBccSender(false);
                            mail.setUseSignature(false);
                            mail.setPlainTextBody('\n' +'Account Name:' + aw.Name + '\n' + 'Account: '+ accounturl + '\n' +'Task Owner Name: '+ ownermap.get(t.id).name + '\n'+ '\n' + 'This Account is already being tracked for Lead Team Revenue. Account Lead Expiry and LEAD Team Member fields were not modified. Review and modify Lead Expiry (and optionally LEAD Team Member) manually, if necessary.');
                            emailtosend.add(mail);
                        }
                        else if(aw.id == t.accountid && (aw.Lead_Expiry__c == null || aw.Lead_Expiry__c < system.today()))
                        {
                            aw.Lead_Expiry__c = system.today().addmonths(+12);
                            aw.LEAD_Revenue_Tracking_Start_Date__c = system.today();
                            aw.leadteammember__c = ownermap.get(t.id).name;
                            aw.Acct_Task_LEAD_Campaign__c = t.LEAD_Campaign__c;
                            acctupdate.add(aw);
                            system.debug('set lead expiry: '+aw.lead_expiry__c);
                        }
                   }
                }
            }
            if(!acctupdate.isempty())
            {
                try
                {
                    update acctupdate;
                }
                catch(exception e)
                {
                    system.debug('Error updating accounts: '+e);
                }
            }
            if(!emailtosend.isempty())
            {
                try
                {
                    messaging.sendemail(emailtosend);
                }
                catch(exception e)
                {
                    system.debug('Error sending emails: ' +e);
                }
            }
            if(!acctupdatemap.isempty())
            {
                try
                {
                    update acctupdatemap.values();
                }
                catch(exception e)
                {
                    system.debug('Error updating accounts: '+e);
                }
            }
        }
    }
/************************************* Prevent Task Deletions *********************************************************/    
//On deletion:
//Prevent Deletion of Tasks by non System Administrators and users not associated with the Lead Team Management User Profile
    if (trigger.isbefore && trigger.isDelete)
    {
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProfileName = PROFILE[0].Name;
        system.debug('<<<<<<<<<' +MyProfileName);
        for(Task t1: Trigger.old)
        {
            system.debug('..........' + PROFILE);
            if((t1.AccountId != Null && (MyProfileName != 'System Administrator' && MyProfileName != 'LEAD Team Management User'))
                && t1.Status == 'Completed' 
                &&(t1.Subject =='LEAD Team On-site Appointment' 
                || t1.Subject =='LEAD Team Appointment - PAT'
                || t1.Subject =='LEAD Team Remote Appointment' 
                || t1.Subject =='LEAD Team On-site Evaluation'))
            {
                t1.addError('This task is not deletable');
            }
        } 
    }
}