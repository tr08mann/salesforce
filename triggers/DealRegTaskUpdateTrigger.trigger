trigger DealRegTaskUpdateTrigger on Opportunity (after update) {
/****************************************************************************************************
    *Description:   Deal Registration - Closes all open deal registration tasks on all deal registration records when opportunity
    *                is closed
    *
    *               Deal Registration - Reassigns all open tasks and deal registration records when the opportunity DRD rep changes   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/21/2014    Alex Schach         Initial version
    *   2.0     11/13/2014   Alex Schach         Updated to reduce SOQL queries and clean up unused elements
    ******************************************************************************************************/   
list<task> taskstoupdate = new list<task>();        //list of tasks that will be updated
list<deal_registration__c> oppdealreg = new list<deal_registration__c>();        //list of deal registrations from closed opps
list<deal_registration__c> newownerdeal = new list<deal_registration__c>();        //list of deal registrations that need to be reassigned
list<deal_registration__c> dealtoupdate = new list<deal_registration__c>();        //list of deal registrations that will be updated
set<id> closeoppids = new set<id>();        //hold opportunity id of opps that are closed
set<id> updateoppids = new set<id>();        //hold opportunity id of opps that have deal registrations to update
map<string,id> drusermap = new map<string,id>();   

    for(opportunity o : trigger.new)
    {
        if(trigger.newmap.get(o.id).isclosed && !trigger.oldmap.get(o.id).isclosed && o.type != 'Renewal Business')
        {
            if(!closeoppids.contains(o.id))
            {
                closeoppids.add(o.id);
            }
        }
        else if(trigger.newmap.get(o.id).deal_reg_desk_rep__c !=null &&(trigger.newmap.get(o.id).deal_reg_desk_rep__c != trigger.oldmap.get(o.id).deal_reg_desk_rep__c) && o.type != 'Renewal Business')
        {
            if(null==drusermap.get('Old'+o.id))
            {
                drusermap.put('Old'+o.id,trigger.oldmap.get(o.id).deal_reg_desk_rep__c);
            }
            if(null==drusermap.get('New'+o.id))
            {
                drusermap.put('New'+o.id,trigger.newmap.get(o.id).deal_reg_desk_rep__c);
            }
            system.debug('drusermap: '+ drusermap);
            if(!updateoppids.contains(o.id))
            {
                updateoppids.add(o.id);
            }
        }
    }
    system.debug('closeoppids: '+closeoppids);
    system.debug('updateoppids: ' + updateoppids);
    if(!closeoppids.isempty())
    {
        oppdealreg = [select id, (select id from tasks where status != 'Completed') from deal_registration__c where opportunity__r.id in:closeoppids];
        system.debug('oppdealreg: '+oppdealreg);
        if(!oppdealreg.isempty())
        {
            for(deal_registration__c d : oppdealreg)
            {
                for(task t : d.tasks)
                {
                    t.status = 'Completed';
                    taskstoupdate.add(t);
                }
            system.debug('taskstoupdate: '+taskstoupdate);
            }
        }
    }
    if(!updateoppids.isempty())
    {
        newownerdeal = [select id, ownerid, opportunity__r.id, (select id, whatid, ownerid from tasks) from deal_registration__c where opportunity__r.id in: updateoppids];
        if(!newownerdeal.isempty())
        {
            for(deal_registration__c d : newownerdeal)
            {
                if(d.ownerid != drusermap.get('New'+d.opportunity__r.id))
                {
                    d.ownerid = drusermap.get('New'+d.opportunity__r.id);
                    dealtoupdate.add(d);
                }
                for(task t : d.tasks)
                {
                    if(t.ownerid != drusermap.get('New'+d.opportunity__r.id))
                    {
                        t.ownerid = drusermap.get('New'+d.opportunity__r.id);
                        taskstoupdate.add(t);
                    }
                }
            }
        }
    }
    if(!taskstoupdate.isempty())
    {
        update taskstoupdate;
    }
    if(!dealtoupdate.isempty())
    {
        update dealtoupdate;
    }
}