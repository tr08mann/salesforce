trigger ArchitectPopulateTrigger on Opportunity (before update){
/****************************************************************************************************
    *Description: Populates the IAM Architect field accordingly:    Eriks Richters when opportunity products Vendors contain Ping Identity &/or CA (Computer Associates) Technologies
                                                                    Taylor Hook when opportunity products Vendors contain Radiant Logic &/or Sailpoint
                  Populates the Mobile Architect field accordingly: when opportunity products contain Mobility SKU = TRUE and
                                                                    Pat Patterson when Region is East
                                                                    Jonathan Ross when Region is West
                                                                    Mike Soto when Region is Central
                  Clears the IAM and Mobile architect fields when none of the above criteria is met.
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     04/10/2014   Alex Schach          Case 101233, Case 101647
    *   2.0     05/16/2014   Alex Schach          updated trigger to run only before update, and only use one SOQL call
    *   2.5     05/27/2014   Alex Schach          updated to reference new vendor lookup field on product
    *   3.0     08/29/2014   Alex Schach          per Buck Bell and Bryan Barkhaus, deactivated the IAM architect population, case 105151. left code in case decision reversed
   **************************************************************************************************/
Set<Id> oppIds = new Set<Id>(); //current opp
set<Id> validOpportunities = new Set<Id>(); //list of opps to update for IAM
set<id> mobileopps = new set<id>(); //list of opps to update for Mobile
Map<string,ID> usersIAMarchitect = new Map<string,ID>{'Eriks'=>'00560000001x09x','Taylor'=>'00560000001wMi4'}; //map of IAM architects
map<string,ID> usersMobileArchitect = new map<string,id>{'Patterson' => '00560000001uR04','Ross' => '00560000001xxKO','Soto'=> '00560000001x67E'}; //map of Mobile architects
boolean erikstrue = false;
boolean taylortrue = false;
List<OpportunityLineItem> oliseriks = new List<OpportunityLineItem>();
List<OpportunityLineItem> olistaylor = new List<OpportunityLineItem>();
List<opportunitylineitem> olismobile = new List<opportunitylineitem>();

for(Opportunity o:trigger.new)
{
    if(!oppids.contains(o.id))
    {
        oppids.add(o.id);
    }
}
system.debug('oppids: '+oppids);
if(!oppids.isEmpty())
{ 
    map<id,opportunitylineitem> olimap = new map<id,opportunitylineitem>([select id, pricebookentry.product2.family, opportunityid, pricebookentry.product2.product_vendor_profile__r.name, pricebookentry.product2.mobility_sku__c
                                                                          from opportunitylineitem where OpportunityLineItem.OpportunityId IN: oppIds AND (OpportunityLineItem.pricebookentry.product2.product_vendor_profile__r.name = 'CA Technologies' 
                                                                          OR OpportunityLineItem.pricebookentry.product2.product_vendor_profile__r.name = 'Ping Identity' OR OpportunityLineItem.pricebookentry.product2.product_vendor_profile__r.name = 'Radiant Logic' 
                                                                          OR OpportunityLineItem.pricebookentry.product2.product_vendor_profile__r.name = 'SailPoint Technologies' OR opportunitylineitem.pricebookentry.product2.mobility_sku__c = TRUE)]);
    if(!olimap.isempty())
    {
        for(opportunitylineitem oli : olimap.values())
        {
            /*if(oli.pricebookentry.product2.product_vendor_profile__r.name == 'CA Technologies' || oli.pricebookentry.product2.product_vendor_profile__r.name == 'Ping Identity')
            {
                oliseriks.add(oli);
            }
            if(oli.pricebookentry.product2.product_vendor_profile__r.name == 'Radiant Logic' || oli.pricebookentry.product2.product_vendor_profile__r.name == 'SailPoint Technologies')
            {
                olistaylor.add(oli);
            }*/
            if(oli.pricebookentry.product2.mobility_sku__c == TRUE)
            {
                olismobile.add(oli);
            }
        }
    }
    system.debug('oliseriks: '+oliseriks);
    system.debug('olistaylor: '+olistaylor);
    system.debug('olismobile: '+olismobile);
    /*if(!oliseriks.isEmpty())
    {
        erikstrue = true;
        for(OpportunityLineItem oli: oliseriks)
        {
            if(!validOpportunities.contains(oli.OpportunityId))
            {
                validOpportunities.add(oli.OpportunityId);
            }
        }
    }
    if(!olistaylor.isEmpty())
    {
        taylortrue = true;
        for (OpportunityLineItem oli: olistaylor)
        {
            if (!validOpportunities.contains(oli.OpportunityId))
            {
                validOpportunities.add(oli.OpportunityId);
            }
        }
    }*/
    if(!olismobile.isempty())
    {
        for(opportunitylineitem oli :olismobile)
        {
            if(!mobileopps.contains(oli.opportunityid))
            {
                mobileopps.add(oli.opportunityid);
            }
        }
    }
}
//Now that we have them mapped out, change the architects
for (Opportunity o:trigger.new)
{
    /*if(!validOpportunities.contains(o.Id))
    {
        o.IAM_Architect__c = null;
    }
    else if(erikstrue == true)
    {
        o.IAM_architect__c = usersIAMArchitect.get('Eriks');
    }
    else if(taylortrue == true)
    {
        o.IAM_architect__c = usersIAMArchitect.get('Taylor');
    }*/
    if(!mobileopps.contains(o.id))
    {
        o.Mobile_Advisor__c = null;
    }
    else if(o.Region_Opp__c == 'East')
    {
        o.Mobile_Advisor__c = usersMobileArchitect.get('Patterson');
    }
    else if(o.Region_Opp__c == 'West')
    {
        o.Mobile_Advisor__c = usersMobileArchitect.get('Ross');
    }
    else if(o.Region_Opp__c == 'Central')
    {
        o.Mobile_Advisor__c = usersMobileArchitect.get('Soto');
    }
    else
    {
        o.mobile_advisor__c = null;
    }
}
}