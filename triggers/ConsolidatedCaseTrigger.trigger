trigger ConsolidatedCaseTrigger on Case (after update, after insert) 
{
 /****************************************************************************************************
    *Description:   Creates and updates tasks when the "Create Follow-up Task" box is checked
    *                
    *               Closes open tasks when case is closed. Updates task due date when follow-up date is changed.
    *               
    *               Creates comment when next case step is changed or case is created.
    *
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/25/2014    Alex Schach         Initial Version
    *   2.0     4/28/2014    Alex Schach         Added auto-updating of task dates. Added next case step commenting.    
    *   3.0     11/25/2014   Alex Schach         Updated to have one query, bulk friendly
    *****************************************************************************************************/  
    set<id> caseids = new set<id>();
    list<task> taskstoupdate = new list<task>();
    list<task> taskstoinsert = new list<task>();
    list<case> stepchangecase = new list<case>();
    list<case> cases = new list<case>();
    list<casecomment> commentstoinsert = new list<casecomment>();
    
    for(case c :trigger.new)
    {
        caseids.add(c.id);
    }
    
    cases = [select id, ownerid,Case_Follow_up_Date__c,Create_Follow_up_Task__c,Next_Step_Case__c,casenumber,subject, (select id, whatid, status, activitydate, subject, whoid, isreminderset
               from tasks where status !=: 'Completed') from case where id in: caseids];
    
    for(case c:cases)
    {
        //complete all open tasks when case is closed
        if(trigger.isupdate && (trigger.newmap.get(c.id).status  == 'Closed' || trigger.newmap.get(c.id).status =='Cancelled' 
           || trigger.newmap.get(c.id).status =='Duplicate'))
        {
            for(task t:c.tasks)
            {
                t.status = 'Completed';
                taskstoupdate.add(t);
            }
        }
        //update task due date when case follow-up date is changed
        if(trigger.isupdate && trigger.newmap.get(c.id).Create_Follow_up_Task__c == TRUE 
        && (trigger.newmap.get(c.id).case_follow_up_date__c != trigger.oldmap.get(c.id).case_follow_up_date__c))
        {
            for(task t:c.tasks)
            {
                t.activitydate = trigger.newmap.get(c.id).case_follow_up_date__c;
                t.isreminderset = false;
                taskstoupdate.add(t);
            }
        }
        //insert task when create task box is checked and case is created or when box is checked for the first time
        if((trigger.isinsert && trigger.newmap.get(c.id).create_follow_up_task__c == true) || 
        (trigger.isupdate && (trigger.newmap.get(c.id).create_follow_up_task__c != trigger.oldmap.get(c.id).create_follow_up_task__c) && trigger.newmap.get(c.id).create_follow_up_task__c == true))
        {
            task newtask = new task();
            string subject = 'Case Follow-up: '+c.subject;
            newtask.whatid = c.id;
            newtask.activitydate = c.case_follow_up_date__c;
            newtask.ownerid = c.ownerid;
            newtask.subject = subject;
            newtask.isreminderset = false;
            taskstoinsert.add(newtask);
        }
        //create comment when next case step is changed
        if(trigger.isupdate && (trigger.oldmap.get(c.id).next_step_case__c != trigger.newmap.get(c.id).Next_Step_Case__c))
        {
            stepchangecase.add(c);
        }
        if(trigger.isinsert && (c.next_step_case__c != null || c.next_step_case__c != ''))
        {
            stepchangecase.add(c);
        }
        if(!stepchangecase.isempty())
        {
            system.debug('stepchangecase: '+stepchangecase);
            casecomment cc = new casecomment();
            cc.commentbody = c.next_step_case__c;
            cc.parentid = c.id;
            cc.ispublished = true;
            commentstoinsert.add(cc);
        }
    }
    if(!commentstoinsert.isempty())
    {
        try
        {
            insert commentstoinsert;
        }
        catch (exception ex)
        {
            system.debug('Unable to insert comment.....'+ex.getmessage());
        } 
    } 
    if(!taskstoupdate.isempty())
    {
        try
        {
            update taskstoupdate;
        }
        catch (exception ex)
        {
            system.debug('Unable to update task.....'+ex.getmessage());
        } 
    }
    if(!taskstoinsert.isempty())
    {
        try
        {
            insert taskstoinsert;
        }
        catch (exception ex)
        {
            system.debug('Unable to insert task.....'+ex.getmessage());
        }         
    }
}