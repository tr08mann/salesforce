trigger stopDelete on Contact (before delete) {
Id[] ids = new Id[]{};
Map<Id, Contact> cmap = new Map<Id, Contact>();
for(Contact c : trigger.old){
	ids.add(c.id);
	cmap.put(c.id, c);	
}
Certification__c[] vlist = [Select Id, Student_Name_2__c from Certification__c where Student_Name_2__c IN :ids];
TIMBASURVEYS__Survey_Summary__c[] slist = [Select Id, TIMBASURVEYS__Contact__c FROM TIMBASURVEYS__Survey_Summary__c WHERE TIMBASURVEYS__Contact__c IN :ids];
if(!slist.isEmpty()){
	string errorpart = ' cannot be deleted as it has a related Survey Summary record';
for(TIMBASURVEYS__Survey_Summary__c s : slist){
	Contact c = cmap.get(s.TIMBASURVEYS__Contact__c);
	string error = c.LastName + errorpart;
	c.adderror(error);
}
}
if(!vlist.isEmpty()){
	string errorpart2 = ' cannot be deleted as it has a related Vendor Certification record';
	for(Certification__c v : vlist){
		Contact c2 = cmap.get(v.Student_Name_2__c);
		string error2 = c2.LastName + errorpart2;
		c2.adderror(error2);
	}
}
}