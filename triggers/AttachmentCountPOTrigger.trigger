trigger AttachmentCountPOTrigger on Attachment (after insert, after delete) {
    /****************************************************************************************************
    *Description:   Counts the number of attachments on the purchase order and updates the field
    *                Number of Attachments
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/28/2014    Alex Schach         Initial Version - Case 101336
    *****************************************************************************************************/  
integer attachmentcount = 0;
id pid = null;
id dpid = null;
string poid = '';
string dpoid = '';
purchase_order__c po = new purchase_order__c();
    
If(trigger.isinsert)
{
    for(attachment a :trigger.new)
    {
        pid = a.parentid;
        system.debug('pid: '+pid);
        
    }
    if(pid!= null)
    {
        attachmentcount = [select count() from attachment where parentid =:pid AND name LIKE 'Deliverable%'];
        system.debug('attachment count: ' +attachmentcount);
        poid = string.valueof(pid);
        if(poid.startsWith('a02'))
        {
            po.id = pid;
            po.Number_of_Attachments__c = attachmentcount;
            update po; 
        }
        
    }
}
if(trigger.isdelete)
{
    for(attachment b: trigger.old)
    {
        dpid = b.parentid;
        system.debug('dpid: '+dpid);
    }
    if(dpid!=null)
    {
        attachmentcount = [select count() from attachment where parentid =:dpid AND name LIKE 'Deliverable%'];
        system.debug('delete attachment count: '+attachmentcount);
        dpoid = string.valueof(dpid);
        if(dpoid.startsWith('a02'))
        {
            po.id = dpid;
            po.Number_of_Attachments__c = attachmentcount;
            update po; 
        }
    }
}
}