trigger ConsolidatedOpportunityTrigger on Opportunity (after update, before insert, before update) {
if (TriggerStopper.stopOpp || TriggerStopper.stopInvoiceRollup)return; //|| !ExecuteNeeded.OnTrigger('All OpporunityTriggers')

    //Close All Tasks on Opportunity Closure
    //Updated 8/25/14 by Alex Schach - combined two task close locations into one, optimized code
    if (trigger.isAfter && trigger.isUpdate)
    { 
        List<id> closedoppids = new List<id>();
        List<Task> tasktocloselist = new List<Task>();
         
        for (Opportunity opp : Trigger.new) 
        {
            if(opp.isClosed == true && trigger.oldmap.get(opp.id).isClosed != true)
            {
                  closedoppids.add(opp.Id);
            }
        }
        if(!closedoppids.isEmpty())
        {
            System.debug('closedoppids.... ::'+closedoppids); 
            for(Task t : [Select Status, Id from Task where WhatId IN :closedoppids and isClosed = false])
            {
                t.status = 'Completed';
                tasktocloselist.add(t);
            }
            System.debug('tasktocloselist size: '+tasktocloselist.size()+' tasktocloselist: '+tasktocloselist);
            try
            {
                update tasktocloselist;
                system.debug('Closed Task List ::' + tasktocloselist);
            }
            catch (Exception ex){
                System.debug('Unable to update tasks....'+ex.getmessage());
            }   
        }
    }
    
    /*OpportunityAfterUpdate trigger*/
    /* stops users from closing an opp except through the mark closed won button when there are products on the opp */
    if (trigger.isAfter)
    {
        List<Opportunity> TriggerNewNoVF = new List<Opportunity>();
        for(Opportunity TriggerOpportunity : trigger.new)
        {
            if(!TriggerOpportunity.Closed_via_VF_Page__c && TriggerOpportunity.IsWon && !trigger.oldmap.get(triggeropportunity.id).iswon) 
            {
                if(triggeropportunity.Number_of_Line_Items__c != 0)
                {
                    triggeropportunity.StageName.addError('This Opportunity contains products requiring renewal.  To mark this item as Closed/Won, you must select \'Mark Closed Won\' on the Opportunity detail page.');
                }
            }            
        }
    }
    
    /****************************************************************************************************
    *Description:   Before Insert/Update items:
    *               -SE/CCA populate
    *               -Accounting/purchasing notes change date
    *               -something to do with the convert company button on leads
    *               -LEAD Team Member populate and MSS Contract trigger
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     8/25/2014   Alex Schach         Reorganized/optimized trigger to work better/reduce queries/DML updates
    *****************************************************************************************************/
    if (trigger.isBefore)
    {
        set<id> owneruserids = new set<id>();
        Map<id, user> ownerusermap = new Map<id, user>();
        Map<String,User> newUsers = new Map<String,User>();
        list<opportunity> populateopps = new list<opportunity>();
        list<String> SEuserListName = new list<String>();
        Map<String,User> SEUserMap = new Map<String,User>();
        /*Sales Engineer/CCA Populate*/
        for(Opportunity o : Trigger.New) 
        {
            Opportunity oldopp = null;
            if(Trigger.IsUpdate)
            {
                oldopp = Trigger.oldMap.get(o.id);                
            }                 
            if(o.Sales_Engineer_NEW__c == null || (oldopp != null && oldopp.ownerid != o.ownerid))// WO 1596 :: agupta@rainmaker-llc.com replaced the Old Picklist with the Look up corresponding to it
            {                
                owneruserids.add(o.ownerid); //JGP 3/22/2013 only add if processing is needed
                populateopps.add(o);
            }
            if((o.Customer_Care_Agent__c == null || (oldopp != null && oldopp.ownerid != o.ownerid)) && o.type == 'Renewal Business')
            {
                owneruserids.add(o.ownerid);
                populateopps.add(o);
            }
            if(o.type != 'Renewal Business')
            {
                o.Customer_Care_Agent__c = null;
            }
        }
        if(!owneruserids.isempty())
        {
            ownerusermap = new Map<id, user>([select id, name, ProfileId, UserRoleId, Profile.Name, UserRole.Name, Opportunity_Sales_Engineer__c, CCA_Rep__c from user where id in :owneruserids]);     
            for(Schema.PicklistEntry a:Schema.getGlobalDescribe().get('User').getDescribe().fields.getMap().get('Opportunity_Sales_Engineer__c').getDescribe().getPickListValues())
            {
                SEuserListName.add(a.getValue()); 
            }
            for(User u1 : [SELECT Id,Name FROM User WHERE Name IN: SEuserListName])
            {
                SEUserMap.put(u1.Name,u1);
            }
            System.debug('SEUserMap ::'+SEUserMap);
        }
        /*If Sales Engineer/CCA field is empty, look up from owner record*/
        for(Opportunity o : populateopps)
        {
            User oppowner = ownerusermap.get(o.ownerid);
            if(oppowner != null)
            {
                User SE = SEUserMap.get(oppowner.Opportunity_Sales_Engineer__c);
                if(SE != null)
                {
                    System.debug('SE ID ::'+SE.Id);
                    o.Sales_Engineer_NEW__c = SE.Id;
                }
                if(SE == null)
                {
                    o.Sales_Engineer_NEW__c = null;
                }
                if(o.type == 'Renewal Business')
                {
                    o.Customer_Care_Agent__c = ownerusermap.get(o.ownerid).CCA_Rep__c; 
                }
            }
        }
        /*accounting notes change date/purchasing notes change date*/
        for(opportunity o:trigger.new)
        {
            Opportunity oldopp = null;
            if(Trigger.IsUpdate)
            {
                oldopp = Trigger.oldMap.get(o.id);                
            }   
            if ((oldopp != null && oldopp.Accounting_Notes__c != o.Accounting_Notes__c) || (oldopp == null && o.Accounting_Notes__c != null && o.Accounting_Notes__c != ''))
            {
                o.Accounting_Notes_Change_Date__c = System.Today();   
            } 
            //Misc notes is notes to purchasing
            if ((oldopp != null && oldopp.Misc__c != o.Misc__c) || (oldopp == null && o.Misc__c != null && o.Misc__c != ''))
            {
                o.Misc_Notes_Change_Date__c = System.Today();
            }            
        }
        /*something to do with the convert company button on leads*/
        if (!Trigger.IsInsert) 
        {
            if (convertCompanyLeadController.Me != null)
            {
                System.assert(trigger.new.size() == 1);
                convertCompanyLeadController.HandlePrimaryOpportunity(trigger.new[0]);
            }
        }
        /*LEAD Team Member populate and MSS Contract trigger*/
        if(trigger.isinsert)
        {
            list<opportunity> MSScontractopps = new list<opportunity>();
            set<id> leadmemberaccids = new set<id>();
            map<id, account> leadmemberaccmap = new map<id, account>();
            for(opportunity o:trigger.new)
            {
                if(o.LEAD_Team_Member__c == null)
                {
                    leadmemberaccids.add(o.accountid);
                }
                //JGP 9/5/2012 Modified the Class so that there's only 1 call to populate the Contracts
                //Process in Bulk
                //Only process Opportunities that have been 'Submitted to Professional Services'
                if(o.Pro_Services_Order_Status__c == 'Submitted to Professional Services')
                {
                    MSScontractopps.add(o);
                }
            }
            if(!leadmemberaccids.isEmpty())
            {
                leadmemberaccmap = new Map<Id,Account>([SELECT Id, Lead_Expiry__c, LeadTeamMember__c, Acct_Task_LEAD_Campaign__c 
                                                        FROM Account WHERE Id IN: leadmemberaccids]);
            }
            for(opportunity opp:trigger.new)
            {
                Account currentAccount = leadmemberaccmap.get(opp.AccountId);
                if (currentAccount != null && currentAccount.Lead_Expiry__c != null && currentAccount.Lead_Expiry__c > Date.today())
                {
                    opp.LEAD_Team_Member__c = currentAccount.LeadTeamMember__c;
                    opp.LEAD_Campaign__c = currentAccount.Acct_Task_LEAD_Campaign__c;
                }
            }
            if(!MSScontractopps.isEmpty())
            {
                OpportunityMSSContract.Process(MSScontractopps);
            }
        }
    }    
    
    /*CCA Initial Touch Task Generation*/
    if(trigger.isbefore)
    {
        List<Task> CCAInitialTouch = new List<Task>();
        for (Opportunity o:trigger.new)
        {
            if (!o.Renewal_Do_Not_Call__c && o.Amount <= 50000 && o.Type == 'Renewal Business' && o.amount != 0 &&
                 o.Probability >= 75 && !o.CCA_Initial_Touch_Created__c && o.Quote_Sent_Date__c != null && 
                String.isNotEmpty(o.Region_Opp__c) && o.Customer_Care_Agent__c != null && !o.isclosed) 
                // WO#1525 change - if opportunity region is federal, do not create CCA touch tasks 
                // WO#1637 change - if opportunity region is federal, do create CCA touch tasks 04/21/2014
                // modified criteria to exclude closed opps 10.21.14 AS
            {
                Task t = new Task();
                t.WhatId = o.Id;
                t.Subject = 'CCA Initial Touch - '+o.Name;
                t.OwnerId = o.Customer_Care_Agent__c;
                //Renewal_vs_Quote_Sent__c is a formula field - which is Deadline__c - Quote_Sent_Date__c
                if(o.Renewal_vs_Quote_Sent__c >= 75) //&& o.Renewal_vs_Quote_Sent__c <= 90
                {
                    t.ActivityDate = o.Quote_Sent_Date__c.addDays(14);
                }
                else if(o.Renewal_vs_Quote_Sent__c < 75)
                {
                    t.ActivityDate = o.Quote_Sent_Date__c.addDays(7);
                }
                //Avoid weekends
                if(t.ActivityDate.daysBetween(t.ActivityDate.toStartOfWeek()) == -6) //Saturday
                {
                    t.ActivityDate = t.ActivityDate + 1;
                }
                if(t.ActivityDate.daysBetween(t.ActivityDate.toStartOfWeek()) == 0) //Sunday
                {
                    t.ActivityDate = t.ActivityDate + 1;
                }
                o.CCA_Initial_Touch_Created__c = true;
                CCAInitialTouch.add(t); 
            }
        }
        if(!CCAInitialTouch.isEmpty())
        {
            insert(CCAInitialTouch);
        }
    }    
    
    /* UpdateOwnerSolomonUserIDField */
    if (trigger.isBefore)
    {
        Set<Id> setUserIds = new Set<Id>();
        Map<Id, User> mapUserObj = new Map<Id, User>();
        //create user map
        for(Opportunity oppObj : Trigger.new)
        {
            Opportunity oldopp = null;
            if(Trigger.IsUpdate)
            {
                oldopp = Trigger.oldMap.get(oppObj.id);                
            }   
            //only query if the owner has changed or if opp owner solomon id is blank
            if(oppObj.OwnerId != null && ((oldopp != null && (oldopp.ownerid != oppObj.ownerid || oppObj.Owner_Solomon_Userid__c == null)) || oldopp == null && oppObj.Owner_Solomon_Userid__c == null))
            {
                setUserIds.add(oppObj.OwnerId);
            }
        }
        if(!setUserids.isempty())
        {
            mapUserObj = new map<id, user>([select id, Solomon_UserID__c from User where id in: setUserIds limit 1000]);
        }
        //for each opportunity being inserted get User from the map and update the field values
        for(Opportunity oppObj : Trigger.new)
        {
            Opportunity oldopp = null;
            if(Trigger.IsUpdate)
            {
                oldopp = Trigger.oldMap.get(oppObj.id);                
            }   
            if(oppObj.OwnerId != null && ((oldopp != null && (oldopp.ownerid != oppObj.ownerid || oppObj.Owner_Solomon_Userid__c == null)) || oldopp == null && oppObj.Owner_Solomon_Userid__c == null))
            {
                if(mapUserObj.containsKey(oppObj.OwnerId))
                {
                    oppObj.Owner_Solomon_Userid__c = mapUserObj.get(oppObj.ownerid).Solomon_UserID__c;
                }  
            }
        }         
    }
    
    /*UpdateContactRoleBilling trigger*/
    /* combined billing and shipping role into one section */
    if (trigger.isBefore)
    {
        // Make list of Opp ids for query of OCR records
        Set<Id> OppIds = new Set<Id>();
        for(Opportunity o : Trigger.new)
        {
            OppIds.add(o.id);
        }
        // Map of Opp id, related list of OCR ids
        Map<Id, List<OpportunityContactRole>> Opp_OCR = new Map<Id, List<OpportunityContactRole>>();
        // find all related OCRs to build Map
        for (OpportunityContactRole ocr : [select id, contactid, Opportunityid, role, isprimary, createddate 
                                           from OpportunityContactRole where opportunityid in :OppIds 
                                           and (role = 'Billing Contact: Professional Services' OR role = 'End-User: Professional Services')])
        {        
            // look for Oppid in master map
            List<OpportunityContactRole> tmp_ocr = new List<OpportunityContactRole>();
            tmp_ocr = Opp_OCR.get(ocr.opportunityid);
            // if Oppid not already in map, add it with this new OCR record
            if(tmp_ocr == null) 
            {
                Opp_OCR.put(ocr.opportunityid, new List<OpportunityContactRole>{ocr});
            }  
            else 
            {
                // otherwise add this new OCR record to the existing list
                tmp_ocr.add(ocr);
                Opp_OCR.put(ocr.opportunityid, tmp_ocr);
            }
        }    
        system.debug('Final OCR map: '+Opp_OCR);
        
        // for each Opp modified in the trigger, try to find relevant contacts
        for (Opportunity opps : Trigger.new) 
        {
            // temporary list of OCRs for this Opportunity populated from the master map
            List<OpportunityContactRole> this_OCR = new List<OpportunityContactRole>();
            List<OpportunityContactRole> bill_OCR = new list<OpportunityContactRole>();
            List<OpportunityContactRole> ship_OCR = new list<OpportunityContactRole>();
            this_OCR = Opp_OCR.get(opps.id);
            system.debug('this Opps ('+opps.id+') list of OCRs: '+this_OCR);
            // if no OCRs related, null out contact field(s)
            if(this_OCR == null)
            {
                opps.Contact_Role_Bill_Name__c = null;
                opps.Contact_Role_Ship_Name__c = null;
            }
            else 
            {
                for (OpportunityContactRole r : this_OCR) 
                {
                    if(r.Role == 'Billing Contact: Professional Services')
                    {
                        bill_OCR.add(r);
                        opps.Contact_Role_Bill_Name__c = r.contactid;
                    }
                    if(r.Role == 'End-User: Professional Services')
                    {
                        ship_OCR.add(r);
                        opps.Contact_Role_Ship_Name__c = r.contactid;
                    }  
                } // end for loop
                if(bill_OCR.isEmpty())
                {
                    opps.Contact_Role_Bill_Name__c = null;
                }
                if(ship_OCR.isEmpty())
                {
                    opps.Contact_Role_Ship_Name__c = null;
                }
            }           
        }  // end for loop of Opps
    }
    
    /* SetAccountOwnerField trigger */
    /* used for stopClosedOppOwnerChange trigger */
    /* fills opp level AccountOwner__c and OppOwner__c fields */
    if (trigger.isBefore)
    {
        if(trigger.isInsert)
        {   
            list<id> accids = new list<Id>();
            for(Opportunity o : trigger.new)
            {
                if(o.AccountId != null)
                {
                    accids.add(o.AccountId);
                }
            }
            if(!accids.isEmpty())
            {
                Map<Id, Account> aMap = new Map<Id, Account>([Select Id, OwnerId from Account where Id IN :accids]);
                for(Opportunity o : trigger.new)
                { 
                    if(o.AccountId != null)
                    {
                        o.AccountOwner__c = aMap.get(o.AccountId).OwnerId;
                        o.OppOwner__c = o.OwnerId;
                    }
                }
            }
        }
        if(trigger.isUpdate)
        {
            //Fill in if field is empty.
            list<id> accids = new list<Id>();
            for(Opportunity o : trigger.new)
            {
                 accids.add(o.AccountId);
                 if(o.OppOwner__c == null || o.OppOwner__c != o.OwnerId)
                 {
                     o.OppOwner__c = o.OwnerId;
                 }
            }
            if(!accids.isEmpty())
            {
                Map<Id, Account> aMap = new Map<Id, Account>([Select Id, OwnerId from Account where Id IN :accids]);
                for(Opportunity o : trigger.new)
                {
                    if(aMap.containsKey(o.AccountId))
                    {
                        o.AccountOwner__c = aMap.get(o.AccountId).OwnerId;
                    }
                }
            }
        }
    }
    
    /*Deal Reg User Populate*/
    if (trigger.isBefore)
    {
        OpportunityDealRegUpdate.Check(trigger.new);
    }
    
    /*ApprovalProfileRestrictions trigger*/
    if (trigger.isBefore && trigger.isUpdate)
    {
        Set<Id> bad = new Set<Id>{'00e60000000hw0i', '00e60000000idcy', '00e60000000iExi'};
        for(Opportunity o : trigger.new)
        {
            if(o.ApprovalChecker__c != trigger.oldMap.get(o.id).ApprovalChecker__c && bad.contains(UserInfo.getProfileId()))
            {
                trigger.new[0].addError('You are not allowed to approve this!');
            }            
        }
    }   
    
    /****************************************************************************************************
    *Description:   Account updates from the opportunity level
    *               -Populates Account.MMS_Account__c (boolean) of an Opportunity if the Opportunity Status is Closed/Won
    *                   And the opportunity contains "MSS" products (product family).
    *               -Updates account realized date on opportunity closure when opp has a LEAD Team Member populated     
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     2/13/2012   Justin Padilla      WO#1273 Initial Version
    *   2.0     8/25/2014   Alex Schach         Updated to also fill account realized date when conditions are met 
    *                                           Combined to reduce DML account updates
    *****************************************************************************************************/
    if(trigger.isbefore && trigger.isupdate)
    {
        map<id, opportunity> closedoppmap = new map<id, opportunity>();
        map<id, account> accttoupdate = new map<id, account>();
        set<id> accids = new set<id>();
        List<OpportunityLineItem> MSSolis = new List<OpportunityLineItem>();
        for(opportunity opp:trigger.new)
        {
            if(opp.isclosed == true && opp.iswon == true && trigger.oldmap.get(opp.id).isclosed != true)
            {
                closedoppmap.put(opp.id, opp);
                accids.add(opp.accountid);
            }
        }
        if(!closedoppmap.isempty())
        {
            //Query for OpportunityLineItems matching MMS Product Line
            MSSolis = [SELECT Id, OpportunityId, opportunity.accountid
                       FROM OpportunityLineItem WHERE OpportunityId IN: closedoppmap.keyset()
                       AND PricebookEntry.Product2.Family = 'MSS'];
            //Query for account information to update LEAD Revenue Realized Date
            accttoupdate = new map<id, account>([SELECT id, LEAD_Revenue_Tracking_Start_Date__c, LEAD_Revenue_Realized_Date__c, MSS_Account__c 
                                                 from account where id in: accids]); 
            for(opportunity o:closedoppmap.values())
            {
                account temp = accttoupdate.get(o.accountid);
                if((temp.LEAD_Revenue_Realized_Date__c == null || temp.LEAD_Revenue_Realized_Date__c < temp.LEAD_Revenue_Tracking_Start_Date__c) && o.LEAD_Team_Member__c != null)
                {
                    temp.LEAD_Revenue_Realized_Date__c = date.today();
                    accttoupdate.put(temp.id,temp);
                }
            }
            for(OpportunityLineItem oli:MSSolis)
            {
                account temp = accttoupdate.get(oli.opportunity.accountid);
                if(temp.MSS_Account__c == false) //Not already set
                {
                    //Set the MMS Account Flag to True and Add to Map (to later update)
                    temp.MSS_Account__c = true;
                    accttoupdate.put(temp.id, temp);
                    system.debug('Account update is required. Opportunity is closed won and has MSS products');
                }
            }
            if (!accttoupdate.isEmpty()) //Map contains Accounts to be updated
            {
                update(accttoupdate.values());
            }
        }
    }
    
    /****************************************************************************************************
    *Description:   Populates the Currency Code for the Opportunity
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     7/15/2013   Justin Padilla      WO#1151 Restore Initial Version
    *****************************************************************************************************/
    /*
    if (trigger.isBefore)
    {
        for (Opportunity Opp: trigger.new)
        {
            if (trigger.isUpdate)
            {
                if (trigger.oldMap.get(Opp.Id).Region_Opp__c !=  Opp.Region_Opp__c &&  Opp.Region_Opp__c != null)
                {
                    //Region has changed
                    if (Opp.Region_Opp__c.contains('United Kingdom'))
                        Opp.CurrencyISOCode = 'GBP';
                    else
                        Opp.CurrencyISOCode = 'USD';
                }
            }
            else
            {
                if (Opp.Region_Opp__c != null && Opp.Region_Opp__c.contains('United Kingdom'))
                    Opp.CurrencyISOCode = 'GBP';
                else
                    Opp.CurrencyISOCode = 'USD';
            }
        }
    }
    */
    
    /* counts the number of invoices. this functionality is duplicated in the RollupOpportunityInvoices trigger */
    //updated with map to properly aggregate and fill in invoice count
    if(Trigger.isbefore && Trigger.isUpdate) 
    {
        Map<Id,AggregateResult> invoiceCountMap = new Map<id,AggregateResult>([SELECT Opportunity__c Id, Count(Id) totalInvoice FROM Invoice_2__c WHERE Opportunity__c IN: Trigger.newMap.keySet() GROUP BY Opportunity__c]); 
        for(Opportunity opp : trigger.new)
        {
            if(invoiceCountMap.containsKey(opp.Id))
            {
                opp.Number_of_Invoice__c = (integer)invoiceCountMap.get(opp.Id).get('totalInvoice');   
            } 
            else 
            {
                opp.Number_of_Invoice__c = 0;    
            }
        }  
    }  
     
    public class MyException extends Exception{}
}