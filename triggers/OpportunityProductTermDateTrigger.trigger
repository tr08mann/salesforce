trigger OpportunityProductTermDateTrigger on Opportunity (before update) {
/****************************************************************************************************
    *Description:   Opportunity - Product Term Date Trigger
    *Test Class: OpportunityProductTermDateTrigger_TEST               
    *        
    * When opportunity is Closed Won (100%) and the Sync to Solomon box is checked, this trigger will
    * update the renewable products' term dates based on the product level Term Length field, or will default 
    * to one year.
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     6/24/2014    Alex Schach         Initial version
    ******************************************************************************************************/ 
    
    List<ID> oppids = new list<ID>();
    List<OpportunityLineItem> olilist = new list<opportunitylineitem>();
    List<OpportunityLineItem> olitoupdate = new list<opportunitylineitem>();
    
    for(opportunity opp:trigger.new)
    {
        if(opp.type != 'Renewal Business' && opp.stagename == 'Closed Won (100%)/SVC-Signed SOW' && opp.Sync_to_Solomon__c == TRUE && trigger.oldmap.get(opp.id).Sync_to_Solomon__c == FALSE)
        {
            oppids.add(opp.id);
        }
    }
    system.debug('oppids: ' + oppids);
    
    if(!oppids.isempty())
    {
        olilist = [SELECT id, opportunityid, opportunity.closedate, pricebookentry.product2.term_length__c, start_date__c, end_date__c from opportunitylineitem where opportunityid in: oppids and pricebookentry.product2.product_vendor_profile__r.name != 'FishNet Security' and pricebookentry.product2.Renewable_yesno__c = 'Yes'];
        system.debug('olilist: '+olilist);
        
        if(!olilist.isempty())
        {
            for(opportunitylineitem oli:olilist)
            {
                date oppclose = oli.opportunity.closedate;
                if(oli.pricebookentry.product2.term_length__c == '2 Years')
                {
                    oli.start_date__c = oppclose;
                    oli.End_Date__c = oppclose.addYears(2);
                }
                else if(oli.pricebookentry.product2.term_length__c == '3 Years')
                {
                    oli.start_date__c = oppclose;
                    oli.End_Date__c = oppclose.addYears(3);
                }
                else if(oli.pricebookentry.product2.term_length__c == '1 Year')
                {
                    oli.start_date__c = oppclose;
                    oli.End_Date__c = oppclose.addYears(1);
                }
                else
                {
                    oli.start_date__c = oppclose;
                }
                olitoupdate.add(oli);
            }
            system.debug('olitoupdate: '+olitoupdate);
            if(!olitoupdate.isempty())
            {
                try
                {
                    update(olitoupdate);
                }
                catch(exception e)
                {
                    system.debug('Error updating opportunity products: '+e);
                }
            }
        }
    }
    
}