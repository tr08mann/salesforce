trigger stopClosedOppOwnerChange on Account (before update, after update) 
{
	Id[] accIds = new Id[]{};
	for(Account a : trigger.new)
	{
		if(a.OwnerId != trigger.oldMap.get(a.id).OwnerId)
			accIds.add(a.id);
	}
	if (!accIds.isEmpty())
	{
		Opportunity[] closedlist = [Select Id, OwnerId, OppOwner__c, AccountOwner__c, AccountId, IsClosed from Opportunity where 
									AccountId IN :accIds AND IsClosed = true];
		Opportunity[] openlist = [Select Id, OwnerId, OppOwner__c, AccountOwner__c, AccountId, IsClosed from Opportunity where 
									AccountId IN :accIds AND IsClosed = false];
		for(Opportunity o : closedlist){
			if(o.OwnerId != o.OppOwner__c && o.AccountOwner__c == o.OppOwner__c && o.OppOwner__c == trigger.oldMap.get(o.AccountId).OwnerId){
				trigger.newMap.get(o.AccountId).addError('You may not transfer ownership of closed Opportunities.');
				//o.addError('You may not transfer ownership of closed Opportunities.');
			}
		}
		for(Opportunity o : openlist){
			o.AccountOwner__c = trigger.newMap.get(o.AccountId).OwnerId;
			if(o.OwnerId != o.OppOwner__c && o.AccountOwner__c == o.OppOwner__c && o.OppOwner__c == trigger.oldMap.get(o.AccountId).OwnerId)
				o.OppOwner__c = trigger.newMap.get(o.AccountId).OwnerId;
		}
		if(!openlist.isEmpty()) update openlist;
	}
}