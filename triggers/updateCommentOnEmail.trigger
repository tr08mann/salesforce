trigger updateCommentOnEmail on EmailMessage (before insert) {
    /****************************************************************************************************
    *Description:   Whenever email messages are sent or recieved and associated with a Case, Add a Case
    *               comment to the case with the email contents (1000) character max
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     9/11/2013   Justin Padilla      WO#1501 Initial Version
    *	1.1		4/21/2014	Justin Padilla		WO#1572 Update for Null commentbody values
    *****************************************************************************************************/  
    List<EmailMessage> validEmailMessages = new List<EmailMessage>();
    Schema.DescribeSObjectResult R = Case.SObjectType.getDescribe();    
    for(EmailMessage email : Trigger.new)
    {
    /*
        string reconstructedCaseId;
        if (email.Incoming)
        {
            //Incoming Email - Attempt to recreate the Id from the Subject, HTMLBody, or TextBody
            if ((email.Subject.contains('ref:_') && (email.Subject.contains(':ref'))))
            {
                string temp = email.Subject.substring(email.Subject.indexOf('ref:_'),email.Subject.indexOf(':ref'));
                //should now have something like: 00D30Uic._50060XCvdm
                string[] parse = temp.split('_');
                if (parse.size() > 1)
                {
                    //parse should now have something like: 50060XCvdm in parse[1]
                    reconstructedCaseId = parse[1].substring(0,3)+'000000'+parse[1].substring(4,5);
                    system.debug('reconstructedCaseId: '+reconstructedCaseId);
                    //email.ParentId = reconstructedCaseId;
                    continue;
                }
            }
        }
        */
        if (string.valueOf(email.ParentId).startsWith(R.getKeyPrefix()))
            validEmailMessages.add(email);
    }
    for (EmailMessage email: validEmailMessages)
    {
        String commentbody  = ''; 
        Case thisC = [SELECT id,description
                      FROM Case
                      WHERE id =: email.ParentID];
        CaseComment cc = new CaseComment(parentID=thisC.id, IsPublished = true);
        
        /*
        if(email.TextBody == null){
            email.TextBody = '';
        }
        */
        
        system.debug('******************email.TextBody************** '+ email.TextBody);
        system.debug('******************email.HTMLBody************** '+ email.HtmlBody);
        if (email.TextBody != null && email.TextBody != '')
        {
            if(email.TextBody.indexOf('--------------- Original Message ---------------') != -1) {
                commentbody = email.TextBody.substring(0,email.TextBody.indexOf('--------------- Original Message ---------------'));    
            } else {
                commentbody = email.TextBody;    
            }
        }
        else if (email.HTMLBody != null && email.HTMLBody != '')
        {
            commentbody = email.HTMLBody;
        }
        if (commentbody == '') commentbody = 'No Text or HTML information provided for Case:'+thisC.Description+' Link:'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+thisC.Id;
        if (commentbody.length() >= 1000)
          cc.CommentBody=commentbody.substring(0,999);
        else
          cc.CommentBody=commentbody;
          
        insert(cc);           
    }
}