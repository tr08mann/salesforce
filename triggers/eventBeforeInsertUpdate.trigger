trigger eventBeforeInsertUpdate on Event (before insert, before update) {
	for (Event e : Trigger.New) {
		if (Trigger.IsInsert)
		{
			string strWhatId = e.WhatId;
			if (strWhatId != null && strWhatId.Startswith('500'))
			{
				e.IsVisibleInSelfService = true;
			}
		}
	}	
}