trigger ConsolidatedDealRegistrationTrigger on Deal_Registration__c ( after insert, after update, before insert, before update) {
 /****************************************************************************************************
    *Description:   Deal Registration - Task Handling
    *   Auto creates, updates, and closes tasks based on the Deal Registration Status field            
    *   Checks and unchecks historical boxes based on the Deal Registration Status field                
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/21/2014    Alex Schach         Initial version
    *   2.0     5/20/2014    Alex Schach         Moved checkboxes from WFR to trigger
    *   2.5     5/23/2014    Alex Schach         Does not create tasks or send emails for renewal opps
    *   2.6     5/29/2014    Alex Schach         Added Tentative Approval task and checkbox, does not create tasks
    *                                            for Opportunity Already Registered status, updated task priority 
    *                                            for pending registrations to match deal registration priority  
    *   3.0     6/17/2014    Alex Schach         Removed FNS professional services. Added "Canceled" status and task.
    *                                            Cleaned up insert check box code.            
    *   3.1     8/12/2014    Alex Schach         Updated task name to include account name, updated task dates
    *                                            Updated checkboxes and email for new Closed/Canceled/Rejected status                                 
    ******************************************************************************************************/   
//check and uncheck boxes
if(trigger.isbefore)
{
    for(deal_registration__c d: trigger.new)
    {
        if(trigger.isinsert)
        {
            if(d.Deal_Registration_Status__c != null)
            {
                if(d.Deal_Registration_Status__c == 'Deal Registration Pending Submission')
                {
                    d.Deal_Reg_Pending_Submission__c = true;
                }
                else if(d.Deal_Registration_Status__c == 'Opportunity Did Not Qualify')
                {
                    d.Deal_Reg_Did_Not_Qualify__c = true;
                }
                else if(d.Deal_Registration_Status__c == 'Vendor NSP Cancels Deal Registration')
                {
                    d.Vendor_NSP_Cancels_Deal_Reg__c = true;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval')
                {
                    d.Deal_Reg_Submitted__c = true;
                }
                else if(d.Deal_Registration_Status__c == 'Vendor Has No Deal Registration Program')
                {
                    d.No_Deal_Reg_Program__c = true;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Already Registered on Another FNS Opportunity')
                {
                    d.Opportunity_Already_Registered_Box__c = true;
                }
            }
        }
        if(trigger.isupdate && trigger.oldmap.get(d.id).deal_registration_status__c != trigger.newmap.get(d.id).deal_registration_status__c)
        {
            if(d.deal_registration_status__c !=null)
            {
                if(d.Deal_Registration_Status__c == 'Deal Registration Pending Submission')
                {
                    d.Deal_Reg_Pending_Submission__c = true;
                    d.Deal_Reg_Approved__c = false;
                    d.Deal_Reg_Denied__c = false;
                    d.Deal_Reg_Submitted__c = false;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = false;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Registration Approved')
                {
                   d.Deal_Reg_Approved__c = true;
                    d.Deal_Reg_Denied__c = false;
                    d.Deal_Reg_Submitted__c = true;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Registration Denied')
                {
                    d.Deal_Reg_Approved__c = false;
                    d.Deal_Reg_Denied__c = true;   
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                    if(d.Deal_Registration_Denied_Reason__c == 'Opportunity did not qualify for deal registration')
                    {
                        d.Deal_Reg_Did_Not_Qualify__c = true;
                    }
                }
                else if(d.Deal_Registration_Status__c == 'Opportunity Did Not Qualify')
                {
                    d.Deal_Reg_Approved__c = false;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = false;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = true;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Vendor NSP Cancels Deal Registration')
                {
                    d.Deal_Reg_Approved__c = false;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = true;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval')
                {
                    d.Deal_Reg_Approved__c = false;
                    d.Deal_Reg_Denied__c = false;
                    d.Deal_Reg_Submitted__c = true;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = false;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Vendor Has No Deal Registration Program')
                {
                    d.Deal_Reg_Pending_Submission__c = false;
                    d.Deal_Reg_Approved__c = false;
                    d.Deal_Reg_Denied__c = false;
                    d.Deal_Reg_Submitted__c = false;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = false;
                    d.No_Deal_Reg_Program__c = true;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = false;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Already Registered on Another FNS Opportunity')
                {
                    d.Deal_Reg_Pending_Submission__c = false;
                    d.Deal_Reg_Approved__c = false;
                    d.Deal_Reg_Denied__c = false;
                    d.Deal_Reg_Submitted__c = false;
                    d.Vendor_NSP_Cancels_Deal_Reg__c = false;
                    d.No_Deal_Reg_Program__c = false;
                    d.Deal_Reg_Did_Not_Qualify__c = false;
                    d.Opportunity_Already_Registered_Box__c = true;
                    d.Cancelled_by_FishNet__c = false;
                }
                else if(d.Deal_Registration_Status__c == 'Deal Reg Canceled/Closed/Rejected')
                {
                    d.Cancelled_by_FishNet__c = true;
                }
            }
        }
    }
}
if(trigger.isafter)
{
    list<deal_registration__c> deallist = new list<deal_registration__c>();
    map<string, Deal_registration__c> dealregmap = new map<string, deal_registration__c>();
    list<task> taskstoupdate = new list<task>();
    list<task> taskstoinsert = new list<task>();

    deal_registration__c currentDR = [select id, Vendor_Profile__r.name, name, Opportunity__r.Id, opportunity__r.isclosed, Opportunity__r.type, opportunity__r.account.id, opportunity__r.account.name, ownerid, deal_expiration__c, 
                                      owner.email, deal_registration_status__c, opportunity__r.inside_sales_NEW__c, opportunity__r.deal_reg_desk_rep__c, opportunity__r.ownerid, opportunity__r.opportunity_number__c, deal_registration_priority__c
                                      from deal_registration__c where id=:trigger.new[0].id limit 1];
                                      system.debug('current deal reg: ' +currentDR);
    if(currentDR!=null && currentDR.opportunity__r.type != 'Renewal Business' && !currentDR.opportunity__r.isclosed)
    {
        dealregmap.put(currentDR.deal_registration_status__c+'Key'+currentdr.vendor_profile__r.name,currentDR);
        system.debug('dealregmap: '+dealregmap);
        list<string> tasksubject = new list<string>();
        tasksubject.add('Pending_'+currentDR.name);
        tasksubject.add('Expiring_'+currentDR.name);
        tasksubject.add('Follow up_'+currentDR.name); 
        tasksubject.add('Canceled/Closed/Rejected_'+currentDR.name);
         
        //complete all tasks when deal reg status goes to something non-deal reg related
        if(trigger.isupdate && (trigger.oldmap.get(currentDR.id).deal_registration_status__c != trigger.newmap.get(currentDR.id).deal_registration_status__c) &&
            (trigger.newmap.get(currentDR.id).deal_registration_status__c =='Opportunity Did Not Qualify'|| 
            trigger.newmap.get(currentDR.id).deal_registration_status__c =='Vendor Has No Deal Registration Program'|| 
            trigger.newmap.get(currentDR.id).deal_registration_status__c =='Vendor NSP Cancels Deal Registration'|| 
            trigger.newmap.get(currentDR.id).deal_registration_status__c =='Vendor Driven Opportunity'|| 
            trigger.newmap.get(currentDR.id).deal_registration_status__c =='Deal Already Registered on Another FNS Opportunity') && !processcontrol.processed)
        {
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }
        }
        
        //complete all tasks when deal reg status goes to denied
        if(trigger.isupdate && (trigger.oldmap.get(currentDR.id).deal_registration_status__c != trigger.newmap.get(currentDR.id).deal_registration_status__c)
           && trigger.newmap.get(currentDR.id).deal_registration_status__c =='Deal Registration Denied' && !processcontrol.processed)
        {
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }
            //create a new task with the deal registration denied subject and complete it as well
            Task dealDenied = (DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, Date.today(), 'Denied', currentDR.ownerId));
            dealDenied.status = 'Completed';
            system.debug('Deal Registration Denied');
            taskstoinsert.add(dealDenied);
        }
        //complete all tasks when deal reg status goes to canceled
        if(trigger.isupdate && (trigger.oldmap.get(currentDR.id).deal_registration_status__c != trigger.newmap.get(currentDR.id).deal_registration_status__c)
           && trigger.newmap.get(currentDR.id).deal_registration_status__c =='Deal Reg Canceled/Closed/Rejected' && !processcontrol.processed)
        {
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }
            //create a new task with the deal registration canceled subject and complete it as well
            Task dealDenied = (DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, Date.today(), 'Canceled/Closed/Rejected', currentDR.ownerId));
            dealDenied.status = 'Completed';
            system.debug('Deal Registration Canceled/Closed/Rejected');
            taskstoinsert.add(dealDenied);
        }
        //Creates a new task and completes the old task when the expiration date is changed
        if(Trigger.isUpdate && Trigger.newMap.get(currentDR.Id).Deal_Expiration__c != Trigger.oldMap.get(currentDR.Id).Deal_Expiration__c && !ProcessControl.processed 
           && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == Trigger.oldMap.get(currentDR.Id).Deal_Registration_Status__c && (trigger.newmap.get(currentDR.id).deal_registration_status__c == 'Deal Registration Approved' || trigger.newmap.get(currentDR.id).deal_registration_status__c == 'Deal Registration Tentatively Approved'))
        {
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }
            {
            taskstoinsert.add(DealRegTask.getTask( currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, currentDR.Deal_Expiration__c.addDays(-15), 'Expiring', currentDR.ownerid));   
            system.debug('Deal Registration Expiration Changed');
            }
        }
        
        //Deal Registration Pending
        if(Trigger.isUpdate && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission' && !ProcessControl.processed
           && trigger.oldmap.get(currentDR.Id).deal_registration_status__c != trigger.newmap.get(currentDR.Id).deal_registration_status__c)
        {
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, System.Today(), 'Pending', currentDR.ownerid));
            system.debug('Pending Submission');
        }     
          
        //Deal Registration Submitted, Pending Approval 
        if(Trigger.isUpdate && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval' && !ProcessControl.processed
           && trigger.oldmap.get(currentDR.Id).deal_registration_status__c != trigger.newmap.get(currentDR.Id).deal_registration_status__c)
        {
            //Complete the Old Task
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            }    
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, System.Today().addDays(5), 'Follow up', currentDR.ownerId));
            system.debug('Registration Submitted, Pending Approval');
        }
        
        // Deal Registration Approved 
        if(Trigger.isUpdate && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Approved' && !ProcessControl.processed
           && trigger.oldmap.get(currentDR.Id).deal_registration_status__c != trigger.newmap.get(currentDR.Id).deal_registration_status__c)
        {
            //Complete the Old Task
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            } 
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, currentDR.Deal_Expiration__c.addDays(-15), 'Expiring', currentDR.ownerId));
            system.debug('Registration Approved');
        }   
        
        // Deal Registration Tentatively Approved
        if(Trigger.isUpdate && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Tentatively Approved' && !ProcessControl.processed
           && trigger.oldmap.get(currentDR.Id).deal_registration_status__c != trigger.newmap.get(currentDR.Id).deal_registration_status__c)
        {
            //Complete the Old Task
            list<task> tempremove = dealregtask.updateTask(currentDR.id, 'Completed', tasksubject);
            system.debug('tempremove: ' +tempremove);
            if(!tempremove.isempty())
            {
                for(task t :tempremove)
                {
                    taskstoupdate.add(t);
                }
            } 
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, currentDR.Deal_Expiration__c.addDays(-15), 'Expiring', currentDR.ownerId));
            system.debug('Registration Tentatively Approved');
        }   
        
        //End of updating tasks. Start of creating new tasks
        //Pending Submission
        if(Trigger.isInsert && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Pending Submission')
        {
            ProcessControl.processed = true;
            task pending = (DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, System.Today(), 'Pending', currentDR.ownerId));
            pending.priority = Trigger.newMap.get(currentDR.Id).deal_registration_priority__c;
            taskstoinsert.add(pending);
        }       
           
        //New Submitted, Pending Approval
        if(Trigger.isInsert && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Submitted, Pending Approval')
        {
            ProcessControl.processed = true;
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_profile__r.name, System.Today().addDays(5), 'Follow up', currentDR.ownerId));
        }     
            
        //New Approved
        /*if(Trigger.isInsert && Trigger.newMap.get(currentDR.Id).Deal_Registration_Status__c == 'Deal Registration Approved')
        {
            ProcessControl.processed = true;
            if(null!=currentDR.Deal_Expiration__c)
            {
            taskstoinsert.add(DealRegTask.getTask(currentDR.Name, currentDR.Id, currentDR.Vendor_Profile__r.name, currentDR.Deal_Expiration__c.addDays(-15), 'Expiring', currentDR.ownerId));
            }
        }*/
        system.debug('taskstoupdate: '+taskstoupdate);
        system.debug('taskstoinsert: '+taskstoinsert);
        
        if(!taskstoupdate.isEmpty())
        {
            update taskstoupdate;
        }
        if(!taskstoinsert.isEmpty())
        {
            insert taskstoinsert;
        }           
 /****************************************************************************************************
    *Description:   Deal Registration Email Notifications
    *               
    *                   
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0    4/21/2014     Alex Schach            Initial Version
    *   1.5    5/23/2014     Alex Schach            Updated to only run SOQL calls if it needs to send an email
                                                    Also updated to use two conditional VF email templates
    *   1.6    5/29/2014     Alex Schach            Updated email to match deal reg priority
    *   2.0    8/12/2014     Alex Schach            Added canceled/closed/rejected emails
    ****************************************************************************************************/ 
        integer dealregistrationstatuschanges = 0;
        //don't go any further if status isn't pending, approved, or denied - no need to send emails otherwise
        for(deal_registration__c d:trigger.new)
        {
            if(trigger.isupdate && d.deal_registration_status__c !=null)
            {
                if(d.deal_registration_status__c !=trigger.oldmap.get(d.id).deal_registration_status__c)
                {
                    if(d.deal_registration_status__c == 'Deal Registration Pending Submission' || d.deal_registration_status__c == 'Deal Registration Approved' 
                       || d.deal_registration_status__c == 'Deal Registration Denied' || d.deal_registration_status__c == 'Deal Reg Canceled/Closed/Rejected')
                    {
                        dealregistrationstatuschanges++;
                    }
                }
            }
            else if(trigger.isinsert && d.deal_registration_status__c == 'Deal Registration Pending Submission')
            {
                dealregistrationstatuschanges++;
            }
        }
        if(Dealregistrationstatuschanges >0)
        {
            system.debug('Deal Registration Status Notification Entered...');
            set<id> drids = new set<id>();
            List<Messaging.SingleEmailMessage> emailToSend = new List<Messaging.SingleEmailMessage>();
            emailtemplate pending = null;
            emailtemplate status = null;
            list<emailtemplate> etemplates = [select id, developername from emailtemplate
                                              where (developername = 'Follow_up_on_Deal_Registration' OR developername = 'Deal_Registration_Status')];
            if(etemplates!=null && !etemplates.isempty())
            {
                for(emailtemplate et:etemplates)
                {   
                    if(et.developername == 'Follow_up_on_Deal_Registration')
                    {
                        pending = et;
                        system.debug('Pending template');
                    }
                    else if(et.developername == 'Deal_Registration_Status')
                    {
                        status = et;
                        system.debug('Status template');
                    }
                }
            }
            for(deal_registration__c d :trigger.new)
            {
                deal_registration__c appdr = dealregmap.get('Deal Registration Approved'+'Key'+currentDR.Vendor_profile__r.name);
                deal_registration__c dendr = dealregmap.get('Deal Registration Denied'+'Key'+currentDR.Vendor_profile__r.name);
                deal_registration__c pendr = dealregmap.get('Deal Registration Pending Submission'+'Key'+currentDR.Vendor_profile__r.name);
                deal_registration__c candr = dealregmap.get('Deal Reg Canceled/Closed/Rejected'+'Key'+currentDR.Vendor_profile__r.name);
                
                if(pendr !=null)
                {
                    if(pending !=null && pendr.ownerid != null && (pendr.Deal_Registration_Status__c.toLowerCase().contains('pending submission')))
                    {
                        system.debug('pending email created');
                        messaging.singleemailmessage mail = new messaging.singleemailmessage();
                        mail.settemplateid(pending.id);
                        mail.settargetobjectid(pendr.ownerid);
                        mail.setsaveasactivity(false);
                        mail.setwhatid(pendr.id);
                        mail.setemailPriority(pendr.deal_registration_priority__c);
                        emailtosend.add(mail);
                    }
                }
                else if(appdr !=null)
                {
                    if(status !=null & appdr.opportunity__r.ownerid != null && (appdr.Deal_Registration_Status__c.toLowerCase().contains('approved')))
                    {
                        system.debug('approved email created');
                        messaging.singleemailmessage mail = new messaging.singleemailmessage();
                        mail.settemplateid(status.id);
                        mail.settargetobjectid(appdr.opportunity__r.ownerid);
                        mail.setsaveasactivity(false);
                        mail.setwhatid(appdr.id);
                        emailtosend.add(mail);
                        if(appdr.opportunity__r.Inside_Sales_NEW__c != null && appdr.opportunity__r.Inside_Sales_NEW__c != appdr.opportunity__r.ownerid)
                        {
                            system.debug('approved ISR email created');
                            messaging.singleemailmessage mail2 = new messaging.singleemailmessage();
                            mail2.settemplateid(status.id);
                            mail2.settargetobjectid(appdr.opportunity__r.Inside_Sales_NEW__c);
                            mail2.setsaveasactivity(false);
                            mail2.setwhatid(appdr.id);
                            emailtosend.add(mail2);
                        }
                    }
                }
                else if(dendr != null)
                {
                    if(status !=null & dendr.opportunity__r.ownerid != null && (dendr.Deal_Registration_Status__c.toLowerCase().contains('denied')))
                    {
                        system.debug('denied email created');
                        messaging.singleemailmessage mail = new messaging.singleemailmessage();
                        mail.settemplateid(status.id);
                        mail.settargetobjectid(dendr.opportunity__r.ownerid);
                        mail.setsaveasactivity(false);
                        mail.setwhatid(dendr.id);
                        emailtosend.add(mail);
                        if(dendr.opportunity__r.Inside_Sales_NEW__c != null && dendr.opportunity__r.Inside_Sales_NEW__c != dendr.opportunity__r.ownerid)
                        {
                            system.debug('denied ISR email created');
                            messaging.singleemailmessage mail2 = new messaging.singleemailmessage();
                            mail2.settemplateid(status.id);
                            mail2.settargetobjectid(dendr.opportunity__r.Inside_Sales_NEW__c);
                            mail2.setsaveasactivity(false);
                            mail2.setwhatid(dendr.id);
                            emailtosend.add(mail2);
                        }
                    }
                }
                else if(candr != null)
                {
                    if(status != null & candr.opportunity__r.ownerid != null && (candr.deal_registration_status__c.toLowerCase().contains('canceled')))
                    {
                        system.debug('canceled email created');
                        messaging.singleemailmessage mail = new messaging.singleemailmessage();
                        mail.settemplateid(status.id);
                        mail.settargetobjectid(candr.opportunity__r.ownerid);
                        mail.setsaveasactivity(false);
                        mail.setwhatid(candr.id);
                        mail.setemailPriority('High');
                        emailtosend.add(mail);
                        if(candr.opportunity__r.Inside_Sales_NEW__c != null && candr.opportunity__r.Inside_Sales_NEW__c != candr.opportunity__r.ownerid)
                        {
                            system.debug('canceled ISR email created');
                            messaging.singleemailmessage mail2 = new messaging.singleemailmessage();
                            mail2.settemplateid(status.id);
                            mail2.settargetobjectid(candr.opportunity__r.Inside_Sales_NEW__c);
                            mail2.setsaveasactivity(false);
                            mail2.setwhatid(candr.id);
                            mail2.setemailPriority('High');
                            emailtosend.add(mail2);
                        }
                    }
                }
            }
            system.debug('emailtosend: '+emailtosend);
            if(!emailtosend.isempty())
            {
                messaging.sendemail(emailtosend);
            }
        }
    }
}
}