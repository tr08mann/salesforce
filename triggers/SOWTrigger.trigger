trigger SOWTrigger on SOW__c (before update) 
{
    list<Messaging.SingleEmailMessage> emailToSend = new list<Messaging.SingleEmailMessage>();
    emailTemplate alertTemplate = [select id, developerName, HtmlValue, body, subject from emailTemplate where developerName = 'SOW_Update_Email2'];
    list<Task> tasksToInsert = new list<Task>();
    set<id> oppids = new set<id>();
    map<id,opportunity> oppmap = new map<id,opportunity>();
    list<SOW__c> sows = new list<SOW__c>();
    
    for(SOW__c s:trigger.new)
    {
        if(s.Task_ISR_to_update_Opp__c && !trigger.oldmap.get(s.id).Task_ISR_to_update_Opp__c && !s.ISR_Task_Created__c)
        {
            oppids.add(s.Opportunity__c);
            sows.add(s);
        }
        //calculate SOW due date to count only business days
        if(s.SLA3__c != null && s.Practice_Approval_Due_Date__c != null)
        {
            integer sla = integer.valueOf(s.SLA3__c); //SLA length
            date loopDate = s.CreatedDate.Date(); //SOW created date
            date tempFinalDate = (s.CreatedDate.addDays(sla)).Date(); //what the final date would have been if we didn't worry about weekends
            date finalDate; //final date counting only business days
            integer daysToAdd = 0; //first number of days to add
            //loop through each day from createddate+1 to end of SLA. Count number of weekend days to be added back on at the end
            for(integer i = 1; i <= sla; i++)
            {
                loopDate = loopDate.addDays(+1);
                if(loopDate.daysBetween(loopDate.toStartOfWeek()) == -6) //Saturday
                {
                    daysToAdd += 1;
                }
                else if(loopDate.daysBetween(loopDate.toStartOfWeek()) == 0) //Sunday
                {
                    daysToAdd += 1;
                }
            }
            //we have to loop through again, because we need to make sure the final date isn't on a weekend
            date loopDate2 = tempFinalDate; //the start of the loop is the temp final date
            integer finalAdd = daysToAdd; //final number of days to be added to the tempFinalDate
            for(integer i = 1; i <= daysToAdd; i++)
            {
                loopDate2 = loopDate2.addDays(+1);
                if(loopDate2.daysBetween(loopDate2.toStartOfWeek()) == -6)
                {
                    finalAdd += 1;
                    //if the last day at the end of the loop is still a Saturday, we have to add an extra day or the date will be a Sunday
                    if(i == daysToAdd)
                    {
                        finalAdd += 1;
                    }
                }
                else if(loopDate2.daysBetween(loopDate2.toStartOfWeek()) == 0)
                {
                    finalAdd += 1;
                }
            }
            finalDate = tempFinalDate.addDays(finalAdd); //the final due date is the tempFinalDate + the final number of days to add
            time finalTime = time.newInstance(16, 0, 0, 0); //instantiate a time of 4pm
            s.SOW_Due_Date2__c = dateTime.newInstance(finalDate, finalTime); //set SOW due date to the finaldate/finaltime
        }
    }
    if(!oppids.isEmpty())
    {
        oppmap = new map<id,opportunity>([select id, Inside_Sales_NEW__c, name, Opportunity_Number__c from opportunity where id in: oppids]);
    }
    for(SOW__c s:sows)
    {
        opportunity opp = oppmap.get(s.Opportunity__c);
        task t = new task();
        
        s.ISR_Task_Created__c = true;
        t.OwnerId = opp.Inside_Sales_NEW__c;
        t.Subject = 'SOW completed. Update Opportunity '+opp.Opportunity_Number__c;
        t.Status = 'Not Started';
        t.WhatId = opp.id;
        t.Priority = 'High';
        t.ActivityDate = Date.today();
        tasksToInsert.add(t);
        
        messaging.SingleEmailMessage email = new messaging.SingleEmailMessage();
        email.setTemplateId(alertTemplate.id);
        email.setTargetObjectId(opp.Inside_Sales_NEW__c);
        email.setWhatId(s.id);
        email.SetSaveAsActivity(false);
        email.setUseSignature(false);
        emailToSend.add(email);
    }
    if(!tasksToInsert.isEmpty())
    {
        try
        {
            insert tasksToInsert;
        }
        catch(exception e)
        {
            trigger.new[0].addError('Error creating ISR SOW task: '+e);
        }
    }
    if(!emailToSend.isEmpty())
    {
        try
        {
            messaging.sendEmail(emailToSend);
        }
        catch(exception e)
        {
            trigger.new[0].addError('Error sending ISR SOW email: '+e);
        }
    }
}