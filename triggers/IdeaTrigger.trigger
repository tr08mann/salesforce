trigger IdeaTrigger on Idea (after insert) {

/***************************************************************************************************************************************
This trigger creates a case and autofills certain fields on the case. It also relates the idea and case together on both objects.

* Revision   |Date        |Author         |Additional Notes
=================================================================================================
* 1.0         4/18/2014     Alex Schach     Case 102310
* 2.0         11/25/2014    Alex Schach     Updated to reduce queries, bulkify, and populate rich text case description__c field
***************************************************************************************************************************************/
set<id> currentidea = new set<id>();
list<case> casestoinsert = new list<case>();
map<string,id> contactmap = new map<string,id>();
map<id,user> usermap = new map<id,user>();
set<id> userid = new set<id>();
    
    for(idea i : trigger.new)
    {
        currentidea.add(i.id);
        userid.add(i.POC_owner__c);
    }        
    
    if(!currentidea.isempty())
    {
        for(user u : [select id, email, name from user where id in: userid])
        {
            usermap.put(u.id, u);
            contactmap.put(u.email.tolowercase(),null);
        }  
        for(contact c : [select id, email from contact where email in: contactmap.keyset() and account.name ='Fishnet Security, Inc. (Parent)'])
        {
            contactmap.put(c.email.tolowercase(), c.id);
        }
        for(Idea ide : trigger.new)
        {
            string ideafulldescription = '';    //stores combination of all fields from idea for case description
            user u = usermap.get(ide.POC_owner__c);
            case newcase = new case(); 
            
            ideafulldescription = 'Owner of the change request (POC): '+u.name + '\r\n'+ '\r\n' + 'New Idea Detail: ' +ide.body + '\r\n'+ '\r\n'
            + 'Change Description: ' + ide.CHANGE_DESCRIPTION_As_a_JOB_TITLE__c + '\r\n'+ '\r\n' +'I want... ' + ide.I_want__c +'\r\n'+ '\r\n' + 'So that... ' +ide.So_that__c+'\r\n' + '\r\n'+
            'Desired Result: ' +ide.DESIRED_RESULT_Detail__c + '\r\n'+ '\r\n' + 'Functional Process Owner: ' + ide.Functional_process_owner__c + '\r\n'+ '\r\n' + 
            'Business Problem this Resolves: ' +ide.BUSINESS_PROBLEM_THIS_RESOLVES__c;
            newcase.RecordTypeId = '012600000009aQ2';
            newcase.accountid = '00130000004csyT';
            newcase.origin = 'IDEAS Module';
            newcase.reason = ide.category;
            newcase.What_business_problem_does_this_resolve__c = ide.BUSINESS_PROBLEM_THIS_RESOLVES__c;
            newcase.status = 'New';
            newcase.Next_Step_Case__c = 'Enhancement Meeting';
            newcase.Enhancement_Meeting_Status__c = 'New Request';
            newcase.subject = ide.Title;
            newcase.Description = ideafulldescription;
            newcase.Related_Idea_1__c = ide.id;
            newcase.ownerid = '00G60000001lZn6';
            newcase.contactid = contactmap.get(u.email.tolowercase());
            casestoinsert.add(newcase);
        }
        if(!casestoinsert.isempty())
        { 
            map<id,id> casemap = new map<id,id>();
            set<id> insertedcaseids = new set<id>();
            database.saveresult[] srlist = database.insert(casestoinsert,true);
            for(database.saveresult sr : srlist)
            {
                if(sr.isSuccess())
                {
                    insertedcaseids.add(sr.getID());
                }
            }
            for(case c : [select id, Related_Idea_1__r.id from case where id in: insertedcaseids])
            {
                casemap.put(c.related_idea_1__r.id, c.id);
            }
            list<idea> updateideas = [select id, related_case__c from idea where id in: currentidea];
            for(idea ide : updateideas)
            { 
                ide.related_case__c = casemap.get(ide.id);
                ide.status = 'Reviewed';
            }
            try
            {
                update updateideas;
            }
            catch(exception e)
            {
                system.debug('Error updating ideas: '+ e);
            }
        }
            
    }

    //set email templates
    emailtemplate newidea = null;
    emailtemplate usernotif = null;
    list <emailtemplate> etemplates = [select id, developername from emailtemplate 
                                        where (developername = 'New_Idea_Notification' OR developername = 'New_Idea_to_User')];
    if(etemplates !=null && !etemplates.isempty())
    { 
        for(emailtemplate et : etemplates)
        {
            if(et.developername == 'New_Idea_Notification')
            {
                newidea = et;
            }
            else if(et.developername == 'New_Idea_to_User')
            {
                usernotif = et;
            }
        }
    }
    
    //send emails
    List<Messaging.SingleEmailMessage> emailToSend = new List<Messaging.SingleEmailMessage>();
    for(idea ide : trigger.new)
    {
        if(newidea !=null)
        {
            //email to Tiffany
            messaging.singleemailmessage mail = new messaging.singleemailmessage();
            mail.settemplateid(newidea.id);
            mail.settargetobjectid('00530000000doC1');
            mail.setsaveasactivity(false);
            mail.setwhatid(ide.id);
            emailtosend.add(mail);
            
            //email to Lyndsay
            messaging.singleemailmessage mail3 = new messaging.singleemailmessage();
            mail3.settemplateid(newidea.id);
            mail3.settargetobjectid('00530000000ePXd');
            mail3.setsaveasactivity(false);
            mail3.setwhatid(ide.id);
            emailtosend.add(mail3);
        }
        if(usernotif !=null)
        {
            messaging.singleemailmessage mail2 = new messaging.singleemailmessage();
            mail2.settemplateid(usernotif.id);
            mail2.settargetobjectid(usermap.get(ide.POC_owner__c).id);
            mail2.setsaveasactivity(false);
            mail2.setwhatid(ide.id);
            emailtosend.add(mail2);
        }
    }
    if(!emailtosend.isempty())
    {
        try
        {
            messaging.sendemail(emailtosend);    
        }
        catch(exception e)
        {
            system.debug('Error sending email: '+e);
        }
    }
}