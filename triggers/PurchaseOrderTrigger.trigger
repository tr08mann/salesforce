trigger PurchaseOrderTrigger on Purchase_Order__c (before insert, before update) {
	
	Set<Id> oppIds = new Set<Id>();
	Map<Id, String> oppOwnersMap = new Map<Id, String>();
	List<Opportunity> oppList = new List<Opportunity>();
	List<User> usersList = new List<User>();	
	Set<String> oppAssignNames = new Set<String>();
	
	Map<String, id> ownersMap = new Map<String, id>();
	
	
	for(Purchase_Order__c p: Trigger.new){
		oppIds.add(p.Opportunity__c);
	}

	if(!oppIds.IsEmpty()){
		
		System.debug('Number of Opportunities related ---> ' + oppIds.size());
		oppList = [SELECT Id, Order_Assigned_To__c FROM Opportunity WHERE id IN: oppIds];
		
		if(!oppList.IsEmpty()){
			
			for(Opportunity o: oppList){
			
				if(o.Order_Assigned_To__c != null){
					oppAssignNames.add(o.Order_Assigned_To__c);
					oppOwnersMap.put(o.Id, o.Order_Assigned_To__c);
				}
			
			}
			
		}
		
		if(!oppAssignNames.isEmpty()){
			System.debug('Number of Opportunities with Purchase Order Name Assigned ---> ' + oppAssignNames.size());
			
			usersList = [SELECT id, name FROM User WHERE name IN: oppAssignNames];
			
		}else{
			System.debug('---> No Opportunities with Purchase Order Name found!!');
		}	
		
		if(!usersList.IsEmpty()){
			
			for(User u: usersList){
				ownersMap.put(u.name, u.id);
			}
				
		}else{
			System.debug('---> No User with Purchase Order Name found!!');
		}	
			
	
		//You should now have Opportunity Map with <Opp ID, Purchase Order Assigned To> and
		//User Map with <User ID, User ID>
		//Loop through Trigger; find opportunity in Map to find Name; then use Name to find Id
		
		for(Purchase_Order__c p: Trigger.new){
			
			String assignName = '';
			Id assignNameOwner = null;
			
			if(p.Opportunity__c != Null){
				if(oppOwnersMap.containskey(p.Opportunity__c)) assignName = oppOwnersMap.get(p.Opportunity__c);
				if(ownersMap.containskey(assignName)) assignNameOwner = ownersMap.get(assignName);
					
				if(assignNameOwner != null)	{
					p.Purchase_Order_Owner__c = assignNameOwner;
				}
					
			}
			 
		}
		
		
	} else{
		System.debug('---> No Opportunities related to Purchase Order!!');
	}
}