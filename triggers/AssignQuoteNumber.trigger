trigger AssignQuoteNumber on SFDC_520_Quote__c (before insert, before update) {
	if (Trigger.IsInsert)
	  for (Integer i = 0; i < Trigger.new.size(); i++) {
		SFDC_520_Quote__c q = Trigger.new[i];
		Opportunity o = [select id, opportunity_number__c from opportunity where id = :q.opportunity__c];
    	//q.Fishnet_Quote_Number__c = o.opportunity_number__c + '-1';
		integer seq = 0;
		Try
		{ 
			for (SFDC_520_Quote__c eq: [select id, Fishnet_Quote_Number__c from SFDC_520_Quote__c where opportunity__c = :q.opportunity__c order by Fishnet_Quote_Number__c desc])
			{
				Try  
				{
					String name = eq.Fishnet_Quote_Number__c;
					if (name != null)
					{
						integer idx = name.lastIndexOf('-');
						if (idx > 0)
						{
							integer val = Integer.valueOf(name.substring(idx + 1, name.length()));
							if (val > seq) 
								seq = val;
						}
					}
				}
				catch (Exception ex)
				{   
				}
			} 
		}
		finally
		{
  	  	    seq = seq + 1;
			q.Fishnet_Quote_Number__c = o.opportunity_number__c + '-' + seq;
		}

		
 
     
   }
}