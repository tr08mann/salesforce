@isTest(seealldata=true)
public class QuoteShippingCalcExtension_TEST {
	public static testMethod void testShippingCalc()
    {
     	pagereference pageref = Page.QuoteShipping;
        test.setcurrentpage(pageref);
        
        user drd = [select id,email, name from user where id=:userinfo.getuserid()];
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        	insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            acc.ownerid = drd.id;
            insert acc;
            
            String pricebookId = '01s300000000OT4AAM';
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        	insert opp;
            
            Contact con = TestDataFactory.createStandardContact(acc.id);
			Insert con;

			OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
			Insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            quote.shipping_method__C = 'Ground Shipping';
            quote.Number_of_Large_Appliances__c = 2;
            quote.Number_of_Small_Appliances__c = 2;
            quote.Overwrite_Shipping_Calculation__c = FALSE;
            quote.Free_Shipping__c = FALSE;
            quote.Free_Shipping_Reason__c = NULL;
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;

            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteShippingCalcExtension pages = new QuoteShippingCalcExtension(controller);
            
            pages.shipmeth = quote.Shipping_Method__c;
            pages.smallapp = quote.Number_of_Small_Appliances__c;
            pages.largeapp = quote.Number_of_Large_Appliances__c;
            pages.freeship = quote.Free_Shipping__c;
            pages.freereason = quote.Free_Shipping_Reason__c;
            pages.Overwrite = quote.Overwrite_Shipping_Calculation__c;

            pages.calculate();
            pages.save();
            pages.clear();           
        }
    }
    
	public static testMethod void testNullShipping()
    {
     	pagereference pageref = Page.QuoteShipping;
        test.setcurrentpage(pageref);
        
        user drd = [select id,email, name from user where id=:userinfo.getuserid()];
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        	insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            acc.ownerid = drd.id;
            insert acc;
            
            String pricebookId = '01s300000000OT4AAM';
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        	insert opp;
            
            Contact con = TestDataFactory.createStandardContact(acc.id);
			Insert con;

			OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
			Insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            quote.shipping_method__C = NULL;
            quote.Number_of_Large_Appliances__c = NULL;
            quote.Number_of_Small_Appliances__c = NULL;
            quote.Overwrite_Shipping_Calculation__c = FALSE;
            quote.Free_Shipping__c = FALSE;
            quote.Free_Shipping_Reason__c = NULL;
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;

            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteShippingCalcExtension pages = new QuoteShippingCalcExtension(controller);
            
            pages.shipmeth = quote.Shipping_Method__c;
            pages.smallapp = quote.Number_of_Small_Appliances__c;
            pages.largeapp = quote.Number_of_Large_Appliances__c;
            pages.freeship = quote.Free_Shipping__c;
            pages.freereason = quote.Free_Shipping_Reason__c;
            pages.Overwrite = quote.Overwrite_Shipping_Calculation__c;

            pages.calculate();
            pages.save();
            pages.clear();           
        }
    }
    
    public static testMethod void testTwoDayShipping()
    {
     	pagereference pageref = Page.QuoteShipping;
        test.setcurrentpage(pageref);
        
        user drd = [select id,email, name from user where id=:userinfo.getuserid()];
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        	insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            acc.ownerid = drd.id;
            insert acc;
            
            String pricebookId = '01s300000000OT4AAM';
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        	insert opp;
            
            Contact con = TestDataFactory.createStandardContact(acc.id);
			Insert con;

			OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
			Insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            quote.shipping_method__C = 'Two Day Shipping';
            quote.Number_of_Large_Appliances__c = 2;
            quote.Number_of_Small_Appliances__c = 2;
            quote.Overwrite_Shipping_Calculation__c = FALSE;
            quote.Free_Shipping__c = TRUE;
            quote.Free_Shipping_Reason__c = 'Account Requirement';
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;

            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteShippingCalcExtension pages = new QuoteShippingCalcExtension(controller);
            
            pages.shipmeth = quote.Shipping_Method__c;
            pages.smallapp = quote.Number_of_Small_Appliances__c;
            pages.largeapp = quote.Number_of_Large_Appliances__c;
            pages.freeship = quote.Free_Shipping__c;
            pages.freereason = quote.Free_Shipping_Reason__c;
            pages.Overwrite = quote.Overwrite_Shipping_Calculation__c;

            pages.calculate();
            pages.save();
            pages.clear();           
        }
    }
    
    public static testMethod void testOverNightShipping()
    {
     	pagereference pageref = Page.QuoteShipping;
        test.setcurrentpage(pageref);
        
        user drd = [select id,email, name from user where id=:userinfo.getuserid()];
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        	insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            acc.ownerid = drd.id;
            insert acc;
            
            String pricebookId = '01s300000000OT4AAM';
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        	insert opp;
            
            Contact con = TestDataFactory.createStandardContact(acc.id);
			Insert con;

			OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
			Insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            quote.shipping_method__C = 'Overnight Shipping';
            quote.Number_of_Large_Appliances__c = 2;
            quote.Number_of_Small_Appliances__c = 2;
            quote.Overwrite_Shipping_Calculation__c = FALSE;
            quote.Free_Shipping__c = TRUE;
            quote.Free_Shipping_Reason__c = NULL;
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;

            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteShippingCalcExtension pages = new QuoteShippingCalcExtension(controller);
            
            pages.shipmeth = quote.Shipping_Method__c;
            pages.smallapp = quote.Number_of_Small_Appliances__c;
            pages.largeapp = quote.Number_of_Large_Appliances__c;
            pages.freeship = quote.Free_Shipping__c;
            pages.freereason = quote.Free_Shipping_Reason__c;
            pages.Overwrite = quote.Overwrite_Shipping_Calculation__c;

            pages.calculate();
            pages.save();
            pages.clear();           
        }
    }
    
    public static testMethod void testOvernightAMShipping()
    {
     	pagereference pageref = Page.QuoteShipping;
        test.setcurrentpage(pageref);
        
        user drd = [select id,email, name from user where id=:userinfo.getuserid()];
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        	insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            acc.ownerid = drd.id;
            insert acc;
            
            String pricebookId = '01s300000000OT4AAM';
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        	insert opp;
            
            Contact con = TestDataFactory.createStandardContact(acc.id);
			Insert con;

			OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
			Insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            quote.shipping_method__C = 'Overnight Early AM Shipping';
            quote.Number_of_Large_Appliances__c = 1;
            quote.Number_of_Small_Appliances__c = 2;
            quote.Overwrite_Shipping_Calculation__c = TRUE;
            quote.Free_Shipping__c = TRUE;
            quote.Free_Shipping_Reason__c = 'Account Requirement';
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;

            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteShippingCalcExtension pages = new QuoteShippingCalcExtension(controller);
            
            pages.shipmeth = quote.Shipping_Method__c;
            pages.smallapp = quote.Number_of_Small_Appliances__c;
            pages.largeapp = quote.Number_of_Large_Appliances__c;
            pages.freeship = quote.Free_Shipping__c;
            pages.freereason = quote.Free_Shipping_Reason__c;
            pages.Overwrite = quote.Overwrite_Shipping_Calculation__c;

            pages.calculate();
            pages.OverrideCheck();
            pages.ClearFreeShipReason();
            pages.OverwriteShip();
            pages.save();
            pages.clear();           
        }
    }
}