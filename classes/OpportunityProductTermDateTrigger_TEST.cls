@isTest
public class OpportunityProductTermDateTrigger_TEST{

    public static testmethod void testall(){
    
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Radiant Logic');
        insert vp;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.id, vp.name, '12345');
        p.Family = 'VAR-IAM';
        p.renewable_yesno__c = 'Yes';
        p.term_length__c = '2 Years';
        p.Solution_Set__c = 'Infrastructure and Operations';
        p.Practice__c = 'Compliance';
        p.mobility_sku__c = true;
        insert p;
        
        Product2 p2 = TestDataFactory.createStandardProduct(vp.id, vp.name, '123456');
        p2.Family = 'VAR-IAM';
        p2.renewable_yesno__c = 'Yes';
        p2.term_length__c = '3 Years';
        p2.Solution_Set__c = 'Infrastructure and Operations';
        p2.Practice__c = 'Compliance';
        insert p2;
        
        Product2 p3 = TestDataFactory.createStandardProduct(vp.id, vp.name, '1234567');
        p3.Family = 'VAR-IAM';
        p3.renewable_yesno__c = 'Yes';
        p3.term_length__c = '1 Year';
        p3.Solution_Set__c = 'Infrastructure and Operations';
        p3.Practice__c = 'Compliance';
        insert p3;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        PricebookEntry pbe2 = TestDataFactory.createPricebookEntry(p2.id);
        insert pbe2;
        
        PricebookEntry pbe3 = TestDataFactory.createPricebookEntry(p3.id);
        insert pbe3;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Closed Won (100%)/SVC-Signed SOW');
        opp.closedate = date.today();
        opp.Closed_Won_Lost_Reason__c = 'Won';
        insert opp;
        
        list<opportunitylineitem> olilist = new list<opportunitylineitem>();
        
        opportunitylineitem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        olilist.add(oli);
        
        opportunitylineitem oli2 = TestDataFactory.createOppLineItem(opp.id, pbe2.id);
        olilist.add(oli2);
        
        opportunitylineitem oli3 = TestDataFactory.createOppLineItem(opp.id, pbe3.id);
        olilist.add(oli3);
        
        insert olilist;
        
        opp.sync_to_solomon__c = true;
        update opp;
    }
}