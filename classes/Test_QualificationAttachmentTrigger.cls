@isTest
public class Test_QualificationAttachmentTrigger{
    
    static testMethod void validateQualificationAttachmentTrigger()
    {
        Test.startTest();
              
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Fishnet Security, Inc (Parent)');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        con.email = userinfo.getUserEmail();
        con.FirstName = userinfo.getFirstName();
        con.LastName = userinfo.getLastName();
        insert con;
        
        Certification__c certi = new Certification__c();
        certi.Certification_Status__c = 'Active';
        certi.Student_Name_2__c = con.Id;
        certi.Proof_of_Certification_Attached__c = true;
        insert certi;
        
        Attachment attach = new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=certi.id;
        insert attach;
        
        certi.Proof_of_Certification_Attached__c = false;
        
        delete attach;
        delete certi;
        
        Test.stopTest();
    }
    
    static testMethod void validateQualificationAttachmentTrigger1()
    {
        Test.startTest();
        
        
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Fishnet Security, Inc (Parent)');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        con.email = userinfo.getUserEmail();
        con.FirstName = userinfo.getFirstName();
        con.LastName = userinfo.getLastName();
        insert con;
        
        Certification__c certi = new Certification__c();
        certi.Certification_Status__c = 'Active';
        certi.Student_Name_2__c = con.Id;
        certi.Proof_of_Certification_Attached__c = false;
        insert certi;
        
        //certi.Proof_of_Certification_Attached__c = false;
        //update certi;
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=certi.id;
        insert attach;
        
        //list<Attachment> attachList = [SELECT Id,Name FROM Attachment WHERE parent.Id =: attach.Id];
        //system.assertEquals(1, attachList.size());
        certi.Proof_of_Certification_Attached__c = true;
        delete attach;
        delete certi;
        Test.stopTest();
    }
}