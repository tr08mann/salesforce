/*******************************************************************************************************************
** Module Name   : TargetCloneExt
** Description   : To clone the Opportunity with corresponding Product list 
** Throws        : NA
** Calls         : NA
** Test Class    : TargetCloneExtUnitTest
** 
** Organization  : Rain Maker
**
** Revision History:-
** Version             Date            Author           WO#         Description of Action
** 1.0                                 Algo             1500
******************************************************************************************************************/
public class TargetCloneExt { 
    //Opplist to save the query record
    public List<Opportunity> opplist{get;set;}
    public String isProductSel{get;set;}
    
    public targetCloneExt(ApexPages.StandardController stdController) { 
       opplist = new list<Opportunity>{[Select o.id, o.StageName, o.CloseDate, o.Region_Opp__c, o.Name, o.Account.Name, o.LeadSource, o.Type, o.Renewal_Type__c,
                    o.Services_Proposal_Type__c,  o.Inside_Sales_NEW__c,  o.Primary_Strategic_Services__c,
                    o.Secondary_Strategic_Services__c, o.Training_BD_Rep__c, Pricebook2Id,  o.Primary_Strategic_Services_Director__c, o.Secondary_Strategic_Services_Directr__c,
                    o.Training_Strategic_Services_Director__c, o.MSS_Strategic_Services_Directr__c, o.Enterprise_Architect__c, o.Enterprise_Architect_Director_Approval__c,
                    o.Vendor_Business_Development_NEW__c, o.Action_Conclusion_Notes__c, o.AccountOwner__c, o.AccountId, o.User_Center_Account__c,o.Deadline__c, 
                    (Select Id, OpportunityId, SortOrder, PricebookEntryId, CurrencyIsoCode, Quantity,
                    TotalPrice, UnitPrice, ListPrice, ServiceDate, HasRevenueSchedule, HasQuantitySchedule, Description, HasSchedule,
                    ConnectionReceivedId, ConnectionSentId, MSRP__c, Discount__c, Line_Number__c, Distributor__c, Total_MSRP__c, 
                     Services_Rate_Type__c, Sales_Category__c, Sales_Rep_Commission_Rate__c, Sales_Rep_Commission__c, Inside_Sales_Commission_Rate__c, Inside_Sales_Commission__c, Adjusted_Unit_Cost__c, 
                     Extended_Cost__c, Profit__c, Extended_Profit__c, Profit_Percent__c, GSA__c, Currency_Discount__c, Product_Sync_ed_to_Solomon__c, Product_Vendor__c, Product_Family__c, Code__c, Originating_Opportunity__c, 
                    Cancelled_Fishnet_Services__c, Closed_Hours_Converted__c, Sales_Discount__c, NEW_FNS_Cost__c, New_FNS_Extended_Profit__c, Extended_List_Price__c, New_FNS_Cost_Category_M__c, 
                    Category_M__c, Serial__c, Start_Date__c, End_Date__c From OpportunityLineItems) 
                    From Opportunity o 
                    where o.Id =: stdController.getId() limit 1]}; 
       isProductSel = Apexpages.currentpage().getparameters().get('withproduct');              
                  
   }
   /* Onload method*/
   public PageReference onLoad() {
        list<OpportunityLineItem> prodlist = new list<OpportunityLineItem>();
        Opportunity cTarget = new Opportunity();
        if(!opplist.isEmpty()){
            cTarget = opplist.get(0).clone(false);
            cTarget.StageName = 'Not Fully Qualified (10%)/SVC-Meeting';
        }
        try {  
            if(cTarget != null){
                insert cTarget;
                if(String.isEmpty(isProductSel)){
                    System.debug(opplist[0].OpportunityLineItems +'opplist[0].OpportunityLineItems.......');
                    prodlist = new list<OpportunityLineItem>();
                    for(OpportunityLineItem pro: opplist[0].OpportunityLineItems){
                        OpportunityLineItem olineitemobj = new OpportunityLineItem();
                        olineitemobj.OpportunityId = cTarget.Id;
                        olineitemobj.Quantity = pro.Quantity;
                        olineitemobj.TotalPrice = pro.TotalPrice;
                        olineitemobj.pricebookEntryId = pro.PricebookEntryId;
                        olineitemobj.Services_Rate_Type__c = pro.Services_Rate_Type__c;
                        olineitemobj.Sales_Category__c = pro.Sales_Category__c;
                        olineitemobj.Line_Number__c = pro.Line_Number__c;
                        olineitemobj.Adjusted_Unit_Cost__c = pro.Adjusted_Unit_Cost__c;
                        olineitemobj.MSRP__c = pro.MSRP__c;
                        olineitemobj.GSA__c = pro.GSA__c;
                        olineitemobj.Description = pro.Description;
                        olineitemobj.ServiceDate = pro.ServiceDate;
                        olineitemobj.Serial__c = pro.Serial__c;
                        olineitemobj.Start_Date__c = pro.Start_Date__c;
                        olineitemobj.End_Date__c = pro.End_Date__c;
                        prodlist.add(olineitemobj);
                    }
                
                    insert prodlist;  
                    System.debug(prodlist+ 'prodlist........');       
                }
            }
        }catch (DmlException exc) { ApexPages.addMessages(exc); return null;
        }
        
      return new PageReference('/'+ cTarget.Id+ '/e?opp9=&00N60000001ljJU=&retURL=' + cTarget.Id); //?clone  =1');  
      
    }
}