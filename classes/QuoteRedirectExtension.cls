Public class QuoteRedirectExtension{

        public boolean Error{get;set;}
        private quote q = new quote();

        public QuoteRedirectExtension(ApexPages.StandardController stdController)
        {
            Error = false;
            this.q = [select Id, opportunityid from quote where Id = :apexpages.currentpage().getparameters().get('Id')];
            
            set<id> quoteidlist = new set<id>();
            set<id> quoteoppid = new set<id>();
            set<string> missingdeals = new set<string>();
            
            list<quotelineitem> qlis = [select id, pricebookentry.product2.renewable_yesno__c, pricebookentry.product2.id, pricebookentry.product2.productcode, pricebookentry.Product2.Product_Vendor_Profile__r.name from quotelineitem where quoteid = : q.id  and pricebookentry.product2.name != 'FishNet Security'];
            set<string> qlivendors = new set<string>();
            list<string> qlilinks = new list<string>();
            set<id> qliids = new set<id>();
            if(!qlis.isempty())
            {
                for(quotelineitem qli : qlis)
                {
                    string temp = qli.pricebookentry.Product2.Product_Vendor_Profile__r.name;
                    if (!qlivendors.contains(temp))
                    {
                        qlivendors.add(temp);
                    }
                    if(qli.pricebookentry.product2.renewable_yesno__c == null && !qliids.contains(qli.pricebookentry.product2.id))
                    {
                        string temp2 = '<a href="'+url.getSalesforceBaseUrl().toExternalForm()+'/'+qli.pricebookentry.product2.id+'" target="_blank">'+qli.pricebookentry.product2.productcode;
                        qlilinks.add(temp2);
                        qliids.add(qli.pricebookentry.product2.id);
                    }
                        
                }
            }
            list<deal_registration__c> deals = [select id, Vendor_Profile__r.name from deal_registration__c where vendor_profile__r.name like: qlivendors and opportunity__r.id =: q.opportunityid];
            set<string> dealvendors = new set<string>();
            if(!deals.isempty())
            {
                for(deal_registration__c d : deals)
                {
                    string temp = d.vendor_profile__r.name;
                    if (!dealvendors.contains(temp))
                    {  
                        dealvendors.add(temp);
                    }
                }
            }
            for(string s:qlivendors)
            {
                if(!dealvendors.contains(s))
                {
                    missingdeals.add(s);
                }
            }
            system.debug('qlivendors: '+qlivendors);
            system.debug('dealvendors : '+dealvendors );
            system.debug('missingdeals: '+missingdeals);
            if(!missingdeals.isempty())
            {
                Error = true;
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You must submit a deal registration record for the following vendor(s) before you may generate a quote: ' + missingdeals));
            }
            if(!qlilinks.isempty())
            {
                Error = true;
                apexpages.addmessage(new apexpages.message(apexpages.severity.ERROR, 'The following products must have the Renewable Y/N field populated before you may generate a quote: ' + qlilinks));
            }
        }
        public void Go(){}
}