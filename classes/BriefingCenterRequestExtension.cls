public with sharing class BriefingCenterRequestExtension {

    public Briefing_Center_Request__c BCR{get;set;}
    List<Briefing_Attendees__c> existingAttendees{get;set;}
    Set<Id> existingContactIds{get;set;}
    public List<Contact> available{get;set;}
    public List<Contact> fns{get;set;}        
    public List<Contact> vendor{get;set;}
    
    public List<ContactAttendee> attendees{get;set;}
    public List<FNSAttendee> fnsattendees{get;set;}
    public List<VendorAttendee> vendorattendees {get;set;}
        
        public BriefingCenterRequestExtension(Apexpages.StandardController controller)
        {
                attendees = new List<ContactAttendee>();
                fnsattendees = new List<FNSAttendee>();
                vendorattendees = new List<VendorAttendee>();             

                existingContactIds = new Set<Id>();
                if (controller.getId()!= null)
                {
                        existingContactIds = new Set<Id>();
                        vendor = new List<Contact>();
                        //Get the Account information for this Briefing Center Request
                        List<string> fieldNames = GetFieldNames('Briefing_Center_Request__c');
            //Construct SQL query String
            string query = 'SELECT ';
            for (string s:fieldNames)
            {
                query += 'Briefing_Center_Request__c.'+s+',';
            }
            //Remove last , from query
            //query = query.substring(0,query.length() - 1);
            //Add on Additional References
            query += 'Briefing_Center_Request__c.Account__r.Id, Briefing_Center_Request__c.Account__r.Name,Briefing_Center_Request__c.Vendor__r.Id FROM Briefing_Center_Request__c WHERE Briefing_Center_Request__c.Id =\''+controller.getId()+'\'';
                        BCR = Database.query(query);
                        /*
                        BCR = [SELECT
                        Briefing_Center_Request__c.Account__c,
                        Briefing_Center_Request__c.Account__r.Id,
                        Briefing_Center_Request__c.Account__r.Name,
                        Briefing_Center_Request__c.Vendor__c,
                        Briefing_Center_Request__c.Vendor__r.Id
                        FROM Briefing_Center_Request__c
                        WHERE Briefing_Center_Request__c.Id =: controller.getId()];
                        */
                        //Get a listing of Contacts already added to this BCR
                        existingAttendees = [SELECT
                        Briefing_Attendees__c.Id,
                        Briefing_Attendees__c.Contact__c,
                        Briefing_Attendees__c.Contact__r.Id
                        FROM Briefing_Attendees__c
                        WHERE Briefing_Attendees__c.Briefing_Center_Request__c =: BCR.Id];
                        for (Briefing_Attendees__c a:existingAttendees)
                        {
                                existingContactIds.add(a.Contact__r.Id);
                        }
                        //Now get All of the Contacts for this Account
                        available = new List<Contact>();
                        if (BCR.Account__r.Id != null)
                        {
                                available = [SELECT
                                Contact.Id,
                                Contact.FirstName,
                                Contact.LastName
                                FROM Contact
                                WHERE Contact.AccountId =: BCR.Account__r.Id
                                AND Contact.Id not IN :existingContactIds];
                        }
                        
                        //Now Get All of the Contacts for the Fishnet Accounts
                        
                        fns = [SELECT
                        Contact.Id,
                        Contact.FirstName,
                        Contact.LastName
                        FROM Contact
                        WHERE Contact.Account.Name = 'Fishnet Security, Inc. (Parent)'
                        AND Contact.Id not IN :existingContactIds];
                        
                        //Now Get all of the Contacts that are in the Vendor Contacts
                        vendor = new List<Contact>();
                        if (BCR.Vendor__r.Id != null)
                        {
                                vendor = [SELECT
                                Contact.Id,
                                Contact.FirstName,
                                Contact.LastName
                                FROM Contact
                                WHERE Contact.AccountId =: BCR.Vendor__r.Id
                                AND Contact.Id not IN :existingContactIds];
                        }
                        
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Vendor Id:'+BCR.Vendor__r.Id+' '+String.valueOf(vendor.size())));
                        
                        if (this.available.size() > 0)               
                                attendees.add(new ContactAttendee(available));
                        if (this.fns.size() > 0)
                                fnsattendees.add(new FNSAttendee(fns));
                        if (this.vendor.size() > 0)
                                vendorattendees.add(new VendorAttendee(vendor));
                }
                else //This is a new Briefing Center Request
                {
                        BCR = new Briefing_Center_Request__c();
                        //Populate FNS Attendees
                        fns = [SELECT
                        Contact.Id,
                        Contact.FirstName,
                        Contact.LastName
                        FROM Contact
                        WHERE Contact.Account.Name = 'Fishnet Security, Inc. (Parent)'
                        AND Contact.Id not IN :existingContactIds];
                        if (this.fns.size() > 0)
                                fnsattendees.add(new FNSAttendee(fns));
                }
                if (Apexpages.currentPage().getParameters().get('accid') != null && Apexpages.currentPage().getParameters().get('accid') != '')
                {
                        this.BCR.Account__c = Apexpages.currentPage().getParameters().get('accid');
                        //throw new CustomException(Apexpages.currentPage().getParameters().get('accid'));
                        PopulateAccount();
                }
                
        }
        public Boolean getshowAttendees()
        {
                return (this.attendees.size() > 0);
        }
        public Boolean getselectAccount()
        {
                return (this.BCR == null || this.BCR.Account__c == null);
        }
        public Boolean getshowFNSAttendees()
        {
                return (this.fnsattendees.size() > 0);
        }
        public Boolean getshowVendorAttendees()
        {
                return (this.vendorattendees.size() > 0);
        }
        public Boolean getselectVendor()
        {
                return (this.BCR == null || this.BCR.Vendor__c == null);
        }
        public void AddNewContactRow()
        {
                this.attendees.add(new ContactAttendee(available));
        }
        public void AddNewFNSRow()
        {
                this.fnsattendees.add(new FNSAttendee(fns));
        }
        public void AddNewVendorRow()
        {
                this.vendorattendees.add(new VendorAttendee(vendor));
        }
        
        public void PopulateAccount()
        {
                attendees = new List<ContactAttendee>();
                if (BCR.Account__c != null)
                {
                        available = [SELECT
                        Contact.Id,
                        Contact.FirstName,
                        Contact.LastName
                        FROM Contact
                        WHERE Contact.AccountId =: BCR.Account__c
                        AND Contact.Id not IN :existingContactIds];
                        if (this.available.size() > 0)               
                                attendees.add(new ContactAttendee(available));
                }
        }
        
        public void PopulateVendor()
        {
                vendorattendees = new List<VendorAttendee>();
                if (BCR.Vendor__c != null)
                {
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Vendor: '+BCR.Vendor__c));                
                        vendor = [SELECT
                        Contact.Id,
                        Contact.FirstName,
                        Contact.LastName
                        FROM Contact
                        WHERE Contact.AccountId =: BCR.Vendor__c
                        AND Contact.Id not IN :existingContactIds];
                        if (this.vendor.size() > 0)
                                        vendorattendees.add(new VendorAttendee(vendor));
                        }
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Vendor Attendee Count: '+vendorattendees.size()));
        }
        
        //Override Default Save
        public Pagereference Save()
        {
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,a.SelectedAttendee));
                List<Briefing_Attendees__c> toSave = new List<Briefing_Attendees__c>();
                Integer Counter = existingContactIds.size() + 1;
                for (ContactAttendee a:attendees)
                {
                        if (a.SelectedAttendee != 'SELECT')
                        {
                                Briefing_Attendees__c ba = new Briefing_Attendees__c();                         
                                ba.Contact__c = a.SelectedAttendee;
                                ba.Contact_Role__c = 'Customer';
                                toSave.add(ba);
                        }
                }
                for (FNSAttendee a:fnsattendees)
                {
                        if (a.SelectedFNS != 'SELECT')
                        {
                                Briefing_Attendees__c ba = new Briefing_Attendees__c();
                                ba.Contact__c = a.SelectedFNS;
                                ba.Contact_Role__c = 'Fishnet Employee';
                                toSave.add(ba);
                        }
                }
                for (VendorAttendee a:vendorattendees)
                {
                        if (a.SelectedVendor != 'SELECT')
                        {
                                Briefing_Attendees__c ba = new Briefing_Attendees__c();
                                ba.Contact__c = a.SelectedVendor;
                                ba.Contact_Role__c = 'Vendor';
                                toSave.add(ba);
                        }
                }
           //     if (toSave.size() <= 0)  
                if (toSave.size() <= 0 && existingContactIds.isEmpty())                 
                {
                        ApexPages.addMessage(new ApexPages.Message (ApexPages.Severity.Error,'ERROR: At least one (1) attendee must be added in order to save'));
                        return null;
                }
                else
                {
                        upsert(BCR);
                        //Get the Name of the Account for naming the Attendees
                        Briefing_Center_Request__c temp = [SELECT
                        Briefing_Center_Request__c.Id,
                        Briefing_Center_Request__c.Account__r.Id,
                        Briefing_Center_Request__c.Account__r.Name,
                        Briefing_Center_Request__c.Vendor__r.Id
                        FROM Briefing_Center_Request__c
                        WHERE Briefing_Center_Request__c.Id =: BCR.Id];
                        for (Briefing_Attendees__c ba:toSave)
                        {
                                ba.Briefing_Center_Request__c = BCR.Id;
                                ba.Name = temp.Account__r.Name+'-';
                                if (Counter < 10)
                                        ba.Name += '00'+Counter;
                                else if (Counter >= 10 && Counter <100)
                                        ba.Name += '0'+Counter;
                                else if (Counter >= 100)
                                        ba.Name += Counter;
                                Counter++;
                        }
                        insert(toSave);
                }                       
                return new Pagereference('/'+BCR.Id);
        }
        
        /* Custom Classes */
        
        public class ContactAttendee{
                public String SelectedAttendee{get;set;}                
                public List<Contact> Attendee{get;set;}
                public ContactAttendee(List<Contact> a)
                {
                        Attendee = a;
                }
                public List<SelectOption> getAttendees()
                {
                        List<SelectOption> o = new List<SelectOption>();
                        for (Contact c:Attendee)
                        {
                                if (c.FirstName != null)
                                        o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
                                else
                                        o.add(new SelectOption(c.Id,c.LastName));
                        }
                        o = SortOptionList(o);
                        if (o.size() >0)
                                o.add(0,new SelectOption('SELECT','SELECT'));
                        return o;
                }
        }
        
        public class FNSAttendee
        {
                public String SelectedFNS{get;set;}             
                public List<Contact> FNS{get;set;}
                public FNSAttendee(List<Contact> f)
                {
                        FNS = f;
                }
                public List<SelectOption> getAttendees()
                {
                        List<SelectOption> o = new List<SelectOption>();
                        for (Contact c:FNS)
                        {
                                if (c.FirstName != null)
                                        o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
                                else
                                        o.add(new SelectOption(c.Id,c.LastName));
                        }
                        o = SortOptionList(o);
                        if (o.size() >0)
                                o.add(0,new SelectOption('SELECT','SELECT'));
                        return o;
                }
        }
        
        public class VendorAttendee
        {
                public String SelectedVendor{get;set;}          
                public List<Contact> Vendor{get;set;}
                public VendorAttendee(List<Contact> v)
                {
                        Vendor = v;
                }
                public List<SelectOption> getAttendees()
                {
                        List<SelectOption> o = new List<SelectOption>();
                        for (Contact c:Vendor)
                        {
                                if (c.FirstName != null)
                                        o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
                                else
                                        o.add(new SelectOption(c.Id,c.LastName));
                        }
                        o = SortOptionList(o);
                        if (o.size() >0)
                                o.add(0,new SelectOption('SELECT','SELECT'));
                        return o;
                }
        }
        
        
        
        
        /*Utilities*/
        
        private static List<SelectOption> SortOptionList(List<SelectOption> ListToSort)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
            return ListToSort;
        List<SelectOption> Less = new List<SelectOption>();
        List<SelectOption> Greater = new List<SelectOption>();
        integer pivot = ListToSort.size() / 2;
        // save the pivot and remove it from the list
        SelectOption pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        for(SelectOption x : ListToSort)
        {
            if(x.getLabel() <= pivotValue.getLabel())
                Less.add(x);
            else if(x.getLabel() > pivotValue.getLabel()) Greater.add(x);   
        }
        List<SelectOption> returnList = new List<SelectOption> ();
        returnList.addAll(SortOptionList(Less));
        returnList.add(pivotValue);
        returnList.addAll(SortOptionList(Greater));
        return returnList; 
    }
    
    private List<String> GetFieldNames(String objectname)
        {
                List<String> retval = new List<String>();
                Map<String,Schema.SObjectField> fields;
                //Use GlobalDescribe to get a list of all available Objects
                Map<String,Schema.Sobjecttype> gd = Schema.getGlobalDescribe();
                Set<String> objectKeys = gd.keySet();
                for (String objectKey:objectKeys) //Iterate through all of the objects until we get to the one we need
                {
                        if (objectKey == objectname.toLowerCase()) //Object exists, get field names
                        {
                                Schema.SObjectType systemObjectType = gd.get(objectKey);
                                Schema.DescribeSObjectResult r = systemObjectType.getDescribe();
                                Map<String, Schema.SObjectField> M = r.fields.getMap();
                                Set <String>fieldNames = M.keySet();
                                fields = new Map<String,Schema.SObjectField>();
                                //Create a copy of the Map with the Display Name, and Properties for retreival in the Selection
                                for (String fieldName:fieldNames)
                                {
                                        //For each field, Add to List
                                        Schema.SObjectField field = M.get(fieldName);
                                        Schema.DescribeFieldResult fieldDesc = field.getDescribe();                                     
                                        //retval.add(fieldDesc.getLabel()); //Adds the Salesforce UI label to listing
                                        retval.add(fieldDesc.getName());
                                        fields.put(fieldDesc.getLabel(),field);
                                }
                        }
                }                               
                return retval;
        }
    public class CustomException extends Exception {}
}