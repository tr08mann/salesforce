@isTest
public class RedirectExtension_Test {

    static testmethod void testRedirectExt()
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;  
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        ApexPages.currentPage().getParameters().put('id',opp.Id);    
        
        //Create controller instance
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);        
        
        RedirectExtension rt = new RedirectExtension(sc);
        
        rt.Url = '/apex/testPage?id=09827272628222?pj=0292829282203';
        rt.ParameterCollection = 'id='+opp.id+';';
        
        Pagereference p = rt.Redirect();
        System.assert(p!=null);
    }
}