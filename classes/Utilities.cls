public with sharing class Utilities {

    private static Boolean salesRepLocationFirstRun;


    public static Boolean BypassTriggers()
    {
        Boolean retval = false;
        try
        {
            //Get the profileId of the logged in user
            Id profileId;
            profileId = UserInfo.getProfileId();
            if (profileId == System.label.BypassTriggerProfileId)
                retval = true;
        }
        catch (Exception ex)
        {
            //Do nothing, we'll still return a null value if an issue occurs
        }       
        return retval;
    }
    
    /* Bypass Trigger Test */
    static testMethod void TestBypassTriggers()
    {
        Utilities.CreateCustomTriggerMesssage('test1<br>test2</br>', true);
        Utilities.SalesRepLocationFirstRun();
        Utilities.SalesRepLocationFirstRunCompleted();
        Utilities.BypassTriggers();
        //Run as a Bypassed User if present
        List<User> users = new List<User>
        ([SELECT User.Id FROM User WHERE User.ProfileId =: System.label.BypassTriggerProfileId]);
        if (!users.isEmpty())
        {
            System.runAs(users[0])
            {
               
                Utilities.BypassTriggers();
               
            }
        }
    }
        
    public static Boolean SalesRepLocationFirstRun()
    {
        if(salesRepLocationFirstRun == null)
            salesRepLocationFirstRun = true;
        return salesRepLocationFirstRun;
    }
    public static void SalesRepLocationFirstRunCompleted()
    {
        salesRepLocationFirstRun = false;
    }
    

    
    public static string CreateCustomTriggerMesssage(string s, Boolean alert)
    {
        string retval = '';
        //Support for Native SF Single Entered Item
        retval += '<script>document.getElementById("errorDiv_ep").innerHTML ="";window.onload = function(){document.getElementById("errorDiv_ep").innerHTML = "</br></br>'+s+'</br></br>";};';
        if (alert)
        {
            string replacedBR = s.replace('<br>','');
            replacedBR = replacedBR.replace('</br>', '.');
            replacedBR = replacedBR.replace('\'', '\\\'');
            retval += 'alert(\''+replacedBR+'\');';
        }
        retval += '</script>';
        //Support for Native Edit All function Saving
        retval += '<script>window.onload = function(){var count = document.getElementsByClassName("errorStyle").length;for (var i=0;i<count;i++){document.getElementsByClassName("errorStyle")[i].innerHTML ="";document.getElementsByClassName("errorStyle")[i].innerHTML = "</br></br>'+s+'</br></br>";};}';
        retval += '</script>';
        return retval;
    }
}