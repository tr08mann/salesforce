@isTest
public class TestDataFactory{
//this class is designed to create data for test classes
    
    public static Account createStandardAccount(string rectype, string name)
    {
        //pass the recordtype developer name in the call method 
        //pass the name in the call method
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get(rectype).getRecordTypeId();
        
        account acc = new account();
        acc.recordtypeid = rtId;
        acc.numberofemployees = 100;
        acc.credit_limit__c = 100000000;
        acc.Credit_Available__c = 100000000;
        acc.SL_Company_Identifier__c = 'US';
        acc.custid__c = '1234';
        acc.name = name;
        acc.billingcountry = 'US';
        acc.billingstate = 'KS';
        acc.ownerid = userinfo.getuserid();
        acc.CurrencyIsoCode = 'USD';
        return acc;
    }
    
    public static Opportunity createStandardOpp(string rectype, id accountId, string stage)
    { 
        //pass the recordtype developer name in the call method
        //pass the account id in the call method
        //record types are: Commercial Opportunity, Federal Opportunity, Locked Commercial Opportunity
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> OpportunityRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = OpportunityRecordTypeInfo.get(rectype).getRecordTypeId();
        
        opportunity opp = new opportunity();
        opp.accountid = accountId;
        opp.closedate = date.today().addyears(1);
        opp.recordtypeid = rtId;
        opp.stagename = stage;
        opp.Region_Opp__c = 'Central';
        opp.name = 'Test_Test_Test';
        opp.type = 'Customer';
        opp.leadsource = 'Other';
        opp.services_proposal_type__c = 'VAR Only';
        opp.ownerid = userinfo.getuserid();
        opp.deal_reg_desk_rep__c = userinfo.getuserid();
        opp.inside_sales_NEW__c = userinfo.getuserid();
        opp.CurrencyIsoCode = 'USD';
        opp.Pricebook2Id = test.getStandardPricebookId();
        return opp;
    }
    
    public static Vendor_Profile__c createVendorProfile(string name)
    {
        //pass the vendor name in the call method
        vendor_profile__c vp = new vendor_profile__c();
        vp.name = name;
        vp.CurrencyIsoCode = 'USD';
        return vp;
    }
    
    public static Deal_Registration__c createStandardDealReg(id OppId, id VPId, string vName, string OppNumber, string DRStatus)
    {
        //pass the opp id in the call method
        //pass the vendor profile id in the call method
        //pass vendor name in the call method
        //pass opp number in the call method
        //pass deal reg status in the call method
        deal_registration__c dr = new deal_registration__c();
        dr.name= vName+'_Test_'+OppNumber;
        dr.vendor_profile__c = VPId;
        dr.opportunity__c = OppId;
        dr.deal_registration_status__c = DRStatus;
        dr.Deal_Registration_Submitted_Date__c = system.today();
        dr.Deal_Registration_Approved_Denied_Date__c = system.today();
        dr.Deal_Expiration__c = system.today().adddays(31);
        dr.Deal_Registration_Code__c = 'N/A';
        dr.deal_registration_denied_reason__c = 'Opportunity did not qualify for deal registration';
        dr.deal_reg_did_not_qualify_reason__c = 'DNQ';
        dr.Deal_Reg_Closed_Canceled_Rejected_Reason__c = 'Canceled by FishNet Sales Team';
        dr.Opportunity_Already_Registered__c = OppId;
        dr.CurrencyIsoCode = 'USD';
        return dr;
    }
    
    public static Lead createStandardLead(string accName)
    {
        //pass the account name in the call method
        //pass the account id in the call method
        lead l = new lead();
        l.ownerid = userinfo.getuserid();
        l.phone = '1234567890';
        l.industry = 'Education';
        l.Lead_Type__c = 'Customer';
        l.rating = 'Warm';
        l.leadsource = 'Customer';
        l.status = 'Open - Not Contacted';
        l.company = accName;
        l.lastname = 'Test';
        l.CurrencyIsoCode = 'USD';
        return l;
    }
    
    public static Lead createConvertedLead(string accName, id AccID)
    {
        //pass the account name in the call method
        //pass the account id in the call method
        lead l = new lead();
        l.ownerid = userinfo.getuserid();
        l.phone = '1234567890';
        l.industry = 'Education';
        l.Lead_Type__c = 'Customer';
        l.rating = 'Warm';
        l.leadsource = 'Customer';
        l.status = 'Open - Not Contacted';
        l.company = accName;
        l.lastname = 'Test';
        l.convertedaccountid = AccID;
        l.isconverted = true;
        l.web_interest__c = 'test';
        l.lead_import_notes__c = 'test';
        l.CurrencyIsoCode = 'USD';
        return l;
    }
    
    public static Contact createStandardContact(id AccID)
    {
        //pass the account id in the call method
        contact c = new contact();
        c.firstname = 'First';
        c.lastname = 'Last';
        c.email = 'test@testorg.com';
        c.phone = '1234567890';
        c.accountid = accid;
        c.CurrencyIsoCode = 'USD';
        return c;
    }
    
    public static Product2 createStandardProduct(id VendorID, string prodname, string sku)
    {
        //pass the vendor profile id in the call method
        //pass the product name in the call method
        Product2 p = new Product2();
        p.name = prodname;
        p.product_vendor_profile__c = VendorID;
        p.ProductCode = sku;
        p.isActive = true;
        p.CurrencyIsoCode = 'USD';
        return p;
    }
    
    public static PricebookEntry createPricebookEntry(id prodID)
    {
        //pass the product ID in the call method
        id pricebookid = test.getStandardPricebookId();
        pricebookentry pbe = new Pricebookentry();
        pbe.pricebook2id = pricebookid;
        pbe.product2id = prodID;
        pbe.unitprice = 1.0;
        pbe.isActive = true;
        pbe.CurrencyIsoCode = 'USD';
        return pbe;
    }
    
    public static OpportunityLineItem createOppLineItem(id oppID, id pbeID)
    {
        //pass the opportunity id in the call method
        //pass the pricebookentry id in the call method
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.quantity = 1;
        oli.Sales_Category__c = 'New Customer/Business - Renewable Item';
        oli.Line_Number__c = 1;
        oli.UnitPrice = 1;
        oli.OpportunityId = oppid;
        oli.Discount = 5;
        oli.PricebookEntryId = pbeID;
        return oli;
    }
    
    public static User createStandardUser(string prefix)
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='First Team Advanced User'];
        user u = new user();
        u.Alias = 'standt';
        u.Email = prefix + 'standarduser@testorg.com';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Testing';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId = p.id;
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.UserName = prefix + 'standarduser@testorg.com';
        u.Employee_ID__c = 8500;
        return u;
    }
    
    public static Purchase_Order__c createPurchaseOrder(id vpId, id oppid)
    {
        purchase_order__c p = new purchase_order__c();
        p.name = '12345';
        p.PO_Vendor_Profile__c = vpId;
        p.PO_Status__c = 'Submitted';
        p.Fulfillment_Type__c = 'Electronic';
        p.Vendor_Estimated_Delivery_date__c = system.today();
        p.Opportunity__c = oppid;
        p.CurrencyIsoCode = 'USD';
        return p;
    }
    
    public static Case createCase(string rectype, id conid)
    {
        //record types are SF.com Support Case, SF.com Support Case - System Enhancement Case
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> CaseRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = CaseRecordTypeInfo.get(rectype).getRecordTypeId();
        
        case c = new case();
        c.RecordTypeid = rtId;
        c.Reason = 'Existing Feature Change';
        c.Origin = 'Web';
        c.related_object__C = 'Account';
        c.Department__c = 'Account Executive';
        c.Case_Escalation_Level__c = 'Level 2';
        c.status = 'New';
        c.Next_Step_Case__c = 'Step 1';
        c.Case_Follow_up_Date__c = system.today();
        c.ContactId = conid;
        c.Enhancement_Meeting_Status__c = 'New Request';
        c.OwnerId = userinfo.getuserid();
        c.Create_Follow_up_Task__c = true;
        c.CurrencyIsoCode = 'USD';
        return c;
    }
    
    public static Task createTask(id whatid, id ownerid)
    {
        task t = new task();
        t.whatid = whatid;
        t.Status = 'Not Started';
        t.Subject = 'Email';
        t.Priority = 'Normal';
        t.OwnerId = ownerid;
        t.CurrencyIsoCode = 'USD';
        return t;
    }
    
    public static Task createWhoTask(id whoid, id ownerid)
    {
        task t = new task();
        t.whoid = whoid;
        t.Status = 'Not Started';
        t.Subject = 'Email';
        t.Priority = 'Normal';
        t.OwnerId = ownerid;
        t.CurrencyIsoCode = 'USD';
        return t;
    }
    
    public static Quote createStandardQuote(id oppid, id vpid)
    {
        Quote q = new Quote();
        q.OpportunityID = oppid;
        q.Name = 'QuoteTest';
        q.Quote_Vendor_Profile__c = vpid;
        q.Pricebook2Id = test.getStandardPricebookId();
        q.Product_Support_Options__c = 'Fishnet Frontline Support Not Available';
        return q;
    }
    
    public static QuoteLineItem createQuoteLineItem(id quoteID, id pbeID)
    {
        //pass the quote id in the call method
        //pass the pricebookentry id in the call method
        QuoteLineItem qli = new QuoteLineItem();
        qli.Quantity = 1;
        qli.Sales_Category__c = 'New Customer/Business - Renewable Item';
        qli.Line__c = 1;
        qli.Cost__c = 5;
        qli.UnitPrice = 10;
        qli.QuoteId = quoteid;
        qli.Discount = 5;
        qli.PricebookEntryId = pbeID;
        qli.Pricing_Method__c = 'Cost + %';
        qli.Amount__c = 101;
        return qli;
    }
    
    public static OpportunityContactRole createOppContactRole(id oppid, id contactid)
    {
        //pass the opportunity id in the call method
        //pass the contact id in the call method
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = contactid;
        ocr.IsPrimary = true;
        ocr.OpportunityId = oppid;
        ocr.Role = 'Quote Contact';
        return ocr;
    }
    
    public static SOW__c createStandardSOW(id oppid, id conid)
    {
        //pass the oppid in the call method
        SOW__c sow = new SOW__c();
        sow.Project_Title__c = 'Test';
        sow.Document_Type__c = 'SOW';
        sow.Status__c = 'Submit for Approval';
        sow.Scope__c = 'Yes';
        sow.Rate__c = 'Yes';
        sow.Rate_Details__c = 'Test';
        sow.Scope_Detail__c = 'Test';
        sow.Custom_Language__c = 'N/A';
        sow.Custom_Language_Details__c = 'Test';
        sow.Scoping_Approval_Notes__c = 'Test';
        sow.Client_Template__c = 'No';
        sow.Multiple_Practice_SOW__c = 'Yes';
        sow.Line_of_Business__c = 'VAR';
        sow.Description__c = 'Test';
        sow.SLA_Justification__c = 'Test';
        sow.Solution_Set__c = 'Managed Services';
        sow.opportunity__c = oppid;
        sow.SOW__c = 'Test';
        sow.CurrencyIsoCode = 'USD';
        sow.AppSec_Approval__c = TRUE;
        sow.Practice_Approval_Due_Date__c = date.today();
        sow.Client_Contact_Info__c = conid;
        return sow;
    }
    
    public static Briefing_Center_Request__c createBriefingCenterRequest(id accid)
    {
        Briefing_Center_Request__c bcr = new Briefing_Center_Request__c();
        bcr.Account__c = accid;
        bcr.Vendor__c = accid;
        return bcr;
    }
    
    public static SFDC_520_Quote__c createOldQuote(id oppid)
    {
        SFDC_520_Quote__c oldquote = new SFDC_520_Quote__c();
        oldquote.Opportunity__c = oppid;
        return oldquote;
    }
    
    public static Sales_Rep_Location__c createSalesRepLocation()
    {
        Sales_Rep_Location__c srl = new Sales_Rep_Location__c();
        srl.name = 'Detroit';
        srl.CurrencyIsoCode = 'USD';
        return srl;
    }
    
    public static Address__c createAddress(id accid, string prefix)
    {
        Address__c adr = new Address__c();
        adr.address_account__C = accId;
        adr.Company_Name__c = 'Company Test';
        adr.Address_City__c = 'Overland Park';
        adr.Address_Country__c = 'US';
        adr.Address_Line_1__c = '1234 Main St.';
        adr.Address_State__c = 'KS';
        adr.Address_Zip__c = '66212';
        adr.Attention_To__c = prefix;
        adr.CurrencyIsoCode = 'USD';
        return adr;
    }
    
    public static Attachment createAttachment(id ownerid, id parentid)
    {
        Attachment att = new Attachment();
        att.name = 'Test File';
        att.ownerid = ownerid;
        att.parentid = parentid;
        return att;
    }
    
    public static Campaign createCampaign()
    {
        Campaign camp = new Campaign();
        camp.name = 'Test Campaign';
        camp.CurrencyIsoCode = 'USD';
        return camp;
    }
    
    public static CampaignMember createCampaignMember(id campid, id leadid)
    {
        CampaignMember cmem = new CampaignMember();
        cmem.CampaignId = campid;
        cmem.LeadId = leadid;
        cmem.status = 'Sent'; // Values: (Attended,Registered,Responded,Sent)
        cmem.CurrencyIsoCode = 'USD';
        return cmem;
    }
    
    public static Invoice_2__c createInvoice(id accid, id oppid)
    {
        Invoice_2__c inv = new Invoice_2__c();
        inv.name = 'Test';
        inv.Account__c = accid;
        inv.Opportunity__c = oppid;
        inv.CurrencyIsoCode = 'USD';
        return inv;
    }
    
    public static EmailMessage createEmailMessage(id parentid, id activityid)
    {
        EmailMessage em = new EmailMessage();
        em.ParentId = parentid;
        em.ActivityId = activityid;
        em.Subject = 'Test';
        em.TextBody = 'Test';
        em.Incoming = true;
        return em;
    }
}