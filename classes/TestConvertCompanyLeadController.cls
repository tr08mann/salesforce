@isTest
public with sharing class TestConvertCompanyLeadController {

    public static Testmethod void testConvertCompanyLeadMethod()
    {
        Lead NewLead = TestDataFactory.createStandardLead('NameCo');
        insert NewLead;
        
        Campaign TestCampaign0 = TestDataFactory.createCampaign();
        TestCampaign0.Type = 'Telemarketing'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        insert TestCampaign0;
        
        Campaign TestCampaign1 = TestDataFactory.createCampaign();
        TestCampaign1.Type = 'Seminar'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        insert TestCampaign1;
        
        Campaign TestCampaign2 = TestDataFactory.createCampaign();
        TestCampaign2.Type = 'Banner Ads'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        insert TestCampaign2;
        
        Campaign TestCampaign3 = TestDataFactory.createCampaign();
        TestCampaign3.Type = 'Telemarketing'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        insert TestCampaign3;
        
        CampaignMember TestCampaignMember0 = TestDataFactory.createCampaignMember(TestCampaign0.id, NewLead.id);         
        insert TestCampaignMember0;
        
        CampaignMember TestCampaignMember1 = TestDataFactory.createCampaignMember(TestCampaign1.id, NewLead.id);
        insert TestCampaignMember1;
        
        CampaignMember TestCampaignMember2 = TestDataFactory.createCampaignMember(TestCampaign2.id, NewLead.id);
        insert TestCampaignMember2;
        
        CampaignMember TestCampaignMember3 = TestDataFactory.createCampaignMember(TestCampaign3.id, NewLead.id);        
        insert TestCampaignMember3;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Verbal (90%)/SVC-Awaiting signed SOW');
        opp.LeadSource = 'Fishnet Sales - LEAD Team Member';
        insert opp;
        
        convertCompanyLeadController CLC = new convertCompanyLeadController(NewLead.Company, NewLead.Id);
        convertCompanyLeadController CLC1 = new convertCompanyLeadController();
        
        CLC.lookupLead();
        CLC.ci.OppName = 'Testing1_2_3';
        CLC.leadMerge();
        CLC.doHandlePrimaryOpportunity(opp);
        CLC.getLead();
        CLC.getInitialConversionComplete();
        CLC.getMaxNumberToConvertAtOnce();
        CLC.getList();
        CLC.getAlert();
        CLC.getInfo();
        CLC.getLead();
        CLC.getAlreadyCreated();
        convertCompanyLeadController.leadwrapper ld= new convertCompanyLeadController.leadwrapper(NewLead);
        CLC.getLeads();
        CLC.doNothing();
        CLC.RemoveTestRecords();
        
        Lead NewLead1 = TestDataFactory.createStandardLead('NameCo');
        insert NewLead1;
        
        //CLC.AddTestRecords();
        //convertCompanyLeadController.doNothing2();
    }
}