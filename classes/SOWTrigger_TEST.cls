@isTest
public class SOWTrigger_TEST 
{
    static testmethod void testISRTask()
    {
        account a = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert a;
        
        contact c = TestDataFactory.createStandardContact(a.id);
        insert c;
        
        opportunity o = TestDataFactory.createStandardOpp('Commercial Opportunity', a.id, 'Verbal (90%)/SVC-Awaiting signed SOW');
        insert o;

        SOW__c s = TestDataFactory.createStandardSOW(o.id, c.id);
        insert s;
        
        s.Task_ISR_to_update_Opp__c = true;
        update s;
    }
    static testmethod void testDueDate()
    {
        account a = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert a;
        
        contact c = TestDataFactory.createStandardContact(a.id);
        insert c;
        
        opportunity o = TestDataFactory.createStandardOpp('Commercial Opportunity', a.id, 'Verbal (90%)/SVC-Awaiting signed SOW');
        insert o;

        SOW__c s = TestDataFactory.createStandardSOW(o.id, c.id);
        insert s;
        s.SLA3__c = 4;
        update s;
        s.SLA3__c = 9;
        update s;
        s.SLA3__c = 11;
        update s;
    }
}