Public class DealRegTask
{
    public static list<Task> updateTask(Id DealRegId, String Status, list<String> Subject)
    {
        list<task> ts = [select id, status from task where whatid =: DealRegId and status != 'Completed' and subject in: Subject];
        list<task> retval = new list<task>();
        for(task t: ts)
        {
            t.status = Status;
            retval.add(t);
        }
        return retval;
    }
    public static Task getTask(String DealRegName, Id DealRegId, String VendorName, Date ExpDate, String Subject, Id ownerId )
    {
        Date tempdate = ExpDate;
        Task ts = new Task();
        ts.WhatId = DealRegId;
        ts.Vendor_Name__c = VendorName;
        ts.Subject = Subject+'_'+DealRegName;
        if(tempdate.daysBetween(tempdate.toStartOfWeek())== -6) //Saturday
        {
            tempdate = tempdate.addDays(-1);
        }
        else if(tempdate.daysBetween(tempdate.toStartOfWeek()) == 0) //Sunday
        {
            tempdate = tempdate.addDays(-2);
        }
        ts.ActivityDate = tempdate;
        if (ownerId != null)
        {
            ts.OwnerId = ownerId;
        }
        else
        {
            throw new MyException('There is no Deal Reg Desk Rep associated to this opportunity. Please add a Deal Reg Desk Rep to the opportunity to continue.');
        }
        return ts;
    }
     public class MyException extends Exception{}
  }