@isTest
public class QuoteSyncutilTest{
    
    static testmethod void doTest()
    {  
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
        insert deal;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        QuoteSyncUtil.getNewQuoteIds();
    }
    
    static testmethod void doTest3()
    {
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
        insert deal;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        QuoteSyncUtil.getNewQuoteIds();
        QuoteSyncUtil.addNewQuoteId(quote.id);
        QuoteSyncUtil.removeNewQuoteId(quote.id);
        Set<Id> sq = new Set<Id>();
        sq.add(quote.Id);
        QuoteSyncUtil.removeAllNewQuoteIds(sq); 
        QuoteSyncUtil.clearNewQuoteIds();
        QuoteSyncUtil.isNewQuote(quote.id);
        String quoteField = 'description';
        QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
        String quoteLineField = 'description';   
        QuoteSyncUtil.getQuoteLineFieldMapTo(quoteLineField);
    }
    
    static testmethod void doTest4()
    {  
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
        insert deal;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        QuoteSyncUtil.getNewQuoteIds();
        QuoteSyncUtil.addNewQuoteId(quote.id);
        QuoteSyncUtil.removeNewQuoteId(quote.id);
        Set<Id> sq = new Set<Id>();
        sq.add(quote.Id);
        QuoteSyncUtil.removeAllNewQuoteIds(sq); 
        QuoteSyncUtil.clearNewQuoteIds();
        QuoteSyncUtil.isNewQuote(quote.id);
        String quoteField = 'description';
        QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
        String quoteLineField = 'description';   
        QuoteSyncUtil.getQuoteLineFieldMapTo(quoteLineField);
        QuoteSyncUtil.addQuoteField('description','description');
        QuoteSyncUtil.getQuoteFieldsString();
        QuoteSyncUtil.getOppFieldsString();
        QuoteSyncUtil.addQuoteLineField('description','description');
        QuoteSyncUtil.removeQuoteField('description');
        QuoteSyncUtil.removeQuoteLineField('description');
        QuoteSyncUtil.getField('Quote', 'description');
        QuoteSyncUtil.getField('QuoteLineItem', 'description');
    }
    
    static testmethod void doTest5()
    {
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
        insert deal;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;       
        
        QuoteSyncUtil.getNewQuoteIds();
        QuoteSyncUtil.addNewQuoteId(quote.id);
        QuoteSyncUtil.removeNewQuoteId(quote.id);
        Set<Id> sq = new Set<Id>();
        sq.add(quote.Id);
        QuoteSyncUtil.removeAllNewQuoteIds(sq); 
        QuoteSyncUtil.clearNewQuoteIds();
        QuoteSyncUtil.isNewQuote(quote.id);
        String quoteField = 'description';
        QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
        String quoteLineField = 'description';   
        QuoteSyncUtil.getQuoteLineFieldMapTo(quoteLineField);
    }
    
    /** ORIGINAL     */
    
    static testmethod void doTest1()
    {  
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
        insert deal;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        QuoteSyncUtil.getNewQuoteIds();
        QuoteSyncUtil.addNewQuoteId(quote.id);
        QuoteSyncUtil.removeNewQuoteId(quote.id);
        Set<Id> sq = new Set<Id>();
        sq.add(quote.Id);
        QuoteSyncUtil.removeAllNewQuoteIds(sq); 
        QuoteSyncUtil.clearNewQuoteIds();
        QuoteSyncUtil.isNewQuote(quote.id);
        String quoteField = 'description';
        QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
        String quoteLineField = 'description';   
        QuoteSyncUtil.getQuoteLineFieldMapTo(quoteLineField);
        QuoteSyncUtil.addQuoteField('description','description');
        QuoteSyncUtil.getQuoteFieldsString();
        QuoteSyncUtil.getOppFieldsString();
        QuoteSyncUtil.addQuoteLineField('description','description');
        QuoteSyncUtil.removeQuoteField('description');
        QuoteSyncUtil.removeQuoteLineField('description');
        QuoteSyncUtil.getField('Quote', 'description');
        QuoteSyncUtil.getField('QuoteLineItem', 'description');
        QuoteSyncUtil.getField('Opportunity', 'description');
        QuoteSyncUtil.getField('OpportunityLineItem', 'description');
        QuoteSyncUtil.populateRequiredFields(quote);
        QuoteSyncUtil.populateRequiredFields(opp);
        QuoteSyncUtil.populateRequiredFields(oli);
        Schema.DescribeFieldResult f = Schema.sObjectType.Quote.fields.Name;
        QuoteSyncUtil.createValue('test','test1',f);
        Schema.DescribeFieldResult f1 = Schema.sObjectType.Quote.fields.Phone;
        QuoteSyncUtil.createValue('test','test1',f1);
        QuoteSyncUtil.getQuoteLineFieldsString();
        QuoteSyncUtil.getOppLineFieldsString();
    }
}