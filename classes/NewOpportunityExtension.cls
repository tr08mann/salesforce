public with sharing class NewOpportunityExtension {
/****************************************************************************************************
    *Description:   Opportunity - Create new opportunity screen
    *               Customized create opportunity screen to reduce size of page and allow more functionality
    *        
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     6/18/2014    Alex Schach         Initial version
    ******************************************************************************************************/ 
    
    public NewOpportunityExtension(ApexPages.StandardController controller)
    {
        string acctid = apexpages.currentpage().getparameters().get('accid');
        opp = new opportunity();
        opp.stagename = 'Not Fully Qualified (10%)/SVC-Meeting';
        opp.currencyisocode = 'USD';
        if(apexpages.currentpage().getparameters().get('RecordType') == '012300000000Prx')
        {
            opp.region_opp__c = 'Federal';
        }
        if(acctid != null)
        {
            opp.accountid = acctid;
        }
    }
    public opportunity opp {get;set;}
    public pagereference save()
    {
        opp.pricebook2id = '01s300000000OT4AAM'; //sets the pricebook of the opp to avoid future problems if user skips adding products to the opp
        if(apexpages.currentpage().getparameters().get('RecordType') != null)
        {
            opp.recordtypeid = apexpages.currentpage().getparameters().get('RecordType');
        }
        insert opp;
        
        //go to standard add product page  
        pagereference addproduct = new pagereference('/p/opp/SelectSearch?addTo='+opp.id+'&retURL=%2F006%2Fe');
        addproduct.setredirect(true);
        return addproduct;
        
        //go straight to created opp
        /*pagereference gotopage = new apexpages.standardcontroller(opp).view();
        gotopage.setredirect(true);
        return gotopage;*/
    }
    public pagereference cancel()
    {
        string acctid = apexpages.currentpage().getparameters().get('accid');
        if(acctid != null)
        {
            pagereference goaccount = new pagereference('/' + acctid);
            goaccount.setredirect(true);
            return goaccount;
        }
        else
        {
            pagereference gocancel = new pagereference('/006/o');
            gocancel.setredirect(true);
            return gocancel;
        }
    }
    public NewOpportunityExtension()
    {
    }
}