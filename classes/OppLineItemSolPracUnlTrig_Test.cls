@isTest
public class OppLineItemSolPracUnlTrig_Test{

    static testmethod void Testdelete()
    {           
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.id, 'FishNet Security', '12345');
        p.Family = 'Enterprise Architecture';
        p.Solution_Set__c = 'Infrastructure and Operations';
        p.Practice__c = 'Data Security';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        OpportunityLineItem ol = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert ol;
        
        delete oli;
    }
}