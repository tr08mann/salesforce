public with sharing class OpportunityMSSContract {
	//Create a Map listing of Contracts by Account Id
	private static Map<Id,List<Contract>> contracts = new Map<id,List<Contract>>();
	//Set to hold Contact AccountIds to reduce SOQL calls
	private static Set<Id> AccountIds = new Set<Id>();
	
	public static void Process(List<Opportunity> opportunities)
	{
		//Get a listing of Contracts for the Opportunity.Account if it has not already been populated
		for (Opportunity opportunity:opportunities)
		{
			if (opportunity.AccountId != null && !AccountIds.contains(opportunity.AccountId))
				AccountIds.add(opportunity.AccountId);
		}
		//Query once for the Contracts
		if (!AccountIds.isEmpty())
		{
			//Get all contracts for Accounts
			List<Contract> contract = new List<Contract>();
			contract = [SELECT
				Contract.Id,
				Contract.RecordTypeId,
				Contract.RecordType.Name,
				Contract.AccountId
				FROM Contract
				WHERE Contract.AccountId IN :AccountIds
				AND Contract.RecordType.Name = 'Fishnet Managed Services Support Contract'];
			if (!contract.isEmpty())
			{
				for (Contract c:contract)
				{
					List<Contract> accountContracts = contracts.get(c.AccountId);
					if (accountContracts == null)
						accountContracts = new List<Contract>();
					accountContracts.add(c);
					contracts.put(c.AccountId,accountContracts);
				}
			}
			/*
			if (!contracts.containsKey(opportunity.AccountId))
			{
				List<Contract> tempcontracts = new List<Contract>();
				tempcontracts = [SELECT
				Contract.Id,
				Contract.RecordTypeId,
				Contract.RecordType.Name
				FROM Contract
				WHERE Contract.AccountId =: opportunity.AccountId
				AND Contract.RecordType.Name = 'Fishnet Managed Services Support Contract'];
				contracts.put(opportunity.AccountId,tempcontracts);
			}
			*/
			//Retreive Listing of Contracts
			for (Opportunity opportunity:opportunities)
			{
				List<Contract> activeMMSContracts = contracts.get(opportunity.AccountId);
				if (activeMMSContracts.size()>0 && opportunity.Pro_Services_Order_Status__c == 'Submitted to Professional Services')
				{
					opportunity.Pro_Services_Internal_Notes__c = 'Active MSS Contract';
				}
			}
		}
	}
	static testMethod void testOpportunityMSSContract()
	{
		//Select an Account that already has a MSS Contract in place
		Contract tcontract = [SELECT 
		Contract.Id,
		Contract.AccountId,
		Contract.RecordTypeId,
		Contract.RecordType.Name
		FROM Contract
		WHERE Contract.RecordType.Name = 'Fishnet Managed Services Support Contract' LIMIT 1];
		//Create a new Opportunity using that Account
		Opportunity tOpportunity = new Opportunity();
		tOpportunity.AccountId = tcontract.AccountId;
		tOpportunity.StageName = 'Prospecting';
		tOpportunity.CloseDate = Date.today();
		tOpportunity.Name = 'Test';
		insert(tOpportunity);
		List<Opportunity> opportunities = new List<Opportunity>();
		opportunities.add(tOpportunity);
		OpportunityMSSContract.Process(opportunities);
	}
}