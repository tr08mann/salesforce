@istest
public with sharing class TestLookupMainController {

public static testMethod void TestLookupMainMethod(){

	Campaign campobj = new Campaign();
	campobj.Name = 'Testcamp';
	insert campobj;
	
	ApexPages.currentPage().getParameters().put('id',campobj.Id);
	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(campobj);
	LookupMainController lookupobj = new LookupMainController(sc);
	lookupobj.savecampaign();
	lookupobj.findContacts();
}
}