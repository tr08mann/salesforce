@isTest
public class updateCommentOnEmailTEST{
    
    static testmethod void TEST()
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Fishnet Security, Inc. (Parent)');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(acc.ID);
        insert con;
        
        //Grab a Case
        Case c = TestDataFactory.createCase('SF.com Support Case', con.id);
        insert c;
        
        EmailMessage em = TestDataFactory.createEmailMessage(c.id, null);
        insert em;   
        
        EmailMessage email2 = TestDataFactory.createEmailMessage(c.id, null);
        email2.TextBody = 'This is a test --------------- Original Message ---------------This is a test';  
        insert email2;

		EmailMessage email3 = TestDataFactory.createEmailMessage(c.id, null);
        email3.HTMLBody = 'This is a test';
        email3.TextBody = '';
        insert email3;        
    }
}