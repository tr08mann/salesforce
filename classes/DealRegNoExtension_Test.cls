@istest
public class DealRegNoExtension_Test{

 public static testmethod void DealRegNoExtenstionTEST(){
       
       pagereference pageref = Page.DealRegNo;
       test.SetCurrentPage(pageref);
       
       apexpages.currentpage().getparameters().put('retURL','http://www.salesforce.com/');
       
       deal_registration__c deal = new deal_registration__c();
       deal.name = 'Test_Test';
       insert deal;
       
       apexpages.standardcontroller controller = new apexpages.standardcontroller(deal);
       
       DealRegNoExtension pages = new DealRegNoExtension(controller);
       
       pages.cancel();
       }

}