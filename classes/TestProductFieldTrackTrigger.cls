@isTest
public  class TestProductFieldTrackTrigger {
    
    static testMethod void testProductFieldTrackTrigger() 
    {
        Account  acct = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acct;
        
        vendor_Profile__c vp = TestDataFactory.createVendorProfile('Blue Coat Systems');
        insert vp;
        
        Product2 prod = TestDataFactory.createStandardProduct(vp.id, vp.name, '12345');
        prod.Frontline__c = 'TestFrontLine';
        insert prod;
        
        prod.Frontline__c = 'TestFrontLine2';
        prod.Frontline_Approved__c = false;   
        update prod;
        
        prod.Frontline_Approved__c = true;
        update prod; 
    }
}