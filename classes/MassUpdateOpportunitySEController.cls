public with sharing class MassUpdateOpportunitySEController
{
/****************************************************************************************************
    *Description:   Opportunity - Controller for SE Mass Update Tool
    *Test Class: MassUpdateOppSESTSController_TEST                   
    *        
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     6/17/2014    Alex Schach         Initial version
    *   1.5     6/23/2014    Alex Schach         Limited stages that can be selected
    *   1.6     6/27/2014    Alex Schach         Disabled Services Stage field if opp has no VAR Revenue
    ******************************************************************************************************/ 
private Map<Id,classOpportunityUpdate> myOpportunityIdToOpportunityUpdate = null;
private List<classOpportunityUpdate> myOpportunityUpdates = null;
public List<classOpportunityUpdate> theOpportunityUpdates
{
    get
    {
        if (myOpportunityUpdates == null)
        {
            myOpportunityUpdates = new List<classOpportunityUpdate>();
            myOpportunityIdToOpportunityUpdate = new Map<Id,classOpportunityUpdate>();
            for (Opportunity Opp : theOpportunities)
            {
                myOpportunityUpdates.add(new classOpportunityUpdate(Opp));
            }
            for (classOpportunityUpdate OppUpdate : myOpportunityUpdates)
            {
                myOpportunityIdToOpportunityUpdate.put(OppUpdate.theOpportunity.Id, OppUpdate);
            }
        }
        if (myOpportunityIdToOpportunityUpdate == null)
        {
        }
        return myOpportunityUpdates;
    }
}
private List<classOpportunityUpdate> myOpportunityUpdatesCount = null;
public string theOpportunityUpdatesCount
{
    get
    {
        if (myOpportunityUpdatesCount == null)
        {
            myOpportunityUpdatesCount = new List<classOpportunityUpdate>();
            for (Opportunity Opp : theOpportunities)
            {
                myOpportunityUpdatesCount.add(new classOpportunityUpdate(Opp));
            }
        }
        if(myOpportunityUpdatesCount.size() == 1)
        {
            return myOpportunityUpdatesCount.size() + ' Opportunity Found';
        }
        else
        {
            return myOpportunityUpdatesCount.size() + ' Opportunities Found';
        }
    }
}
private List<Opportunity>  myOpportunities = null;
public List<Opportunity> theOpportunities 
{ 
    get
    {
        if (myOpportunities == null)
        { 
            myOpportunities = 
            [
                select
                Name,
                owner.name,
                closedate,
                StageName,
                AccountId,
                Account.Name,
                Services_Stage__c,
                Sales_Engineer_NEW__c,
                Mobile_Advisor__c,
                Enterprise_Architect__c,
                enterprise_architect__r.name,
                mobile_advisor__r.name,
                Professional_Services_Total__c,
                VAR_Total__c
                from 
                Opportunity
                where
                IsClosed = false
                and
                CloseDate >= :MinDate
                and
                CloseDate <= :MaxDate
                and
                Professional_Services_Total__c  != 0
                and
                Sales_Engineer_NEW__c = :userinfo.getuserid()
                ORDER BY CloseDate ASC
                limit
                1000
            ];

        }
        system.debug('myopps: '+ myopportunities);
        return myOpportunities; 
    }
}

private boolean myFirstLoad = true;
public boolean FirstLoad 
{ 
    get
    {
        boolean OriginalValue = myFirstLoad;
        myFirstLoad = false;
        return OriginalValue;
    } 
}
public MassUpdateOpportunitySEController()
{
    SelectedFilter = 'All';
}
public Date MinDate 
{ 
    get
    {
        if (SelectedFilter == '-')
        {
            return date.newInstance(1900,1,1);
        }
        else if (SelectedFilter == 'All')
        {
            return date.newInstance(1900,1,1);
        }
        else if (SelectedFilter == '+120')
        {
            return date.today();
        }
        else if (SelectedFilter == '-120')
        {
            return date.today().addDays(-120);
        }
        else if (SelectedFilter == '+60')
        {
            return date.today();
        }
        else if (SelectedFilter == '+30')
        {
            return date.today();
        }
        else if (SelectedFilter == 'Quarter')
        {
            Date aNow = date.today();
            integer aYear = aNow.year();
            integer aMonth = aNow.month();
            integer aQuarter = (aMonth - 1) / 3;
            aYear = aNow.year();
            aMonth = (aQuarter * 3) + 1;
            integer aDay = 1;
            return date.newInstance(aYear, aMonth, aDay);
        }
        else
            return date.newInstance(1900,1,1);
    }
}
public Date MaxDate 
{ 
    get
    {
        if (SelectedFilter == '-')
        {
            return date.today();
        }
        else if (SelectedFilter == 'All')
        {
            return date.newInstance(2100,1,1);
        }
        else if (SelectedFilter == '+120')
        {
            return date.today().addDays(120);
        }
        else if (SelectedFilter == '-120')
        {
            return date.today();
        }
        else if (SelectedFilter == '+60')
        {
            return date.today().addDays(60);
        }
        else if (SelectedFilter == '+30')
        {
            return date.today().addDays(30);
        }
        else if (SelectedFilter == 'Quarter')
        {
            Date aNow = date.today();
            integer aYear = aNow.year();
            integer aMonth = aNow.month();
            integer aQuarter = (aMonth - 1) / 3;
            aYear = aNow.year();
            aMonth = (aQuarter * 3) + 1;
            integer aDay = 1;
            return date.newInstance(aYear, aMonth, aDay).AddMonths(3).AddDays(-1);
        }        
        else
            return date.newInstance(2100,1,1);
    }
}

public string SelectedFilter { get; set; }
public List<SelectOption> FilterOptions
{
    get
    {
        List<SelectOption> RetVal = new List<SelectOption>();
        RetVal.add(new SelectOption('-','Closing in past'));
        RetVal.add(new SelectOption('+120','Next 120 days'));
        RetVal.add(new SelectOption('-120','Last 120 days'));
        RetVal.add(new SelectOption('+60','Next 60 days'));
        RetVal.add(new SelectOption('+30','Next 30 days'));
        RetVal.add(new SelectOption('Quarter','Current quarter'));
        RetVal.add(new SelectOption('All','All Open'));
        return RetVal;
    }
}
private List<OpportunityStage> myStages = null;
private List<OpportunityStage> theStages
{
    get
    {
        if (myStages == null)
        {
            myStages = 
            [
                select
                Id,
                MasterLabel
                from
                OpportunityStage
                where
                IsActive = true
                and
                (
                DefaultProbability <= 90
                and
                isclosed = false
                and
                MasterLabel != 'Opportunity Unlocked'
                and
                MasterLabel != 'At Risk Rejected (90%)'
                )
                order by
                SortOrder
            ];
        }
        return myStages;
    }
}
public string StageOption { get; set; }
public List<SelectOption> StageOptions
{
    get
    {
        List<SelectOption> RetVal = new List<SelectOption>();
        for(OpportunityStage Stage : theStages)
        {
            RetVal.add(new SelectOption(Stage.MasterLabel, Stage.MasterLabel));
        }
        return RetVal;  
    }
}
static Schema.DescribeFieldResult p = opportunity.Services_Stage__c.getdescribe();
static List<Schema.PicklistEntry> servestage = p.getpicklistvalues();
private List<schema.picklistentry> myServicesStages = null;
private List<schema.picklistentry> theServicesStages
{
    get
    {
        system.debug('servestage: '+servestage);
        system.debug('p: '+p);
        return servestage;
    }
}
public string ServicesStageOption { get; set; }
public List<SelectOption> ServicesStageOptions
{
    get
    {
        List<SelectOption> RetVal = new List<SelectOption>();
        RetVal.add(new SelectOption('', '--None--'));
        for (schema.picklistentry Stage : theServicesStages)
        {
            RetVal.add(new SelectOption(Stage.getlabel(), Stage.getvalue()));
        }
        return RetVal;  
    }
}  
public PageReference Requery()
{
    myOpportunityUpdates = null;
    myOpportunityUpdatesCount = null;
    myOpportunityIdToOpportunityUpdate = null;
    myOpportunities = null;
    myHadErrors = false;
    mycomplete = false;
    return null;
}
private string mycompletecounts = '';
public string completecounts
{
    get
    {
        if(successcount == 1 && failcount == 0)
        {
            mycompletecounts = successcount + ' Success, ' + failcount + ' Errors';
        }
        else if(successcount == 1 && failcount == 1)
        {
            mycompletecounts = successcount + ' Success, ' + failcount + ' Error';
        }
        else if(successcount == 0 && failcount == 1)
        {
            mycompletecounts = successcount + ' Successes, ' + failcount + ' Error';
        }
        else
        {
            mycompletecounts = successcount + ' Successes, ' + failcount + ' Errors';
        }          
        system.debug('successcount: '+successcount);
        system.debug('failcount: '+failcount);
        return mycompletecounts;
    }
}
private boolean myHadErrors = false;
private boolean mycomplete = false;
private integer successcount = 0;
private integer failcount = 0;
public boolean complete {get {return mycomplete; } }
public boolean HadErrors { get { return myHadErrors; } } 
public PageReference Save()
{
    myHadErrors = false;
    mycomplete = false;
    successcount = 0;
    failcount = 0;
    List<Opportunity> OppsToUpdate = new List<Opportunity>();
    for (classOpportunityUpdate OppUpdate : theOpportunityUpdates)
    {
        OppsToUpdate.add(OppUpdate.theOpportunity);  
    }
    Database.SaveResult[] Results = Database.update(OppsToUpdate, false);
    for (integer i=0; i < theOpportunityUpdates.size(); ++i)
    {
        classOpportunityUpdate OppUpdate = theOpportunityUpdates[i];
        Database.SaveResult Result = Results[i];
        OppUpdate.theResult = 'Ok';
        successcount ++;
        if(!Result.isSuccess())
        {
            OppUpdate.theResult = '';
            boolean First = true;
            for (Database.Error Err : Result.getErrors())
            {  
                myHadErrors = true;
                if (!First)
                {
                    OppUpdate.theResult += ', ';
                }
                else
                {
                    First = false;
                    failcount ++; 
                    successcount --; 
                    OppUpdate.theResult += Err.getMessage();
                }
            } 
        }
    }
    mycomplete = true;
    return null;
}
public PageReference Cancel()
{
    myOpportunityUpdates = null;
    myOpportunityUpdatesCount = null;
    myOpportunityIdToOpportunityUpdate = null;
    myOpportunities = null;
    myHadErrors = false;
    mycomplete = false;
    return null;
} 
}