public with sharing class RedirectExtension {

    Pagereference page;
    public String Url{get;set;}
    public String ParameterCollection{get;set;}
    private Id ObjectId;
    public Boolean Error{get;set;}
    
    public RedirectExtension(ApexPages.StandardController stdController)
    {
        ObjectId = stdController.getId();
    }
    
    public Pagereference Redirect()
    {
        List<String> Params = new List<String>();
        if (ParameterCollection != null && ParameterCollection !=  '')
            Params = ParameterCollection.split(';');
        if (Url != null & Url != '')
        {
            page = new Pagereference(Url);
            //Get a List of the Parameters
            for (string s:Params)
            {
                if (s.split('=').size() == 2)
                {
                    string[] pairs = s.split('=');
                    page.getParameters().put(pairs[0],pairs[1]);
                }
            }
            if (ObjectId != null && page.getParameters().get('id') == null)
                page.getParameters().put('Id',ObjectId);
            page.setRedirect(true);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Url: '+Url));
            this.Error = true;
            return page;
        }
        else
            throw new MyException('Url was not Supplied');
    }
    public class MyException Extends Exception {}
}