@isTEST
public class MassUpdateOppSESTSController_TEST{

    public static testmethod void testSEController()
    {
        user SE = TestDataFactory.createStandardUser('testsecontroller');
        insert SE;
        
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.id, 'FishNet Security', '12345');
        p.Family = 'Security Integration';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.ownerid = SE.id;
        opp.sales_engineer_NEW__c = SE.id;
        opp.services_stage__c = '25% Scoping the Services';
        insert opp;
        
        opportunitylineitem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
                
        pagereference pageref = page.MassUpdateOpportunitySE;
        test.SetCurrentPage(pageref);
           
        system.runas(SE)
        {            
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            list<selectOption> testoptions = controller.ServicesStageOptions;
            list<selectOption> testoptions2 = controller.StageOptions;
            list<selectOption> testoptions3 = controller.FilterOptions;
            string oppcount = controller.theopportunityupdatescount;
            boolean first = controller.FirstLoad;
            string completecount = controller.completecounts;
            controller.selectedfilter = 'Quarter';
            controller.requery();
            controller.save();
            controller.cancel(); 
        }       
        system.runas(SE)
        {            
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = 'All';
            controller.save();
        }             
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '-';
            controller.save();
        }     
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '+120';
            controller.save();
        }
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '-120';
            controller.save();
        } 
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '+60';
            controller.save();
        }
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '+30';
            controller.save();
        } 
        system.runas(SE)
        {
            MassUpdateOpportunitySEController controller = new MassUpdateOpportunitySEController();
            controller.selectedfilter = '';
            controller.save();
        } 
    }

    public static testmethod void testSTScontroller()
    {
        user STS = TestDataFactory.createStandardUser('teststscontroller');
        insert STS;
                  
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.id, 'FishNet Security', '12345');
        p.Family = 'Security Integration';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.ownerid = STS.id;
        opp.sales_engineer_NEW__c = STS.id;
        opp.services_stage__c = '25% Scoping the Services';
        opp.Primary_Strategic_Services_Director__c = STS.id;
        insert opp;
        
        pagereference pageref = page.MassUpdateOpportunitySTS;
        test.SetCurrentPage(pageref);
           
        system.runas(STS)
        {            
            MassUpdateOpportunitySTSController controller = new MassUpdateOpportunitySTSController();
            list<selectOption> testoptions = controller.ServicesStageOptions;
            list<selectOption> testoptions2 = controller.StageOptions;
            list<selectOption> testoptions3 = controller.FilterOptions;
            string oppcount = controller.theopportunityupdatescount;
            boolean first = controller.FirstLoad;
            string completecount = controller.completecounts;
            controller.selectedfilter = 'Quarter';
            controller.requery();
            controller.save();
            controller.cancel(); 
        }       
        system.runas(STS)
        {            
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = 'All';
            controller.save();
        }             
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '-';
            controller.save();
        }     
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '+120';
            controller.save();
        }
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '-120';
            controller.save();
        } 
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '+60';
            controller.save();
        }
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '+30';
            controller.save();
        } 
        system.runas(STS)
        {
            MassUpdateOpportunitySTSController controller = new  MassUpdateOpportunitySTSController();
            controller.selectedfilter = '';
            controller.save();
        }
    }
}