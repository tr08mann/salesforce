@isTest
public class TriggerTests{
    
    static testMethod void ReAssocTask()
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        SFDC_520_Quote__c q = TestDataFactory.createOldQuote(opp.id);
        insert q;   
        
        Task t = TestDataFactory.createTask(q.id, userinfo.getUserId());
        t.Subject = 'Follow up on Deal Registration Status';
        insert t;
        
        Task test = [Select Id, WhatId from Task where Id = :t.id];
        if(test.WhatId == q.id) test.adderror('Houston, we have a problem.');
    }
    
    static testMethod void testCaseComment() 
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Fishnet Security, Inc. (Parent)');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Task t = TestDataFactory.createTask(acc.id, userinfo.getUserId());
        insert t;
        
        Case c = TestDataFactory.createCase('SF.com Support Case', con.id);
        insert c;
        
        Task t1 = TestDataFactory.createTask(c.id, userinfo.getUserId());
        insert t1;
        
        Event e = new Event();
        e.Subject = 'test';
        e.whatid = c.id;
        e.durationinminutes = 10;
        e.activitydate = system.today();
        e.activitydatetime = system.now();
        insert e;
        
        EmailMessage em = TestDataFactory.createEmailMessage(c.id, t.id);
        insert em;        
    }
    
    static testMethod void testLeadCompany()
    {
        Lead a = TestDataFactory.createStandardLead('Fun');        
        insert a;
        
        Lead b = TestDataFactory.createStandardLead('Fun');   
        insert b;
        
        b.primary__c = true;
        update b;
    }
    
    static testMethod void testTrackLegalDocStatus()
    {
        Legal_Document__c temp = new Legal_Document__c();
        insert temp;
        temp.Status__c = 'Test';
        update temp;
    }
}