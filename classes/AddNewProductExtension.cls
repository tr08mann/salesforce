public class AddNewProductExtension {
        
        private Quote quote;
        private Product2 newproduct = new Product2();
        public Boolean Duplicate{get;set;}
        private Boolean PreviousDuplicate {get;set;}
        
        public AddNewProductExtension(Apexpages.StandardController stdcontroller)
        {
                Duplicate = false;
                if (Apexpages.currentPage().getParameters().get('Id') != null && Apexpages.currentPage().getParameters().get('Id') != '')
                {
                        //Retreive Quote/Opportunity Information *Id of Quote
                        this.quote = [SELECT
                        Quote.Id,
                        Quote.OpportunityId,
                        Quote.Opportunity.CurrencyIsoCode,
                        Quote.Pricebook2Id
                        FROM Quote
                        WHERE Quote.Id =: Apexpages.currentPage().getParameters().get('Id')];
                }
        }
        public Product2 getProduct()
        {
                return this.newproduct;
        }
        public void setProduct(Product2 prod)
        {
                this.newproduct = prod;
        }
        public Pagereference Cancel()
        {
                if (Apexpages.currentPage().getParameters().get('Id') != null && Apexpages.currentPage().getParameters().get('Id') != '')
                {
                        Pagereference page = new Pagereference('/'+Apexpages.currentPage().getParameters().get('Id'));
                        page.setRedirect(true);
                        return page;
                }
                else return null;
        }
        public void Save()
        {
                //Attempt to insert new Product, Set the selected Product Id
                //Ensure that there is not already a product with the same product code
                List<Product2> existing = new List<Product2>();
                existing = [SELECT
                Product2.Id
                FROM Product2
                WHERE Product2.ProductCode =: newproduct.ProductCode];
                if (existing.size() > 0)
                {
                        Duplicate = true;
                        if (PreviousDuplicate == null || PreviousDuplicate == false)
                        {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'A Product with this Product Code already exists. Press the Save button once again to Save with the duplicated Product Code, or change the Product Code.'));
                                PreviousDuplicate = true;
                                return;
                        }
                        else
                        {
                                Duplicate = false;
                                PreviousDuplicate = true;
                        }
                }
                else
                        Duplicate = false;
                ConfirmedSave();
        }
        public void ConfirmedSave()
        {
                try
                {        
                        newproduct.CurrencyIsoCode = this.quote.Opportunity.CurrencyIsoCode;
                        newproduct.IsActive = true;
                        upsert(newproduct);
                        PricebookEntry newEntry = new PricebookEntry();
                        newEntry.IsActive = true;
                        newEntry.Pricebook2Id = this.quote.Pricebook2Id;
                        newEntry.Product2Id = newproduct.Id;
                        newEntry.UnitPrice = newproduct.MSRP__c;
                        newEntry.CurrencyIsoCode = this.quote.Opportunity.CurrencyIsoCode;
                        insert(newEntry);                       
                        //ApexPages.addMessage(new ApexPages.Message (ApexPages.Severity.INFO,'New ProductId: '+newproduct.Id));
                }
                catch (Exception e)
                {
                        //if (newproduct.Id != null)
                        //      delete(newproduct);
                        ApexPages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,'Error inserting Product: '+e.getMessage()));
                }
        }
        
        static testMethod void AddNewProductExtensionTEST()
        {
            //Test Method for adding a new Product on the fly
            //Create a Quote and Add it to the Controller
            
            vendor_profile__c vp = new vendor_profile__c();
            vp.name = 'Symantec';
            insert vp;
            
            Quote q = [SELECT
            Quote.Id
            FROM Quote LIMIT 1];
            
            Apexpages.currentPage().getParameters().put('id',q.Id);
            Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(q);
            AddNewProductExtension page = new AddNewProductExtension(controller);
            //Get an existing product to test Already Existing
            PricebookEntry existing = [SELECT
            PricebookEntry.Pricebook2Id,            
            PricebookEntry.Product2.Id,
            PricebookEntry.Product2.ProductCode,
            PricebookEntry.Product2.RecordTypeId
            FROM PricebookEntry
            WHERE PricebookEntry.Product2.ProductCode != null ANd pricebookentry.product2.name != null
            AND PricebookEntry.IsActive = true LIMIT 1];
            page.setProduct(existing.Product2);
            page.Save();
            //Create a New Product where the ProductCode is not found
            Product2 newProduct = new Product2();
            newProduct.ProductCode = 'JustinPadilla';
            newProduct.RecordTypeId = existing.Product2.RecordTypeId;
            newProduct.Name = 'Symantec';
            newProduct.product_vendor_profile__c = vp.id;
            insert newProduct;
            page.setProduct(newProduct);
            page.Save();
            page.getProduct();
            page.Cancel();
        }
}