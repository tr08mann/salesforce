public class TriggerStopper {
    public static boolean stopOpp = false;
    public static boolean stopQuote = false;
    public static boolean stopOppLine = false;
    public static boolean stopQuoteLine = false;
    public static boolean stopInvoiceRollup = false;
    
    public static boolean isOppLineItemSync = false;
    public static boolean isQuoteLineItemSync = false;
}