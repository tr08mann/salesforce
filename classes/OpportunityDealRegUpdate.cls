public with sharing class OpportunityDealRegUpdate {
	
	public static void Check(List<Opportunity> opportunities)
	{
		Map<id, user> usermap = new Map<id, user>();
		Set<Id> userIds = new Set<Id>();
		
		for (Opportunity o:opportunities)
		{
			if (!userIds.contains(o.OwnerId))
				userIds.add(o.OwnerId);
		}
		//Query Once for all of the Owners
		usermap = new Map<id, user>([select 
				id, 
				name, 
				ProfileId, 
				UserRoleId, 
				Profile.Name, 
				UserRole.Name, 
				Opportunity_Sales_Engineer__c, 
				Deal_Reg_Desk_Rep__c 
				from user 
				where id IN:userIds]);
		/*
		for (Opportunity o:opportunities)
		{
			
			if (!usermap.containsKey(o.OwnerId))
			{
				List<User> temp = [select 
				id, 
				name, 
				ProfileId, 
				UserRoleId, 
				Profile.Name, 
				UserRole.Name, 
				Opportunity_Sales_Engineer__c, 
				Deal_Reg_Desk_Rep__c 
				from user 
				where id =: o.OwnerId];
				if (temp.size() > 0)
				{
					usermap.put(temp[0].Id,temp[0]);
				}
			}
			*/
		for (Opportunity o:opportunities)
		{
			User workingUser = usermap.get(o.ownerid);
			//JGP Insert of Deal_Reg_Desk_Rep__c to Opportunity
	        if (o.Deal_Reg_Desk_Rep__c == null)
 	       	{
        		if (workingUser != null)
        			o.Deal_Reg_Desk_Rep__c = workingUser.Deal_Reg_Desk_Rep__c;
        	}
		}
	}
	
	static testMethod void OpportunityDealRegUpdateTest()
	{
		User user1 = [select id from user where isactive = true and profileid <> '00e30000000gCMR' limit 1];
		
		Opportunity o = new Opportunity();
        o.closedate = system.today();
        o.Credit_Limit_Override__c = true;
        o.name = 'Test_Test_Test';
        o.StageName = 'Not Fully Qualified (10%)/SVC-Meeting';
        o.Region__c = 'East';
        //o.accountid = a.id;
        o.type = 'Prospect';
        o.leadsource = 'Other';
        o.Product_Type_s__c = 'NONE';
        o.Services_Proposal_Type__c = 'RFP Response';
        o.ownerid = user1.id;
        o.Lead_Status__c = 'Submitted to Sales';
        o.Vendors_Called_On__c = 'Check Point';
        OpportunityDealRegUpdate.Check(new List<Opportunity>{o});
	}
	
}