global class QLISort implements Comparable {

    public searchandaddmultiplecontroller.extendedproduct ep;
    
    public QLISort(searchandaddmultiplecontroller.extendedproduct p)
    {
        ep = p;
    }
    
    global integer compareTo(object compareTo)
    {
        QLISort cp = (QLISort)compareTo;
        integer returnvalue = 0;
        if(ep.id >= cp.ep.id)
        {
            returnvalue = 1;
        }
        else if(ep.id <= cp.ep.id)
        {
            returnvalue = -1;
        }
        return returnvalue;
    }
}