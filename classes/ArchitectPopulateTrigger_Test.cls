@isTest
public class ArchitectPopulateTrigger_Test{

    static testmethod void TestArchitectPopulateTrigger()
    {
       
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Radiant Logic');
        insert vp;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.id, vp.name, '12345');
        p.Family = 'VAR-IAM';
        p.mobility_sku__c = true;
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
                 
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        opportunitylineitem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
                
        opp.region_opp__c = 'East';
        update opp;
        
        opp.region_opp__c = 'West';
        update opp;
        
        test.starttest();
        
        opp.region_opp__c = 'SMB';
        update opp;
        
        delete oli;
        
        test.stoptest();
    }
}