public with sharing class DealRegUserExtension {
        private String selectedUser;
        List<User> users = new List<User>();
        User currentUser;
        public DealRegUserExtension(Apexpages.StandardController controller)
        {
        	//Grab profiles that contain the word first in it for populating the available User List
        	List<Profile> firstProfiles = new List<Profile>();
        	firstProfiles = [SELECT
        	Profile.Id
        	FROM Profile
        	WHERE Profile.Name like '%First%'];

            //Populate Available Users
            users = [SELECT
            User.Id,
            User.FirstName,
            User.LastName
            FROM User
            WHERE User.ProfileId IN :firstProfiles];                
            //Grab CurrentUser
            currentUser = [SELECT
            User.Id,
            User.Deal_Reg_Desk_Rep__c
            FROM User
            WHERE User.Id =: apexpages.currentPage().getParameters().get('id')];
                
            this.selectedUser = currentUser.Deal_Reg_Desk_Rep__c;
        }
        
        public String getselectedUser(){return this.selectedUser;}
        public void setselectedUser(String s){this.selectedUser = s;}
        
        public List<SelectOption> getUsers()
        {
                List<SelectOption> o = new List<SelectOption>();
                for (User u:this.users)
                {
                        if (u.FirstName != null)
                                o.add(new SelectOption(u.Id,u.LastName+','+u.FirstName));
                        else
                                o.add(new SelectOption(u.Id,u.LastName));
                }
                o = SortOptionList(o);
                if (o.size() > 0)
                	o.add(0,new SelectOption('None','None'));
                return o;
        }
        
        public void SaveUser()
        {
        		if (this.selectedUser == 'None')
        			currentUser.Deal_Reg_Desk_Rep__c = null;
        		else
                	currentUser.Deal_Reg_Desk_Rep__c = this.selectedUser;
                update(currentUser);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'User has been Updated'));
        }
        
        
        /* Utilities */
    private static List<SelectOption> SortOptionList(List<SelectOption> ListToSort)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
            return ListToSort;
        List<SelectOption> Less = new List<SelectOption>();
        List<SelectOption> Greater = new List<SelectOption>();
        integer pivot = ListToSort.size() / 2;
        // save the pivot and remove it from the list
        SelectOption pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        for(SelectOption x : ListToSort)
        {
            if(x.getLabel() <= pivotValue.getLabel())
                Less.add(x);
            else if(x.getLabel() > pivotValue.getLabel()) Greater.add(x);   
        }
        List<SelectOption> returnList = new List<SelectOption> ();
        returnList.addAll(SortOptionList(Less));
        returnList.add(pivotValue);
        returnList.addAll(SortOptionList(Greater));
        return returnList; 
    }
    static testMethod void DealRegUserExtensiontest()
    {
        User u = [SELECT User.Id FROM User WHERE User.IsActive = true LIMIT 1];
        apexpages.Standardcontroller controller = new apexpages.Standardcontroller(u);
        Apexpages.currentPage().getParameters().put('id',u.Id);
        DealRegUserExtension page = new DealRegUserExtension(controller);
        page.setselectedUser(page.getselectedUser());
        List<SelectOption> options = page.getUsers();
        if (options.size() > 0)
        {
            page.setselectedUser(options[0].getvalue());
        }
        page.SaveUser();
        if (options.size() > 1)
        {
            page.setselectedUser(options[1].getvalue());
        }
        page.SaveUser();
    }
}