public without sharing class NewCustomerFormExtension{

    public string acctid {get;set;}
    public string CustNbr {get; set;}
    public string ConcatID {get; set;} //concatentation of account id and customer number that user inputs
    public string LookupID {get; set;} //concatentation of account id and customer number that is in SFDC
    public string SelectedAttachId {get; set;}
    private account acc;
    public account ghostacc {get;set;}
    public contact contact {get; set;}
    public address__c address {get;set;}
    public list<string> labels = new list<string>();
    public boolean SubmitClose {get; set;} //to store whether or not to auto close form when user submits and there are no errors
    public boolean PageAshow {get; set;}
    public boolean PageBshow {get; set;}
    public boolean PageCshow {get; set;}
    public boolean ShowSubmit {get; set;}
    public boolean ShowClose {get; set;} 
    public boolean ShowAttach {get; set;}
    public boolean ShowNext {get; set;}
    public boolean ShowTable {get; set;}
    public boolean ShowUpload {get; set;}
    public boolean ShowBack {get; set;}
    public integer tempCount {get; set;} //count number of attachments at time of load
    public List<SelectOption> filesCountList {get; set;} //Picklist of integer values to hold file count
    public String FileCount {get; set;} //Selected count
    public List<Attachment> allFileList {get; set;}
    public List<Attachment> attachFiles {get; set;}
    private map<string,string> recordtypemap = new map<string,string>();
    
    public NewCustomerFormExtension() 
    {
        PageAshow = true;
        PageBshow = false;
        PageCshow = false;
        ShowSubmit = false;
        ShowAttach = false;
        ShowNext = true;
        ShowUpload = false;
        ShowTable = false;
        ShowBack = false;
        ShowClose = false;
                
        LoadData();
        LoadCount();
                
        ghostacc = new account();
        address = new address__c();
        this.labels = getLabels();
        
        contact = new contact();
        
        //Initialize  
        filesCountList = new List<SelectOption>() ;
        FileCount = '' ;
        allFileList = new List<Attachment>() ;
        
        //Adding values count list - you can change this according to your need
        for(Integer i = 1 ; i < 11 ; i++)
            filesCountList.add(new SelectOption(''+i , ''+i)) ;
    }
        
    public void Next()
    {   
        integer result = 0;
         
        this.acc = getAccount();
        
        ConcatID = acctid + CustNbr;
        
        lookupid = acc.id;
        result = lookupid.length();
        
        if(result == 18)
        {
            LookupID = LookupID.left(15) + acc.company_number__C;
        } Else {
            lookupid = lookupid + acc.company_number__C;
        }
        
        if (ConcatID != LookupID)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Account information does not exist.  Please reenter the Account Key and Customer Number from the email, or contact your Account Executive for assistance.'));
            return;
        } Else if(acc.Guest_User_Updated__c ==true)
          {
              ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Account information has already been updated.  Please contact your account executive to unlock the account for updates.'));
              return;
          }
        
        Else {
                PageAshow = false;
                PageBshow = true;
                PageCshow = false;
                showSubmit = true;
                showAttach = true;
                showNext = false;
                ShowUpload = false;
                ShowTable = true;
                ShowBack = false;
                ShowClose = false;
                
                LoadData();    
                
                map<string, schema.sobjectfield> schemafieldmap = schema.sobjecttype.account.fields.getmap();
                for(schema.fieldsetmember f : this.getFields())
                {
                    if(schemafieldmap.get(f.getFieldPath()) != null)
                    {
                        schema.sobjectfield temp = schemafieldmap.get(f.getFieldPath());
                        object accvalue= acc.get(temp);
                        ghostacc.put(temp, accvalue);
                    }
                }
          }
    }
    
    public void submit()
    {
        loaddata();
        loadcount();
        SubmitClose = false;
    
        map<string, schema.sobjectfield> schemafieldmap = schema.sobjecttype.account.fields.getmap();
            for(schema.fieldsetmember f : this.getFields())
            {
                if(schemafieldmap.get(f.getFieldPath()) != null)
                {
                    schema.sobjectfield temp = schemafieldmap.get(f.getFieldPath());
                    object ghostaccvalue= ghostacc.get(temp);
                    acc.put(temp, ghostaccvalue);
                }
            }
        
        map<string, schema.sobjectfield> schemafieldmap2 = schema.sobjecttype.account.fields.getmap();
            for(schema.fieldsetmember f : this.getFields2())
            {
                if(schemafieldmap.get(f.getFieldPath()) != null)
                {
                    schema.sobjectfield temp = schemafieldmap.get(f.getFieldPath());
                    object ghostaccvalue= ghostacc.get(temp);
                    acc.put(temp, ghostaccvalue);
                }
            }
        
        if((FileCount == null || FileCount == '') && (tempCount == NULL || tempcount == 0) && acc.Exempt_from_State_and_Local_Taxes__c == 'YES')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please attach necessary documents for tax exempt status'));
            SubmitClose = false;
            return;
        } 
        
        
        Else{    
                PageAshow = false;
                PageBshow = false;
                PageCshow = false;
                showSubmit = false;
                showAttach = false;
                showNext = false;
                ShowUpload = false;
                ShowTable = false;
                ShowBack = false;
                ShowClose = true;  
                SubmitClose = true;     
        
                try{
                    acc.Guest_User_Updated__c = true;
                    if(acc.Invoicing_Instructions__c != NULL)
                    {
                        String st = '\r\nAddress information can be found on the related Address list for the account';
                    	acc.Invoicing_Instructions__c += st;
                    }Else
                    {
                        acc.Invoicing_Instructions__c = 'Address information can be found on the related Address list for the account';
                    }
                    
                    update acc;
                   }
                catch(Exception e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                    }
                
                //grabbing record type ids    
                for(recordtype r : [select id, name, developername from recordtype where (developername = 'Bill_to_Address' OR developername = 'Ship_to_Address')])
                {
                    recordtypemap.put(r.developername, string.valueof(r.id).left(15));
                }                  
                    
                Address__C ShippingAddress = new address__C();
                try{
                    ShippingAddress.name = 'temporary name';
                    ShippingAddress.recordtypeid = recordtypemap.get('Ship_to_Address');
                    ShippingAddress.company_Name__c = acc.name;
                    ShippingAddress.address_account__C = acctid;
                    ShippingAddress.Attention_To__c = acc.Shipping_Attention_To__c;
                    ShippingAddress.Address_Line_1__c = acc.BillingStreet;
                    ShippingAddress.Address_City__C = acc.BillingCity;
                    ShippingAddress.Address_State__c = acc.BillingState;
                    ShippingAddress.Address_Zip__C = acc.BillingPostalCode;
                    ShippingAddress.Address_Country__C = acc.BillingCountry;
                    insert shippingaddress;
                    }
                catch(Exception e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                    }
                
                
                //inserting address
                try{        
                    address.name = 'temporary name';
                    ShippingAddress.recordtypeid = recordtypemap.get('Bill_to_Address');
                    address.Company_Name__c = acc.name;
                    address.Address_Account__c = acctid;
                    insert address;
                    } 
                catch(Exception e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                    }
                
                //inserting contact
                try{
                    contact.firstname = this.contact.firstname;
                    contact.lastname = this.contact.lastname;
                    contact.phone = this.contact.phone;
                    contact.email = this.contact.email;
                    contact.accountid = acctid;
                    contact.ownerid = acc.ownerid;
                    insert contact;
                    }
                catch(Exception e) {
                    System.debug('The following exception has occurred: ' + e.getMessage());
                    }
             }
    }
    
    Public void DeleteFile()
    {     
        if (SelectedAttachId == null)
        {
            return;
        }
            
        Attachment tobeDeleted = null;
        for(Attachment a : attachFiles)
            if (a.Id == SelectedAttachId)
            {
                tobeDeleted = a;
                break;
            }
        if (tobeDeleted != null)
        {
            Delete tobeDeleted;
        }
        
        LoadData();
    }
    
    Public void LoadCount()
    {
        tempCount = attachFiles.size();
    }
    
    Public void LoadData() 
    {
        attachFiles =
            [Select Id, 
                IsDeleted, 
                ParentId, 
                Name, 
                IsPrivate, 
                ContentType,  
                Body, 
                OwnerId, 
                CreatedDate,
                CreatedById, 
                LastModifiedDate, 
                LastModifiedById, 
                SystemModstamp, 
                Description 
            FROM Attachment
            where CreatedBy.profileid =: userinfo.getProfileId() AND parentid =: acctid];
    }

    
    public void attach()
    {
        PageAshow = false;
        PageBshow = false;
        PageCshow = true;
        ShowNext = false;
        ShowSubmit = false;
        ShowAttach = false;
        ShowUpload = true;
        ShowTable = false;
        ShowBack = true;       
    }
    
    public void Back()
    {
        PageAshow = false;
        PageBshow = true;
        PageCshow = false;
        ShowNext = false;
        ShowSubmit = true;
        ShowAttach = true;
        ShowUpload = false;
        ShowTable = true;
        ShowBack = false;
        
        LoadData();       
    }
    
    public Pagereference SaveAttachments()
        {
            if(FileCount == null || FileCount == '')
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select how many files you want to upload.'));
            }
    
            List<Attachment> listToInsert = new List<Attachment>() ;
            
            for(Attachment a: allFileList)
            {
                if(a.name != '' && a.body != null)
                    listToInsert.add(new Attachment(parentId = acctid, name = a.name, body = a.body)) ;
            }
            
            //Inserting attachments
            if(listToInsert.size() > 0)
            {
                insert listToInsert ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, listToInsert.size() + ' file(s) are uploaded successfully'));
                FileCount = '' ;
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please select at-least one file'));
            }    
            return null;
    }
    
    public PageReference ChangeCount()
    {
        allFileList.clear() ;
        if(FileCount != NULL){
            //Adding multiple attachments instance
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;
            return null ;
        }Else{
            return null;
        }
    }

    public account getAccount()
    {       
        string query = 'SELECT ';
        for(Schema.FieldSetMember f:this.getFields())
        {
            query += f.getFieldPath() + ', ';
        }
        for(schema.fieldsetmember q:this.getFields2())
        {
            query += q.getFieldPath() + ', ';
        }
        query += 'id, ownerid, company_number__c, Guest_User_Updated__c from account where id =: acctid';
        return database.query(query);
    } 
    public list<Schema.FieldSetMember> getFields()
    {
        return sObjecttype.account.fieldsets.newcustomerform.getfields();
    }
    public list<Schema.FieldSetMember> getFields2()
    {
        return sObjecttype.account.fieldsets.NewCustomerFormPicklists.getfields();
    }
    public list<Schema.FieldSetMember> getFields3()
    {
        return sObjecttype.address__c.fieldsets.NewCustomerForm.getfields();
    }
    public string getAcctid()
    {
        return acctid;
    }
    public list<string> getlabels()
    {
        list<string> templabel = new list<string>();
        templabel .add('Preferred method of receiving invoice');
        templabel .add('Does your company require a Purchase Order to issue payment?');
        templabel .add('Is your company exempt from state and local taxes?');
        return templabel;
    }
}