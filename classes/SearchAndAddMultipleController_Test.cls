@isTest (seealldata=true)
public class SearchAndAddMultipleController_Test{

    static testMethod void testisrenewal()
    {                       
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.Type = 'Renewal Business';
        opp.Renewal_Type__c = 'Existing Renewal';
        opp.services_proposal_type__c = 'Multi-Year MSS';
        opp.Deadline__c = system.today();
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Quote q = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert q;
        
        test.setCurrentPage(new Apexpages.Pagereference('/apex/SearchAndAddMultiple'));
        Apexpages.currentPage().getParameters().put('id',q.Id);
        
        SearchAndAddMultipleController controller = new SearchAndAddMultipleController();
        
        controller.getisrenewal();
    }
    static testMethod void SearchAndAddMultipleControllerTEST()
    {       
        list<pricebookentry> pbelist = new list<pricebookentry>();
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Quote q = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert q;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.ID, vp.Name, '1234567890');
        p.Family = 'Enterprise Architecture';
        p.Solution_Set__c = 'Infrastructure and Operations';
        p.Practice__c = 'Compliance';
        p.currencyisocode = 'USD';
        p.recordtypeid = '0126000000098uE';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        pbelist.add(pbe);
        
        vendor_profile__c vp2 = TestDataFactory.createVendorProfile('Blue Coat Systems');
        insert vp2;
        
        Product2 p2 = TestDataFactory.createStandardProduct(vp2.id, vp2.Name, '12345678');
        p2.Family = 'VAR-Security';
        p2.currencyisocode = 'USD';
        p2.frontline__c = 'No';
        p2.recordtypeid ='0126000000098uJ';
        insert p2;
        
        PricebookEntry pbe2 = TestDataFactory.createPricebookEntry(p2.id);
        insert pbe2;
        pbelist.add(pbe2);
        
        deal_registration__c newdealreg = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.name, opp.Opportunity_Number__c, 'Pending');
        insert newdealreg;

        test.setCurrentPage(new Apexpages.Pagereference('/apex/SearchAndAddMultiple'));
        Apexpages.currentPage().getParameters().put('id',q.Id);
        
        SearchAndAddMultipleController controller = new SearchAndAddMultipleController();
        
        string temp = controller.sortDir;
        temp = controller.sortField;
        temp = controller.debugSoql;

        controller.getshowSelection();
        controller.getShowGovernment();
        controller.getshowSelectionService();
        controller.getIsRenewal();
        controller.getselectionSize();
        controller.getselectionServiceSize();
      
        controller.toggleSort();
        
        controller.setSelectedFamily(controller.getSelectedFamily());        
        controller.setselectedField(controller.getselectedField());
        controller.setselectedOperator(controller.getselectedOperator());
        controller.setfieldSearch(controller.getfieldSearch());
        controller.setquickSearch(controller.getquickSearch());
        controller.setQuote(controller.getQuote());
        controller.setAllChecked(controller.getAllChecked());
        
        controller.getFamilies();
        controller.getFields();
        controller.getPricingMethods();
        controller.getContractVehicles();
        controller.getRegions();
        controller.getOperators();
        controller.getOpportunities();
        controller.getSalesCategories();
        
        list<SearchAndAddMultipleController.ExtendedProduct> products = new List<SearchAndAddMultipleController.ExtendedProduct>();
        if (pbelist.size()>0)
        {
            for (PricebookEntry entry:[Select PricebookEntry.UnitPrice,PricebookEntry.Product2Id,PricebookEntry.Product2.Name,PricebookEntry.Product2.ProductCode,PricebookEntry.Product2.Description,PricebookEntry.Product2.Corporate_Price__c,PricebookEntry.Product2.RecordType.Name,PricebookEntry.Product2.Family,PricebookEntry.Product2.IsActive FROM PricebookEntry where PricebookEntry.id in:pbelist])
            {
                products.add(new SearchAndAddMultipleController.ExtendedProduct(entry));
            }
        }
        system.debug('products: '+products);
        for (SearchAndAddMultipleController.ExtendedProduct pr:products)
        {
                    pr.Action = true;
                    pr.Qty = 2;
                    pr.Serial = '1234;56789';
        }
        controller.products=products;
        system.debug('controllerproducts: ' + controller.products);
        
        controller.SelectProduct();

        controller.getLineCount();
        controller.getQuoteTotal();
        controller.getCostTotal();
        controller.getProfitTotal();

        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selection)
        {
                pr.Action = true;
                pr.Qty = 2;
                pr.Serial = '1234;5678;';
                break;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selectionservice)
        {
            pr.Action = true;
            pr.Qty = 2;
            pr.Serial = '1234;5678;';
            break;
        }
        controller.ExpandSelected();
        
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selection)
        {
                pr.Action = true;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selectionservice)
        {
                pr.Action = true;
        }
        
        controller.Collapse();
        
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selection)
        {
                pr.Action = true;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selectionservice)
        {
                pr.Action = true;
        }
        
        controller.popupAmount = '2';
        controller.UpdateEdit();
        controller.Save();
        controller.Cancel();
        
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selection)
        {
            pr.Action = true;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr:controller.selectionservice)
        {
            pr.Action = true;
        }
        controller.CloneQuote();

        controller.startorder();
        controller.startorderserv();
        
        controller.PopupNewProduct = controller.selection[0].PricebookEntry.Product2Id;
        controller.AddNewProduct();
    }
    
    static testMethod void SearchAndAddMultipleControllerTEST2()
    {             
        //Test Additional Edit Functions from an existing QLI 
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Quote q = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert q;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.ID, vp.Name, '1234567890');
        p.Family = 'Enterprise Architecture';
        p.Solution_Set__c = 'Infrastructure and Operations';
        p.Practice__c = 'Compliance';
        p.currencyisocode = 'USD';
        p.recordtypeid = '0126000000098uE';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        vendor_profile__c vp2 = TestDataFactory.createVendorProfile('Blue Coat Systems');
        insert vp2;
        
        Product2 p2 = TestDataFactory.createStandardProduct(vp2.id, vp2.Name, '12345678');
        p2.Family = 'VAR-Security';
        p2.currencyisocode = 'USD';
        p2.frontline__c = 'No';
        p2.recordtypeid ='0126000000098uJ';
        insert p2;
        
        PricebookEntry pbe2 = TestDataFactory.createPricebookEntry(p2.id);
        insert pbe2;
        
        deal_registration__c newdealreg = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.name, opp.Opportunity_Number__c, 'Pending');
        insert newdealreg;
        
        QuoteLineItem qli = TestDataFactory.createQuoteLineItem(q.id, pbe2.id);
        insert qli;       

        QuoteLineItem qliService = qli.Clone();
        insert qliService;
                
        test.setCurrentPage(new Apexpages.Pagereference('/apex/MultilineEdit'));
        Apexpages.currentPage().getParameters().put('id',qli.QuoteId);        
        SearchAndAddMultipleController controller2 = new SearchAndAddMultipleController();
        
        controller2.Submit();
        controller2.Save();
        
        for (SearchAndAddMultipleController.ExtendedProduct pr: controller2.selection)
        {
                pr.setselectedType(pr.getselectedType());
                pr.getTypes();
                pr.setQty(pr.getQty());
                pr.setSelectedPricingMethod(pr.getSelectedPricingMethod());
                pr.setAmount(pr.getAmount());
                pr.setUnitPrice(pr.getUnitPrice());
                pr.getExtPrice(pr.getExtPrice());         
                pr.setSelectedContractVehicle(pr.getSelectedContractVehicle());
                pr.setSelectedRegion(pr.getSelectedRegion());
                pr.setTerm(pr.getTerm());
                pr.setSerial(pr.getSerial());
                pr.setSelectedSalesCategory(pr.getSelectedSalesCategory());
        }
        controller2.Submit();
        for (SearchAndAddMultipleController.ExtendedProduct pr: controller2.selection)
        {
                pr.Action = true;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr: controller2.selectionService)
        {
                pr.Action = true;
        }
        controller2.popupamount = '100';
        controller2.UpdateEdit();
        for (SearchAndAddMultipleController.ExtendedProduct pr: controller2.selection)
        {
                pr.Action = false;
        }
        for (SearchAndAddMultipleController.ExtendedProduct pr: controller2.selectionService)
        {
                pr.Action = false;
        }
        controller2.DeleteSelected();
        controller2.Submit();
        controller2.Save();        
    }
    public static testMethod void testsearch()
    {   
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Quote q = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert q;
        
        Product2 p = TestDataFactory.createStandardProduct(vp.ID, vp.Name, '1234567890');
        p.Family = 'Enterprise Architecture';
        p.Solution_Set__c = 'Infrastructure and Operations';
        p.Practice__c = 'Compliance';
        p.currencyisocode = 'USD';
        p.recordtypeid = '0126000000098uE';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        test.setCurrentPage(new Apexpages.Pagereference('/apex/MultilineEdit'));
        Apexpages.currentPage().getParameters().put('id',q.Id);        
        SearchAndAddMultipleController controller2 = new SearchAndAddMultipleController();
        
        //Test Search Function
        Apexpages.currentPage().getParameters().put('family','TEST');
        Apexpages.currentPage().getParameters().put('field',controller2.getFields()[1].getValue());
        Apexpages.currentPage().getParameters().put('operator','Like');
        Apexpages.currentPage().getParameters().put('searchfield','TEST');
        Apexpages.currentPage().getParameters().put('quicksearch','TEST');
        controller2.runSearch();
        //controller2.submit();
        //controller2.clonequote();
        //controller2.save();
        //controller2.runquery();
    }
    public static testMethod void testerrors()
    {       
        test.setCurrentPage(new Apexpages.Pagereference('/apex/MultilineEdit'));
        SearchAndAddMultipleController controller3 = new SearchAndAddMultipleController();
        Apexpages.currentPage().getParameters().put('id','');   
        controller3.getShowGovernment();
        controller3.getisrenewal();
        controller3.runquery();
        controller3.getpricingmethods();
        controller3.getcontractvehicles();
        try
        {
            controller3.save();
        }
        catch(exception e){}
        controller3.clonequote();
    }
}