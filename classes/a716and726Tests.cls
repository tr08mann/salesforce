@isTest
private class a716and726Tests{
    
    static testMethod void stopDeleteTrigTests()
    {
        Account a1 = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert a1;
        
        Contact c = TestDataFactory.createStandardContact(A1.ID);
        insert c;
        
        TIMBASURVEYS__Survey_Summary__c s = new TIMBASURVEYS__Survey_Summary__c();
        s.TIMBASURVEYS__Contact__c = c.id;
        insert s;
        
        try{ delete c;
           }catch(System.DmlException e){
               system.debug('Caught!');
           }
    }
}