public with sharing class ConsolidatedLeads {

    //JGP 8/27/2012 Create a task and send out an email to the owner of the Account that this lead belongs to
    static Map<Id,Account> accounts = new Map<Id,Account>();
    public static void LeadConvertedTask(List<Lead> leads)
    {
        /****************************************************************************************************
        *Description:   Create Task and Send Email to the Owner upon conversion
        *               
        *                  
        *
        *Revision   |Date       |Author             |AdditionalNotes
        *====================================================================================================
        *   1.0     4/25/2013   Justin Padilla      WO#1355 - Remove dependancy on Lead_Import_Notes__c and modify email sent to owner
        *   1.1     5/9/2013    Justin Padilla      WO#1355 - Task modification
        *   1.2     1/5/2015    Alex Schach         Added try catch blocks to help prevent unexpected exceptions when company was blank
        *****************************************************************************************************/
        //Create a Task and Send out an Email on Task Conversion
        Set<Id> accountIds = new Set<Id>();
        List<Lead> continueToProcess = new List<Lead>();
        for (Lead l:leads)
        {
            //JGP 4/25/2013 WO#1355 - Removed Lead Import Notes Requirement
            //l.Lead_Import_Notes__c != null && 
            //Get the Account Owner Information if it's not already present
            if (l.ConvertedAccountId != null && !accountIds.contains(l.ConvertedAccountId))
            {
                accountIds.add(l.ConvertedAccountId);
                continueToProcess.add(l);
            }
        }
        accounts = new Map<Id,Account>([SELECT
                    Account.Id,
                    Account.OwnerId,
                    Account.CreatedDate
                    FROM Account WHERE Account.Id IN :accountIds]);
        
        List<Task> tasks = new List<Task>();        
        for (Lead l:continueToProcess)
        {
            Account account = accounts.get(l.ConvertedAccountId);
            //throw new MyException('AccountOwnerId: '+account.OwnerId);
            //Create a task
            Task t = new Task();
            Datetime nowMinus1Minute = Datetime.now().addMinutes(0-1);
            if (account.CreatedDate > nowMinus1Minute) //If the Account has not yet been created, set the Id to the LeadOwner
            t.OwnerId = l.OwnerId;
            else
            t.OwnerId = account.OwnerId;
            t.Subject = 'Web to Lead Inquiry for your Account';
            t.Status = 'Completed';
            t.ActivityDate = date.today();
            //t.Description = l.Lead_Import_Notes__c;
            string Description = '';
            Description += 'Web Interest: ';
            if (l.Web_Interest__c != null)
                Description += l.Web_Interest__c;
            Description +='\r\n';
            Description += 'How did you hear about us?: ';
            if (l.LeadSource != null)
                Description += l.LeadSource;
            Description +='\r\n';
            Description += 'Lead Import Notes: ';
            if (l.Lead_Import_Notes__c != null)
                Description += l.Lead_Import_Notes__c;
            t.Description = Description;
            t.WhatId = account.Id;
            t.WhoId = l.ConvertedContactId;
            //tasks.add(t);
            insert(t);
            System.debug('Task Created: '+t.Id);
            
            //Create an Email
            Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
            message.setSubject('Web to Lead Inquiry for your Account');
            message.setSenderDisplayName('Salesforce Administrator');
            message.setSaveAsActivity(false);
            message.setTargetObjectId(account.OwnerId);
            String text = 'Subject: Web to Lead Inquiry for your Account\nComments:'+l.Lead_Import_Notes__c+' \n Link to task: '+URL.getSalesforceBaseUrl().toExternalForm()+'/'+t.Id+'\n\n\n Created as a result of a Lead Conversion';
            message.setPlainTextBody(text);
            try
            {
                Messaging.sendEmail(new Messaging.Singleemailmessage[]{message});
            }
            catch (Exception e)
            {
                System.debug('Error sending email message: '+e.getmessage());
            }           
        }
        if (tasks.size() > 0) insert(tasks);
    }
    
    public static void SetPrimaryLead(Boolean IsInsert, Boolean IsUpdate, List<Lead> leads, Map<Id,Lead> TriggerNewMap)
    {   
        //JGP 8/28/2012 Setting of HasPrimary Lead on Account
        Set<string> hasprimary = new Set<string>();
        Set<string> companylist = new Set<string>();
        system.debug('leads list from trigger: ' +leads);
        
        for (Lead l:leads)
        {
            if (!l.IsConverted && !companylist.contains(l.company.toUpperCase())) //Add the Company to the Listing for Querying later on
                companylist.add(l.company.toUpperCase());
                system.debug('companylist: '+companylist);
            if (l.primary__c && !l.IsConverted) //Company Already has a Primary Lead
                if (hasPrimary.contains(l.company.toUpperCase())) {
                    l.primary__c = false; //Set this one to false
                    system.debug('hasprimary: '+hasprimary);
                }
                else
                {
                    hasprimary.add(l.company.toUpperCase());  //Company did not already have a PrimaryLead, leave this one as primary
                }             
        }
        if(IsUpdate)
        {
            //Lead is being updated, Look at only Leads that are not being updated
            //Complete the listing of Companies with Primary Leads
            try
            {
                for (Lead l : [select id, company, primary__c from lead where isconverted = false and primary__c = true and company in :companylist and id not in :TriggerNewMap.keyset()]) 
                {
                    if (!hasprimary.contains(l.company.toUpperCase())) 
                    {
                        hasprimary.add(l.company.toUpperCase());  
                    }
                }
            }
            catch(exception e)
            {
                system.debug('Cannot set primary lead: '+e);
            }
        }
        if(IsInsert)
        {
            //Lead is being inserted and can not yet be queried so look at all in database.
            try
            {
                for (Lead l : [select id, company, primary__c from lead where isconverted = false and primary__c = true and company in :companylist]) 
                {
                    if (!hasprimary.contains(l.company.toUpperCase())) 
                    {
                        hasprimary.add(l.company.toUpperCase());  
                    }
                }
            }
            catch(exception e)
            {
                system.debug('Cannot set primary lead: '+e);
            }
        }
        //Now that we have a complete listing of Companies with Primary Leads, If we have a company without a primary, set it to primary
        for (Lead l : leads) {
            if (!l.primary__c && !hasPrimary.contains(l.company.toUpperCase())) {
                    l.primary__c = true;    
                    hasprimary.add(l.company.toUpperCase());  
            }
        }
    
    }
    
    public static void SetPrimaryLeadAfter(List<Lead> leads, Map<Id,Lead> leadMap)
    {
        //This operation will take any other Primary Leads already existing within a Company and will remove it if another one has been assigned
        Set<string> hasprimary = new Set<string>();
        Set<string> companylist = new Set<string>();
        List<lead> updateleads = new List<lead>();
        
        for (Lead l:leads)
        {
            if (!companylist.contains(l.company.toUpperCase()))
                companylist.add(l.company.toUpperCase());
            if (l.primary__c && !l.IsConverted && !hasPrimary.contains(l.company.toUpperCase())) {
                    hasprimary.add(l.company.toUpperCase());  
            }
        }
        try
        {
            for (Lead l : [select id, company, primary__c from lead where isconverted = false and primary__c = true and company in :companylist and id not in :leadMap.keyset()]) 
            {
                if (l.company != null && hasprimary.contains(l.company.toUpperCase())) 
                {
                    system.debug('Setting to false:  ' + l.id);
                    l.primary__c = false;
                    updateleads.add(l); 
                }
            }
        }
        catch(exception e)
        {
            system.debug('Cannot set primary lead: '+e);
        }
        if (updateleads.size() > 0)
        update updateleads;
    }
}