@isTest
public with sharing class TestPurchaseOrderTrigger {

    static testMethod void testPurchaseOrderTrigger() 
    {        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Crossbeam');
        insert vp;
        
        Account  acct = TestDataFactory.createStandardAccount('Customer', 'Test'); 
        insert acct;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acct.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.Order_Assigned_To__c = 'Rainmaker FishNet Admin';
        insert opp;
        
        Purchase_Order__c po = TestDataFactory.createPurchaseOrder(vp.Id, opp.id);
        po.Distributor__c = 'Acal';        
        insert po;        
    }
}