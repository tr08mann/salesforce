@istest
public with sharing class TestCompanyLeadController {

public static TestMethod void TestCompanyMethod(){
    
    Lead leadObj = new Lead();
    leadObj.Company = 'Leadcompany';
    leadObj.LastName = 'testLead';
    leadObj.Primary__c = true;
   // leadObj.Name = 'Test';
    insert leadObj;
    
    system.currentPageReference().getParameters().put('id', leadObj.Id);
    
    companyLeadController companyObj = new companyLeadController();
    companyLeadController.leadwrapper leadwrapperObj = new companyLeadController.leadwrapper(leadObj);
    companyObj.lookupLead();
    companyObj.getLead();
    companyObj.getAlert();
    companyObj.primarychange();
    //leadwrapper leadObj = new leadwrapper();
    companyObj.leadmerge();
    companyLeadController.DoNothing2();
    companyObj.getLeads();
    companyObj.leadstatus();
    companyObj.doNothing();
   
    }
}