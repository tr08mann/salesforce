public with sharing class ConsolidatedTasks {
	
	public static void SendEmailToLeadTeamManager(List<Task> tasks)
	{
		//Create a List of Tasks to perform operations on
		List<Task> validTasks = new List<Task>();
		//Create a Set of Valid Task Subject to send Emails on
		//JGP 07/09/2013 WO#1441 Added 'LEAD Team Appointment - PAT' to the valid subject set
		set<string> validSet = new set<string>{'LEAD Team On-site Appointment','LEAD Team Remote Appointment', 'LEAD Team Remote Appointment - Federal/NIT',
				'LEAD Team On-site Evaluation','LEAD Team Remote Appointment - Federal','LEAD Team Appointment - PAT'};

		Set<Id> contactIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		Set<Id> ownerIds = new Set<Id>();
		//Gather Task information for further processing
		for (Task t:tasks)
		{
			if (validSet.contains(t.Subject) && t.AccountId != null)
			{
				validTasks.add(t);
				if(t.WhoId != null && !contactIds.contains(t.WhoId))
					contactIds.add(t.whoId);		
				if(t.AccountId != null && !accountIds.contains(t.AccountId))
					accountIds.add(t.accountId);
				if (!ownerIds.contains(t.OwnerId))
					ownerIds.add(t.OwnerId);
			}
		}
		if (!validTasks.isEmpty()) //Only continue processing if we need to
		{
			//Create Maps with the Name of the Contacts, Accounts needed for the email
			Map<Id,Contact> contactMap = new Map<Id,Contact>([select Id, Name from contact where id IN :contactIds]);
			Map<Id,Account> accountMap = new Map<Id, Account>([select Id, ownerId, Name from Account where id IN :accountIds]);
			//For each of the Accounts selected, get thier owner information
			for(Account acc : accountMap.values()){
				if (!ownerIds.contains(acc.ownerId))
					ownerIds.add(acc.ownerId);
			}
			//Now get the Owner Information
			Map<ID,User> ownerMap = new Map<Id, User>([select Id, Email from User where id IN :ownerIds]);
			//Create Emails for all of the validTasks
			Messaging.SingleEmailMessage[] messages = new Messaging.SingleEmailMessage[]{};
			for (Task t:validTasks)
			{
				string[] sendTo = new string[]{};
				if(accountMap.containsKey(t.accountId) && ownerMap.containsKey(accountMap.get(t.accountId).ownerId) && ownerMap.get(accountMap.get(t.accountId).ownerId).email != null){
					//Add the Account Owner Email to the sendTo listing
					sendTo.add(ownerMap.get(accountMap.get(t.accountId).ownerId).email);
					//Create and add a ccTo for the Task Owner
					string[] ccTo = new string[]{};
					if(ownerMap.containsKey(t.ownerId) && ownerMap.get(t.ownerId).email != null){
						ccTo.add(ownerMap.get(t.ownerId).email);
					}
					ccTo.add(System.Label.SalesManagerEmailCC);
					//Create the Email Message
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(sendTo);
					mail.setCcAddresses(ccTo);
					mail.setSubject('New Lead Team Appointment Has Been Created For You');
					mail.setBccSender(false);
					mail.setUseSignature(false);
					string body = '';
					if(accountMap.containsKey(t.AccountId))
						body +='Company: '+accountMap.get(t.AccountId).Name+'<br />';
					if(task.WhoId != null && contactMap.containsKey(t.whoId))
						body +='Contact: '+contactMap.get(t.whoId).Name+'<br />';
					body +='Subject: ' + t.Subject + '<br />';
					if(t.Description != null)
						body +='Notes: '+t.Description+'<br />';
					if(t.ActivityDate != null)
						body +='Completed Date: '+t.ActivityDate.format()+'<br />';					
					body +='Please click on the link for more information <a href='+ 'https://na4.salesforce.com/'+ t.Id + '>' + t.Id +'</a>';					
					mail.setHtmlBody(body);					
					messages.add(mail);
				}
			}
			if (!messages.isEmpty())
			{
				//Send each email individually so that if there's an error in an email address, it won't stop sending the rest of the messages if there are any
				for (Messaging.SingleEmailMessage message:messages)
				{
					try
					{
						Messaging.sendEmail(new Messaging.SingleEmailMessage[]{message});
					}
					catch (Exception e)
					{
						System.debug('Error Sending Email Message: '+e.getMessage());
					}
				}
			}
		}
	}
}