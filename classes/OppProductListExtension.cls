public class OppProductListExtension {
    private String sortDirection = 'ASC';  
    private String sortExp = 'Pricebookentry.Product2.ProductCode';   
    public String oppId{get;private set;}
    public List<OpportunityLineItem> oppLineRecords;
    public String oppPriceBook{get; private set;}
   
    public OppProductListExtension(ApexPages.StandardController stdController) {
        oppPriceBook = '';
        Opportunity Opp = (Opportunity)stdController.getRecord();
        if(Opp == null || Opp.Id == null ){
                
        } else {
            oppId = Opp.Id;
            opp = [Select Pricebook2Id, Pricebook2.Name from Opportunity WHERE Id = :oppId LIMIT 1];
            
            if(Opp.Pricebook2Id != null){
                
                oppPriceBook  =  '(' + opp.Pricebook2.Name + ')';
            } 
        }   
    }
    
    public String sortExpression {
         get{return sortExp;}
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
             sortDirection = 'ASC';
           sortExp = value;
         }
    }
   
    public String getSortDirection() {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
       return 'ASC';
    else
       return sortDirection;
    } 
    
    public void setSortDirection(String value) {  
       sortDirection = value;
    }
    
    public List<OpportunityLineItem> getoppLineRecords() {
       string sortFullExp = sortExpression  + ' ' + sortDirection;
       oppLineRecords = Database.query('SELECT Id, Pricebookentry.Product2Id, Pricebookentry.Product2.Name, Pricebookentry.Product2.ProductCode,'
                                       +  'Pricebookentry.Product2.Solomon_Item_Class__c, Pricebookentry.Product2.Solomon_UpdateDate__c,' 
                                       + ' Quantity, UnitPrice, TotalPrice, Adjusted_Unit_Cost__c,'
                                       + ' Extended_Cost__c, Extended_Profit__c, Profit_Percent__c, Discount__c'
                                       + ' FROM OpportunityLineItem WHERE OpportunityId = :oppId '
                                       + ' ORDER by ' + sortFullExp );
       return oppLineRecords;
    }
    
    public PageReference ViewData() {
       //build the full sort expression
       
       string sortFullExp = sortExpression  + ' ' + sortDirection;
      
       //query the database based on the sort expression
       oppLineRecords = Database.query('SELECT Id, Pricebookentry.Product2Id, Pricebookentry.Product2.Name, Pricebookentry.Product2.ProductCode,'
                                       +  'Pricebookentry.Product2.Solomon_Item_Class__c, Pricebookentry.Product2.Solomon_UpdateDate__c,' 
                                       + ' Quantity, UnitPrice, TotalPrice, Adjusted_Unit_Cost__c,'
                                       + ' Extended_Cost__c, Extended_Profit__c, Profit_Percent__c, Discount__c'
                                       + ' FROM OpportunityLineItem WHERE OpportunityId = :oppId '
                                       + ' ORDER by ' + sortFullExp );
       return null;
    }
}