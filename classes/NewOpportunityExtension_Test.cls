@isTest
public class NewOpportunityExtension_Test {

    public static testmethod void testnoaccount()
    {
        pagereference pageref = Page.NewOpportunity;
        test.SetCurrentPage(pageref);
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
        
        NewOpportunityExtension pages = new NewOpportunityExtension(controller);

        pages.cancel(); 
    }
    
    public static testmethod void testaccount()
    {
        pagereference pageref = Page.NewOpportunity;
        test.SetCurrentPage(pageref);
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Federal Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');

        
        apexpages.currentPage().getparameters().put('accid',acc.id);
        apexpages.currentPage().getParameters().put('RecordType',opp.RecordTypeId);
        
        apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
        
        NewOpportunityExtension pages = new NewOpportunityExtension(controller);
        
        pages.opp = opp;
        
        pages.save();
        pages.cancel();
    }
        
}