@isTest
public class BriefingCenterRequestExtension_Test 
{
    static testmethod void TestExtension()
    {
        Account acc = TestDataFactory.createStandardAccount('Vendor', 'Test');
        insert acc;
        
        Account acc2 = TestDataFactory.createStandardAccount('Customer', 'Fishnet Security, Inc. (Parent)');
        insert acc2;

        Contact c = TestDataFactory.createStandardContact(Acc.ID);
        insert c;
        
        Contact c2 = TestDataFactory.createStandardContact(Acc2.ID);
        insert c2;
        
        //Create a BRC
        Briefing_Center_Request__c bcr = TestDataFactory.createBriefingCenterRequest(acc.id);
        insert bcr;
        
        Apexpages.currentPage().getParameters().put('accid',acc.id);
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(bcr);
        BriefingCenterRequestExtension page = new BriefingCenterRequestExtension(controller);
        
        page.getshowAttendees();
        page.getshowFNSAttendees();
        page.getshowVendorAttendees();
        page.getselectAccount();
        page.getselectVendor();                
                
        List<SelectOption> a = page.attendees[0].getAttendees();
        if (a.size() > 0)
        {
            page.attendees[0].SelectedAttendee = a[0].getValue(); //SELECT Option
            //Test Save with SELECTS chosen
            page.Save();
            //Change Selections
            page.attendees[0].SelectedAttendee = a[1].getValue(); //First valid value
            
            page.AddNewContactRow();
            page.Save();
        }
        List<SelectOption> f = page.fnsattendees[0].getAttendees();
        if (f.size() > 0)
        {
            page.fnsattendees[0].SelectedFNS = f[0].getValue(); //SELECT Option
            //Test Save with SELECTS chosen
            page.Save();
            //Change Selections
            page.fnsattendees[0].SelectedFNS = f[1].getValue(); //First valid value
            
            page.AddNewFNSRow();
            page.Save();
        }
            List<SelectOption> v = page.vendorattendees[0].getAttendees();
            if (v.size() > 0)
        {
            page.vendorattendees[0].SelectedVendor = v[0].getValue(); //SELECT Option
            //Test Save with SELECTS chosen
            page.Save();
            //Change Selections
            page.vendorattendees[0].SelectedVendor = v[1].getValue(); //First valid value
            
            page.AddNewVendorRow();
            page.Save();
        }
        page.PopulateAccount();
        page.PopulateVendor();
    }
}