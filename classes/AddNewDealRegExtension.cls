public class AddNewDealRegExtension 
{
/****************************************************************************************************
    *Description:   Deal Registration - Extension for VF page AddNewDealReg
    *                
    *        
    *
    *Revision   |Date       |Author             |AdditionalNotes
    *====================================================================================================
    *   1.0     4/22/2014    Alex Schach         Initial version
    *   2.0     5/23/2014    Alex Schach         updated conditional DNQ reason, available deal reg statuses
    *                                            allowed users with Deal Reg Object permission set to create duplicates
    *   2.5     5/29/2014    Alex Schach         Updated available deal reg statuses
    *   2.6     6/06/2014    Alex Schach         Added submitted date field to page
    *   2.7     6/17/2014    Alex Schach         Removed FNS professional services. Added "Canceled" status.
    *   3.0     8/12/2014    Alex Schach         Updated DR Name, available statuses
    ******************************************************************************************************/ 
private opportunity opp;
private deal_registration__c newdealreg = new deal_registration__c();
public boolean permissions {get;set;}
public boolean Duplicate {get;set;}
public boolean disabl {get;set;}
public boolean previousduplicate {get;set;}
public boolean Error{get;set;}
public string uniqueid ='';
public set<PermissionSetAssignment> permission = new set<PermissionSetAssignment>([select assigneeid from permissionsetassignment where permissionsetid = '0PS60000000WMhB']);


    public AddNewDealRegExtension(ApexPages.StandardController stdcontroller) 
    {
        Duplicate = false;
        disabl = false;
        Error = false;
        permissions = false;
            if(apexpages.currentpage().getparameters().get('Id')!=null && apexpages.currentpage().getparameters().get('Id')!='')
            {
                this.opp = [select
                Id,
                currencyisocode,
                stageName,
                isclosed,
                account.name,
                owner.id,
                deal_reg_desk_rep__c,
                region_opp__c,
                opportunity_number__c,
                inside_sales_NEW__c,
                type,
                recordtypeid                
                from opportunity
                where Id = :apexpages.currentpage().getparameters().get('Id')];
                system.debug('oppid: '+opp);
            }
            if(opp.inside_sales_NEW__c == null || opp.deal_reg_desk_rep__c == null)
            {
                apexpages.addmessage(new apexpages.message(apexpages.severity.ERROR, 'There must be an Inside Sales and Deal Registration representative on the opportunity to sumbit for deal registration.'));
                disabl = true;
            }
            if(opp.recordtypeid == '012600000009Lr3')
            {
                apexpages.addmessage(new apexpages.message(apexpages.severity.ERROR, 'This opportunity is locked. Unlock the opportunity to sumbit a deal registration.'));
                disabl = true;
            }
            if(opp.isclosed)
            {
                Duplicate = true;
                disabl = true;
                system.debug('opp closed: '+Duplicate);
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,'This opportunity has been closed. Please change the stage to submit for deal registration.'));
            }
            for(permissionsetassignment p: permission)
            {
                if(p.assigneeid == userinfo.getuserid())
                {
                    permissions=true;
                }
            }
            system.debug('permission:' +permission);
    }
    static Schema.DescribeFieldResult p = deal_registration__c.deal_registration_status__c.getdescribe();
    static List<Schema.PicklistEntry> statusop = p.getpicklistvalues();
    private list<schema.picklistentry> thestatuses
    {
        get
        {
            return statusop;
        }
    }
    public List<SelectOption> DealRegStatusOptions
    {
        get
        {
            List<SelectOption> RetVal = new List<SelectOption>();
            for(schema.picklistentry status: thestatuses)
            {
                if(status.getlabel() != 'Deal Registration Submitted, Pending Approval' && status.getlabel() != 'Deal Registration Approved' && status.getlabel() != 'Deal Registration Denied' && status.getlabel() != 'Deal Registration Tentatively Approved' && status.getlabel() != 'Deal Reg Canceled/Closed/Rejected')
                {
                    RetVal.add(new SelectOption(status.getlabel(), status.getvalue())); 
                }
            }
            return RetVal;  
        }
    }
    public List<SelectOption> DealRegStatusOptions2
    {
        get
        {
            List<SelectOption> RetVal = new List<SelectOption>();
            for(schema.picklistentry status: thestatuses)
            {
                if(status.getlabel() != 'Deal Registration Approved' && status.getlabel() != 'Deal Registration Denied' && status.getlabel() != 'Deal Registration Tentatively Approved' && status.getlabel() != 'Deal Reg Canceled/Closed/Rejected')
                {
                    RetVal.add(new SelectOption(status.getlabel(), status.getvalue())); 
                }
            }
            return RetVal;  
        }
    }
    public deal_registration__c getDealreg()
    {
        return this.newdealreg;
    }
    public void setDealreg(deal_registration__c deal)
    {
        this.newdealreg = deal;
    }
    public void savedeal()
    {
        if(newdealreg.vendor_profile__c == null)
        {
            Error = true;
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You must enter a vendor to submit for deal registration.'));
            return;
        }
        else
        {
        system.debug('oppid: '+opp);
        list<deal_registration__c> existing = new list<deal_registration__c>();
        string vendorname = '';
        integer length = opp.account.name.length();
        string accountname = '';
        if(length >=40)
        {
            accountname = opp.account.name.substring(0,40);
        }
        else
        {
            accountname = opp.account.name;
        }
        //pull vendor name from the vendor profile, as it will not work with vendor_profile__r.name without a query
        try
        {
            vendorname = [select name from vendor_profile__c where id =: newdealreg.vendor_profile__c limit 1].name;
        }
        catch(exception e)
        {
            system.debug('vendor name: '+e);
        }
        uniqueid = vendorname + '_'+ accountname + '_' + opp.opportunity_number__c;
        system.debug('tempuniqueID: ' +uniqueid);
        //select existing deal registration records to prevent duplicates
        existing = [select deal_registration__c.Id, vendor_profile__r.name from deal_registration__c
                    where deal_registration__c.DealRegUniqueID__c like: uniqueid limit 1];
                    system.debug('existingdealreg: ' +existing);
                    system.debug('userid: '+ userinfo.getuserid());
                    system.debug('permissions: '+permissions);
        if(permissions ==false)
        {
            if(existing.size()>0)
            {
                Duplicate = true;
                disabl = true;
                system.debug('existing found Duplicate: '+Duplicate);
                apexpages.addmessage(new apexpages.message(apexpages.severity.error,'A Deal Registration for this vendor already exists on this opportunity. Please notify the Deal Registration Desk Representative if changes need to be made.'));
                return;
            }
            else
            {
                 Duplicate = false;
                 disabl = false;
                 system.debug('existing not found Duplicate: '+Duplicate);
                 ConfirmedSave();
            }
        }
        else 
        {   
            //allows users with deal reg object permissions to create duplicates
            system.debug('previous: ' +previousduplicate);
            if(existing.size()>0)
            {
                Duplicate = true;
                disabl = false;
                if(previousduplicate == null || previousduplicate == false)
                {
                    system.debug('existing found Duplicate: '+Duplicate);
                    apexpages.addmessage(new apexpages.message(apexpages.severity.error,'A Deal Registration for this vendor already exists on this opportunity. To save a duplicate, click "Save" again.'));
                    previousduplicate = true;
                    return;
                }
                else
                {
                    duplicate = false;
                    previousduplicate = false;
                    ConfirmedSave();
                }
            }
            else
            {
                Duplicate = false;
                disabl = false;
                system.debug('duplicate created by rep: '+Duplicate);
                ConfirmedSave();  
            }
        }  
        }                   
    }
    public void ConfirmedSave()
    {
        Error = false;
        system.debug('confirmedsave Duplicate: '+Duplicate);
        if(newdealreg.deal_registration_status__c == 'Vendor Has No Deal Registration Program' || newdealreg.deal_registration_status__c == 'Deal Already Registered on Another FNS Opportunity' || this.opp.type == 'Renewal Business')
        {
            newdealreg.ownerid = this.opp.inside_sales_NEW__c;
            newdealreg.Opportunity__c = this.opp.Id;
            newdealreg.name = uniqueid;
            if(this.opp.recordtypeid != '012300000000Prx')
            {
                newdealreg.region__c = this.opp.region_opp__c;
            }
            else
            {
                newdealreg.region__c = 'Federal';
            }
            system.debug('newdealreg: '+newdealreg);
        }
        else
        {
            newdealreg.ownerid = this.opp.deal_reg_desk_rep__c;
            newdealreg.Opportunity__c = this.opp.Id;
            newdealreg.name = uniqueid;
            if(this.opp.recordtypeid != '012300000000Prx')
            {
                newdealreg.region__c = this.opp.region_opp__c;
            }
            else
            {
                newdealreg.region__c = 'Federal';
            }
            system.debug('newdealreg: '+newdealreg);
        }
        try
        {
            upsert newdealreg;    
        }
        catch(exception e)
        {
            apexpages.addmessage(new apexpages.message(apexpages.severity.ERROR, 'Error inserting Deal Registration: '+e.getmessage()));
            Error = true;
        }
        if(Error == false)
        {
            apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, 'Deal Registration Saved!'));
        }
    }
}