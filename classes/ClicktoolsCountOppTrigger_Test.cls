@isTest
public class ClicktoolsCountOppTrigger_Test{

    static testmethod void test1()
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;

        clicktools_survey__c ct = new clicktools_survey__c();
        ct.opportunity__c = opp.id;
        ct.OwnerId = userinfo.getuserid();
        insert ct;  
        delete ct;
    }
}