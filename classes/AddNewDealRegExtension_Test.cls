@isTest
public class AddNewDealRegExtension_Test
{
    public static testmethod void testnoisr()
    {    
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testnoisr');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_new__c = null;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
    
            deal_registration__c newdealreg = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdealreg);            
            list<selectOption> testoptions = pages.DealRegStatusOptions;
            list<selectOption> testoptions2 = pages.DealRegStatusOptions2;
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testnoduplicate()
    {
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testnoduplicate');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            vendor_profile__c vp2 = TestDataFactory.createVendorProfile('Blue Coat Systems');
            insert vp2;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
            
            deal_registration__c newdeal2 = TestDataFactory.createStandardDealReg(opp.id, vp2.id, vp2.name, opp.opportunity_number__c, 'Vendor Has No Deal Registration Program');
            pages.setdealreg(newdeal2);
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testfederal()
    {
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testfederal');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            vendor_profile__c vp2 = new vendor_profile__c();
            vp2.name = 'Blue Coat Systems';
            insert vp2;
            
            opportunity opp = TestDataFactory.createStandardOpp('Federal Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
            
            deal_registration__c newdeal2 = TestDataFactory.createStandardDealReg(opp.id, vp2.id, vp2.name, opp.opportunity_number__c, 'Vendor Has No Deal Registration Program');
            pages.setdealreg(newdeal2);
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testduplicate()
    {
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testduplicate');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Federal Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            insert newdeal;
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testclosed()
    {    
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testclosed');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Federal Opportunity', acc.id, 'Closed Won (100%)/SVC-Signed SOW');
            opp.ownerid = drd.id;
            opp.CloseDate = system.today();
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            opp.Opportunity_Purchase_Requirements__c = 'No PO Required';
            opp.Closed_Won_Lost_Reason__c = 'WON: Best Price';
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testpermissionsnoduplicate()
    {    
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        
        PermissionSet ps = [SELECT id from PermissionSet WHERE name = 'Deal_Registration_Object'];
        user drd = TestDataFactory.createStandardUser('testpermissionsnoduplicate');
        insert drd;
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = drd.id, PermissionSetId = ps.id);
        insert psa;
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.previousduplicate = true;
            pages.savedeal();
        }
    }
    public static testmethod void testpermissionsduplicate()
    {    
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        
        PermissionSet ps = [SELECT id from PermissionSet WHERE name = 'Deal_Registration_Object'];
        user drd = TestDataFactory.createStandardUser('testpermissionsduplicate');
        insert drd;
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = drd.id, PermissionSetId = ps.id);
        insert psa;
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Federal Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            insert newdeal;
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testlocked()
    {
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testlocked');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Locked Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
            
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);  
            pages.savedeal();
            pages.getdealreg();
        }
    }
    public static testmethod void testnovendor()
    {
        pagereference pageref = Page.AddNewDealReg;
        test.SetCurrentPage(pageref);
        user drd = TestDataFactory.createStandardUser('testnovendor');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
            insert vp;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
                  
            apexpages.currentpage().getparameters().put('id',opp.id);
            
            apexpages.standardcontroller controller = new apexpages.standardcontroller(opp);
            
            AddNewDealRegExtension pages = new AddNewDealRegExtension(controller);
            
            deal_registration__c newdeal = TestDataFactory.createStandardDealReg(opp.id, null, 'Symantec Corporation', opp.opportunity_number__c, 'Deal Registration Pending Submission');
            pages.setdealreg(newdeal);
            pages.savedeal();
            pages.getdealreg();
        }
    }
}