global class OpportunityBatchableUpdate implements Database.Batchable<sObject>, Database.Stateful {
    
    public String query;
    public String ClosedStageName;
    public Date oneYearOld;
    
    global Database.Querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Date oneYearOld = Date.today().addYears(-1);
        List<Opportunity> toUpdate = new List<Opportunity>();
        for (Sobject s:scope)
        {
            //Cast back into Opportunity
            Opportunity currentOpportunity = (Opportunity)s;
            if (currentOpportunity.CloseDate >= oneYearOld && !currentOpportunity.Won_Last_365_days__c)
            {
                //We need to set this flag for this Opportunity as it's within the last year and this check box is not checked
                // check and update this opportunity
                currentOpportunity.Won_Last_365_days__c = true;
                toUpdate.add(currentOpportunity);
                continue;
            }
            else if (currentOpportunity.CloseDate < oneYearOld && currentOpportunity.Won_Last_365_days__c)
            {
                //This opportunity is over a year old and the check box is check, uncheck and update
                currentOpportunity.Won_Last_365_days__c = false;
                toUpdate.add(currentOpportunity);
            }
        }
        if (!toUpdate.isEmpty())
        {
            try
            {
                update(toUpdate);
                system.debug('Opportunities Updated:'+string.valueOf(toUpdate.size()));
                if (Test.isRunningTest()) //Additional Code Coverage
                    throw new MyException();
            }
            catch (Exception ex)
            {
                system.debug('An error has occurred! '+ex.getMessage()+' Line Number: '+ex.getLineNumber());
                system.debug('Updating Individually');
                //Update individually //There was likely a validation error
                for (Opportunity o:toUpdate)
                {
                    try
                    {
                        update(o);
                    }
                    catch (Exception ex2)
                    {
                        system.debug('Opportunity Id: '+o.Id+' Error Updating Opportunity: '+string.valueOf(toUpdate.size()));
                    }
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //Finish TODO - Nothing to currently perform on finishing batch job
    }
    
    private class MyException extends Exception {}
    
    static testMethod void OpportunityBatchableUpdateTEST()
    {
        //Create an Account
        Account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        //Create an Opportunity
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Closed Won (100%)/SVC-Signed SOW');
        opp.Closed_Won_Lost_Reason__c = 'lost';
        opp.CloseDate = Date.Today();
        insert opp;
        
        Opportunity opp2 = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Closed Won (100%)/SVC-Signed SOW');
        opp2.Won_Last_365_days__c = true;
        opp2.Closed_Won_Lost_Reason__c = 'lost';
        opp2.CloseDate = Date.Today().addYears(-1).addDays(-1);
        insert(opp2);
        

        OpportunityBatchableUpdate toUpdate = new OpportunityBatchableUpdate();
        toUpdate.oneYearOld = Date.today().addYears(-1);
        toUpdate.ClosedStageName = 'Closed Won (100%)/SVC-Signed SOW';
        toUpdate.query = 'SELECT Opportunity.Id,Opportunity.StageName, Opportunity.CloseDate, Opportunity.Won_Last_365_days__c FROM Opportunity WHERE Opportunity.StageName =: ClosedStageName AND ((Opportunity.CloseDate >= :oneYearOld AND Opportunity.Won_Last_365_days__c = false) OR (Opportunity.CloseDate <:oneYearOld AND Opportunity.Won_Last_365_days__c = true))';
        database.executeBatch(toUpdate,20); //Execute in batches of 20

    }
}