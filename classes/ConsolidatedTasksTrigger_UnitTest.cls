@isTest (seealldata=true)
public with sharing class ConsolidatedTasksTrigger_UnitTest {
  
    static testMethod void testoldquote()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
        insert acc;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
        opp.CloseDate = Date.today();
        opp.Deadline__c = Date.today(); 
        insert opp;
        
        SFDC_520_Quote__c temp = new SFDC_520_Quote__c();
        temp.Opportunity__c = opp.id;
        temp.Deal_Registration_Status__c = 'Deal Registration Pending Submission';
        insert temp;
        
        Task t = new Task();
        t.WhatId = temp.Id;
        t.Activity_Type__c = 'Talk To';
        t.Subject = 'CCA Initial Touch';
        insert t;
        
    }
    static testMethod void testnulldeadline()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
        insert acc;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
        opp.CloseDate = Date.today(); 
        insert opp;
        
        Task t = TestDataFactory.createTask(opp.id, opp.ownerid);
        t.Activity_Type__c = 'Talk To';
        t.Subject = 'CCA Initial Touch';
        insert t;
    }
    static testMethod void testdonotcall()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
        insert acc;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
        opp.CloseDate = Date.today(); 
        opp.Renewal_Do_Not_Call__c = true;
        opp.Deadline__c = date.today(); 
        insert opp;  
        
        Task t = TestDataFactory.createTask(opp.id, opp.ownerid);
        t.Activity_Type__c = 'Talk To';
        t.Subject = 'CCA Initial Touch';
        insert t;
    }
    static testMethod void testinitialtouch()
    {
        User u2 = TestDataFactory.createStandardUser('testinitialtouchcca');
        u2.Employee_ID__c = 8501;
        u2.title = 'Customer Care Agent';
        insert u2;
          
        User u = TestDataFactory.createStandardUser('testinitialtouchuser');
        u.CCA_Rep__c = u2.id;
    
        system.runAs(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
            
            Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
            opp.CloseDate = Date.today();
            opp.Deadline__c = Date.today().addmonths(2); 
            opp.ownerid = u.id;
            opp.type = 'Renewal Business';
            opp.renewal_type__c = 'Existing Renewal Business';
            opp.Inside_Sales_NEW__c = Userinfo.getUserId();
            insert opp;    
            
            Task t = TestDataFactory.createTask(opp.id, u2.id);
            t.Subject = 'CCA Initial Touch';
            insert t;
            
            t.Activity_Type__c = 'Talk to';
            update t;
                            
            t.Activity_Type__c = 'Voicemail';
            t.CCA_Completed_Date__c = date.today();
            update t; 
        }
    }
    static testMethod void testsecondtouch()
    {
        User u2 = TestDataFactory.createStandardUser('testsecondtouchcca');
        u2.Employee_ID__c = 8502;
        u2.title = 'Customer Care Agent';
        insert u2;
          
        User u = TestDataFactory.createStandardUser('testsecondtouchuser');
        u.CCA_Rep__c = u2.id;
    
        system.runAs(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
            
            Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
            opp.CloseDate = Date.today();
            opp.Deadline__c = Date.today().addmonths(2); 
            opp.ownerid = u.id;
            opp.type = 'Renewal Business';
            opp.renewal_type__c = 'Existing Renewal Business';
            opp.Inside_Sales_NEW__c = Userinfo.getUserId();
            opp.Customer_Care_Agent__c = u2.id;
            insert opp;    
                    
            Task t = TestDataFactory.createTask(opp.id, u2.id);
            t.Subject = 'CCA 2nd Touch';
            insert t;
            
            t.Activity_Type__c = 'Talk To';
            update t;
            
            t.Activity_Type__c = 'Voicemail';
            update t;
            
            t.Activity_Type__c = 'Talk To';
            t.cca_Last_result__c = 'Talk To';
            update t;
            
            t.Activity_Type__c = 'Voicemail';
            update t;
            
            t.Activity_Type__c = 'Initial Touch Reply';
            update t;
            
            t.Activity_Type__c = '2nd/3rd Touch Reply';
            update t;   
        }     
    }
    static testMethod void testthirdtouch()
    {
        User u2 = TestDataFactory.createStandardUser('testthirdtouchcca');
        u2.Employee_ID__c = 8503;
        u2.title = 'Customer Care Agent';
        insert u2;
          
        User u = TestDataFactory.createStandardUser('testthirdtouchuser');
        u.CCA_Rep__c = u2.id;
    
        system.runAs(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
            
            Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
            opp.CloseDate = Date.today();
            opp.Deadline__c = Date.today().addmonths(2); 
            opp.ownerid = u.id;
            opp.type = 'Renewal Business';
            opp.renewal_type__c = 'Existing Renewal Business';
            opp.Inside_Sales_NEW__c = Userinfo.getUserId();
            opp.Customer_Care_Agent__c = u2.id;
            insert opp;    
                    
            Task t = TestDataFactory.createTask(opp.id, u2.id);
            t.Subject = 'CCA 3rd Touch';
            insert t;
            
            t.Activity_Type__c = 'Talk To';
            update t;
            
            t.Activity_Type__c = 'Voicemail';
            update t;
            
            t.Activity_Type__c = 'Talk To';
            t.cca_Last_result__c = 'Talk To';
            update t;
            
            t.Activity_Type__c = 'Voicemail';
            update t;
            
            t.Activity_Type__c = 'Initial Touch Reply';
            update t;
            
            t.Activity_Type__c = '2nd/3rd Touch Reply';
            update t;
        }
    }
    static testMethod void testfourthfifthtouch()
    {
        User u2 = TestDataFactory.createStandardUser('testfourthfifthcca');
        u2.Employee_ID__c = 8503;
        u2.title = 'Customer Care Agent';
        insert u2;
          
        User u = TestDataFactory.createStandardUser('testfourthfifthuser');
        u.CCA_Rep__c = u2.id;
    
        system.runAs(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
            
            Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
            opp.CloseDate = Date.today();
            opp.Deadline__c = Date.today().addmonths(2); 
            opp.ownerid = u.id;
            opp.type = 'Renewal Business';
            opp.renewal_type__c = 'Existing Renewal Business';
            opp.Inside_Sales_NEW__c = Userinfo.getUserId();
            opp.Customer_Care_Agent__c = u2.id;
            insert opp;    
                    
            Task t = TestDataFactory.createTask(opp.id, u2.id);
            t.Subject = 'CCA 4th Touch';
            insert t;
            
            t.Activity_Type__c = 'Talk To';
            update t;
            
            t.Activity_Type__c = 'Voicemail';
            update t;
            
            t.Activity_Type__c = '2nd/3rd Touch Reply';
            update t; 
            
            Task t2 = TestDataFactory.createTask(opp.id, u2.id);
            t2.Subject = 'CCA 5th Touch';
            insert t2;
            
            t2.Activity_Type__c = 'Talk To';
            update t2;
        }
    }
    static testMethod void testdelete()
    {
        User u = TestDataFactory.createStandardUser('testdeleteuser');
    
        system.runas(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
                    
            Task t = TestDataFactory.createTask(acc.id, acc.ownerid);
            t.Subject = 'LEAD Team On-site Appointment';
            t.Status = 'Completed';
            insert t;
            
            try
            {
                delete t;
            }
            catch(exception e)
            {
                
            }
        }
            
    }  
    static testMethod void testnoCCA()
    {
        User u = TestDataFactory.createStandardUser('testnoccauser');
    
        system.runAs(u)
        {
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            insert acc;
            
            Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');      
            opp.CloseDate = Date.today();
            opp.Deadline__c = Date.today().addmonths(2); 
            opp.ownerid = u.id;
            opp.type = 'Renewal Business';
            opp.renewal_type__c = 'Existing Renewal Business';
            insert opp;    
            
            Task t = TestDataFactory.createTask(opp.id, opp.ownerid);
            t.Subject = 'CCA Initial Touch';
            insert t;
            
            t.Activity_Type__c = 'Talk to';
            try
            {
                update t;
            }
            catch(exception e)
            {
                
            }
        }
    }    
    static testMethod void AddressablePoolOfAccounts()
    {
        //Find a User with the Role that we need
        id role = [SELECT id from UserRole WHERE name ='LEAD Team Agent' limit 1].id;
        
        user u = TestDataFactory.createStandardUser('addressablepooluser');
        u.userroleid = role;
        
        system.runAs(u)
        {
            Campaign camp = new Campaign();
            camp.Name = 'Test Campaign';
            insert camp;
            
            Account acc = TestDataFactory.createStandardAccount('Customer', 'TestAccount'); 
            acc.lead_call_campaign__c = camp.id;
            acc.Lead_Campaign_Participant__c = false;
            acc.Lead_Team_Touched__c = false;
            acc.Lead_Team_Contacted__c = false;
            acc.Lead_Team_Penetrated__c = false;    
            insert acc;
            
            //Create a Contact for the Account
            Contact c = TestDataFactory.createStandardContact(acc.id);
            c.OwnerId = u.id;
            insert c;
            
            List<Task> toInsert = new List<Task>();
            //Participant Criteria
            //Create Tasks for the Account
            Task task1 = TestDataFactory.createTask(acc.id, u.id);
            task1.WhoId = c.Id;
            task1.Subject = 'Call';
            task1.Activity_Type__c = 'Spoke';
            toInsert.add(task1);
            
            //Touched Criteria
            Task task2 = TestDataFactory.createTask(acc.id, u.id);
            task2.WhoId = c.Id;
            task2.Subject = 'Call';
            task2.Status = 'Completed';
            task2.Activity_Type__c = 'Not Started';
            task2.ActivityDate = date.today().addDays(5);
            toInsert.add(task2);
            
            //Contacted Criteria
            Task task3 = TestDataFactory.createTask(acc.id, u.id);
            task3.WhoId = c.Id;
            task3.Subject = 'Call';
            task3.Status = 'Completed';
            task3.Activity_Type__c = 'Spoke';
            task3.ActivityDate = date.today().addDays(5);
            toInsert.add(task3);
            
            //Penetrated Criteria
            Task task4 = TestDataFactory.createTask(acc.id, u.id);
            task4.WhoId = c.Id;
            task4.Subject = 'LEAD Team On-site Appointment';
            task4.Status = 'Completed';
            task4.ActivityDate = date.today().addDays(5);
            toInsert.add(task4);
            insert toInsert;
            
            acc.Lead_Expiry__c = date.today().addmonths(2);
            update acc;
            
            Task task5= TestDataFactory.createTask(acc.id, u.id);
            task5.whoId = c.Id;
            task5.Subject = 'LEAD Team On-site Appointment';
            task5.Status = 'Completed';
            task5.ActivityDate = date.today().addDays(5);
            insert task5;
        }
    }
}