public with sharing class CampaignlookupController {
    /*
    public Campaign campobj{get;set;}
    */
    public Opportunity oppObj{get;set;}
    Public List<Campaign> camp{get;set;}
    public String query {get; set;}
    public CampaignlookupController(ApexPages.StandardController controller) {    
    	/*
    	campobj= new Campaign();
    	Campaign tempCamp = getCampaign(controller.getId());
    	*/
    	oppObj = new Opportunity();
    	Opportunity tempOpp = getOpportunity(controller.getId());
    	//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Opportunity: '+tempOpp));
    	//camp = [Select Name, Id, Region__c, District__c from Campaign where IsActive=true AND ((District__c=:tempOpp.District__c) Or (Region__c=:tempOpp.Region__c))];
    	
    	List<String> options = new List<String>();
    	string soql = 'Select Name, Id, Region__c, District__c from Campaign where IsActive=true';
    	if (tempOpp.District__c != null && tempOpp.District__c != '')
    		options.add('District__c= \''+ tempOpp.District__c+'\'');
    	if (tempOpp.Region_Opp__c != null && tempOpp.Region_Opp__c != '')
    		options.add('Region__c= \''+tempOpp.Region_Opp__c+'\'');
    	if (!options.isEmpty())
    	{
    		if(options.size() == 1)
    			soql += ' AND '+options[0];
    		else if (options.size() == 2)
    			soql += ' AND ('+options[0]+' OR '+options[1]+')';
    	}
		camp = database.query(soql);
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Query: '+soql));
    	//camp = [Select Name, Id, Region__c, District__c from Campaign where IsActive=true AND (District__c=:tempCamp.District__c Or Region__c=:tempCamp.Region__c)];
    }
    
    public PageReference runQuery(){
       return null;
    }
    
    public void onChange(){
     	//camp = [Select Name, Id, Region__c, District__c from Campaign where IsActive=true AND (Region__c=:campobj.Region__c)];
     	camp = [Select Name, Id, Region__c, District__c from Campaign where IsActive=true AND (Region__c=:oppObj.Region__c)];
    }
    /*
    public Campaign getCampaign(String camPId){
    	return [Select Name, Id, Region__c, District__c from Campaign where Id =: camPId];
    }
    */
    public Opportunity getOpportunity(String oppPId){
    	return [Select Name, Id, Region_Opp__c, District__c from Opportunity where Id =: oppPId];
    }
}