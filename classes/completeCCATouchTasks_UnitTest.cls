@isTest
public with sharing class completeCCATouchTasks_UnitTest{
    
    static testMethod void completeCCATouchTasks_M1()
    {
        Account accObj = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert accObj;
        
        Opportunity oppObj = TestDataFactory.createStandardOpp('Commercial Opportunity', accObj.id, 'Prospect');
        oppObj.Closed_Won_Lost_Reason__c = 'Lost';
        oppObj.CloseDate = date.today();
        insert oppObj;
        
        oppObj.StageName = 'Closed-Cancelled';
      
        
        Task TaskObj = new Task();
        TaskObj.Subject = 'CCA Initial Touch';
        TaskObj.WhatId = oppObj.id;
        TaskObj.Status = 'Deferred';
        
        //Create task Record
        insert TaskObj; 
        
        //update Opportunity record     
        update oppObj; 
    }   
}