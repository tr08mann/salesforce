@isTest
public class QuoteRedirectExtension_Test{

    public static testmethod void NoDealReg()
    {
        pagereference pageref = Page.QuoteRedirect;
        test.SetCurrentPage(pageref);
        
        user drd = TestDataFactory.createStandardUser('NoDealReg');
        
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
            insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            insert acc;
            
            contact con = TestDataFactory.createStandardContact(Acc.ID);
            insert con;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            insert opp;
            
            OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
            insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, vp.name, '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            
            try
            {
                insert qli;
            }
            catch(exception e)
            {
                system.debug(e);
            }
            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteRedirectExtension pages = new QuoteRedirectExtension(controller);
            
            pages.go();
        }
    }
    
    public static testmethod void WithDealReg()
    {
        pagereference pageref = Page.QuoteRedirect;
        test.SetCurrentPage(pageref);
        
        user drd = TestDataFactory.createStandardUser('WithDealReg');
        
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
            insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            insert acc;
            
            contact con = TestDataFactory.createStandardContact(Acc.ID);
            insert con;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            insert opp;
            
            OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
            insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
            p.renewable_yesno__c = 'Yes';
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
            insert deal;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;
          
            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteRedirectExtension pages = new QuoteRedirectExtension(controller);
        }
    }
    
    public static testmethod void WithNonMarkedProducts()
    {
        pagereference pageref = Page.QuoteRedirect;
        test.SetCurrentPage(pageref);
        
        user drd = TestDataFactory.createStandardUser('WithNonMarkedProducts');
        
        system.runas(drd)
        {
            vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
            insert vp;
            
            account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
            insert acc;
            
            contact con = TestDataFactory.createStandardContact(Acc.ID);
            insert con;
            
            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            insert opp;
            
            OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
            insert ocr;
            
            Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
            insert quote;
            
            Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
            insert p;
        
            PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
            insert pbe;
            
            Deal_Registration__c deal = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, opp.Opportunity_Number__c, 'Pending');
            insert deal;
            
            quotelineitem qli = TestDataFactory.createQuoteLineItem(quote.id, pbe.id);
            insert qli;
          
            apexpages.currentpage().getparameters().put('id',quote.id);
           
            apexpages.standardcontroller controller = new apexpages.standardcontroller(quote);
           
            QuoteRedirectExtension pages = new QuoteRedirectExtension(controller);

        }
    }
}