public with sharing class AddBriefingAttendeesExtension {
	
	public Briefing_Center_Request__c BCR{get;set;}
	List<Briefing_Attendees__c> existingAttendees{get;set;}
	Set<Id> existingContactIds{get;set;}
	public List<Contact> available{get;set;}
	public List<Contact> fns{get;set;}	
	public List<Contact> vendor{get;set;}
	
	public List<ContactAttendee> attendees{get;set;}
	public List<FNSAttendee> fnsattendees{get;set;}
	public List<VendorAttendee> vendorattendees {get;set;}
		
	public AddBriefingAttendeesExtension(Apexpages.StandardController controller)
	{
		if (controller.getId()!= null)
		{
			existingContactIds = new Set<Id>();
			attendees = new List<ContactAttendee>();
			fnsattendees = new List<FNSAttendee>();
			vendorattendees = new List<VendorAttendee>();
			vendor = new List<Contact>();
			//Get the Account information for this Briefing Center Request
			BCR = [SELECT
			Briefing_Center_Request__c.Account__c,
			Briefing_Center_Request__c.Account__r.Id,
			Briefing_Center_Request__c.Account__r.Name,
			Briefing_Center_Request__c.Vendor__c,
			Briefing_Center_Request__c.Vendor__r.Id
			FROM Briefing_Center_Request__c
			WHERE Briefing_Center_Request__c.Id =: controller.getId()];
			//Get a listing of Contacts already added to this BCR
			existingAttendees = [SELECT
			Briefing_Attendees__c.Id,
			Briefing_Attendees__c.Contact__c,
			Briefing_Attendees__c.Contact__r.Id
			FROM Briefing_Attendees__c
			WHERE Briefing_Attendees__c.Briefing_Center_Request__c =: BCR.Id];
			for (Briefing_Attendees__c a:existingAttendees)
			{
				existingContactIds.add(a.Contact__r.Id);
			}			
			
			//Now get All of the Contacts for this Account
			available = new List<Contact>();
			if (BCR.Account__r.Id != null)
			{
				available = [SELECT
				Contact.Id,
				Contact.FirstName,
				Contact.LastName
				FROM Contact
				WHERE Contact.AccountId =: BCR.Account__r.Id
				AND Contact.Id not IN :existingContactIds];
			}
			
			//Now Get All of the Contacts for the Fishnet Accounts
			fns = [SELECT
			Contact.Id,
			Contact.FirstName,
			Contact.LastName
			FROM Contact
			WHERE Contact.Account.id = '00130000004csyT'
			//WHERE Contact.Account.Name = 'Fishnet Security, Inc.'			
			AND Contact.Id not IN :existingContactIds];
			
			//Now Get all of the Contacts that are in the Vendor Contacts
			vendor = new List<Contact>();
			if (BCR.Vendor__r.Id != null)
			{
				vendor = [SELECT
				Contact.Id,
				Contact.FirstName,
				Contact.LastName
				FROM Contact
				WHERE Contact.AccountId =: BCR.Vendor__r.Id
				AND Contact.Id not IN :existingContactIds];
			}
			
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Vendor Id:'+BCR.Vendor__r.Id+' '+String.valueOf(vendor.size())));
			
			if (this.available.size() > 0)		
				attendees.add(new ContactAttendee(available));
			if (this.fns.size() > 0)
				fnsattendees.add(new FNSAttendee(fns));
			if (this.vendor.size() > 0)
				vendorattendees.add(new VendorAttendee(vendor));
		}	
	}
	public Boolean getshowAttendees()
	{
		return (this.attendees.size() > 0);
	}
	public Boolean getshowFNSAttendees()
	{
		return (this.fnsattendees.size() > 0);
	}
	public Boolean getshowVendorAttendees()
	{
		return (this.vendorattendees.size() > 0);
	}
	public void AddNewContactRow()
	{
		this.attendees.add(new ContactAttendee(available));
	}
	public void AddNewFNSRow()
	{
		this.fnsattendees.add(new FNSAttendee(fns));
	}
	public void AddNewVendorRow()
	{
		this.vendorattendees.add(new VendorAttendee(vendor));
	}
	public Pagereference Save()
	{
		List<Briefing_Attendees__c> toSave = new List<Briefing_Attendees__c>();
		Integer Counter = existingContactIds.size() + 1;
		for (ContactAttendee a:attendees)
		{
			if (a.SelectedAttendee != 'SELECT')
			{
				Briefing_Attendees__c ba = new Briefing_Attendees__c();
				ba.Briefing_Center_Request__c = BCR.Id;
				ba.Contact__c = a.SelectedAttendee;
				ba.Contact_Role__c = 'Customer';
				ba.Name = BCR.Account__r.Name+'-';
				if (Counter < 10)
					ba.Name += '00'+Counter;
				else if (Counter >= 10 && Counter <100)
					ba.Name += '0'+Counter;
				else if (Counter >= 100)
					ba.Name += Counter;
				Counter++;
				toSave.add(ba);
			}
		}
		for (FNSAttendee a:fnsattendees)
		{
			if (a.SelectedFNS != 'SELECT')
			{
				Briefing_Attendees__c ba = new Briefing_Attendees__c();
				ba.Briefing_Center_Request__c = BCR.Id;
				ba.Contact__c = a.SelectedFNS;
				ba.Contact_Role__c = 'Fishnet Employee';
				ba.Name = BCR.Account__r.Name+'-';
				if (Counter < 10)
					ba.Name += '00'+Counter;
				else if (Counter >= 10 && Counter <100)
					ba.Name += '0'+Counter;
				else if (Counter >= 100)
					ba.Name += Counter;
				Counter++;
				toSave.add(ba);
			}
		}
		for (VendorAttendee a:vendorattendees)
		{
			if (a.SelectedVendor != 'SELECT')
			{
				Briefing_Attendees__c ba = new Briefing_Attendees__c();
				ba.Briefing_Center_Request__c = BCR.Id;
				ba.Contact__c = a.SelectedVendor;
				ba.Contact_Role__c = 'Vendor';
				ba.Name = BCR.Account__r.Name+'-';
				if (Counter < 10)
					ba.Name += '00'+Counter;
				else if (Counter >= 10 && Counter <100)
					ba.Name += '0'+Counter;
				else if (Counter >= 100)
					ba.Name += Counter;
				Counter++;
				toSave.add(ba);
			}
		}
		if (toSave.size() > 0)
			insert(toSave);
		return null;
	}
	
	
	public class ContactAttendee{
		public String SelectedAttendee{get;set;}		
		public List<Contact> Attendee{get;set;}
		public ContactAttendee(List<Contact> a)
		{
			Attendee = a;
		}
		public List<SelectOption> getAttendees()
		{
			List<SelectOption> o = new List<SelectOption>();
			for (Contact c:Attendee)
			{
				if (c.FirstName != null)
					o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
				else
					o.add(new SelectOption(c.Id,c.LastName));
			}
			o = SortOptionList(o);
			if (o.size() >0)
				o.add(0,new SelectOption('SELECT','SELECT'));
			return o;
		}
	}
	
	public class FNSAttendee
	{
		public String SelectedFNS{get;set;}		
		public List<Contact> FNS{get;set;}
		public FNSAttendee(List<Contact> f)
		{
			FNS = f;
		}
		public List<SelectOption> getAttendees()
		{
			List<SelectOption> o = new List<SelectOption>();
			for (Contact c:FNS)
			{
				if (c.FirstName != null)
					o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
				else
					o.add(new SelectOption(c.Id,c.LastName));
			}
			o = SortOptionList(o);
			if (o.size() >0)
				o.add(0,new SelectOption('SELECT','SELECT'));
			return o;
		}
	}
	
	public class VendorAttendee
	{
		public String SelectedVendor{get;set;}		
		public List<Contact> Vendor{get;set;}
		public VendorAttendee(List<Contact> v)
		{
			Vendor = v;
		}
		public List<SelectOption> getAttendees()
		{
			List<SelectOption> o = new List<SelectOption>();
			for (Contact c:Vendor)
			{
				if (c.FirstName != null)
					o.add(new SelectOption(c.Id,c.FirstName+' '+c.LastName));
				else
					o.add(new SelectOption(c.Id,c.LastName));
			}
			o = SortOptionList(o);
			if (o.size() >0)
				o.add(0,new SelectOption('SELECT','SELECT'));
			return o;
		}
	}
	
	/*Utilities*/
	
	private static List<SelectOption> SortOptionList(List<SelectOption> ListToSort)
    {
        if(ListToSort == null || ListToSort.size() <= 1)
            return ListToSort;
        List<SelectOption> Less = new List<SelectOption>();
        List<SelectOption> Greater = new List<SelectOption>();
        integer pivot = ListToSort.size() / 2;
        // save the pivot and remove it from the list
        SelectOption pivotValue = ListToSort[pivot];
        ListToSort.remove(pivot);
        for(SelectOption x : ListToSort)
        {
            if(x.getLabel() <= pivotValue.getLabel())
                Less.add(x);
            else if(x.getLabel() > pivotValue.getLabel()) Greater.add(x);   
        }
        List<SelectOption> returnList = new List<SelectOption> ();
        returnList.addAll(SortOptionList(Less));
        returnList.add(pivotValue);
        returnList.addAll(SortOptionList(Greater));
        return returnList; 
    }
    
    static testMethod void AddBriefingAttendeesExtensionTEST()
    {
    	//Retrieve a Contact for testing
    	Contact c = [SELECT
    	Contact.Id,
    	Contact.AccountId
    	FROM Contact
    	WHERE Contact.AccountId != null
    	AND Contact.Account.RecordType.Name = 'Vendor' LIMIT 1];
    	//Create a BRC
    	Briefing_Center_Request__c bcr = new Briefing_Center_Request__c();
    	bcr.Account__c = c.AccountId;
    	bcr.Vendor__c = c.AccountId;
    	/*bcr.Name = 'TEST';*/
    	insert(bcr);
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(bcr);
    	AddBriefingAttendeesExtension page = new AddBriefingAttendeesExtension(controller);
    	
    	page.getshowAttendees();
    	page.getshowFNSAttendees();
    	page.getshowVendorAttendees();

    	List<SelectOption> a = page.attendees[0].getAttendees();
    	if (a.size() > 0)
    	{
    		page.attendees[0].SelectedAttendee = a[0].getValue(); //SELECT Option
    		//Test Save with SELECTS chosen
    		page.Save();
    		//Change Selections
    		page.attendees[0].SelectedAttendee = a[1].getValue(); //First valid value
    		
    		page.AddNewContactRow();
    		page.Save();
    	}
    	List<SelectOption> f = page.fnsattendees[0].getAttendees();
    	if (f.size() > 0)
    	{
    		page.fnsattendees[0].SelectedFNS = f[0].getValue(); //SELECT Option
    		//Test Save with SELECTS chosen
    		page.Save();
    		//Change Selections
    		page.fnsattendees[0].SelectedFNS = f[1].getValue(); //First valid value
    		
    		page.AddNewFNSRow();
    		page.Save();
    	}
		List<SelectOption> v = page.vendorattendees[0].getAttendees();
		if (v.size() > 0)
    	{
    		page.vendorattendees[0].SelectedVendor = v[0].getValue(); //SELECT Option
    		//Test Save with SELECTS chosen
    		page.Save();
    		//Change Selections
    		page.vendorattendees[0].SelectedVendor = v[1].getValue(); //First valid value
    		
    		page.AddNewVendorRow();
    		page.Save();
    	}
    }
}