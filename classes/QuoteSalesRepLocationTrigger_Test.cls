@istest
public class QuoteSalesRepLocationTrigger_Test{

    static testmethod void testQuoteSalesRepLocationTrigger()
    {
        Sales_Rep_Location__c srl = TestDataFactory.createSalesRepLocation();
        insert srl;
        
        user owner1 = TestDataFactory.createStandardUser('SRL');
        owner1.Sales_Rep_Location_ID__c = srl.id;
        insert owner1;
        
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec Corporation');
        insert vp;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.OwnerId = owner1.id;
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
    }
}