public class QuoteTask{
		public static Task getTask(String QuoteName, Id oppId, String VendorName, Date ExpDate, String Subject, Id ownerId  ){
			Task ts = new Task();
			ts.WhatId = oppId;
			ts.Vendor_Name__c = VendorName;
			if (Subject.contains('Pending'))
			{
				//Perform a split by _ on the Subject. It should contain 'Expired_AccountName'
				//for reformatting to 'Expired_VendorName_Account_QuoteName
				string[] splits = Subject.split('_');
				if (splits != null && splits.size() == 2)
				{
					ts.Subject = splits[0]+'_'+VendorName+'_'+splits[1]+'_'+QuoteName;
				}
				else
					ts.Subject = Subject+'_'+VendorName+'_'+QuoteName;
			}
			else
				ts.Subject = Subject+'_'+VendorName+'_'+QuoteName;
			if (ownerId != null)
				ts.OwnerId = ownerId;
			else
				throw new MyException('There is no Deal Reg Desk Rep associated to this opp email sf.com support for assistance');
			ts.ActivityDate = ExpDate; 
			return ts;
		
		}
		 public class MyException extends Exception{}
	}