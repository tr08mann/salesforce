@isTest
private class TestRollupOpportunityInvoiceTrigger{
    
    static testMethod void myTest()
    {        
        Account A = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert A;
        
        Opportunity Opp = TestDataFactory.createStandardOpp('Commercial Opportunity', a.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert Opp;
        
        Invoice_2__c test1 = TestDataFactory.createInvoice(a.id,opp.id);
        insert test1;
        
        Opp = [Select Id, Name, Number_of_Invoice__c from Opportunity where Id = :Opp.Id];
        
        system.assert(opp.Number_of_Invoice__c==1);
		
        delete test1;
        
        Opp = [Select Id, Name, Number_of_Invoice__c from Opportunity where Id = :Opp.Id];
        
        system.assert(opp.Number_of_Invoice__c==0);
        
        TriggerStopper.stopInvoiceRollup = false;
    }
}