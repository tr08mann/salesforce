@isTest
public class ConsolidatedLeads_Test{
//this test class is designed to test the ConsolidatedLeads class

    static testmethod void testLeadConvertedTask()
    {
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        lead l = TestDataFactory.createConvertedLead(acc.name, acc.id);
        insert l;
        list<lead> leadlist = new list<lead>();
        leadlist.add(l);
        ConsolidatedLeads.LeadConvertedTask(leadlist);
    }
    
    static testmethod void testPrimary()
    {
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        lead l = TestDataFactory.createStandardLead(acc.name);
        insert l;
        lead l2 = TestDataFactory.createStandardLead(acc.name);
        insert l2;
        
        l.primary__c = true;
        update l;
        l2.primary__c = true;
        update l2;
    }
}