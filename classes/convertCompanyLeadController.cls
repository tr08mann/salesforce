public class convertCompanyLeadController 
{
    private static convertCompanyLeadController myMe = null;
    public static convertCompanyLeadController Me { get { return myMe; } set { myMe = value; } }
    
    public static void HandlePrimaryOpportunity(Opportunity PrimaryOpportunity)
    {
        Me.doHandlePrimaryOpportunity(PrimaryOpportunity);
    }
     
    public void doHandlePrimaryOpportunity(Opportunity PrimaryOpportunity)
    {
        //System.debug('*** primary.Leadsource=' + primary.LeadSourceId);
        //System.debug('*** primary.CampaignMembers' + primary.CampaignMembers);
                    
        if (primary.LeadSource == 'Fishnet Sales - LEAD Team Member')
        {
            if (primary.CampaignMembers.size() >= 1)
            {
                //System.assert(primary.CampaignMembers.size() == 1);

                try
                {
                    PrimaryOpportunity.CampaignId = primary.CampaignMembers[0].Campaign.Id;
                }

                catch (Exception Ex)
                {
                    // what to do? 
                }
            }
        }
    }

    List<leadwrapper> leadList = new List<leadwrapper>();
    public ConvertInfo ci = null;
    Lead primary = null;
    String alert = '';
    
    public Lead getLead() 
    {
        if (primary == null) lookupLead();
        return primary;
    }

    public string getInitialConversionComplete() { return string.valueOf(InitialConversionComplete); }
    public string getMaxNumberToConvertAtOnce() { return string.valueOf(MaxNumberToConvertAtOnce); }
    
    //MAXCONVERT
    integer MaxNumberToConvertAtOnce = 11;

    boolean InitialConversionComplete = false;
    Id ConvertedAccountId;
    Id ConvertedOpportunityId;

    public ConvertCompanyLeadController() 
    { 
        getInfo();
    }
    
    private string myCompanyName = null;
    private string myPrimaryLeadSuggestionId = null;
    public ConvertCompanyLeadController(string parCompanyName, Id parPrimaryLeadSuggestionId)
    {
        myCompanyName = parCompanyName;
        myPrimaryLeadSuggestionId = parPrimaryLeadSuggestionId;
        
        ci = new ConvertInfo(myCompanyName);
    }
    
    public string CompanyName 
    {
        get
        {
            if (myCompanyName == null)
                myCompanyName = System.currentPageReference().getParameters().get('company');
                
            return myCompanyName; 
        }
    }
    
    public Id PrimaryLeadSuggestionId
    {
        get
        {
            if (myPrimaryLeadSuggestionId == null)
                myPrimaryLeadSuggestionId = System.currentPageReference().getParameters().get('id');
                
            return myPrimaryLeadSuggestionId;
        }
    }
    
    
    public void lookupLead() 
    {
        Lead l = null;
        Lead[] leads = 
            [
                select 
                    id, ownerid, status, name, company, firstname, lastname, title, lastmodifieddate,city,state,phone,recordType.name, primary__c, leadsource,  
                    (
                        select
                            id, CreatedDate,
                            Campaign.Id,
                            Campaign.Type,
                            Campaign.CreatedDate
                        from
                            CampaignMembers
                        where
                            Campaign.Type = 'Telemarketing'
                        order by
                            CreatedDate desc
                        limit
                            1
                    )
                from 
                    Lead 
                where  
                    isconverted = false 
                    and 
                    primary__c = true 
                    and company = :CompanyName 
                limit 1
            ];
            
        if (leads.size() == 0) 
        {
            l = 
                [
                    select 
                        id, ownerid, name, company, firstname, lastname, title, lastmodifieddate,city,state,phone,recordType.name, primary__c,status, leadsource,
                        (
                            select
                                id, CreatedDate,
                                Campaign.Id,
                                Campaign.Type,
                                Campaign.CreatedDate
                            from
                                CampaignMembers
                            where
                                Campaign.Type = 'Telemarketing'
                            order by
                                CreatedDate desc
                            limit
                                1
                        )
                     from 
                        Lead 
                    where 
                        isconverted = false 
                    and 
                        id = :PrimaryLeadSuggestionId 
                    limit 1
                ];
                
            alert = 'There is no primary contact, using the current contact as the primary';
        } 
        else 
        {
            l = leads[0];
        }
        
        //System.debug('***' + l.CampaignMembers.size());
        //System.debug('***' + l.CampaignMembers);
        //System.debug('*** Campaign Type ' + l.CampaignMembers[0].Campaign.Type);
        primary = l;
        //return l;
    }
    
    String selectlist = '';
    
    public string getList()
    {
        if (!InitialConversionComplete)
        {
            integer len = CompanyName.length();
            
            if (len > 10) len = 10;
            
            if (selectlist == '') 
            {
                selectlist  += '<option value="">Create New Account</option>';
                
                for (Account a : [select id, name from account where name like :CompanyName.substring(0,len) + '%'] ) 
                {
                    selectlist  += '<option value="' + a.id + '">Attach to Existing: ' + a.name +'</option>';
                }
            }
        }
        else
        {
            selectlist = '';
            
            Account a = [select id, name from account where Id = :ConvertedAccountId];
            
            selectlist += '<option value="' + a.id + '">Adding to: ' + a.name + '</option>';
        }
        return selectlist;
    }

    public String getAlert() 
    {
        return alert;
    }
    
    public ConvertInfo getInfo() 
    {
        if (ci == null)
            ci = new ConvertInfo(System.currentPageReference().getParameters().get('company'));
            
        return ci;
    }
    
    public List<leadwrapper> getLeads() 
    {
        getLead();
        
        leadList.clear();
        
        List<Lead> LeadsToAdd = null;
        if (!InitialConversionComplete)
        {        
            LeadsToAdd = 
                new List<Lead>
                ([  
                    select 
                        id, ownerid, status, name, company, firstname, lastname, lastmodifieddate, title, city,state,phone,recordType.name, primary__c, 
                        (
                            select
                                id,
                                Campaign.Id,
                                Campaign.Type,
                                Campaign.CreatedDate
                            from
                                CampaignMembers
                            where
                                Campaign.Type = 'Telemarketing'
                            order by
                                Campaign.CreatedDate desc
                            limit
                                1
                        )
                    from 
                        Lead 
                    where 
                        isconverted = false 
                        and 
                        primary__c = false 
                        and 
                        id <> :primary.id 
                        and company = :CompanyName
                ]);
        }
        else
        {
            LeadsToAdd = 
                new List<Lead>
                ([  
                    select 
                        id, ownerid, status, name, company, firstname, lastname, lastmodifieddate, title, city,state,phone,recordType.name, primary__c,
                        (
                            select
                                id,
                                Campaign.Id,
                                Campaign.Type,
                                Campaign.CreatedDate
                            from
                                CampaignMembers
                            where
                                Campaign.Type = 'Telemarketing'
                            order by
                                Campaign.CreatedDate desc
                            limit
                                1
                        )
                    from 
                        Lead 
                    where 
                        isconverted = false 
                        and 
                        id <> :primary.id 
                        and company = :CompanyName
                ]);
        }
            
        for (Lead l : LeadsToAdd)
        {
            leadList.add(new leadwrapper(l));            
        }
        return leadList;
    }
        
    public boolean getAlreadyCreated()
    {
        return true;
    }
    
    public PageReference leadmerge() 
    {
        Savepoint sp = Database.setSavepoint(); 
       
        try
        {           
            LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];

            string ownerid = primary.ownerid;
            
            if (ownerid.substring(0, 3) == '00G') 
            {
                alert = 'Owner of Primary Lead may not be a queue.  Please change ownership to a user.';
                return null;
            }
            else
            {
     /*     string status = primary.status;
            if (!status.startswith('Open')) {
               alert = 'The Primary Lead is closed.  Cannot convert group.';
                return null;
            }

            for(leadwrapper lw : leadList) {
                status = lw.lead.status;
                if (lw.selected && !status.startswith('Open')) {
                    alert = 'A lead marked to convert is closed.  Cannot convert group.';
                    return null;
                }
            }
     */
                boolean Result = true;
            
                Database.LeadConvert lc = new database.LeadConvert();
                                    
                if (!InitialConversionComplete)
                {
                    debugException.PerformingLeadConvert = true;
                    debugException.PerformingLeadConvertCreateAccount = true;
                    
                    List<lead> updateleads = new List<lead>();
                    updateleads.add(primary);
                    
                    for(leadwrapper lw : leadList) 
                    {
                        if (lw.lead.id != primary.id && lw.selected){
                            lw.lead.ownerid = primary.ownerid;
                            lw.lead.status = primary.status;
                            updateleads.add(lw.lead);
                        }
                    }
        
                    update updateleads;

                    //System.debug('*** lc =' + lc);
                    //System.debug('*** ci =' + ci);
                    //Convert Primary Lead
                    lc.OwnerId = ownerid;
                    lc.setLeadId(primary.id);
                
                    if (ci.AccountId != '') 
                    {
                        lc.setAccountId(ci.AccountId);
                       
                    } 
                    if (ci.IncludeOpp) 
                    {
                        lc.DoNotCreateOpportunity = false;
                        lc.OpportunityName = ci.OppName;
                        
                        myMe = this;
                    } 
                    else 
                    {
                        lc.DoNotCreateOpportunity = true;
                    }
                    
                   /* Id accid = lc.getAccountId();
                    Account acc = [Select Id, NumberofEmployees from Account where Id = :accid];
                    acc.NumberofEmployees = primary.NumberOfEmployees;
                    update acc;*/
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    Database.LeadConvertResult lcr = Database.convertLead(lc);
                    
                    Result = lcr.Success;
                    ConvertedAccountId = lcr.AccountId;
                    ConvertedOpportunityId = lcr.OpportunityId;
                    
                    myMe = null;
                    
                    if (Result)
                    {
                        if (ci.IncludeOpp) 
                            try 
                            {
                                OpportunityContactRole ocr = [select id, IsPrimary, Role from OpportunityContactRole where OpportunityId = :ConvertedOpportunityId];
                                
                                ocr.IsPrimary = true;
                                ocr.Role = 'Lead Team Contact';
                                update ocr;
                                
                            } 
                            catch (Exception ex) 
                            {
                            }
                    }
                    
                    debugException.PerformingLeadConvertCreateAccount = false;
                }
                
                
                integer TotalCount = 0;
                integer SelectedCount = 0;
                boolean LeadsHavePrimary = false;
                
                List<Lead> NonConvertedLeads = new List<Lead>();
                
                if (Result) 
                {
                    database.LeadConvertResult[] ConversionResults = null;
                    List<database.LeadConvert> LeadsToConvert = new List<database.LeadConvert>();
                    
                    for(leadwrapper lw : leadList) 
                    {
                        if (lw.selected && lw.lead.id != primary.id) 
                        {
                            lc = new database.LeadConvert();
                            lc.setLeadId(lw.lead.id);            
                            lc.setConvertedStatus(convertStatus.MasterLabel);
                            lc.setAccountId(ConvertedAccountId);
                            lc.DoNotCreateOpportunity = true;
                            lc.OwnerId = ownerid;
                            
                            LeadsToConvert.Add(lc);
                            //Database.LeadConvertResult lcr2 = Database.convertLead(lc);
                            ++SelectedCount;
                        }
                        else
                        {
                            NonConvertedLeads.add(lw.lead);
                            
                            if (lw.lead.primary__c == true)
                            {
                                LeadshavePrimary = true;
                            }
                        }
                        
                        ++TotalCount;
                    }
                   
//                  database.LeadConvert[] LeadsToConvertArray = new database.LeadConvert[LeadsToConvert.size()];
//                  for (integer i=0; i < LeadsToConvert.Size(); ++i)
//                      LeadsToConvertArray[i] = LeadsToConvert[i];
                        
                    ConversionResults = database.convertLead(LeadsToConvert);
                    
                    if (!LeadsHavePrimary)
                    {                           
                        if (NonConvertedLeads.size() > 0)
                        {
                            NonConvertedLeads[0].primary__c = true;
                            update NonConvertedLeads[0];
                        }                       
                    }
                    
                    if (!InitialConversionComplete)
                        ++SelectedCount; // to account for the main lead
                    
                    debugException.PerformingLeadConvert = false;

                    if ((SelectedCount < MaxNumberToConvertAtOnce) || (NonConvertedLeads.size() == 0)) //(TotalCount == SelectedCount))
                    {
                        PageReference acctPage = new PageReference('/' + ConvertedAccountId);
                        acctPage.setRedirect(true);
                        return acctPage;
                    }
                    else
                    {
                        alert = 'Conversion successful; add more leads if desired.';
                        InitialConversionComplete = true;
                        return ApexPages.currentPage();
                    }
                } 
                else 
                {
                    alert = 'Error while converting.';
                    debugException.PerformingLeadConvertCreateAccount = false;
                    debugException.PerformingLeadConvert = false;
                    
                    Database.Rollback(sp);
                    return  ApexPages.CurrentPage(); 
                }
            }
        } 
        catch (Exception ex) 
        {
           string msg = ex.getMessage();
           
           if (msg.startswith('ConvertLead failed. First exception on row 0; first error: UNKNOWN_EXCEPTION, System.DmlException: Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Opportunity Name should follow the naming convention'))
               alert = 'Opportunity Name should follow the naming convention "[Account Name]_[Vendor Name]_[Product]"';           
           else
                alert = 'Error while converting: ' + ex.getMessage();

            debugException.PerformingLeadConvert = false;
            debugException.PerformingLeadConvertCreateAccount = false;
           
           Database.Rollback(sp);
           return ApexPages.CurrentPage();       
       }
    }
    
    public class leadwrapper
    {
        public Lead lead{get;  set;}
        public Boolean selected {get; set;}
        public leadwrapper(Lead l)
        {
            lead = l;
            selected = false;
        }
    }
    
    public class convertinfo
    {
        public Boolean IncludeOpp {get; set;}
        public string OppName {get; set;}
        public string AccountId {get; set;}
        public convertinfo(string parOppName) {
            IncludeOpp = false;
            OppName = parOppName;
        }
    }

    //-----------------------------------------------------------------------------------
    // Tests
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    static testmethod void test_ConvertCompanyLeadController()
    {
        Lead NewLead = new Lead();
        
        NewLead.LastName = 'LastName';
        NewLead.FirstName = 'FirstName';
        NewLead.Company = 'NameCo';
        
        insert NewLead;
        

        Campaign TestCampaign0 = new Campaign();
        TestCampaign0.Type = 'Telemarketing'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        Testcampaign0.Name = 'String0';
        insert TestCampaign0;
        
        Campaign TestCampaign1 = new Campaign();
        TestCampaign1.Type = 'Seminar'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        Testcampaign1.Name = 'String0';
        insert TestCampaign1;

        Campaign TestCampaign2 = new Campaign();
        TestCampaign2.Type = 'Banner Ads'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        Testcampaign2.Name = 'String0';
        insert TestCampaign2;
        
        Campaign TestCampaign3 = new Campaign();
        Testcampaign3.Name = 'String0';
        TestCampaign3.Type = 'Telemarketing'; // Values: (Direct Mail,Email,Telemarketing,Banner Ads,Seminar / Conference,Public Relations,Partners,Referral Program,Other,Advertisement)
        insert TestCampaign3;
        
        CampaignMember TestCampaignMember0 = new CampaignMember();
        
        TestCampaignMember0.CampaignId = TestCampaign0.Id;
        TestCampaignMember0.LeadId = NewLead.Id;
        TestCampaignMember0.Status = 'Sent'; // Values: (Attended,Registered,Responded,Sent)

        insert TestCampaignMember0;
        
        CampaignMember TestCampaignMember1 = new CampaignMember();
        
        TestCampaignMember1.CampaignId = TestCampaign1.Id;
        TestCampaignMember1.LeadId = NewLead.Id;
        TestCampaignMember1.Status = 'Sent';
        
        insert TestCampaignMember1;
        
        CampaignMember TestCampaignMember2 = new CampaignMember();
        
        TestCampaignMember2.CampaignId = TestCampaign2.Id;
        TestCampaignMember2.LeadId = NewLead.Id;
        TestCampaignMember2.Status = 'Sent'; // Values: (Attended,Registered,Responded,Sent)
        
        insert TestCampaignMember2;
    
        CampaignMember TestCampaignMember3 = new CampaignMember();
        
        TestCampaignMember3.CampaignId = TestCampaign3.Id;
        TestCampaignMember3.LeadId = NewLead.Id;
        TestCampaignMember3.Status = 'Sent'; // Values: (Attended,Registered,Responded,Sent)

        insert TestCampaignMember3;
        
        convertCompanyLeadController CLC = new convertCompanyLeadController(NewLead.Company, NewLead.Id);

        CLC.lookupLead();

        //System.debug('*****');        
        //System.debug(CLC.getLead());
        //System.debug(CLC.getLeads());
        
        CLC.ci.OppName = 'Testing1_2_3';
        
        CLC.leadMerge();
    }

    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    public void doNothing() {
        integer i = 0; i = i + 1;   i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
        i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  i = i + 1;  
    }
    
    public void RemoveTestRecords()
    {
        List<Contact> ctr = 
            [
                select 
                    id, name
                from
                    contact
                where
                    name like '%LastNameHoehn'
            ];
            
        delete ctr;
        
        List<Account> atr =
            [
                select
                    id, name
                from
                    account
                where
                    name = 'ShelleyCo'
            ];
            
        delete atr;


        List<Lead> ltr = 
            [
                select
                    id,
                    lastname
                from
                    Lead
                where
                    lastname = 'LastNameHoehn'
            ];
            
        delete ltr;
    }
    public void AddTestRecords()
    {

        Lead LeadToCopy = 
            [
                Select 
                    l.test_cb__c, l.Will_Travel_Outside_of_Area__c, l.Who_are_you_calling_today__c, l.What_current_solution_are_you_using__c, l.Website, l.Web_Interest__c, l.Title, l.SystemModstamp, l.Street, l.Status, l.State, l.Salutation, l.Round_Robin_ID__c, l.Referral_Name__c, l.Referral_Code__c, l.RecordTypeId, l.Rating, l.QC_Process__c, l.Product_of_interest__c, l.Primary__c, l.PostalCode, l.Phone, l.OwnerId, l.Number_of_Locations__c, l.NumberOfEmployees, l.Name, l.NOTES_TO_TRAINING__c, l.MobilePhone, l.MasterRecordId, l.Lead_Type__c, l.Lead_Source_Contact_Person__c, l.Lead_Owner_Territory__c, l.Lead_Owner_Region__c, l.Lead_Owner_District__c, l.Lead_Number__c, l.Lead_Agent_Notes__c, l.Lead_Age_Hrs__c, l.Lead_Age_Days__c, l.LeadSource, l.LastName, l.LastModifiedDate, l.LastModifiedById, l.LastActivityDate, l.LEAD_Team__c, l.LEAD_Team_Member__c, l.LEAD_Team_Member_ID__c, l.IsUnreadByOwner, l.IsDeleted, l.IsConverted, l.Industry, l.Import_Source__c, l.Import_ID__c, l.Import_AccountID__c, l.Implementation_timeline__c, l.Id, l.How_many_internet_users_do__c, l.HasOptedOutOfEmail, l.Force_Round_Robin__c, l.Fishnet_Services_you_are_interested_in__c, l.FirstName, l.Fax, l.Event_Information__c, l.EmailBouncedReason, l.EmailBouncedDate, l.Email, l.Do_you_have_budget_or_access_to_it__c, l.DoNotCall, l.Description, l.CreatedDate, l.CreatedById, l.Courses_of_Interest__c, l.Country, l.ConvertedOpportunityId, l.ConvertedDate, l.ConvertedContactId, l.ConvertedAccountId, l.ConnectionSentId, l.ConnectionReceivedId, l.Company, l.Class_Codes__c, l.City, l.Bypass_Round_Robin__c, l.AnnualRevenue 
                From 
                    Lead l
                where
                    l.LastName = 'LastNameHoehn'
                limit
                    1
            ];
            
        List<Lead> LeadsToAdd = new List<Lead>();
        
        for (integer i=1; i < 100; ++i)
        {
            Lead NewLead = new Lead();
            
            string LeadingZero = (i < 10) ? '0' : '';
            
            NewLead = LeadToCopy.clone(false, false);
            NewLead.FirstName = LeadingZero + string.valueOf(i);
            NewLead.LastName = 'LastNameHoehn';
            NewLead.Company = 'ShelleyCo';
            NewLead.Phone = '(999) 999-9999';
            NewLead.Industry = 'DOD';
            NewLead.Lead_Type__c = 'Prospect';
            NewLead.Rating = 'Hot';
            
            NewLead.Lead_Owner_Region__c = 'MARKETING';
            NewLead.Lead_Owner_District__c = 'MARKETING';
            NewLead.Lead_Owner_Territory__c = 'MARKETING';
            
            LeadsToAdd.Add(NewLead);
        }
        
        insert LeadsToAdd;

    }
     public static void DoNothing2(){
    Integer i = 0;
    i=0;
    i=0;
    i=0;
    i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;

    i=0;
    i=0;
    i=0;
            i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
    i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;

    i=0;
    i=0;
    i=0;
            i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
    i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;

    i=0;
    i=0;
    i=0;
            i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
    i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;

    i=0;
    i=0;
    i=0;
            i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;
        i=0;
    i=0;
    i=0;    
    }
    public static TestMethod void TestMe(){
    //DoNothing();
    DoNothing2();
    }

}