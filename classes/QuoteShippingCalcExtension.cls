public class QuoteShippingCalcExtension {

    public decimal largeapp {get;set;} //varible to store number of large appliances
    public decimal smallapp {get;set;} //varible to store number of small appliances
    public decimal totalship {get;set;} //varible to store total shippling and handling
    public decimal initaltotship {get;set;} //varible to store the intial shipping and handling (while calculating)
    public decimal onloadship {get;set;} //variable to store the shipping handling total at time of page load
    private quote q; //new quote record
    public quote ghostquote {get;set;} //new ghost quote record - allow user to calculate mutliple times before saving
    public string shipmeth {get;set;} //variable to store shipping method
    public string freereason {get;set;} //variable to store free shipping reason
    public boolean freeship {get;set;} //variable to store if it's free shipping
    public boolean isShowCalc{get; set;} //show/hid calculate button
    public boolean isShowSave{get; set;} //show/hide save button
    public boolean isShowClear{get; set;} //show/hide recalculation button
    public boolean Overwrite{get;set;} //store value of Overwrite Shipping Checkbox
    public boolean isShowOver{get; set;} //to show/hide the overwrite checkbox
    public boolean isShowOPLabel{get; set;} //to show/hide the shipping handling output label
    public boolean isShowIPLabel{get; set;} //to show/hide the shipping handling input label
    public boolean haveError{get; set;} //to track if there is an error
    public boolean isShowAllOPFields{get; set;} //to show/hide output fields
    public boolean isShowAllIPFields{get; set;} //to show/hide input fields
    
    public QuoteShippingCalcExtension(ApexPages.StandardController controller) 
    {
        smallapp = 0;
        largeapp =0;
        totalship = 0;
        freeship = false;
        overwrite = false;
        
        if(apexpages.currentpage().getparameters().get('Id')!=null && apexpages.currentpage().getparameters().get('Id')!='')
        {
            this.q = [Select name, 
                        Quote.Fishnet_Quote_Number__c, 
                        Shipping_Method__c,
                        Number_of_Small_Appliances__c,
                        Free_Shipping__c,
                        Number_of_Large_Appliances__c,
                        Free_Shipping_Reason__c,
                        ShippingHandling,
                        Overwrite_Shipping_Calculation__c
                    from quote 
                    where id =:apexpages.currentpage().getparameters().get('Id')];
        }
        ghostquote = new quote();
        ghostquote.shipping_method__c = q.shipping_method__c;
        ghostquote.free_shipping__c = q.free_shipping__c;
        ghostquote.Overwrite_Shipping_Calculation__c = q.Overwrite_Shipping_Calculation__c;
        ghostquote.free_shipping_reason__c = q.free_shipping_reason__c;
        ghostquote.number_of_small_appliances__c = q.number_of_small_appliances__c;
        ghostquote.number_of_large_appliances__c = q.number_of_large_appliances__c;
        ghostquote.shippinghandling = q.shippinghandling;
        ghostquote.overwrite_shipping_calculation__c = q.overwrite_shipping_calculation__C;
                
        if(ghostquote.shippinghandling != null)
        {
            onloadship = ghostquote.shippinghandling;
        }
        else
        {
            onloadship = null;
        }
        isShowSave = false;
        isShowAllOPFields = false;
        isShowCalc = true;
        isShowAllIPFields = true;
        if(ghostquote.overwrite_shipping_calculation__c)
        {
            isShowOver = true;
            isShowOPLabel = false;
            isShowIPLabel = true;
        }
        else
        {
            isShowOver = false;
            isShowOPLabel = true;
            isshowIPLabel = false;
        }
    }
    
    public void calculate()
    {
        smallapp = 0;
        largeapp = 0;
        totalship = 0;
        isshowsave = false;
        haveError = false;
        
        smallapp = this.ghostquote.Number_of_Small_Appliances__c;
        largeapp = this.ghostquote.Number_of_Large_appliances__c;
        shipmeth = this.ghostquote.shipping_method__c;
        freereason = this.ghostquote.free_shipping_reason__c;
        freeship = this.ghostquote.free_shipping__C;
        initaltotship = this.ghostquote.shippinghandling;
        
        if (shipmeth == NULL){
            isShowSave = False;
            isShowAllOPFields = false;
            haveError = TRUE;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Shipping Method must be provided'));
        }
        
        IF (smallapp == NULL) {
            smallapp = 0;
        }
        
        IF (largeapp == NULL) {
            largeapp = 0;
        }
        
        IF (shipmeth == 'Ground Shipping') {
            smallapp = smallapp *15;
            largeapp = largeapp * 50;
        }
        
        IF (shipmeth == 'Two Day Shipping') {
            smallapp = smallapp * 30;
            largeapp = largeapp * 100;
        }
        
        IF (shipmeth == 'Overnight Shipping') {
            smallapp = smallapp * 45;
            largeapp = largeapp * 150;
        }
        
        IF (shipmeth == 'Overnight Early AM Shipping') {
            smallapp = smallapp * 60;
            largeapp = largeapp * 200;
        }
           
        IF (freeship == TRUE) {
            totalship = 0;
            this.ghostquote.shippinghandling = totalship;
        } ELSE {
                totalship = smallapp + largeapp;
                this.ghostquote.shippinghandling = totalship;
          }
        IF (totalship == 0 && (freeship==FALSE || freereason==NULL)){
            isShowSave = False;
            isShowAllOPFields = false;
            haveError = TRUE;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Free Shipping Reason must be provided and/or Free Shipping Checkbox must be checked'));
        }
          
        IF ((freeship ==TRUE && freereason == NULL)||(freeship ==FALSE && freereason != NULL)) {
                isShowSave = False;
                isShowAllOPFields = false;
                haveError = TRUE;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Free Shipping Reason must be provided and/or Free Shipping Checkbox must be checked'));
        }  
        IF (haveError != TRUE){

                 isShowAllOPFields = true;
                 isShowCalc = False;
                 isShowAllIPFields = False;
                 isShowClear = TRUE;
                 isShowOver = TRUE;
            if(ghostquote.overwrite_shipping_calculation__c)
            {
                isshowsave =false;
            }
            else
            {
                isShowSave = TRUE;
            }
         }
}         

    public void save()
    {
        haveerror = false;
               isshowsave = false;
        IF (ghostquote.shippinghandling < totalship) {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Overwritten Shipping/Handling amount cannot be less than calculated amount'));
           isShowSave = FALSE;
           if(ghostquote.overwrite_shipping_calculation__c)
           {
               isshowIpLabel = true;
           }
           
           haveError = TRUE;

        } ELSE if (haveError == FALSE)
        {
        isshowsave = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, 'shipping on save:' + this.ghostquote.shippinghandling));
               
            q.shippinghandling = this.ghostquote.shippinghandling;
            q.shipping_method__c = this.ghostquote.shipping_method__c;
            q.free_shipping__c = this.ghostquote.free_shipping__c;
            q.free_shipping_reason__C = this.ghostquote.free_shipping_reason__C;
            q.Number_of_Small_Appliances__c = this.ghostquote.number_of_small_appliances__C;
            q.number_of_large_appliances__c = this.ghostquote.number_of_large_appliances__C;
            q.Overwrite_Shipping_Calculation__c = this.ghostquote.Overwrite_Shipping_Calculation__c;
            update q;
            }
    }
    
    public void ClearFreeShipReason()
    {
         freeship = this.ghostquote.free_shipping__C;
         
        If (freeship==FALSE) {
             this.ghostquote.free_shipping_reason__C = NULL;
         }
    }
    
    public void OverwriteShip()
    {    
        Overwrite = this.ghostquote.overwrite_shipping_calculation__C;

        IF (Overwrite == TRUE){
            if (freeship ==TRUE){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Overwritten Shipping/Handling amount cannot be less than calculated amount'));
                haveerror = true;
                isshowsave = false;
            }
            Else{isshowsave = false;
                 isShowOPLabel = false;
                 isShowIPLabel = true;}
        } ELSE{
            isShowOPLabel = true;
            isshowsave = true;
            isshowIPLabel = false;
          }
    }
    
    public void OverrideCheck()
    {    
        haveerror = false;
        IF (/*ghostquote.shippinghandling < initaltotship ||*/ ghostquote.shippinghandling < totalship) {
           isShowSave = FALSE;
           isShowAllOPFields = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Overwritten Shipping and Handling amount cannot be less than calculated amount.'));
           isshowiplabel = true;
           haveError = true;
       }     
       else
       {
           isshowsave = true;
       }
       
    }      
    
    public void clear()
    {
        isShowSave = FALSE;
        isShowAllOPFields = false;
        isShowCalc = TRUE;
        isShowAllIPFields = true;
        isShowIPLabel = false;
        isShowOPLabel = true;
        isShowClear = FALSE;
        haveError = FALSE;
        this.ghostquote.shippinghandling = totalship;
        isShowOver = FALSE;
        this.ghostquote.overwrite_shipping_calculation__C = FALSE;
    }
    
    public quote getQuote()
    {
        return this.q;
    }
    public void setQuote(quote quo)
    {
        this.q = quo;
    }

}