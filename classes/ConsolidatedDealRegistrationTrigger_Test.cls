@isTest (seealldata = true)
public class ConsolidatedDealRegistrationTrigger_Test
{
    static testmethod void testupdate()
    {
        user drd = TestDataFactory.createStandardUser('dealregtrigger');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;

            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            insert opp;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
            insert vp;
            
            deal_registration__c dealreg = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Pending Submission');
            dealreg.ownerid = drd.id;
            insert dealreg;
            processcontrol.processed = false;
            dealreg.deal_registration_status__c = 'Deal Registration Submitted, Pending Approval';
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Registration Tentatively Approved';
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Registration Approved';
            update dealreg;
            dealreg.deal_expiration__c = system.today().adddays(61);
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Registration Denied';
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Registration Pending Submission';
            update dealreg;
            dealreg.deal_registration_status__c = 'Opportunity Did Not Qualify';
            update dealreg;
            dealreg.deal_registration_status__c = 'Fishnet Professional Services';
            update dealreg;
            dealreg.deal_registration_status__c = 'Vendor NSP Cancels Deal Registration';
            update dealreg;
            dealreg.deal_registration_status__c = 'Vendor Has No Deal Registration Program';
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Already Registered on Another FNS Opportunity';
            update dealreg;
            dealreg.deal_registration_status__c = 'Deal Reg Canceled/Closed/Rejected';
            update dealreg;
        }
    }
    static testmethod void testinsert()
    {
        user drd = TestDataFactory.createStandardUser('dealregtrigger');
        
        system.runas(drd)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Test');
            acc.ownerid = drd.id;
            insert acc;

            opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
            opp.ownerid = drd.id;
            opp.deal_reg_desk_rep__c = drd.id;
            opp.inside_sales_NEW__c = drd.id;
            insert opp;
            
            vendor_profile__c vp = TestDataFactory.createVendorProfile('Symantec');
            insert vp;

            deal_registration__c dealreg = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Approved');
            insert dealreg;

            deal_registration__c dealreg2 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Denied');
            insert dealreg2;
            
            deal_registration__c dealreg3 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Opportunity Did Not Qualify');
            insert dealreg3;
            
            deal_registration__c dealreg4 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Vendor NSP Cancels Deal Registration');
            insert dealreg4;
            
            deal_registration__c dealreg5 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Vendor Has No Deal Registration Program');
            insert dealreg5;
            
            deal_registration__c dealreg6 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Already Registered on Another FNS Opportunity');
            insert dealreg6;
            
            deal_registration__c dealreg7 = TestDataFactory.createStandardDealReg(opp.id, vp.id, vp.name, opp.opportunity_number__c, 'Deal Registration Submitted, Pending Approval');
            insert dealreg7;
        }
    }
}