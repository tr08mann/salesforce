public with sharing class vfxMassUpdateOpportunity 
{
	public ApexPages.StandardController theController { get; set; }
	public boolean renderbottom {
			get
		{
			if (myOpportunityUpdates == null)
			{
				return false;
			}
			else if (myOpportunityUpdates.size() == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	private Map<Id,classOpportunityUpdate> myOpportunityIdToOpportunityUpdate = null;
	private List<classOpportunityUpdate> myOpportunityUpdates = null;
	public List<classOpportunityUpdate> theOpportunityUpdates
	{
		get
		{
			if (myOpportunityUpdates == null)
			{
				myOpportunityUpdates = new List<classOpportunityUpdate>();
				
				for (Opportunity Opp : theOpportunities)
					myOpportunityUpdates.add(new classOpportunityUpdate(Opp));

				myOpportunityIdToOpportunityUpdate = new Map<Id,classOpportunityUpdate>();
				
				for (classOpportunityUpdate OppUpdate : myOpportunityUpdates)
					myOpportunityIdToOpportunityUpdate.put(OppUpdate.theOpportunity.Id, OppUpdate);
			}

			if (myOpportunityIdToOpportunityUpdate == null)
			{
			}
			
			return myOpportunityUpdates;
		}
	}
	
	private List<Opportunity>  myOpportunities = null;
	public List<Opportunity> theOpportunities 
	{ 
		get
		{
			if (myOpportunities == null)
			{ 
				myOpportunities = 
					[ 
						select
							Name,
							Description,
							StageName,
							ForecastCategoryName,
							CloseDate,
							AccountId,
							Account.Name,
							Closed_Won_Lost_Reason__c,
							Competitor__c
						from 
							Opportunity
						where
							IsClosed = false
							and
								CloseDate >= :MinDate
							and
								CloseDate <= :MaxDate
							and
								OwnerId = :Userinfo.getUserId()
						limit
							1000
					];
					
			}
			
			return myOpportunities;  
		}
	}
	
	private boolean myFirstLoad = true;
	public boolean FirstLoad 
	{ 
		get
		{
			boolean OriginalValue = myFirstLoad;
			myFirstLoad = false;
			return OriginalValue;
		} 
	}
	public vfxMassUpdateOpportunity()
	{
		SelectedFilter = 'All';
	}
	
	public Date MinDate 
	{ 
		get
		{
			if (SelectedFilter == '-')
				return date.newInstance(1900,1,1);
			else
			if (SelectedFilter == 'All')
				return date.newInstance(1900,1,1);
			else
			if (SelectedFilter == '+120')
				return date.today();
			else
			if (SelectedFilter == '-120')
				return date.today().addDays(-120);
			else
			if (SelectedFilter == '+60')
				return date.today();
			else
			if (SelectedFilter == '+30')
				return date.today();
			else
			if (SelectedFilter == 'Quarter')
			{
				Date aNow = date.today();
					
				integer aYear = aNow.year();
				integer aMonth = aNow.month();
				integer aQuarter = (aMonth - 1) / 3;
				
				aYear = aNow.year();
				aMonth = (aQuarter * 3) + 1;
				integer aDay = 1;
				
				return date.newInstance(aYear, aMonth, aDay);
			}
			else
				return date.newInstance(1900,1,1);
		}
	}
	public Date MaxDate 
	{ 
		get
		{
			if (SelectedFilter == '-')
				return date.today();
			else
			if (SelectedFilter == 'All')
				return date.newInstance(2100,1,1);
			else
			if (SelectedFilter == '+120')
				return date.today().addDays(120);
			else
			if (SelectedFilter == '-120')
				return date.today();
			else
			if (SelectedFilter == '+60')
				return date.today().addDays(60);
			else
			if (SelectedFilter == '+30')
				return date.today().addDays(30);
			else
			if (SelectedFilter == 'Quarter')
			{
				Date aNow = date.today();
					
				integer aYear = aNow.year();
				integer aMonth = aNow.month();
				integer aQuarter = (aMonth - 1) / 3;
				
				aYear = aNow.year();
				aMonth = (aQuarter * 3) + 1;
				integer aDay = 1;
				
				return date.newInstance(aYear, aMonth, aDay).AddMonths(3).AddDays(-1);
			}				
			else
				return date.newInstance(2100,1,1);
		}
	}
		
	public string SelectedFilter { get; set; }
	public List<SelectOption> FilterOptions
	{
		get
		{
			List<SelectOption> RetVal = new List<SelectOption>();
			
			RetVal.add(new SelectOption('-','Closing in past'));
			RetVal.add(new SelectOption('+120','Next 120 days'));
			RetVal.add(new SelectOption('-120','Last 120 days'));
			RetVal.add(new SelectOption('+60','Next 60 days'));
			RetVal.add(new SelectOption('+30','Next 30 days'));
			RetVal.add(new SelectOption('Quarter','Current quarter'));
			RetVal.add(new SelectOption('All','All Open'));
	
			return RetVal;
		}
	}
	
	private List<OpportunityStage> myStages = null;
	private List<OpportunityStage> theStages
	{
		get
		{
			if (myStages == null)
			{
				myStages = 
					[
						select
							Id,
							MasterLabel
						from
							OpportunityStage
						where
							IsActive = true
							and
							(
								IsClosed = false
								or
								IsWon = false
							)
						order by
							SortOrder
					];
			}
			
			return myStages;
		}
	}
	
	public string StageOption { get; set; }
	public List<SelectOption> StageOptions
	{
		get
		{
			List<SelectOption> RetVal = new List<SelectOption>();
			
			for (OpportunityStage Stage : theStages)
//				RetVal.add(new SelectOption(Stage.Id, Stage.MasterLabel));
				RetVal.add(new SelectOption(Stage.MasterLabel, Stage.MasterLabel));
			
			
			return RetVal;	
		}
	}

	public List<SelectOption> ClosedWonLostReasonOptions
	{
		get
		{
			Schema.Describefieldresult FR = Opportunity.Closed_Won_Lost_Reason__c.getDescribe();
			List<Schema.Picklistentry> Entries = FR.getPicklistValues();
			
			List<SelectOption> RetVal = new List<SelectOption>();

			RetVal.add(new SelectOption('', '--None--'));

			for (Schema.Picklistentry PE : Entries)
			{
				if (PE.GetLabel().Startswith('LOST'))
					RetVal.add(new SelectOption(PE.getLabel(), PE.getValue()));
			}
			
			return RetVal;
		}
	}
	
	public PageReference Requery()
	{
		myOpportunityUpdates = null;
		myOpportunityIdToOpportunityUpdate = null;
		myOpportunities = null;
		myHadErrors = false;
		return null;
	}
	
	private boolean myHadErrors = false;
	public boolean HadErrors { get { return myHadErrors; } } 
	public PageReference Save()
	{
		myHadErrors = false;
		List<Opportunity> OppsToUpdate = new List<Opportunity>();
		
		for (classOpportunityUpdate OppUpdate : theOpportunityUpdates)
		{
			OppsToUpdate.add(OppUpdate.theOpportunity);	
		}
		
		Database.SaveResult[] Results = Database.update(OppsToUpdate, false);

		for (integer i=0; i < theOpportunityUpdates.size(); ++i)
		{
			classOpportunityUpdate OppUpdate = theOpportunityUpdates[i];
			Database.SaveResult Result = Results[i];
			
			OppUpdate.theResult = 'Ok';
			
			if (!Result.isSuccess())
			{
				OppUpdate.theResult = '';
					
				boolean First = true;
				
				for (Database.Error Err : Result.getErrors())
				{	
					myHadErrors = true;
					if (!First)
						OppUpdate.theResult += ', ';
					else
						First = false;
						
					OppUpdate.theResult += Err.getMessage();
				} 
			}
		}
		
		return null;
	}
	
	public PageReference Cancel()
	{
		myOpportunityUpdates = null;
		myOpportunityIdToOpportunityUpdate = null;
		myOpportunities = null;
		myHadErrors = false;
		return null;
	}
	
}