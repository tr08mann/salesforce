@isTest
public with sharing class TestCampaignLookupController {

    public static testMethod void TestCampaignMethod()
    {
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Campaign campobj= TestDataFactory.createCampaign();
        insert campobj;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.CampaignId = campobj.Id;
        insert opp;
        
        Apexpages.currentPage().getParameters().put('id',opp.Id);
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(opp);
        CampaignlookupController campaignobj = new CampaignlookupController(controller);
        
        campaignobj.runQuery();
        campaignobj.onChange();
    }
}