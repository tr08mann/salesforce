@isTest
public class ConsolidatedCaseTrigger_TEST{
    public static testmethod void ConsolidatedCaseTriggerTEST()
    {
        User tempuser = TestDataFactory.createStandardUser('casetriggertest');

        system.runAs(tempuser)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Fishnet Security, Inc. (Parent)');
            insert acc;
            
            Contact Con = TestDataFactory.createStandardContact(acc.id);
            con.email = tempuser.email;
            insert Con;
            
            case c = TestDataFactory.createCase('SF.com Support Case - System Enhancement Case', con.id);
            insert c;
            
            task t = TestDataFactory.createTask(c.id, c.ownerid);
            insert t;
            
            c.Next_Step_Case__c = 'Step 2';
            c.case_follow_up_date__c = system.today().adddays(2);
            update c;
            
            c.Status = 'Closed';
            update c;
        }
    }
}