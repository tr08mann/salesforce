/*****************************************************************************************************************************************************************************
Author: Anup Prakash (agupta@rainmaker-llc.com); 

Description: Test Class for AssignOwnerBasedOnStudentName

Created Date : May 29th 2014
*****************************************************************************************************************************************************************************/
@isTEST
private class Test_AssignOwnerBasedOnStudentName {

    static testMethod void myUnitTest() {
       
        Contact ContactObj = new Contact(Accountid = '00130000004csyT', FirstName = 'Test', LastName = 'Test', Email = userinfo.getuseremail(), CurrencyIsoCode = 'USD');
        insert ContactObj;
        System.debug('ContactObj ::' + ContactObj);
        
        Vendor_Profile__c vpObj = new Vendor_Profile__c(Name = 'Test');
        insert vpObj;
        System.debug('vpObj :: '+vpObj);
        
        Cert_Requirement__c certObj = new Cert_Requirement__c(Name = 'Test' , Vendor_Profile_2__c = vpObj.Id);
        insert certObj;
        System.debug('certObj :: '+certObj);
        
        Certification__c vcObj = new Certification__c(Cert_Requirement__c = certObj.Id, Student_Name_2__c =ContactObj.ID);
        insert vcObj;
        System.debug('vcObj ::'+vcObj);    
    }
    
}