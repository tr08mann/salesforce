@isTest
public class DealRegTaskUpdateTrigger_Test{

	static testmethod void TESTDealRegTaskUpdate()
    {    
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
       
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        opp.closedate = date.today();
        insert opp;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Blue Coat Systems');
        insert vp;
        
        deal_registration__c dr1 = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, Opp.Opportunity_Number__c, 'Deal Registration Pending Submission');
        insert dr1;
        
        opp.stagename = 'Closed not selected';
        opp.Closed_Won_Lost_Reason__c = 'Won';
        update opp;
    }

    static testmethod void TESTDealRegTaskUpdate2()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
       
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('Blue Coat Systems');
        insert vp;
        
        deal_registration__c dr1 = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, Opp.Opportunity_Number__c, 'Deal Registration Pending Submission');
        insert dr1;
        
        opp.deal_reg_desk_rep__c = TestDataFactory.createStandardUser('Test2').id;
        update opp;
    }
}