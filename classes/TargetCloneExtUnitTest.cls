@isTest
public class TargetCloneExtUnitTest{

    public static testMethod void testTargetClone()
    {
        //Create Account
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        //Create Opportunity
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        //Create Product
        Product2 p = TestDataFactory.createStandardProduct(Vp.ID, '3COM', '12345');
        p.renewable_yesno__c = 'Yes';
        insert p;
        
        PricebookEntry pbe = TestDataFactory.createPricebookEntry(p.id);
        insert pbe;
        
        // Insert OpportunityLineItem
        OpportunityLineItem oli = TestDataFactory.createOppLineItem(opp.id, pbe.id);
        insert oli;
        
        TargetCloneExt cloneExt= new TargetCloneExt(new ApexPages.StandardController(opp));
        cloneExt.onLoad();
    }
}