public with sharing class LookupMainController 
{
    public Campaign objCampaign{get;set;}

    public LookupMainController(ApexPages.StandardController controller) {
        objCampaign = [SELECT Name,Parent.Name FROM Campaign WHERE Id=:controller.getID()];//(Campaign)controller.getRecord();
    }


    public PageReference savecampaign() {
        update objCampaign;
        return null;
    }


    public String camapignId { get; set; }
    public String CampaignName {get; set;}
    public Id accountId {get; set;}
    public List<Contact> contacts {get;set;}
    
    public PageReference findContacts()
    {
        if (null!=accountId)
        {
           contacts=[select id,FirstName, LastName from Contact where AccountId=:accountId];
        }
        
        return null;
    }
}