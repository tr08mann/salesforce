global class OpportunityScheduledUpdate implements Schedulable {
	global void execute(SchedulableContext ctx)
	{
		//Call Batchable Opportunity Update Class
		OpportunityBatchableUpdate toUpdate = new OpportunityBatchableUpdate();
		toUpdate.oneYearOld = Date.today().addYears(-1);
		toUpdate.ClosedStageName = 'Closed Won (100%)/SVC-Signed SOW';
		toUpdate.query = 'SELECT Opportunity.Id,Opportunity.StageName, Opportunity.CloseDate, Opportunity.Won_Last_365_days__c FROM Opportunity WHERE Opportunity.StageName =: ClosedStageName AND ((Opportunity.CloseDate >= :oneYearOld AND Opportunity.Won_Last_365_days__c = false) OR (Opportunity.CloseDate <:oneYearOld AND Opportunity.Won_Last_365_days__c = true))';
		//database.executeBatch(toUpdate,2000); //Execute in batches of 2000
		//JGP WO#1700 Update to fix to many DML rows
		//database.executeBatch(toUpdate,1000); //Execute in batches of 1000
		//AS changed back to batches of 20 as anything higher FAILS
		database.executeBatch(toUpdate,20); //Execute in batches of 20
	}
	
	static testMethod void OpportunityScheduledUpdateTEST()
	{
		test.startTest();
		OpportunityScheduledUpdate sch = new OpportunityScheduledUpdate();
		SchedulableContext ctx;
		sch.execute(ctx);	
		test.stopTest();
	}

}