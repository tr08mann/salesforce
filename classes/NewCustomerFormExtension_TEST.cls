@isTest
public class NewCustomerFormExtension_TEST{
    
    public static testMethod void testNCFwithoutAttachment() 
    {
        pagereference pageref = Page.NewCustomerForm;
        test.setCurrentPage(pageRef);
        
        test.startTest();
        account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        acc.Preferred_Method_of_Receiving_Invoice__c = 'Email';
        acc.Account_Purchase_Requirements__c = 'PO REQUIRED';
        acc.Exempt_from_State_and_Local_Taxes__c = 'NO';
        acc.Guest_User_Updated__c = false;
        insert acc;
          
        address__c adr = TestDataFactory.createAddress(acc.id,'NCF');
        insert adr;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        Attachment attach = TestDataFactory.createAttachment(userinfo.getUserId(), acc.id);
        
        test.stopTest();
              
        NewCustomerFormExtension pages = new NewCustomerFormExtension();
        
        acc = [select id, company_number__c from account where id=:acc.Id];
		
        string tempid = acc.id;
        tempid = tempid.left(15);
        
        system.debug(acc.Company_Number__c);
        
        pages.acctid = tempid;
        pages.custnbr = acc.company_number__c;
        
        pages.Next();
        pages.submit();
        
        acc.Guest_User_Updated__c = true;
        update acc;
        
        pages.Next();
        pages.submit();
        
        acc.Guest_User_Updated__c = false;
        acc.Exempt_from_State_and_Local_Taxes__c = 'YES';
        Update acc;
        
        pages.FileCount = '1';
        
        pages.Next();
        pages.attach();
        pages.Back();
        pages.SaveAttachments();
        pages.DeleteFile();
        pages.ChangeCount();
        pages.submit();        
    }
}