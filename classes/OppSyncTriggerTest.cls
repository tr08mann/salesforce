@isTest(seeAllData = True)
public class OppSyncTriggerTest{ 
    
    static testMethod void runUnitTest()
    {   
        account acc = TestDataFactory.createStandardAccount('Customer','Test');
        insert acc;
        
        contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr;
        
        vendor_profile__c vp = TestDataFactory.createVendorProfile('FishNet Security');
        insert vp;
        
        deal_registration__c newdealreg = TestDataFactory.createStandardDealReg(Opp.Id, VP.Id, vp.Name, Opp.Opportunity_Number__c, 'Deal Registration Pending Submission');
        insert newdealreg;
        
        Product2 objProduct = TestDataFactory.createStandardProduct(Vp.ID, 'FishNet Security', '123456789');
        insert objProduct;
        
        PricebookEntry objPriceBookEntry = TestDataFactory.createPricebookEntry(objProduct.id);
        insert objPriceBookEntry;
        
        Quote quote = TestDataFactory.createStandardQuote(opp.id, vp.id);
        insert quote;
        
        QuoteLineItem objQuoteLineItem = TestDataFactory.createQuoteLineItem(quote.ID, objPriceBookEntry.id);
        insert objQuoteLineItem;
        
        OpportunityLineItem objOpportunityLineItem = TestDataFactory.createOppLineItem(opp.ID, objQuoteLineItem.PricebookEntryId);
        objOpportunityLineItem.Quantity = objQuoteLineItem.Quantity;
        objOpportunityLineItem.UnitPrice = objQuoteLineItem.UnitPrice;
        objOpportunityLineItem.Discount = objQuoteLineItem.Discount;
        insert objOpportunityLineItem;
        
        TriggerStopper.stopOpp =false;  
        opp.SyncedQuoteId = quote.Id;
        test.startTest();
        update opp;
        test.stopTest();
    }
}