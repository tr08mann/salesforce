@isTest
public class MultiEditExtension_Test
{
    static testMethod void testMultiEditExtension()
{           
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new Account());
        MultiEditExtension page = new MultiEditExtension(controller);
        page.getshowContractVehicle();
        page.getshowService();
        if (page.getContractVehicles().size() > 1)
        {
                page.setselectedContractVehicle(page.getContractVehicles()[1].getvalue());
                page.getselectedContractVehicle();
        }
        if (page.getPricingMethods().size() > 1)
        {
                page.setselectedPricingMethod(page.getPricingMethods()[1].getvalue());
                page.getselectedPricingMethod();
        }
        if (page.getRegions().size() > 1)
        {
                page.setselectedRegion(page.getRegions()[1].getvalue());
                page.getselectedRegion();
        }
        if (page.getSalesCategories().size() > 1)
        {
                page.setselectedSalesCategory(page.getSalesCategories()[1].getvalue());
                page.getselectedSalesCategory();
        }
        QuoteLineItem qli = page.getQLI();
        page.setQLI(qli);
    }
}