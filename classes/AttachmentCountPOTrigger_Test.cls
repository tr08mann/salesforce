@isTest
public class AttachmentCountPOTrigger_Test{

    static testmethod void AttachmentCountTriggerTEST()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
    
        vendor_profile__c vp = TestDataFactory.createVendorProfile('3COM');
        insert vp;
        
        opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        purchase_order__c p = TestDataFactory.createPurchaseOrder(vp.id, opp.id);
        insert p;
                
        attachment a = new attachment();
        a.parentid = p.id;
        a.name = 'Deliverable_Test';
        a.body = blob.valueOf('Test File');
        insert a;
        delete a;
    }
}