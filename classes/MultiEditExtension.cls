public with sharing class MultiEditExtension {
        
    //List of String to hold Available Pricing Methods
    private List<String> PricingMethods;
    //List of Strings to Hold the Contract Vehicle Information
    private List<String> ContractVehicles;
    //List of Strings to hold the reqions
    private List<String> Regions;
    //List of Strings to hold the sales categories
    private List<String> SalesCategories;
    
    private String selectedPricingMethod;
    private String selectedContractVehicle;
    private String selectedRegion;
    private String selectedSalesCategory;
    
    QuoteLineItem qli = new QuoteLineItem();
    
    
    public MultiEditExtension(ApexPages.StandardController stdController)
    {
        //Populate DropDown Options
        SearchAndAddMultipleController master = new SearchAndAddMultipleController();
        //Populate Pricing Methods
        PricingMethods = master.RetreivePicklistValues('QuoteLineItem', 'Pricing_Method__c');
        //Populate Contract Vehicles
        ContractVehicles = master.RetreivePicklistValues('OpportunityLineItem', 'GSA__c');
        //Populate the Regions
        Regions = master.RetreivePicklistValues('QuoteLineItem', 'Region__c');
        //Populate the Sales Categories
        SalesCategories = master.RetreivePicklistValues('QuoteLineItem', 'Sales_Category__c');
    }
    public Boolean getshowContractVehicle()
    {
        String temp = Apexpages.currentPage().getParameters().get('Government');
        return (temp != null && temp != '');            
    }
    public Boolean getshowService()
    {
        string temp = Apexpages.currentPage().getParameters().get('Service');
        return (temp != null && temp != '');
    }
        
    //Get Pricing Methods
    public List<SelectOption> getPricingMethods()
    {
        List<SelectOption> o = new List<SelectOption>();
        if (PricingMethods.size() > 0)
        {
            for (String s:PricingMethods)
            {
                o.add(new SelectOption(s,s));   
            }
            o.add(0,new SelectOption('-SELECT-','-SELECT-'));
        }
        else
        {
            o.add(new SelectOption('-SELECT-','-SELECT-'));
        }
        return o;
    }
    //Get Contract Vehicles
    public List<SelectOption> getContractVehicles()
    {
        List<SelectOption> o = new List<SelectOption>();
        if (ContractVehicles.size() > 0)
        {
            for (String s:ContractVehicles)
            {
                o.add(new SelectOption(s,s));   
            }
            o.add(0,new SelectOption('-SELECT-','-SELECT-'));
        }
        else
        {
            o.add(new SelectOption('-SELECT-','-SELECT-'));
        }
        return o;
    }
    //Get Regions
    public List<SelectOption> getRegions()
    {
        List<SelectOption> o = new List<SelectOption>();
        if (Regions.size() > 0)
        {
            for (String s:Regions)
            {
                o.add(new SelectOption(s,s));   
            }
            o.add(0,new SelectOption('-SELECT-','-SELECT-'));
        }
        else
        {
            o.add(new SelectOption('-SELECT-','-SELECT-'));
        }
        return o;
    }
    ///Get Sales Categories
    public List<SelectOption> getSalesCategories()
    {
        List<SelectOption> o = new List<SelectOption>();
        if(SalesCategories.size() > 0)
        {
            for(String s:SalesCategories)
            {
                o.add(new SelectOption(s,s));
            }
            o.add(0,new SelectOption('-SELECT-','-SELECT-'));
        }
        else
        {
            o.add(0,new SelectOption('-SELECT-','-SELECT-'));
        }
        return o;
    }
    public String getselectedPricingMethod()
    {
        return this.selectedPricingMethod;
    }
    public void setselectedPricingMethod(String s)
    {
        this.selectedPricingMethod = s;
    }
    public String getselectedContractVehicle()
    {
        return this.selectedContractVehicle;
    }
    public void setselectedContractVehicle(String s)
    {
        this.selectedContractVehicle = s;
    }
    public String getselectedRegion()
    {
        return this.selectedRegion;
    }
    public void setselectedRegion(String s)
    {
        this.selectedRegion = s;
    }
    public String getSelectedSalesCategory()
    {
        return this.selectedSalesCategory;
    }
    public void setSelectedSalesCategory(String s)
    {
        this.selectedSalesCategory = s;
    }
    public QuoteLineItem getQLI()
    {
        return this.qli;
    }
    public void setQLI(QuoteLineItem q)
    {
        this.qli = q;
    }
}