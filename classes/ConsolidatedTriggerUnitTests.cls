@isTest
private class ConsolidatedTriggerUnitTests {
  //Task Trigger Tests
   
    static testMethod void TaskTester()
    {
		Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Not Fully Qualified (10%)/SVC-Meeting');
        insert opp;
        
        //Attach to an old quote object    
        SFDC_520_Quote__c temp = TestDataFactory.createOldQuote(opp.id);
        insert temp;
        
        Task t = TestDataFactory.createTask(temp.id, userinfo.getuserid());
        insert t;
        
        //Grab a contact and the account to associate to the task
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;
        
        //Create a new task
        Task t2 = TestDataFactory.createWhoTask(con.id, UserInfo.getUserId());
        t2.Subject = 'LEAD Team On-site Appointment';
        t2.Description = 'Description Test';
        t2.ActivityDate = date.today();
        insert t2;
  	}
  
    static testMethod void LeadTester()
    {
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        Lead l = TestDataFactory.createStandardLead(acc.name);
        insert l;
        
        Lead l2 = TestDataFactory.createStandardLead(acc.name);
        l2.LastName = l2.LastName+'2';
        insert l2;
        
        l.Lead_Import_Notes__c = 'This is the notes section';
        update l;
        
        Database.Leadconvert lc = new database.LeadConvert();
        lc.setLeadId(l.id);
        lc.setAccountId(acc.Id);
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
	}
  
    static testMethod void OpportunityTester()
    {        
		//Grab an account
        Account acc = TestDataFactory.createStandardAccount('Customer', 'Test');
        insert acc;
        
        //Grab a contact and the account to associate to the task
        Contact con = TestDataFactory.createStandardContact(Acc.ID);
        insert con;        
        
        //Create an Opportunity
        Opportunity opp = TestDataFactory.createStandardOpp('Commercial Opportunity', acc.Id, 'Verbal (90%)/SVC-Awaiting signed SOW');
        insert opp;
        
        OpportunityContactRole ocr = TestDataFactory.createOppContactRole(opp.id, con.id);
        ocr.IsPrimary = False;
        ocr.Role = 'Billing Contact: Professional Services';
        insert ocr;
        
        OpportunityContactRole ocr2 =TestDataFactory.createOppContactRole(opp.id, con.id);
        ocr2.IsPrimary = false;
        ocr2.Role = 'End-User: Professional Services';
        insert ocr2;
        
        OpportunityContactRole ocr3 = TestDataFactory.createOppContactRole(opp.id, con.id);
        insert ocr3;
        
        update opp;
        
        Task t = TestDataFactory.createTask(opp.id, UserInfo.getUserId());
        t.Subject = 'Renewal';
        insert t;
        
        Task t1 = TestDataFactory.createTask(opp.id, UserInfo.getUserId());
        insert t1;
        
        update opp;
        t1.Status = 'Completed';
        update t1;
        
        update opp; 
        
        System.Assert(opp.Id != Null);
    }
}