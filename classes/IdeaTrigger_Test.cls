@istest
public class IdeaTrigger_Test{

    static testmethod void TestIdeaTrigger()
    {
        User tempuser = TestDataFactory.createStandardUser('testideatrigger');

        system.runAs(tempuser)
        {
            account acc = TestDataFactory.createStandardAccount('Customer','Fishnet Security, Inc. (Parent)');
            insert acc;
            
            contact c = TestDataFactory.createStandardContact(acc.id);
            c.email = tempuser.email;
            insert c;
            
            idea thisidea = new idea();
            thisidea.BUSINESS_PROBLEM_THIS_RESOLVES__c = 'Test';
            thisidea.CHANGE_DESCRIPTION_As_a_JOB_TITLE__c = 'Test';
            thisidea.DESIRED_RESULT_Detail__c = 'Test';
            thisidea.I_want__c = 'Test';
            thisidea.POC_owner__c = tempuser.id;
            thisidea.So_that__c = 'Test';
            thisidea.title = 'Test';
            thisidea.category = 'New Feature';
            thisidea.body = 'Test';
            thisidea.status = 'New';
            thisidea.communityid = '09a600000004LIz';
            thisidea.currencyisocode = 'USD';
            insert thisidea;
        }
    }
}